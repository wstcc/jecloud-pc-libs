import { createApp } from 'vue';
import App from './app.vue';
import router from './router';
import { setupCommon } from './helper';
import ui from '@jecloud/ui';
import func from '@jecloud/func';
import '@/assets/themes/theme.less';
import '@jecloud/ui/src/style.less';
import '@jecloud/ui/src/assets/fonts/index.css';
import '@jecloud/func/src/style.less';
const app = createApp(App);
setupCommon(app).then(() => {
  app.use(router).use(ui).use(func).mount('#app');
});
