// 路由规则：rootCode/childCode，如：/ui/button
// 路由文件路径规则：rootCode/childCode，如：ui/button.vue
// 英文：code转大写

export const menus = [
  {
    text: '组件库',
    code: 'ui',
    root: true,
    children: [
      {
        text: '基础组件',
        code: 'base-components',
        children: [
          {
            code: 'button',
            text: '按钮',
          },
          {
            code: 'input',
            text: '文本框',
          },
          {
            code: 'radio',
            text: '单选框',
          },
          {
            code: 'checkbox',
            text: '多选框',
          },
          {
            code: 'input-number',
            text: '数值框',
          },
          {
            code: 'date-picker',
            text: '日期选择器',
          },
          {
            code: 'time-picker',
            text: '时间选择器',
          },
          {
            code: 'select',
            text: '下拉框',
          },
          {
            code: 'upload',
            text: '文件上传',
          },
          {
            code: 'switch',
            text: '开关',
          },
          {
            code: 'input-select',
            text: '选择字段',
          },
          {
            code: 'pinyin',
            text: '拼音',
          },
          {
            code: 'search',
            text: '搜索框',
          },
          {
            code: 'color',
            text: '颜色选择器',
          },
          {
            code: 'icon',
            text: '图标选择器',
          },
          {
            code: 'editor-html',
            text: 'HTML编辑器',
          },
          {
            code: 'editor-code',
            text: '代码编辑器',
          },
          {
            code: 'editor-draw',
            text: '画图编辑器',
          },
          {
            code: 'editor-markdown',
            text: 'markdown预览',
          },
          {
            code: 'func-querys',
            text: '查询条件拼装器',
          },
        ],
      },
      {
        code: 'data-components',
        text: '展示组件',
        children: [
          {
            code: 'loading',
            text: '加载组件',
          },
          {
            code: 'qrcode',
            text: '二维码',
          },
          {
            code: 'toolbar',
            text: '工具条',
          },
          {
            code: 'form',
            text: '表单',
          },
          {
            code: 'grid',
            text: '表格',
          },
          {
            code: 'tree',
            text: '树形',
          },
          {
            code: 'panel',
            text: '面板',
          },
          {
            code: 'tabs',
            text: 'Tabs 标签页',
          },
          {
            code: 'modal',
            text: '对话框',
          },
          {
            code: 'drawer',
            text: '抽屉面板',
          },
          {
            code: 'collapse',
            text: '折叠面板',
          },
          {
            code: 'form-design',
            text: '表单规划器',
          },
        ],
      },

      {
        code: 'scroll-components',
        text: '滚动组件',
        children: [
          {
            code: 'scroller',
            text: '基础滚动',
          },
          {
            code: 'scroller-action',
            text: '滚动函数',
          },
        ],
      },
    ],
  },
  {
    text: '工具类',
    code: 'utils',
    root: true,
    children: [
      {
        code: 'introduce',
        text: '说明',
      },
      {
        code: 'base',
        text: '基础',
      },
      {
        code: 'websocket',
        text: 'WebSocket',
      },
      {
        code: 'lodash',
        text: 'lodash',
        url: 'https://www.lodashjs.com/',
      },
      {
        code: 'dayjs',
        text: 'dayjs',
        url: 'https://dayjs.fenxianglu.cn/',
      },
      {
        code: 'axios',
        text: 'axios',
        url: 'http://www.axios-js.com/zh-cn/docs/',
      },
      {
        code: 'loadjs',
        text: 'loadjs',
        url: 'https://github.com/muicss/loadjs',
      },
    ],
  },
  {
    text: '功能引擎',
    code: 'funcs',
    root: true,
    children: [
      {
        code: 'func',
        text: '功能解析器',
      },
      {
        code: 'func-workflow',
        text: '工作流审批',
      },
    ],
  },
  {
    text: '图标库',
    code: 'icons',
    root: true,
    url: '/icons.html',
  },
];
