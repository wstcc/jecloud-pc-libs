import { createRouter, createWebHistory } from 'vue-router';
import { useRoutes } from './routes';
import Login from '../views/login/index.vue';
import { getToken, initSystem, setToken } from '../helper/system';
import { Icon } from '@jecloud/ui';

let routes = useRoutes();
const baseRoute = { path: '/', redirect: routes[0].path };
const loginRoute = { path: '/login', name: 'Login', component: Login, meta: { hideSider: true } };
const iconsRoute = {
  path: '/icons',
  name: 'Icons',
  component: Icon.IconPanel,
  meta: { hideSider: true },
};
routes = [baseRoute, loginRoute, iconsRoute].concat(routes);

const router = createRouter({
  history: createWebHistory('/'),
  routes,
});

router.beforeEach((to, from, next) => {
  // 路由白名单，已经登录
  if (getToken()) {
    initSystem()
      .then(() => {
        next();
      })
      .catch(() => {
        setToken();
        next({ name: 'Login' });
      });
  } else if (to.name !== 'Login') {
    next({ name: 'Login' });
  } else {
    next();
  }
});

export default router;
