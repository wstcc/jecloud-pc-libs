import { createI18n } from 'vue-i18n';
import { GlobalSettingsEnum } from '@/enums/global';
import { ref, watch } from 'vue';
/**
 * 支持的语言
 */
export const SUPPORT_LOCALES = [
  { code: 'zh_CN', text: '简体中文' },
  { code: 'en_US', text: 'English' },
];

let i18n;
export function useI18n() {
  return i18n?.global;
}

/**
 * 监听国际化
 * @returns
 */
export function watchI18n() {
  // 国际化处理
  const locale = ref();
  watch(
    () => i18n.global.locale,
    () => {
      locale.value = getAntdLocale();
    },
    { immediate: true },
  );
  return { locale };
}
/**
 * 安装i18n
 *
 * @export
 * @param {App} app
 */
export async function setupIi8n(app) {
  const localeI18n = getLocale();
  i18n = createI18n({});
  await changeLocale(localeI18n);
  app.use(i18n);
}

/**
 * 切换多语言
 *
 * @export
 * @param {string} locale
 */
export async function changeLocale(locale) {
  // 加载文件
  await loadLocaleMessages(locale);
  // 设置语言
  i18n.global.locale = locale;
  // 设置缓存
  localStorage.setItem(GlobalSettingsEnum.GLOBAL_SETTINGS_LOCALE, locale);
  // 设置页面
  document.querySelector('html')?.setAttribute('lang', locale);
}

/**
 * 异步加载语言文件
 *
 * @param {string} locale
 * @return {*}
 */
async function loadLocaleMessages(locale) {
  const messages = await import(`./lang/${locale}.js`);
  const locales = messages.default;
  i18n.global.setLocaleMessage(locale, locales);
  return locales;
}

/**
 * 获得antd语言包
 *
 * @export
 * @return {*}
 */
export function getAntdLocale() {
  return i18n.global.getLocaleMessage(getLocale())?.antdLocale ?? {};
}
/**
 * 获得当前激活语言
 *
 * @export
 * @return {*}
 */
export function getLocale() {
  return SUPPORT_LOCALES[0].code;
  /* const local = localStorage.getItem(GlobalSettingsEnum.GLOBAL_SETTINGS_LOCALE);
  if (
    local &&
    SUPPORT_LOCALES.find((item) => {
      return item.code === local;
    })
  ) {
    return local;
  }
  return SUPPORT_LOCALES[0].code; */
}

// 为什么要编写此函数？
// 主要用于配合vscode i18nn ally插件。此功能仅用于路由和菜单。请在其他地方使用useI18n
export const t = (key) => key;
