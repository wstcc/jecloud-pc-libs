import { set } from '@jecloud/utils';
/**
 * 解析国际化文件
 *
 * @export
 * @param {*} files
 * @return {*}
 */
export function buildLocales(files) {
  const locales = {};
  Object.keys(files).forEach((key) => {
    // 将文件路径改为属性路径：./en_US/index.js => en_US.index
    // 排除：./en_US
    const keyPath = key.replace('.js', '').split('/').splice(2).join('.');
    set(locales, keyPath, files[key].default);
  });
  return locales;
}
