export default {
  title: 'System login',
  username: 'Username',
  usernamePlaceholder: 'Please enter username',
  password: 'Password',
  passwordPlaceholder: 'Please enter password',
  language: 'Language',
  loginText: 'Login',
  resetText: 'Reset',
  loginSuccess: 'Login successfully!',
};
