// 加载antd国际化文件
import antdLocale from 'ant-design-vue/es/locale/en_US';
import { buildLocales } from '../utils';
const files = import.meta.globEager('./en_US/**/*.js');
const locales = {
  ...buildLocales(files),
  antdLocale, // antd
};

export default locales;
