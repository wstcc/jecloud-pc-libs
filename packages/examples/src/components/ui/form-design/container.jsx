import { defineComponent, computed } from 'vue';
import Group from './group';
import Field from './field';
import { useDraggable } from './hooks/use-draggable';
import { Row } from 'ant-design-vue';

export default defineComponent({
  name: 'DargContainer',
  components: { Field, Group, Row },
  props: {
    data: {
      type: Array,
      default() {
        return [];
      },
    },
    columns: { type: Number, default: 3 },
  },
  setup(props) {
    const { dragEl } = useDraggable({ owner: 'container', props });
    // 所占列数
    const span = computed(() => {
      return Math.ceil(24 / props.columns);
    });
    // 子项数据
    const itemsSlot = ({ data, groupId, span }) => {
      const items = [];
      data.forEach((item) => {
        if (item.groupId == groupId) {
          if (item.type === 'group') {
            items.push(
              <Group
                data={item}
                span={span}
                key={item.id}
                v-slots={{
                  default(options) {
                    return itemsSlot({ data, groupId: item.id, span: options.span });
                  },
                }}
              ></Group>,
            );
          } else {
            items.push(<Field data={item} span={span} key={item.id}></Field>);
          }
        }
      });
      return items;
    };

    return () => (
      <Row class="container" data-type="container" gutter={[4, 4]} ref={dragEl}>
        {itemsSlot({ data: props.data, span: span.value })}
      </Row>
    );
  },
});
