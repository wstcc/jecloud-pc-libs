import { cookie, initSystemApi, logoutApi } from '@jecloud/utils';
import { setAjaxDefaultConfig } from './http';
export const tokenKey = 'authorization';
let inited = false;
/**
 * 初始化系统数据
 *
 * @export
 * @return {*}
 */
export function initSystem() {
  // 已经初始化数据，设置默认菜单
  if (inited) {
    return Promise.resolve();
  }

  // 初始系统数据
  return initSystemApi().then(() => {
    // 系统数据初始化成功
    inited = true;
    setAjaxDefaultConfig();
  });
}

export function setToken(token) {
  if (token) {
    cookie.set(tokenKey, token);
  } else {
    cookie.remove(tokenKey);
  }
}
export function getToken() {
  return cookie.get(tokenKey);
}

export function logout(next) {
  return logoutApi()
    .catch(() => {})
    .finally(() => {
      setToken();
      if (next) {
        return next({ name: 'Login' });
      }
    });
}
