import * as Vue from 'vue';
import * as Utils from '@jecloud/utils';
import * as Ui from '@jecloud/ui';
import { FuncUtil } from '@jecloud/func';
import { CLI_ENVS } from './constant';

/**
 * 初始化JE工具类
 * 页面可以通过JE.调用utils里的所有工具函数
 */
export function setupJE(vue) {
  const JE = Utils.setupJE(vue, { Ui, Utils, Vue, Func: FuncUtil, CLI_ENVS });

  // 绑定全局JE
  window.JE = window.JE || JE;
}
