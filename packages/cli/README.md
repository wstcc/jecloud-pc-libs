# JECloud 脚手架
通过 `jecli` 全局命令快速创建项目

```bash
# 1. 全局安装jecli
npm install @jecloud/cli -g

# 2. 查看jecli的帮助
jecli -h

# 3. 输出帮助信息，然后根据帮助信息进行操作

Options:
  -V, --version  output the version number
  -h, --help     output usage information

Commands:
  init           初始化项目模板
  list|ls        查看所有项目模板
  add            添加本地项目模板, 地址#分支 例如 https://github.com/xxx/xxx.git#master
  delete|del     删除本地项目模板

```
