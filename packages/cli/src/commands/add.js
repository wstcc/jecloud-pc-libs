#!/usr/bin/env node

const chalk = require('chalk');
const path = require('path');
const symbols = require('log-symbols');
const fs = require('fs');
const inquirer = require('inquirer');
const { getAllTemplates, showTable } = require('../util');

const allTemplates = getAllTemplates();
const cusRegs = require('../json/customregist.json');

function onAdd() {
  inquirer
    .prompt([
      {
        type: 'input',
        name: 'key',
        message: `请输入项目名称(对应 ${chalk.yellow('package.json')} 中的 ${chalk.yellow(
          'name',
        )})`,
        validate(val) {
          if (!val) {
            return '项目名称不能为空！';
          } else if (allTemplates[val]) {
            return val + ' 已存在，请更换项目名称！';
          } else {
            return true;
          }
        },
      },
      {
        type: 'input',
        name: 'description',
        message: '请输入项目描述',
        validate(val) {
          if (!val) {
            return '项目描述不能为空！';
          } else {
            return true;
          }
        },
      },
      {
        type: 'input',
        name: 'url',
        message: `请输入项目下载地址，${chalk.red('格式：地址#分支')}`,
        validate(val) {
          if (!val) {
            return '项目下载地址不能为空！';
          } else {
            return true;
          }
        },
      },
    ])
    .then((answers) => {
      const { key, description, url } = answers;
      // 添加
      let tmp = {};
      tmp[key] = {
        description,
        url,
      };
      let result = { ...tmp, ...cusRegs };
      const cliPath = path.join(__dirname, '/../json/customregist.json');
      fs.writeFileSync(cliPath, JSON.stringify(result, null, '  '));
      console.log(chalk.green(symbols.success), chalk.green(`添加成功，请查看最新模板列表：\n`));
      showTable(result);
    });
}

module.exports = onAdd;
