const fs = require('fs');
const path = require('path');
const ora = require('ora');
const inquirer = require('inquirer');
const download = require('download-git-repo');
const symbols = require('log-symbols');
const chalk = require('chalk');
const { getSelectTemplates, getAllTemplates, emptyTemplatesLog } = require('../util');

const spinner = ora(chalk.green('下载中...'));

const cwd = process.cwd();

async function create() {
  const choices = getSelectTemplates();
  if (choices.length === 0) {
    emptyTemplatesLog();
    return;
  }
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'projectTemplate',
        message: '请选择项目模板?',
        choices,
      },
      {
        type: 'input',
        name: 'projectName',
        message: '请填写项目名称',
        default: 'jecloud-frontend-plugin',
        validate(val) {
          if (!val) {
            return '项目名称不能为空！';
          } else {
            const targetDir = path.resolve(cwd, val || '.');
            if (fs.existsSync(targetDir)) {
              return '当前路径已存在同名目录，请修改项目名称！';
            }
            return true;
          }
        },
      },
    ])
    .then(downloadGitRepo);
}

/**
 * 下载 git 仓库
 * @param {项目名} projectName
 */
function downloadGitRepo(answers) {
  const { projectTemplate, projectName } = answers;
  console.log(symbols.info, chalk.blue('正在拉取' + projectTemplate + '项目模板'));
  spinner.start();
  const allRegistries = getAllTemplates();
  const url = allRegistries[projectTemplate].url;
  download(`direct:${url}`, projectName, { clone: true }, function (err) {
    if (err) {
      // 下载失败
      spinner.fail();
      console.log(chalk.red(symbols.error), chalk.red(`下载失败. ${err}`));
    } else {
      // 下载成功
      spinner.succeed();
      console.log(chalk.green(symbols.success), chalk.green('下载成功！'));
    }
  });
}

module.exports = (...args) => {
  return create(...args).catch((err) => {
    console.log(symbols.error, chalk.red(err));
    process.exit(1);
  });
};
