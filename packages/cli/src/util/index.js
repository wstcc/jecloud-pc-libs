const chalk = require('chalk');
const symbols = require('log-symbols');
const customregist = require('../json/customregist.json');
const registries = require('../json/registries.json');

/**
 * 输出table
 * @param {*} tempList
 */
function showTable(tempList) {
  const list = Object.keys(tempList);
  if (list.length > 0) {
    const tableData = [];
    list.forEach((key) => {
      const { description, url } = tempList[key];
      tableData.push({
        ['名称']: key,
        ['描述']: description,
        ['地址']: url,
      });
    });
    console.table(tableData);
    process.exit();
  } else {
    emptyTemplatesLog();
  }
}
/**
 * 获取所有template
 */
function getAllTemplates() {
  let all = {
    ...registries,
    ...customregist,
  };
  return all;
}
/**
 * 选择列表
 * @returns
 */
function getSelectTemplates(list) {
  let all = getAllTemplates();
  return Object.keys(list || all).map((key) => ({
    value: key,
    name: `${key}（${all[key].description}）`,
  }));
}
/**
 * 空模板输出
 */
function emptyTemplatesLog() {
  console.log('\n');
  console.log(symbols.info, chalk.blue(`本地项目模板空空如也，请快去添加吧！`));
  console.log('\n');
  console.log(chalk.green('  jecli add'));
  process.exit();
}
module.exports = {
  showTable,
  getAllTemplates,
  getSelectTemplates,
  emptyTemplatesLog,
};
