import { WorkflowUtil } from '../src';
import { Button, Tooltip, Dropdown, Menu } from '@jecloud/ui';

/**
 * 功能流程
 * @param {*} param0
 * @returns
 */
export function useFuncWorkflow({ $func }) {
  /**
   * 流程按钮插槽
   * @returns
   */
  const workflowButtonSlot = () => {
    const buttonSlot = WorkflowUtil.useWorkflowButtonSlot({
      $func,
    });
    return buttonSlot();
  };
  /**
   * 流程追踪插槽
   * @returns
   */
  const workflowMainButtonSlot = () => {
    const { $workflow } = $func;
    if (!$workflow.isEnable()) {
      return null;
    }
    const buttons = $workflow
      .getOperationButtons()
      .filter((_button) => $workflow.doStartExpression(_button));
    const singleButton = $workflow.isStart() || buttons.length === 1;
    const showModal = (button) => {
      button = button || buttons[0];
      WorkflowUtil.showWorkflowMain({
        params: {
          $func,
          button,
        },
      });
    };
    return (
      <Tooltip title="流程追踪">
        <Dropdown
          trigger={['click']}
          placement="bottomRight"
          v-slots={{
            overlay() {
              return singleButton ? null : (
                <Menu
                  onClick={(e) => {
                    showModal(buttons.find((button) => button.pdid === e.key));
                  }}
                >
                  {buttons.map((button) => {
                    return (
                      <Menu.Item key={button.pdid}>{button.modelName || button.name}</Menu.Item>
                    );
                  })}
                </Menu>
              );
            },
          }}
        >
          <Button
            icon="fal fa-sitemap"
            class="bgcolor-grey"
            onClick={() => {
              if (singleButton) {
                showModal();
              }
            }}
          />
        </Dropdown>
      </Tooltip>
    );
  };

  return { workflowButtonSlot, workflowMainButtonSlot };
}
