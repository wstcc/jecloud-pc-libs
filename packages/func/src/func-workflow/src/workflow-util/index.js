export * from './src';
import * as utils from './src';
import * as enums from '../workflow-manager/enum';
export const WorkflowUtil = {
  ...utils,
  ...enums,
};
export default WorkflowUtil;
