import { Func } from '@jecloud/utils';
export const {
  getWorkflowInfoApi,
  getSubmitNodeInfoApi,
  getSubmitNodeUsersApi,
  getButtonParamsApi,
  doButtonOperateApi,
  getTaskNodeInfoApi,
  getPassroundUsersApi,
  getCounterSignerOperationalUsersApi,
  loadWorkflowHistoryApi,
  previewImageApi,
  getCountersignApprovalOpinionApi,
  getPassRoundUsersListByTaskIdApi,
  getMessageTypeApi,
  getUrgeDataApi,
  getAssigneeByTaskIdApi,
  getCommonUsersApi,
} = Func;
