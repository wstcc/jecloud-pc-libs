import JeWorkflowMain from './src';
import { withInstall } from '../../../utils';
export const WorkflowMain = withInstall(JeWorkflowMain);
export default WorkflowMain;
