import { nextTick, onMounted, ref, watch } from 'vue';
import { useWorkflowButtonSlot } from '../../workflow-util';
export function useWorkflowMain({ props, context }) {
  const buttons = ref([]);
  const wfImage = ref();
  const wfHistory = ref();
  const { $func, button } = props.params;
  const { $workflow } = $func;

  // 初始化流程信息
  const refreshWorfkowInfo = () => {
    const params = { beanId: $workflow.getBeanId(), pdid: $workflow.getPdid() };
    buttons.value = [];
    if ($workflow.isStart()) {
      buttons.value = $workflow.getOperationButtons();
    } else {
      buttons.value = [button];
      params.pdid = button.pdid;
    }
    //找到图片组件,执行刷新方法
    wfImage.value?.refresh(params);
    //找到历史流程组件,执行刷新方法
    $workflow.isStart() && wfHistory.value?.refresh(params);
  };

  watch(
    () => $workflow.state.refreshEmitter,
    () => {
      refreshWorfkowInfo();
    },
  );
  onMounted(() => {
    nextTick(() => {
      refreshWorfkowInfo();
    });
  });

  // 按钮插槽
  const buttonSlot = useWorkflowButtonSlot({
    buttons,
    $func,
    hideLine: true,
  });

  return { buttonSlot, wfImage, wfHistory, buttons };
}
