import { Col, Row } from '@jecloud/ui';
import { computed, defineComponent } from 'vue';
import { useInjectWorkflow } from './hooks/context';
import { decode } from '@jecloud/utils';
import WorkflowOperationComment from './operation-comment';
import WorkflowOperationExigency from './operation-exigency';

export default defineComponent({
  name: 'WorkflowProcessThree',
  inheritAttrs: false,
  setup() {
    const { $operation } = useInjectWorkflow();
    const { state } = $operation;
    const users = computed(() => {
      const data = [];
      Object.values(state.assignees).forEach((item) => {
        data.push(...item.users);
      });
      return data;
    });
    return () => (
      <div
        class="je-workflow-operation-process-three"
        style={{ display: state.activeStep === 3 ? undefined : 'none' }}
      >
        {/* 选中节点 */}
        <div class="task-node is--active">
          {state.activeNode?.name || state.name}
          <i class="icon fal fa-check" />
        </div>
        {/* 选中的人员 */}
        <div class="task-users" style={{ display: users.value.length ? undefined : 'none' }}>
          <Row gutter={[10, 10]}>
            {users.value.map((item) => {
              const info = decode(item.nodeInfo) || {};
              return (
                <Col span={item.type === 'team' ? 8 : 4}>
                  <div
                    class={{
                      'task-users-selecked-item': true,
                      women: info.sex === 'WOMAN',
                      team: info.sex === 'team',
                    }}
                  >
                    {item.bean?.ACCOUNT_NAME || info.ACCOUNT_NAME}
                    <i class="icon fal fa-check" />
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
        {/* 审批意见 */}
        <WorkflowOperationComment v-model:taskComment={state.comment}></WorkflowOperationComment>
        {/**紧急程度 */}
        <WorkflowOperationExigency />
      </div>
    );
  },
});
