import { defineComponent } from 'vue';
import { useWorkflowOperationExigency } from './hooks/use-workflow-operation-exigency';

export default defineComponent({
  name: 'WorkflowOperationExigency',
  inheritAttrs: false,
  props: {
    params: Object, // 操作按钮
  },
  emits: ['operate-done', 'closeModal'],
  setup(props, context) {
    const { state, doHandlerExigency } = useWorkflowOperationExigency({ props, context });
    return () =>
      state.exigency ? (
        <div class="task-exigency">
          <div class="task-exigency-title">
            <i class="icon fal fa-light-emergency-on"></i>
            <span class="text">紧急程度</span>
          </div>
          <div class="task-exigency-context">
            {state.ExigencyTypes.map((item) =>
              state.disabled && state.exigencyValue != item.key ? null : (
                <span
                  onClick={() => {
                    doHandlerExigency(item.key);
                  }}
                  class={[
                    'task-exigency-context-item',
                    item.key,
                    state.exigencyValue == item.key ? 'selected' : '',
                  ]}
                  key={item.key}
                >
                  {item.text}
                </span>
              ),
            )}
          </div>
        </div>
      ) : null;
  },
});
