import { defineComponent } from 'vue';
import { useWorkflowOperationUser } from './hooks/use-workflow-operation-user';
import { Tree, Button, Radio } from '@jecloud/ui';
import { renderSecurityWorkflowUserMark } from '../../../../func-manager';
import { useInjectWorkflow } from './hooks/context';
/**
 * 流程人员选择器
 */
export default defineComponent({
  name: 'WorkflowOperationUser',
  inheritAttrs: false,
  props: {
    showSequentialConfig: Boolean,
    multiple: Boolean,
    selectAll: Boolean,
    sequential: String,
    userDisabled: Boolean,
    wfButton: Object,
    draggable: { type: Boolean, default: true },
    result: { type: Array, default: () => [] },
    users: { type: Array, default: () => [] },
    funcObj: Object,
  },
  emits: ['update:sequential', 'update:result', 'changeSequential'],
  setup(props, context) {
    const {
      userConfig,
      $userTree,
      treeStore,
      $userResult,
      result,
      sequential,
      selectUser,
      moveUser,
    } = useWorkflowOperationUser({ props, context });
    return () => (
      <div class="je-workflow-operation-user">
        <div class="task-user-selector">
          {/* 树形人员选择器 */}
          <Tree
            ref={$userTree}
            store={treeStore}
            class="task-user-selector-tree"
            allowDeselect={false}
            onCellClick={selectUser}
            v-slots={{
              nodeDefault({ row }) {
                return row.nodeInfoType == 'json' ? (
                  <div class="node-context">
                    {row.text}
                    {renderSecurityWorkflowUserMark({ $func: props.funcObj, data: row })}
                  </div>
                ) : (
                  <div>{row.text}</div>
                );
              },
            }}
          />
          {/* 操作按钮 */}
          <div class="task-user-selector-buttons">
            <Button
              icon="fal fa-chevron-double-right"
              disabled={!props.multiple || !props.userDisabled}
              onClick={() => moveUser('moveTreeUsers')}
            />
            <Button
              icon="fal fa-chevron-right"
              disabled={!props.userDisabled}
              onClick={() => moveUser('moveTreeUser')}
            />
            <Button
              icon="fal fa-chevron-double-left"
              disabled={!props.userDisabled}
              onClick={() => moveUser('moveSelectUsers')}
            />
            <Button
              icon="fal fa-chevron-left"
              disabled={!props.userDisabled}
              onClick={() => moveUser('moveSelectUser')}
            />
          </div>
          {/* 选中人员 */}
          <div
            class={`task-user-selector-users ${
              !props.userDisabled ? 'task-user-selector-users-disabled' : ''
            }`}
          >
            <div class="users-wrapper" ref={$userResult}>
              {result.value.map((item) => {
                return (
                  <div
                    class={{
                      'users-item': true,
                      'is--active': item.id === userConfig.activeUser?.id,
                    }}
                    data-id={item.id}
                    key={item.id}
                    onClick={() => {
                      userConfig.activeUser = item;
                    }}
                  >
                    <i class="user-icon fal fa-user-alt"></i>
                    {item.text}
                    <i
                      class="close-icon fal fa-times"
                      style={`display:${!props.userDisabled ? 'none' : ''}`}
                      onClick={() => {
                        userConfig.activeUser = item;
                        moveUser('moveSelectUser');
                      }}
                    ></i>
                  </div>
                );
              })}
            </div>
            {/* 选中信息 */}
            <div class="users-info">选择（{result.value.length ?? 0}）人</div>
            {/* 多人审批 */}
            {props.showSequentialConfig ? (
              <div class="users-countersign">
                <Radio.Group
                  justify="center"
                  disabled={!props.userDisabled || true}
                  v-model:value={sequential.value}
                  options={[
                    { value: '0', label: '并序执行' },
                    { value: '1', label: '顺序执行' },
                  ]}
                ></Radio.Group>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  },
});
