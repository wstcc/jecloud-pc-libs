import { defineComponent, ref } from 'vue';
import { loadDDItemByCode, useModelValue } from '@jecloud/utils';
import { Input, Dropdown, Menu } from '@jecloud/ui';

export default defineComponent({
  name: 'WorkflowOperationComment',
  inheritAttrs: false,
  props: { taskComment: String },
  setup(props, context) {
    const taskComment = useModelValue({ props, context, key: 'taskComment' });
    const messages = ['请您审批', '同意', '不同意', '已阅'];
    const taskMessages = ref('');
    loadDDItemByCode('JE_WF_APPROOPINION').then((data) => {
      taskMessages.value = data;
    });
    return () => (
      <div class="task-message">
        <Input.TextArea
          placeholder="请输入您的审批意见..."
          v-model:value={taskComment.value}
          class="task-message-textarea"
        />
        {/* 常用意见 */}
        <div class="task-message-list">
          {messages.map((text) => (
            <div
              class="task-message-list-item"
              onClick={() => {
                taskComment.value = text;
              }}
            >
              {text}
            </div>
          ))}
          <Dropdown
            trigger={['click']}
            overlayStyle="height:200px;overflow:auto;background-color: #fff;box-shadow: 0 3px 6px -4px rgba(0,0,0,.12), 0 6px 16px 0 rgba(0,0,0,.08), 0 9px 28px 8px rgba(0,0,0,.05);"
            v-slots={{
              overlay() {
                return (
                  <Menu
                    onClick={({ key }) => {
                      taskComment.value = key;
                    }}
                  >
                    {taskMessages.value.map(({ text }) => (
                      <Menu.Item key={text}>{text}</Menu.Item>
                    ))}
                  </Menu>
                );
              },
            }}
          >
            <div class="task-message-list-button" onClick={() => {}}>
              常用意见 <i class="fas fa-caret-down"></i>
            </div>
          </Dropdown>
        </div>
      </div>
    );
  },
});
