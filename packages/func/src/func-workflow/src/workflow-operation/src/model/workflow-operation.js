import { WorkflowOperation as _ } from '../../../workflow-manager';
import { isEmpty, forEach, Modal, getSystemConfig } from '@jecloud/utils';

export default class WorkflowOperation extends _ {
  constructor(options) {
    super(options);
  }

  initOperation() {
    const operation = this;
    return this.initOperationInfo().then(() => {
      // 直接操作，不需要步骤
      if (operation.state.directOperation) {
        //在第一步(节点有多个)
        if (operation.state.taskNodes.length > 1) {
          operation.state.activeStep = 1;
        } else {
          //在第最后一步
          operation.state.activeStep = 3;
        }
      } else if (operation.state.taskNodes.length === 1) {
        // 如果是结束节点直接第三步
        if (operation.state.activeNode.end) {
          operation.state.activeStep = 3;
        } else {
          // 如果只有一个节点，转到下一步
          operation.nextSetp();
        }
      }
    });
  }

  /**
   * 下一步
   */
  nextSetp() {
    // this.state.directOperation == true 单个节点不选人直接到第三步
    // this.state.activeNode.submitDirectly == true  第一步有多个节点 该值为true不选人直接跳第三步
    this.state.activeStep =
      this.state.directOperation || this.state.activeNode?.submitDirectly
        ? 3
        : this.state.activeStep + 1;
  }
  /**
   * 上一步
   */
  prevSetp() {
    // this.state.directOperation == true 单个节点不选人直接到第一步
    // this.state.activeNode.submitDirectly == true 第一步有多个节点 该值为true不选人直接跳第一步
    this.state.activeStep =
      this.state.directOperation || this.state.activeNode?.submitDirectly
        ? 1
        : this.state.activeStep - 1;
  }
  /**
   * 设置送交节点人员
   * @param {*} nodeId
   * @param {*} users
   */
  setAssigneeUsers(nodeId, users = []) {
    this.state.assignees[nodeId].users = users;
  }
  /**
   * 获取送交节点人员
   * @param {*} nodeId
   * @returns
   */
  getAssigneeUsers(nodeId) {
    return this.state.assignees[nodeId]?.users;
  }
  /**
   * 操作步骤
   * @param {*} param0
   */
  doOperationSetp({ next }) {
    if (next) {
      switch (this.state.activeStep) {
        // 前往第2步
        case 1:
          this.nextSetp();
          break;
        // 前往第3步
        case 2:
          const userKeys = Object.keys(this.state.assignees);
          if (isEmpty(userKeys)) {
            Modal.alert(`请选择人员信息！`, Modal.status.warning);
            return;
          }
          let userEmpty = false;
          forEach(userKeys, (key) => {
            const assignee = this.state.assignees[key];
            if (isEmpty(assignee.users)) {
              // 开启该变量,流程人员为空也可提交
              let flag = false;
              if (getSystemConfig('JE_WORKFLOW_CLOSE_PERSONNEL_ANOMLY') == '1') {
                const assigneeNodes = this.state.activeNode?.assigneeNodes || [];
                assigneeNodes.forEach((item) => {
                  if (item.id == key && item.users.length <= 0) {
                    flag = true;
                    return false;
                  }
                });
              }
              if (!flag) {
                userEmpty = true;
                Modal.alert(`请选择【${assignee.name}】的人员信息！`, Modal.status.warning);
                return false;
              }
            }
          });

          // 如果有空的人员，停止下一步
          !userEmpty && this.nextSetp();
          break;
        // 提交操作
        case 3:
          if (this.state.comment) {
            this.$workflow.action.doButtonSubmit({ operation: this });
          } else {
            Modal.alert('请填写审批意见！', Modal.status.warning);
          }
          break;
      }
    } else {
      this.prevSetp();
    }
  }
}
