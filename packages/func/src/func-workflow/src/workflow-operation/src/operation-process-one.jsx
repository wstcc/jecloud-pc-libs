import { defineComponent } from 'vue';
import { useInjectWorkflow } from './hooks/context';

export default defineComponent({
  name: 'WorkflowProcessOne',
  inheritAttrs: false,
  setup() {
    const { $operation } = useInjectWorkflow();
    const { state } = $operation;
    return () => (
      <div
        class="je-workflow-operation-process-one"
        style={{ display: state.activeStep === 1 ? undefined : 'none' }}
      >
        {state.taskNodes?.map((node) => {
          return (
            <div
              class={{ 'task-node': true, 'is--active': node.id === state.activeNode?.id }}
              onClick={() => {
                state.activeNode = node;
              }}
              key={node.id}
            >
              {node.name || state.name}
              <i v-show={node.id === state.activeNode?.id} class="icon fal fa-check" />
            </div>
          );
        })}
      </div>
    );
  },
});
