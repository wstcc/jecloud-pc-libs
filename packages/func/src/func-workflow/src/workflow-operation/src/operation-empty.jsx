import { defineComponent } from 'vue';
import { Panel, Button, Toolbar, Modal } from '@jecloud/ui';

export default defineComponent({
  name: 'WorkflowOperationEmpty',
  inheritAttrs: false,
  setup() {
    const $model = Modal.useInjectModal();
    return () => (
      <Panel class="je-workflow-operation">
        <Panel.Item>
          <div class="je-workflow-operation-null-context">
            <span>没有节点信息，请联系管理员~</span>
          </div>
        </Panel.Item>

        <Panel.Item region="bbar">
          <Toolbar class="je-workflow-operation-bbar">
            <Toolbar.Fill />
            <Button
              onClick={() => {
                $model?.close();
              }}
            >
              关闭
            </Button>
          </Toolbar>
        </Panel.Item>
      </Panel>
    );
  },
});
