import { reactive } from 'vue';
import { useInjectWorkflow } from './context';
export function useWorkflowOperationExigency({ props, context }) {
  const { $func, button } = useInjectWorkflow();
  const { workflowConfig } = button;
  const state = reactive({
    ExigencyTypes: [
      { text: '一般流程', key: 'ordinary' },
      { text: '加急流程', key: 'anxious' },
      { text: '紧急流程', key: 'urgency' },
    ],
    exigencyValue: workflowConfig.exigencyValue || 'ordinary',
    exigency: button.exigency || workflowConfig.exigency,
    disabled: button.code != 'sponsorBtn',
  });
  // 选取紧急程度
  const doHandlerExigency = (val) => {
    state.exigencyValue = val;
    $func.$workflow.state.exigency = val;
  };
  return { state, doHandlerExigency };
}
