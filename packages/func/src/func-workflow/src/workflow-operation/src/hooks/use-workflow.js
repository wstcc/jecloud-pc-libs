import { useProvideWorkflow } from './context';
import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
import WorkflowOperation from '../model/workflow-operation';
export function useWorkflow({ props, context }) {
  const { expose } = context;
  const loading = ref(true);
  const nodeDataEmpty = ref(false);
  const $modal = Modal.useInjectModal();
  const { $func, button } = props.params;
  const { $workflow } = $func;
  // 审批操作对象
  const $operation = new WorkflowOperation({
    $workflow,
    button,
    finishSubmit() {
      $modal.close();
    },
  });

  // 流程对象
  const workflow = {
    props,
    context,
    $func,
    $workflow,
    $operation,
    button,
  };
  // 对外暴露功能
  expose(workflow);
  // 对子暴露功能
  useProvideWorkflow(workflow);

  // 初始化数据
  loading.value = true;
  $operation
    .initOperation()
    .catch(() => {
      nodeDataEmpty.value = true;
    })
    .finally(() => {
      loading.value = false;
    });

  return { loading, nodeDataEmpty };
}
