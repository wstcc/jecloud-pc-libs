import { defineComponent, watch, ref } from 'vue';
import { useWorkflowProcessTwo } from './hooks/use-workflow-process-two';
import { Tabs } from '@jecloud/ui';
import OperationUser from './operation-user';
import WorkflowCommonUser from './operation-common-user';
export default defineComponent({
  name: 'WorkflowProcessTwo',
  inheritAttrs: false,
  setup(props, context) {
    const { userConfig, loading, state, selectUser, $user, $func, changeSequential } =
      useWorkflowProcessTwo({
        props,
        context,
      });
    const workflowOperationProcessTwo = ref();

    return () => (
      <div
        v-loading={loading.value}
        class="je-workflow-operation-process-two"
        style={{ display: state.activeStep === 2 ? undefined : 'none' }}
        ref={workflowOperationProcessTwo}
      >
        {/* 选中节点 */}
        <div class="task-node is--active">
          {state.activeNode?.name}
          <i class="icon fal fa-check" />
        </div>
        {/* 分支任务 */}
        {userConfig.nodes.length > 1 ? (
          <div class="task-user-type">
            <div class="task-user-type-label">分支任务：</div>
            <Tabs
              class="task-user-type-items fork-nodes"
              type="card"
              v-model:activeKey={userConfig.activeNodeId}
            >
              {userConfig.nodes.map((item) => {
                return (
                  <Tabs.TabPane
                    class={{
                      'task-user-type-items-item': true,
                      'is--active': item.id === userConfig.activeNodeId,
                    }}
                    key={item.id}
                    tab={item.name}
                  />
                );
              })}
            </Tabs>
          </div>
        ) : null}
        {/* 筛选条件 */}
        {userConfig.types.length > 1 ? (
          <div class="task-user-type">
            <div class="task-user-type-label">筛选条件：</div>
            <Tabs
              class="task-user-type-items"
              type="card"
              v-model:activeKey={userConfig.activeTypeId}
            >
              {userConfig.types.map((item) => {
                return (
                  <Tabs.TabPane
                    class={{
                      'task-user-type-items-item': true,
                      'is--active': item.id === userConfig.activeTypeId,
                    }}
                    key={item.id}
                    tab={item.name}
                  />
                );
              })}
            </Tabs>
          </div>
        ) : null}
        {/* 人员选择器 */}
        <OperationUser
          ref={$user}
          funcObj={$func}
          multiple={userConfig.activeNode?.multiple}
          selectAll={userConfig.activeNode?.selectAll}
          showSequentialConfig={userConfig.activeNode?.showSequentialConfig}
          v-model:result={userConfig.resultUsers}
          sequential={userConfig.activeNode?.sequential}
          userDisabled={userConfig.activeNode?.personnelAdjustments}
          onChangeSequential={changeSequential}
          wfButton={userConfig.wfButton}
        ></OperationUser>
        {/**常用审批人 */}
        <WorkflowCommonUser onSelectUser={selectUser} />
      </div>
    );
  },
});
