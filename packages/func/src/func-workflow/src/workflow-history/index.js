import JeWorkflowHistory from './src';
import { withInstall } from '../../../utils';
export const WorkflowHistory = withInstall(JeWorkflowHistory);
export default WorkflowHistory;
