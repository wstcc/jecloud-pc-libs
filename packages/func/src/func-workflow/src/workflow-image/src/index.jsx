import { defineComponent, h, ref } from 'vue';
import { previewImageApi } from '../../workflow-manager';
/**
 * 流程历史
 */
export default defineComponent({
  name: 'WorkflowImage',
  inheritAttrs: false,
  setup(props, { expose }) {
    //获得流程图previewImageApi
    const imgUrl = ref();
    expose({
      refresh({ beanId, pdid }) {
        imgUrl.value = '';
        if (beanId && pdid) {
          previewImageApi({ beanId, pdid }).then((data) => {
            imgUrl.value = data;
          });
        }
      },
    });
    return () =>
      h('div', {
        innerHTML: imgUrl.value,
        class: 'je-workflow-image',
      });
  },
});
