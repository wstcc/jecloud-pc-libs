import { defineComponent } from 'vue';
import { Grid } from '@jecloud/ui';
import { operationGrid } from './hooks/operation-grid';

export default defineComponent({
  name: 'WorkFlowOperation',
  inheritAttrs: false,
  props: { type: String, taskOperationParams: Object, typeKey: String, funcObj: Object },
  setup(props, content) {
    const { loading, gridStore, columnData } = operationGrid({
      props,
      content,
    });
    return () => (
      <div style="height:100%;padding:0 20px;">
        <Grid
          border
          stripe
          auto-resize
          show-overflow="ellipsis"
          keep-source
          show-header-overflow
          highlight-hover-row
          resizable
          height="100%"
          store={gridStore}
          v-loading={loading.value}
          size="mini"
          header-align="center"
        >
          <Grid.Column title="序号" type="seq" width="50" align="center"></Grid.Column>
          {columnData.value.map((item) => {
            return (
              <Grid.Column
                field={item.field}
                title={item.title}
                align="center"
                width={item.width}
                min-width={item.minWidth}
              ></Grid.Column>
            );
          })}
        </Grid>
      </div>
    );
  },
});
