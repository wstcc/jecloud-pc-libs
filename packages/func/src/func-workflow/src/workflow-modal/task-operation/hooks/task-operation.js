import { reactive, ref } from 'vue';
import { cloneDeep } from '@jecloud/utils';

export function tackOperation({ props, context, Form }) {
  const { $workflow } = props.funcObj;
  const loading = ref(true);
  const okBtnLoading = ref(false);
  const verifyData = [
    {
      required: true,
      message: '该输入项为必填项',
    },
  ];
  const { emit } = context;
  const sendFormData = reactive({
    reminderMethod: props.urgeData.selectMessages || 'WEB',
    personBeingUrgedIds: '',
    personBeingUrgedNames: '',
    urgentContent: props.urgeData.urgeContent || '',
    ccUserIds: '',
    ccUserNames: '',
    ccContent: '',
  });
  const rulesRef = reactive({
    reminderMethod: verifyData,
    personBeingUrgedIds: verifyData,
    urgentContent: verifyData,
  });
  const { validate, validateInfos } = Form.useForm(sendFormData, rulesRef);
  // 激活面板的key
  const activeKey = ref('promptDispatch');
  // 获得通知类型
  const messageDatas = ref(props.urgeData.urgeMessages);
  // 获得被催办的人
  const uraeUsers = ref(props.urgeData.urgeUsers);
  // 选中或取消被催办人
  const selectUser = (user) => {
    user.checked = !user.checked;
    getSelectUser();
    validate('personBeingUrgedIds');
  };

  // 处理被催办人数据
  const getSelectUser = () => {
    const userId = [],
      userName = [];
    uraeUsers.value.forEach((user) => {
      if (user.checked) {
        userId.push(user.id);
        userName.push(user.name);
      }
    });
    sendFormData.personBeingUrgedIds = userId.join(',');
    sendFormData.personBeingUrgedNames = userName.join(',');
  };

  // 保存操作
  const onSave = () => {
    okBtnLoading.value = true;
    const params = cloneDeep(sendFormData);
    validate()
      .then(() => {
        $workflow.action
          .doButtonSubmit({
            params,
          })
          .finally(() => {
            okBtnLoading.value = false;
          });
      })
      .catch((e) => {
        okBtnLoading.value = false;
      });
  };
  //取消操作
  const onClose = () => {
    emit('modalClose', { type: 'close' });
  };
  setTimeout(() => {
    loading.value = false;
  }, 200);
  return {
    activeKey,
    messageDatas,
    sendFormData,
    uraeUsers,
    selectUser,
    onSave,
    onClose,
    validateInfos,
    okBtnLoading,
    loading,
  };
}
