import { Modal } from '@jecloud/ui';
import { getBodyHeight } from '@jecloud/utils';
import WorkFlowTaskOperation from './task-operation';
export function showTaskOperationModal({ data, button, $func }) {
  const taskOperationParams = $func.$workflow.getOperationParams();
  // 当前任务节点ID
  taskOperationParams.currentNodeId = $func.$workflow.state.currentTaskNode?.id;
  const modalClose = () => {
    $Modal && $Modal.close();
  };
  const $Modal = Modal.dialog({
    title: button.name,
    icon: button.icon,
    class: 'je-workflow-task-operation-modal',
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 100,
    showFooter: false,
    headerStyle: { height: '50px' },
    resize: false,
    content: () => {
      return (
        <WorkFlowTaskOperation
          onModalClose={modalClose}
          taskOperationParams={taskOperationParams}
          urgeData={data.urgeData}
          funcObj={$func}
        ></WorkFlowTaskOperation>
      );
    },
  });
}
