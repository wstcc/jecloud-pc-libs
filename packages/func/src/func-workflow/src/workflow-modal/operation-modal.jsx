import { Modal } from '@jecloud/ui';
import { getBodyHeight } from '@jecloud/utils';
import WorkflowOperation from '../workflow-operation';
/**
 * 操作窗口
 * @param {*} param0
 */
export function showWorkflowOperation({ button, $func }) {
  const modal = Modal.window({
    title: button.name,
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 100,
    content() {
      return <WorkflowOperation params={{ button, $func }} />;
    },
  });
  return modal;
}
