import { Func } from '@jecloud/utils';
export const {
  FuncGridView,
  FuncFormView,
  FuncTreeView,
  FuncGroupQueryView,
  FuncManager,
  GridQueryParser,
  TreeQueryParser,
  FuncInfo,
} = Func;
