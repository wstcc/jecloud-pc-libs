import { Modal } from '@jecloud/ui';
import { useManager, FuncRefEnum, useProvideFunc, useWatchActiveBean } from '../index';
import { watch } from 'vue';
import { useAction } from '../action';

/**
 * 功能管理
 * @param {*} options
 * @returns
 */
export function useFuncManager(options) {
  const { props, context } = options;
  const { expose } = context;
  const $func = useManager(options);
  // 增加action处理
  Object.assign($func.action, useAction($func));

  // 功能表单
  if ($func.isFuncForm) {
    $func.setActiveView(FuncRefEnum.FUNC_EDIT);
  }
  // 监听功能只读
  watch(
    () => props.readonly,
    (readonly) => {
      $func.store.readonly = readonly;
    },
  );
  // 对外暴露功能
  expose($func);
  // 对子暴露功能
  useProvideFunc($func);

  // 绑定窗口对象
  if ($func.isWindow) {
    $func.$modal = Modal.useInjectModal();
  }
  // 监听bean
  useWatchActiveBean({ $func });

  return $func;
}
