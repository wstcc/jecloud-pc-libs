import { FuncRefEnum, doEvents4Sync } from '../index';
import { onTreeSelectChange as _onTreeSelectChange } from './use-func-data-events';
import { Modal } from '@jecloud/ui';
/**
 * 功能树形面板事件
 * @param {*} options
 * @returns
 */
export function useFuncTreeEvents({ $func }) {
  return {
    onTreeSelectChange: onTreeSelectChange.bind(this, $func),
    onTreeActionClick: onTreeActionClick.bind(this, $func),
    onTreeMove: onTreeMove.bind(this, $func),
    onTreeBeforeMove: onTreeBeforeMove.bind(this, $func),
  };
}

/**
 * 左侧树形查询
 * @param {*} param0
 * @param {*} options
 */
export function onTreeSelectChange($func, options) {
  const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE);
  // 检索模式
  if ($tree.value.singleMode) {
    treeQuery4Single($func, options);
  } else {
    // 查询模式
    treeQuery($func, options);
  }
}
/**
 * 查询
 * @param {*} $func
 * @param {*} options
 */
function treeQuery($func, options) {
  _onTreeSelectChange($func, options);
}
/**
 * 检索
 * @param {*} $func
 * @param {*} options
 */
function treeQuery4Single($func, options) {
  const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE).value;
  const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
  const { records = [] } = options;
  const row = records[0];
  if (!row) {
    $func.action.doGridInsert();
    return;
  }
  // 选中列表节点
  let gridSelectPromise = Promise.resolve();
  if (!$grid.getRowById(row.id)) {
    gridSelectPromise = $grid.setQuerys({ type: 'tree', querys: row, $tree });
  }
  // 如果列表节点没有，请求节点，然后再选中
  gridSelectPromise
    .then(() => {
      $grid.clearCheckboxRow();
      const editRow = $grid.getRowById(row.id);
      if (editRow) {
        return $grid.setSelectRow(editRow);
      } else {
        return Promise.reject();
      }
    })
    .then(() => {
      $func.action.doGridEdit({ row: $grid.getRowById(row.id) });
    })
    .catch((error) => {
      console.error(error);
      Modal.alert('当前节点不支持检索操作！', Modal.status.warning);
    });
}

/**
 * 左侧树形Action按钮
 * 点击action按钮，会选中树形和列表数据
 * @param {*} param0
 * @param {*} options
 */
export function onTreeActionClick($func, options) {
  const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE).value;
  const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
  const { row } = options;
  // 选中树形节点
  $tree.setSelectRow(row);
  // 选中列表节点  TODO不要级联选中左侧的列表
  /* let gridSelectPromise = Promise.resolve();
  if (!$grid.getRowById(row.id)) {
    gridSelectPromise = $grid.setQuerys({ type: 'tree', querys: row, $tree });
  }
  // 如果列表节点没有，请求节点，然后再选中
  return gridSelectPromise.then(() => {
    $grid.clearCheckboxRow();
    const gridRow = $grid.getRowById(row.id);
    return gridRow && $grid.setSelectRow(gridRow);
  }); */
}

/**
 * 树形节点移动
 * @param {*} $func
 * @param {*} param1
 */
export function onTreeMove($func, options) {
  const funcData = $func.getFuncData();
  // 自定义移动事件
  const treeDropEvent = funcData.info.gridEvents['tree-drop'];
  if (treeDropEvent) {
    const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE);
    return doEvents4Sync({
      $func,
      eventOptions: { ...options, $tree },
      code: treeDropEvent,
    });
  } else {
    const { id, toId, place } = options.info;
    $func.action.doTreeMove({ id, toId, place });
  }
}

/**
 * 树形拖放前
 * @param {*} $func
 * @param {*} options
 */
export function onTreeBeforeMove($func, options) {
  const funcData = $func.getFuncData();
  const treeBeforeDropEvent = funcData.info.gridEvents['tree-before-drop'];
  if (treeBeforeDropEvent) {
    const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE);
    return doEvents4Sync({
      $func,
      eventOptions: { ...options, $tree },
      code: treeBeforeDropEvent,
    });
  }
}
