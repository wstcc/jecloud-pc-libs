import { computed, toRaw, isRef } from 'vue';
import { Modal } from '@jecloud/ui';
import {
  isNotEmpty,
  isEmpty,
  encode,
  findDDAsyncNodes,
  isString,
  isFunction,
  forEach,
  isPromise,
  execScript4Return,
  toTemplate,
} from '@jecloud/utils';
import { parseTreeSearchItem } from '../../utils';
import { parseConfigInfo } from '../../func-util';
import { onTreeSelectChange } from './use-func-data-events';
/**
 * 功能选择面板事件
 * @param {*} options
 * @returns
 */
export function useFuncSelectEvents({ type, $func, modal, selectOptions }) {
  // 查询选择配置
  const { configInfo } = selectOptions;
  // 解析配置信息
  const selectConfig = parseConfigInfo({ type, configInfo, options: selectOptions }) ?? {};
  // 窗口对象
  const $modal = computed(() => {
    return isRef(modal) ? modal.value : modal;
  });
  // 追加到$func
  Object.assign($func, {
    selectConfig,
    selectOptions,
    $modal,
  });
  return {
    selectConfig,
    onGridCellDblclick: onGridCellDblclick.bind(this, $func),
    onTreeCellDblclick: onTreeCellDblclick.bind(this, $func),
    onTreeSelectChange: onTreeSelectChange.bind(this, $func),
    onTreeRendered: onTreeRendered.bind(this, $func),
  };
}

/**
 * 列表双击选择数据
 * @param {*} param0
 * @param {*} options
 */
function onGridCellDblclick($func, options) {
  onCellDblclick($func, { $plugin: $func.getFuncGrid().getPlugin(), ...options });
}

/**
 * 树形双击选择数据
 * @param {*} param0
 * @param {*} options
 */
function onTreeCellDblclick($func, options) {
  onCellDblclick($func, { $plugin: $func.getPlugin(), ...options });
}
/**
 * 树形初始化
 * @param {*} $func
 * @returns
 */
function onTreeRendered($func) {
  const { selectOptions, selectConfig, $tree } = $func;
  const { value, name, idProperty, model = {} } = selectOptions;
  if (isEmpty(value)) return;
  // 树形插件
  const $plugin = $tree.value.getPlugin();

  // 1.根据idProperty获取对应的value
  let treeIdProperty = idProperty ?? 'code';
  let fieldName = name;
  forEach(['code', 'id', 'text'], (key) => {
    const index = selectConfig.sourceFields.indexOf(key);
    if (index > -1) {
      treeIdProperty = key;
      fieldName = selectConfig.targetFields[index];
      return false;
    }
  });
  const val = model[fieldName] ?? value;
  const vals = val ? val.split(',') : [];

  // 2.查找符合条件的值，进行初始化选中
  const initSelectNodes = function (nodes) {
    // 批量展开节点
    const expandPromises = nodes.map((node) =>
      $plugin.setTreeExpand4Path({
        path: parseTreeSearchItem(node).nodePath,
        async: selectConfig.async,
      }),
    );
    // 展开父级目录
    Promise.all(expandPromises).then(() => {
      const rows = nodes.map((node) => $plugin.getRowById(node.id));
      $plugin.setSelectRow(rows);
      // 滚动到选中第一个节点的位置
      $plugin.scrollToRow(rows[0]);
    });
  };
  // 异步处理
  if (selectConfig.async) {
    // 查询参数缓存
    const params = $tree.value.store.getLoadOptionsCache()?.params;
    // 字典项信息
    findDDAsyncNodes({
      type: treeIdProperty,
      value: vals.join(','),
      strData: params?.strData,
    }).then(initSelectNodes);
  } else {
    let rows = [];
    vals.forEach((val) => {
      const items = $plugin.findRecords((row) => val == row[treeIdProperty]);
      rows = rows.concat(items);
    });
    initSelectNodes(rows);
  }
}
/**
 * 双击操作
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function onCellDblclick($func, options = {}) {
  const { row, $plugin } = options;
  const { selectOptions, selectConfig, $modal } = $func;
  let { selectExp, callback, disableDblclick } = selectOptions;
  // 禁用双击
  if (disableDblclick) return;
  const rows = $func.getResultRecords?.() ?? $plugin.getSelectedRecords();
  // 双击处理
  if (row && !rows.includes(row)) {
    rows.push(row);
  }
  // 可选表达式处理setSelectRow
  if (selectExp) {
    const message = '选中的数据无效，请重新选择...';
    if (isString(selectExp)) {
      selectExp = { exp: selectExp, message };
    } else if (isFunction(selectExp)) {
      selectExp = { fn: selectExp, message };
    } else {
      selectExp.message = selectExp.message || message;
    }

    // 可选表达式，不支持异步
    const disabledRows = rows.filter(
      // 增加外部出入的功能对象
      (item) => {
        if ($func.action) {
          return !$func.action.doFormFieldExp4Sync({ model: item, exp: selectExp });
        } else {
          return !execScript4Return(toTemplate(selectExp.exp, item));
        }
      },
    );
    if (isNotEmpty(disabledRows)) {
      Modal.confirm(selectExp.message, {
        text: '智能剔除无效数据',
        handler: function () {
          $plugin.setSelectRow(disabledRows, false);
        },
      });
      return;
    }
  }
  // 原始数据
  const rawRows = rows.map((item) => toRaw(item));
  // 增加树形路径信息
  let paths = [];
  if (selectConfig.type == 'tree') {
    paths = rawRows.map((item) => {
      return {
        id_: $plugin.store.getNodePath(item, 'id'),
        code_: $plugin.store.getNodePath(item, 'code'),
        text_: $plugin.store.getNodePath(item, 'text'),
      };
    });
  }
  // 触发回调函数
  const result = callback?.({ rows: rawRows, $modal: $modal.value, config: selectConfig, paths });
  if (isPromise(result)) {
    result
      .then(() => {
        $modal.value?.close?.();
      })
      .catch((e) => {
        e && console.log(e);
      });
  } else if (result != false) {
    $modal.value?.close?.();
  }
}
