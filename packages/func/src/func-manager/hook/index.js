import { Func } from '@jecloud/utils';
export const {
  /*use-func-content.js*/
  FuncContextKey,
  FuncFormContextKey,
  FuncEditContextKey,
  MainFuncContextKey,
  useProvideFunc,
  useInjectFunc,
  useProvideFuncForm,
  useInjectFuncForm,
  useProvideFuncEdit,
  useInjectFuncEdit,
  useProvideMainFunc,
  useInjectMainFunc,
  /*use-func-manager.js*/
  useManager,
  setFuncManager,
  getFuncManager,
  /*use-func-exps.js*/
  useWatchFieldExps,
  useFuncGridStore,
  useFuncTreeStore,
  /*use-func-active-bean.js*/
  useWatchActiveBean,
  /*use-func-children-field.js*/
  useFuncChildrenField,
} = Func;

export * from './use-func-manager';
export * from './use-func-common';
export * from './use-func-data-events';
export * from './use-func-select-events';
export * from './use-func-tree-events';
