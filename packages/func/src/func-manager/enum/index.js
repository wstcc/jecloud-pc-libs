import { Func } from '@jecloud/utils';
export const {
  /*func-column-enum.js*/
  FuncColumnTypeEnum,
  /*func-enum.js*/
  FuncChildrenLayoutEnum,
  FuncRefEnum,
  FuncChangeViewActionEnum,
  FuncTypeEnum,
  FuncTreeTypeEnum,
  FuncDataTypeEnum,
  FuncButtonTypeEnum,
  /*func-field-enum.js*/
  FuncFieldTypeEnum,
  /*func-query-enum.js*/
  QueryTypeEnum,
  QueryOrderEnum,
  QueryConnectorEnum,
  QueryConnectorTextEnum,
} = Func;
