import { useTreeAction } from './tree-action';
import { useGridAction } from './grid-action';
import { useExtendAction } from './extend-action';
export function useAction($func) {
  const actions = {};
  const funcs = {
    ...useTreeAction(),
    ...useGridAction(),
    ...useExtendAction(),
  };
  Object.keys(funcs).forEach((key) => {
    actions[key] = funcs[key].bind(this, $func);
  });
  return actions;
}
