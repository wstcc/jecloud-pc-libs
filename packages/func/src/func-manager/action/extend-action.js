import { FuncButtonTypeEnum } from '../enum';
import { isNotEmpty } from '@jecloud/utils';

/**
 * 扩展action
 * @returns
 */
export function useExtendAction() {
  return { doFormSaveAfter };
}
/**
 * 表单保存后业务处理
 * @param {*} $func
 * @param {*} param1
 */
function doFormSaveAfter($func, { insert, bean, node }) {
  // 弹出表单处理
  const $baseFunc = $func.getParams()?.$baseFunc || $func;
  if (insert) {
    // 树形添加
    node && $baseFunc.action.doTreeSave4View({ node });
    // 绑定row
    const row = $baseFunc.action.doGridSave4View({ record: bean, node });
    $baseFunc.store.setActiveRow(row);
  } else {
    // 树形修改
    node && $baseFunc.action.doTreeUpdate4View({ nodes: [node] });
    // 更新列表数据
    $baseFunc.action.doGridUpdate4View({ records: [bean], nodes: [node] });
  }
  // 刷新统计数据
  $baseFunc.getFuncGrid()?.store.loadStatistics();
  // 级联保存子功能
  const $childFuncObj = $baseFunc.getChildFunc() || {};
  for (let key in $childFuncObj) {
    // 子功能
    const $child = $childFuncObj[key];
    // 子功能保存按钮
    const saveBtn = $child?.getGridButtons(FuncButtonTypeEnum.GRID_UPDATE_BUTTON);
    const $childGrid = $child?.getFuncGrid();
    // 子功能列表是否有数据改动
    const changes = $childGrid?.store.getChanges();
    if (saveBtn && !saveBtn.hidden && isNotEmpty(changes)) {
      saveBtn.click();
    }
  }
}
