import { encode, isEmpty, excelImport } from '@jecloud/utils';
import { Modal } from '@jecloud/utils';
import { showFuncForm4Promise } from '../../func-util';
import { showExportForm } from '../../func-data';
import { doBatchModifyListApi } from '../api';

export function useGridAction() {
  return {
    doGridRemove,
    doGridEdit,
    doGridInsert,
    doGridInsert4Multi,
    doGridUpdateList,
    doGridExport,
    doGridImport,
    doGridRemove4View,
    doGridInsert4Grid,
    doGridBatchModify,
  };
}

/**
 * 列表导出
 * @param {Object} $func
 * @param {String} templateCode 模板编码
 * @param {Object} customParams 自定义参数
 * @param {Object} config 自定义配置
 * @param {Object} config.title 标题
 * @param {Object} config.fileName 文件名
 * @param {Object} config.exportType 类型 (当前页 NOWPAGE,选择数据 SELECTION,全部 ALL)默认NOWPAGE
 */
function doGridExport($func, templateCode, customParams = {}, config = {}) {
  showExportForm({ $func, templateCode, customParams, config });
}
/**
 * 列表导入
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doGridImport($func, options = {}) {
  const { autoLoad = true, templateCode, params } = options;
  return excelImport(templateCode, params)
    .then(() => {
      if (autoLoad) {
        $func.getFuncGrid().loadPage(1);
      }
    })
    .catch(({ message }) => {
      Modal.alert(message, 'error');
    });
}

/**
 * 列表删除
 * @param {*} $func
 */
function doGridRemove($func, options = {}) {
  const grid = $func.getFuncGrid();
  return $func.action.doBaseGridRemove(options).then(({ ids }) => {
    $func.action.doTreeRemove4View({ ids });
    setTimeout(() => {
      doGridRemove4View($func, { ids });
      grid?.refreshSelection();
    }, 100);
  });
}
/**
 * 列表批量修改
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doGridUpdateList($func, options = {}) {
  const $grid = $func.getFuncGrid();
  return $func.action.doBaseGridUpdateList(options).then(({ funcTree, nodes }) => {
    // 树形功能
    if (funcTree) {
      $func.action.doTreeUpdate4View({ nodes });
    }
    // 刷新统计数据
    $grid.store.loadStatistics();

    // 刷新编号
    $grid.getPlugin().updateSeqConfig({
      currentPage: $grid.store.currentPage,
      pageSize: $grid.store.pageSize,
    });
    return options;
  });
}
/**
 * 列表批量编辑
 * @param {*} $func
 * @param {*} options
 */
function doGridBatchModify($func, options = {}) {
  const { type, ids = [], j_query, bean } = options;
  if (isEmpty(bean)) {
    return Promise.reject({ message: '请输入要修改的数据！' });
  } else if (type === 'select' && ids.length === 0) {
    return Promise.reject({ message: '请选择要修改的数据！' });
  }
  const funcData = $func.getFuncData();
  const { productCode, tableCode, funcCode, funcAction } = funcData.info;
  const $grid = $func.getFuncGrid();
  return doBatchModifyListApi({
    params: {
      type,
      bean: encode(bean),
      ids: ids.join(','),
      j_query,
      totalCount: $grid?.store.totalCount,
      tableCode,
      funcCode,
    },
    pd: productCode,
    action: funcAction,
  }).then(() => {
    $grid.reload();
  });
}

/**
 * 添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert($func, options = {}) {
  const funcData = $func.getFuncData();
  return $func.action.doGridBeforeInsert(options).then(({ bean }) => {
    if (funcData.info.insertType === 'FIRSTADD') {
      //首行录入
      return doGridInsert4Grid($func, { records: [bean] });
    } else if (['FORMWIN', 'FORMDRAWER'].includes(funcData.info.insertType)) {
      //弹出表单
      return doGridInsert4Window($func, { bean, showType: funcData.info.insertType });
    } else {
      //表单录入
      return doGridInsert4Form($func, { bean });
    }
  });
}

/**
 * 表单添加
 * @param {*} $func
 * @param {*} param1
 * @returns
 */
function doGridInsert4Form($func, options) {
  return $func.action.doBaseGridInsert4Form(options);
}
/**
 * 列表添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert4Grid($func, options = {}) {
  return $func.action.doBaseGridInsert4Grid(options);
}

/**
 * 弹窗添加
 * @param {*} $func
 * @param {*} options
 */
function doGridInsert4Window($func, { bean, showType }) {
  return showFuncForm4Promise($func.funcCode, {
    bean,
    isDDFunc: $func.isDDFunc,
    params: { $baseFunc: $func },
    showType,
  }).then(($modal) => ({
    $modal,
  }));
}
/**
 * 多选添加
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doGridInsert4Multi($func, options = {}) {
  return $func.action.doBaseGridInsert4Multi(options);
}

/**
 * 页面编辑
 * @param {*} $func
 * @param {*} param1
 */
function doGridEdit($func, options = {}) {
  return $func.action
    .doGridBeforeEdit(options)
    .then((bean) => {
      const funcData = $func.getFuncData();
      const { link } = options;
      // 列表超链接，弹出表单  action编辑按钮和双击列编辑根据功能配置,弹出表单
      if (
        link?.openForm ||
        (!link && ['FORMWIN', 'FORMDRAWER'].includes(funcData.info.insertType))
      ) {
        return doGridInsert4Window($func, { bean, showType: funcData.info.insertType });
      } else {
        return doGridInsert4Form($func, { bean });
      }
    })
    .catch(() => {});
}

/**
 * 删除数据
 * @param {*} param0
 * @returns
 */
function doGridRemove4View($func) {
  const grid = $func.getFuncGrid();
  const changes = grid.store.getChanges() || [];
  // 单前页没有数据刷新数据 (数据有变动并且单前页有数据不刷新)
  if (
    grid.store.pageSize !== -1 &&
    (changes.length <= 0 || grid.store.data.length == 0) &&
    grid.store.totalCount > grid.store.pageSize
  ) {
    grid.store.reload();
  } else {
    // 刷新统计数据
    grid.store.loadStatistics?.();
  }
}
