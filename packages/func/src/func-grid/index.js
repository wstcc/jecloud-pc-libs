import JeFuncGrid from './src/grid';
import { withInstall } from '../utils';

export const FuncGrid = withInstall(JeFuncGrid);
export default FuncGrid;
