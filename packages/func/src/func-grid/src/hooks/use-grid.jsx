import { nextTick, onMounted, reactive, ref, watch, computed } from 'vue';
import { useGridStore } from './use-grid-store';
import { Grid, Hooks } from '@jecloud/ui';
import { useColumn } from './use-column';
import { FuncButtonTypeEnum, QueryTypeEnum } from '../../../func-util';
import { FuncGridView, useInjectFunc, FuncTypeEnum } from '../../../func-manager';
import { useFuncDataEvents } from '../../../func-manager';
import { isNotEmpty, shiftNumberFormat, camelCase } from '@jecloud/utils';
import { useProvideFuncGrid } from '../context';
export function useGrid({ props, context }) {
  const { emit, expose } = context;
  const $plugin = ref();
  const selection = ref();
  // 功能对象
  const $func = useInjectFunc();
  // 功能数据
  const funcData = $func.getFuncData();
  const { gridOptions } = funcData.info;
  // 隐藏分页条
  const pagerHidden = gridOptions.sort || props.enableSort || gridOptions.pagePostion === 'hidden';
  // 数据store
  const store = useGridStore({
    $func,
    pageSize: pagerHidden ? -1 : null,
    ...props,
  });
  // 列表属性
  const gridAttrs = reactive({});
  const $grid = new FuncGridView({
    $func,
    props,
    context,
    store,
    pluginRef: $plugin,
  });
  mixinGrid({ $grid, $func, selection, gridAttrs });
  // 继承plugin方法
  $grid.mixin(Hooks.useExtendMethods({ plugin: $plugin, keys: Grid.MethodKeys }));

  // 暴露列表对象
  expose($grid);
  useProvideFuncGrid($grid);

  // 监听列表选择数据操作，方便列表按钮条切换按钮
  watch(
    () => $plugin.value?.selection,
    (value) => {
      selection.value = value;
    },
    { deep: true },
  );
  watch(
    () => props.readonly,
    () => {
      $grid.setReadOnly(props.readonly);
    },
  );

  onMounted(() => {
    nextTick(() => {
      // 设置默认查询策略，防止列表二次请求
      $grid.initQuerys().then(() => {
        // 列表默认加载
        if (props.autoLoad) {
          store.load().then(() => {
            emit('rendered', { $grid });
          });
        } else {
          emit('rendered', { $grid });
        }
      });
      if (props.readonly) {
        $grid.setReadOnly(true);
      }
    });
  });

  // 校验规则
  const editRules = funcData.getValidRules({ type: 'column', $grid, $func });

  const { helpMessage } = gridOptions;
  // 帮助提示
  const emtpySlot = () => {
    return helpMessage ? (
      <div class="je-func-help-message">
        <div class="content">{helpMessage}</div>
      </div>
    ) : null;
  };

  // 表格配置
  Object.assign(gridAttrs, {
    editRules,
    ...parseGridAttrs({ props, context, $func, $grid }),
    ...parseFuncGridAttrs({ props, context, $func, $grid }),
  });

  return { $plugin, $func, emtpySlot, gridAttrs, store };
}
/**
 * 解析列表配置
 * @param {*} param0
 * @returns
 */
function parseGridAttrs({ props, context, $func, $grid }) {
  // 功能数据
  const funcData = $func.getFuncData();
  const { gridOptions } = funcData.info;
  const { pagePostion, hideRightBorder, hideBottomBorder, multiple, size, stripe } = gridOptions;
  // 表格配置
  const gridAttrs = { size: props.size ?? size, stripe };

  // 分页条信息
  let pagerConfig = true;
  if (['left', 'right'].includes(pagePostion)) {
    pagerConfig = { align: pagePostion, pageSizes: [30, 100, 500, 1000] };
  } else if (pagePostion === 'hidden') {
    pagerConfig = false;
  }
  gridAttrs.pagerConfig = pagerConfig;

  // 边框
  let border = true;
  if (hideBottomBorder && hideRightBorder) {
    border = 'outer';
  } else if (hideBottomBorder) {
    border = 'vertical';
  } else if (hideRightBorder) {
    border = 'default';
  }
  gridAttrs.border = border;

  // 允许多选
  gridAttrs.multiple = props.multiple ?? multiple;
  // 允许取消选择
  gridAttrs.allowDeselect = true;
  if (gridAttrs.multiple) {
    gridAttrs.rowConfig = { isCurrent: false };
    gridAttrs.highlightCurrentRow = false;
  }

  // 解析功能列
  gridAttrs.columns = useColumn({ gridAttrs, $grid, $func });

  // 列排序
  gridAttrs.sortConfig = {
    iconAsc: 'fas fa-caret-up',
    iconDesc: 'fas fa-caret-down',
  };
  gridAttrs.onSortChange = ({ property, order }) => {
    // 查询
    $grid.setQuerys({ type: QueryTypeEnum.ORDER, querys: { code: property, type: order } });
  };
  // 事件处理
  const { hasListener, fireListener } = Hooks.useListeners({ props, context });

  // 样式事件处理
  const styleEvents = [
    'cell-style',
    'row-style',
    'header-cell-style',
    'header-row-style',
    'footer-cell-style',
    'footer-row-style',
  ];
  styleEvents.forEach((eventName) => {
    if (hasListener(eventName)) {
      gridAttrs[camelCase(eventName)] = (options) => {
        return fireListener(eventName, options);
      };
    }
  });

  return gridAttrs;
}
/**
 * 解析功能列表的配置项
 * @param {*} param0
 * @returns
 */
function parseFuncGridAttrs({ props, context, $func, $grid }) {
  // 表格配置
  const gridAttrs = {};
  // 查询选择不处理
  if (props.type === 'select') return gridAttrs;
  // 功能数据
  const funcData = $func.getFuncData();
  const { funcType } = funcData.info;
  const { sort, rowEdit } = funcData.info.gridOptions;
  const { showType, lazy } = funcData.info.treeOptions;
  // 拖动排序
  if (!props.readonly && (sort || props.enableSort)) {
    const { onBaseGridSort } = useFuncDataEvents({ $func });
    gridAttrs.draggable = true;
    gridAttrs.onDrop = onBaseGridSort;
    // 取消分页条
    gridAttrs.pagerConfig = false;
  }
  // 行编辑
  gridAttrs.editConfig = {
    enabled: !props.readonly && $func.hasGridSavePerm(),
    trigger: 'click',
    showStatus: true,
    mode: rowEdit ? 'row' : 'cell',
  };
  // 选择编辑
  gridAttrs.mouseConfig = { selected: gridAttrs.editConfig.enabled };

  // 列表统计
  const statisticsColumnFields = funcData.columns.statistics;
  const statisticsType = funcData.info.gridOptions.statisticsType;
  if (statisticsColumnFields.length > 0 && isNotEmpty(statisticsType)) {
    gridAttrs.footerCellClassName = 'je-footer-statistics-cell';
    gridAttrs.footerRowClassName = 'je-footer-statistics-row';
    gridAttrs.showFooter = true;
    // 表尾统计数据处理
    gridAttrs.footerMethod = ({ columns, data }) => {
      const footerData = [];
      const statistics = $grid.store.doStatistics({ $grid, columns, data });
      if (statistics.single.show) {
        footerData[0] = statistics.single.data;
      }
      if (statistics.all.show) {
        footerData[1] = statistics.all.data;
      }
      return footerData;
    };
  }

  // 事件处理
  const { hasListener, fireListener } = Hooks.useListeners({ props, context });
  // 编辑前事件
  if (hasListener('before-edit-actived')) {
    gridAttrs.editConfig.beforeEditMethod = (options) => {
      return fireListener('before-edit-actived', options);
    };
  }
  // 编辑后事件，用于列表 多选框，选择字段使用
  if (hasListener('edit-closed')) {
    gridAttrs.editConfig.editClosedMethod = (options) => {
      return fireListener('edit-closed', options);
    };
  }

  // 如果功能列表是树形展示
  if (showType == 'tree' && funcType === FuncTypeEnum.TREE) {
    // 去掉分页条
    gridAttrs.pagerConfig = false;
    gridAttrs.treeConfig = {
      rowField: 'id',
      parentField: 'parent',
      lazy,
      treeTable: true,
      loadMethod({ row }) {
        // 异步加载
        const store = $grid.store;
        const params = store.queryParser.toParams({
          rootId: row.id,
        });
        row.loaded = true;
        return store.proxy
          .read({ params })
          .then((data) => data.map((item) => store._addCache(item, true)));
      },
    };
    gridAttrs.rowConfig = { isCurrent: true, keyField: 'id' };
  }
  return gridAttrs;
}

/**
 * 复写表格方法
 * @param {*} param0
 */
function mixinGrid({ $func, $grid, selection, gridAttrs }) {
  $grid.mixin({
    selection,
    /**
     * 只读
     * @param {*} readonly
     * @returns
     */
    setReadOnly(readonly) {
      // 1. 按钮隐藏
      Object.values(this.getButtons()).forEach((button) => {
        if (button.code !== FuncButtonTypeEnum.GRID_EDIT_BUTTON && !button.noReadOnly) {
          button.hidden = readonly;
        }
      });

      // 2. 编辑
      gridAttrs.editConfig.enabled = !readonly;
      gridAttrs.mouseConfig.selected = !readonly;
      if (isNotEmpty(gridAttrs.draggable, true)) {
        gridAttrs.draggable = !readonly;
      }
    },
    reset() {
      // 快速查询树取消选中
      $func.getFuncTree()?.clearSelectedRecords();
      // 重置查询条件
      this.initQuerys().then(() => {
        this.store.loadPage(1);
      });
    },
    /**
     *单页统计
     * @param {*} data
     * @param {*} statisticsConfig
     * @return {*}
     * @memberof Func
     */
    getPageStatistics(data, statisticsConfig, field) {
      const val = [];
      const columnDatas = [];
      data.forEach((item) => {
        columnDatas.push(Number(item[field]));
      });
      statisticsConfig.forEach((item) => {
        if (item.isShow) {
          const type = item.name;
          const number = item.number;
          let num = '';
          switch (type) {
            case 'count':
              num = data.length;
              break;
            case 'min':
              num = Math.min.apply(Math, columnDatas);
              break;
            case 'max':
              num = Math.max.apply(Math, columnDatas);
              break;
            case 'sum':
              num = columnDatas.reduce((acc, curr) => acc + curr);
              break;
            case 'average':
              num = columnDatas.reduce((acc, curr) => acc + curr) / columnDatas.length;
              break;
          }
          let msg = '';
          if (isNotEmpty(num) && isNotEmpty(number)) {
            num = shiftNumberFormat(num, number);
          }
          if (isNotEmpty(item.desc)) {
            msg = item.desc + num;
          } else {
            msg = num;
          }
          val.push(msg);
        }
      });
      return val;
    },
    /**
     *
     *
     * @param {*} data // 后台返回的数据
     * @param {*} statisticsColumnObj // 记录列下标数据
     * @param {*} statisticsColumnsConfig // 统计列的统计类型信息
     * @param {*} columnsInfo // 统计配置信息
     * @return {*}
     * @memberof Func
     */
    getAllStatistics(data, statisticsColumnObj, statisticsColumnsConfig, columnsInfo) {
      const val = [];
      for (const key in statisticsColumnsConfig) {
        // 1.获得统计列的配置信息
        const statisticsConfig = columnsInfo.get(key).statistics.config;
        const array = statisticsColumnsConfig[key];
        // 字段的位置(下标)
        const columnsIndex = statisticsColumnObj[key];
        val[columnsIndex] = [];
        array.forEach((item) => {
          // 获得配置信息中的提示语
          const msgObj = statisticsConfig.filter((config) => {
            return config.name == item;
          });
          const totalDesc = msgObj[0]?.totalDesc;
          const number = msgObj[0]?.number;
          if (item == 'msg') {
            val[columnsIndex].push(totalDesc);
          } else {
            const numObj = data.filter((d) => {
              return d.code == key;
            })[0];
            let num = Number(numObj[item]);
            if (isNotEmpty(num) && isNotEmpty(number)) {
              num = shiftNumberFormat(num, number);
            }
            val[columnsIndex].push(totalDesc + num);
          }
        });
      }
      return val;
    },
  });
}
