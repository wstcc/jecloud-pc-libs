import { reactive, nextTick } from 'vue';
import { encode } from '@jecloud/utils';
import {
  getStatisticsApi,
  useFuncGridStore,
  useFuncTreeStore,
  FuncTypeEnum,
} from '../../../func-manager';

export function useGridStore({ $func, querys, orders, pageSize, type }) {
  const funcData = $func.getFuncData();
  const { treeOptions, funcType } = funcData.info;

  let store = null;
  // 如果是树形功能并且是树形展示模式
  if (treeOptions.showType == 'tree' && funcType === FuncTypeEnum.TREE) {
    store = useFuncTreeStore({
      type,
      $func,
      querys,
      orders,
      params: { async: treeOptions.lazy },
    });
  } else {
    store = useFuncGridStore({ $func, querys, orders, pageSize, type });
  }
  // 列表统计数据
  store.statistics = reactive({
    single: { data: [], show: false },
    all: { data: [], show: false },
  });
  // 列表统计数据方法
  store.doStatistics = ({ $grid, columns, data }) => {
    const funcData = $func.getFuncData();
    const statisticsType = funcData.info.gridOptions.statisticsType;
    const columnsInfo = funcData.basic.columns;
    const singleStatistics = []; // 单页统计
    // 获得统计列code
    const statisticsColumnFields = funcData.columns.statistics;
    if (statisticsColumnFields.length > 0 && columns.length > 0 && data.length > 0) {
      // 获取到统计列表的下标
      const statisticsColumnObj = {};
      const statisticsColumnsConfig = {};
      columns.forEach((item, index) => {
        if (statisticsColumnFields.indexOf(item.field) != -1) {
          statisticsColumnObj[item.field] = index;
          statisticsColumnsConfig[item.field] = [];
          // 获得统计列的计算过的数据
          // 1.获得统计列的配置信息
          const statisticsConfig = columnsInfo.get(item.field).statistics.config;
          statisticsConfig.forEach((config) => {
            if (config.isShow) {
              statisticsColumnsConfig[item.field].push(config.name);
            }
          });
          // 配置有单页统计
          if (statisticsType.indexOf('single') != -1) {
            // 2.调用方法获得计算后的值
            const val = $grid.getPageStatistics(data, statisticsConfig, item.field);
            // 3.把值放入统计数据中
            singleStatistics[index] = val;
          }
        }
      });
      if (singleStatistics.length > 0) {
        store.statistics.single.data = singleStatistics;
        store.statistics.single.show = true;
      }
      // 总统计处理
      if (!store.statistics.all.show && statisticsType.indexOf('all') != -1) {
        store.statistics.all.show = true;
        getStatisticsApi({
          params: {
            tableCode: funcData.info.tableCode,
            funcCode: funcData.info.funcCode,
            strData: encode(statisticsColumnsConfig),
            j_query: encode(store.queryParser.getQuerys()),
          },
          pd: funcData.info.productCode,
          action: funcData.info.funcAction,
        }).then((data) => {
          const val = $grid.getAllStatistics(
            data,
            statisticsColumnObj,
            statisticsColumnsConfig,
            columnsInfo,
          );
          if (val.length > 0) {
            store.statistics.all.data = val;
          }
          $grid?.updateFooter();
        });
      }
      if (statisticsType.indexOf('all') == -1 && store.statistics.all.show) {
        store.statistics.all.show = false;
      }
    } else {
      store.statistics.single.data = [];
      store.statistics.all.data = [];
      store.statistics.all.show = false;
      store.statistics.single.show = false;
    }
    return store.statistics;
  };
  // 刷新统计数据
  store.loadStatistics = () => {
    const funcData = $func.getFuncData();
    const statisticsColumnFields = funcData.columns.statistics;
    if (statisticsColumnFields.length > 0) {
      const $grid = $func.getFuncGrid();
      store.statistics.all.show = false;
      $grid?.updateFooter();
      store.refreshData();
    }
  };
  // 查询前解析查询条件
  store.on('load', function () {
    const $grid = $func.getFuncGrid();
    // 刷新统计数据
    store.loadStatistics();
    // 功能列表刷新后，取消未在本页选中数据，恢复工具条选中状态
    type === 'func' &&
      nextTick(() => {
        $grid?.refreshSelection();
      });
  });
  return store;
}
