import { ref } from 'vue';
import { Input, Modal } from '@jecloud/ui';
import { useInjectFunc } from '../../../../func-manager';
/**
 * 关键字查询
 * @param {*} param0
 * @returns
 */
export function useKeywordQuery({ $grid, type }) {
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  // 关联查询进入的功能始终显示关键字查询
  const disabled = funcData.info.disableQuerys.includes('keyword') && type !== 'select';
  const keyword = ref('');
  const onSearch = function (val) {
    $grid.value.setQuerys({ type: 'keyword', querys: val }).catch(({ message }) => {
      Modal.notice(message, Modal.status.warning);
    });
  };

  const keywordSlot = function () {
    return disabled ? null : (
      <Input.Search
        class="je-func-grid-search-keyword"
        allowClear
        enter-button="搜索"
        v-model:value={keyword.value}
        onSearch={onSearch}
        v-slots={{
          prefix() {
            return <i class="fal fa-search"></i>;
          },
        }}
      />
    );
  };
  return { keywordSlot };
}
