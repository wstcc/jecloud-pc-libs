import { Panel, Button, Form, Row, Col } from '@jecloud/ui';
import { debounce, pick } from '@jecloud/utils';
import { useProvideFuncTopQuery } from './use-query-context';
import QueryItem from '../group-query-item';
import { watch, ref } from 'vue';
import { QueryTypeEnum } from '../../../../func-util';
import { FuncGroupQueryView, useInjectFunc, FuncButtonTypeEnum } from '../../../../func-manager';
/**
 * 高级查询
 * @returns
 */

export function useGroupQuery({ props, context }) {
  const { expose } = context;
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  const { bindButtonEvents } = $func.action;
  const $plugin = ref();
  const $groupQuery = new FuncGroupQueryView({
    $func,
    props,
    context,
    pluginRef: $plugin,
    panelItem: Panel.useInjectPanelItem(),
  });
  $groupQuery.mixin({
    getFuncGrid() {
      return $func.getFuncGrid();
    },
  });

  const { info, groupQuerys } = funcData;
  const { groupQueryOptions } = info;
  const { layout, cols, strategy, changeQuery, enableQuery, labelWidth, enableExport } =
    groupQueryOptions;
  const resting = ref(false);
  // 查询modal
  const { model, formModel } = $groupQuery;
  // 操作方法
  const { doQuery, doReset, doSaveQuerys, doSort } = useGroupAction({
    $groupQuery,
    model,
    resting,
  });

  // 获得功能导出按钮
  const buttons = $func.getGridButtons();
  const exportButtonConfig = buttons.filter(
    (btn) => btn.code == FuncButtonTypeEnum.GRID_EXPORT_BUTTON,
  );

  $groupQuery.mixin({
    doQuery,
    doReset,
    doSort,
  });
  groupQuerys.forEach((field) => {
    $groupQuery.createQueryItem(field);
    // 值改变，进行查询
    if (changeQuery) {
      watch(
        () => [model[field.name].type, model[field.name].value, model[field.name].betweenValue],
        () => {
          // 重置不需要查询
          !resting.value && doQuery();
        },
      );
      // 更新formModel
      watch(
        () => [model[field.name].values],
        ([value]) => {
          Object.assign(formModel, value);
        },
        { deep: true },
      );
    }
  });
  // 格栅布局
  const gridLayout = layout === 'gridLayout';
  const parseQuerys = (options) => {
    console.log(options);
  };

  // 查询表单
  const groupQuerySlot = () => {
    return (
      <Form
        ref={$plugin}
        model={model}
        class={{ 'je-func-group-query': true, 'grid-layout': gridLayout }}
      >
        <Row>
          {groupQuerys.map((field) => itemSlot(field))}
          {buttonSlot()}
        </Row>
      </Form>
    );
  };
  // 操作按钮
  const buttonSlot = () =>
    enableQuery ? (
      <Col span={gridLayout ? 24 : undefined} class="query-buttons">
        <Button type="primary" onClick={() => doQuery()}>
          搜索
        </Button>
        <Button onClick={doReset} class="bgcolor-grey">
          清空
        </Button>
        {/* TODO: 禁用查询策略 */}
        {/* {strategy ? (
        <Button type="primary" bgColor="#169454" borderColor="#169454" onClick={doSaveQuerys}>
          存为查询策略
        </Button>
      ) : null} */}
        {/* 导出按钮 */}
        {exportButtonSlot()}
      </Col>
    ) : null;
  // 导出按钮
  const exportButtonSlot = () => {
    // 开启导出按钮并且功能上有导出按钮
    return enableExport && exportButtonConfig.length > 0
      ? exportButtonConfig.map((button) => {
          return (
            <Button
              type="primary"
              data-code={button.code}
              loading={button.loading}
              style={button.children?.length > 0 ? 'padding-right:4px;' : ''}
              {...pick(button, ['icon', 'iconColor', 'bgColor', 'fontColor', 'borderColor'])}
              {...bindButtonEvents({ type: 'grid', button })}
            >
              {button.text}
            </Button>
          );
        })
      : null;
  };
  // 查询项
  const itemSlot = (field) => {
    return field.isBr ? (
      <Col span={24} />
    ) : (
      <Col
        class="query-col-item"
        span={gridLayout ? field.colSpan * (24 / cols) : undefined}
        style={{ width: gridLayout ? undefined : field.width + 'px' }}
      >
        <QueryItem
          labelCol={{ style: `width:${(gridLayout ? labelWidth : field.labelWidth) || 80}px` }}
          label={field.label}
          field={field}
          v-model:value={model[field.name]}
          onSortClick={(options) => doSort(options)}
        ></QueryItem>
      </Col>
    );
  };

  expose($groupQuery);
  useProvideFuncTopQuery($groupQuery);
  return { groupQuerySlot };
}
/**
 * 操作方法
 * @param {*} param0
 * @returns
 */
function useGroupAction({ $groupQuery, model, resting }) {
  // 查询
  const doQuery = debounce((reset) => {
    // 重置排序条件
    if (reset) {
      $groupQuery.getFuncGrid().setQuerys({ type: QueryTypeEnum.ORDER, load: false });
    }
    $groupQuery
      .getFuncGrid()
      .setQuerys({ type: QueryTypeEnum.GROUP, querys: reset ? undefined : model });
  }, 300);
  // 排序
  const doSort = debounce((order) => {
    // 重置其他排序条件
    Object.keys(model).forEach((code) => {
      if (code !== order.code) {
        model[code].sort = '';
      }
    });
    // 添加列头排序标识
    $groupQuery.getFuncGrid().sort({
      field: order.code,
      order: order.type ? order.type.toLocaleLowerCase() : null,
    });
    // 查询
    $groupQuery.getFuncGrid().setQuerys({ type: QueryTypeEnum.ORDER, querys: order });
  }, 100);
  // 重置
  const doReset = () => {
    resting.value = true;
    $groupQuery.getFuncGrid().clearSort(); // 清空列头排序标识
    Object.values(model).forEach((value) => {
      Object.assign(value, {
        sort: '',
        value: '',
        betweenValue: '',
        type: value.defaultType,
      });
    });
    resting.value = false;
    doQuery(true);
  };

  /**
   * 存为查询策略
   */
  const doSaveQuerys = () => {
    const { group } = $groupQuery.getFuncGrid().store.queryParser;
    console.log(group);
  };

  return { doReset, doQuery, doSaveQuerys, doSort };
}
