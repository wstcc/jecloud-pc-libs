import { ref } from 'vue';
import { Search } from '@jecloud/ui';
import { useInjectFunc } from '../../../../func-manager';
import { isEmpty, pinyin } from '@jecloud/utils';
import { parseTreeSearchItem } from '../../../../utils';
/**
 * 树形节点搜索
 * @param {*} param0
 * @returns
 */
export function useTreeQuery({ $grid, type }) {
  const searchValue = ref('');
  const onSearch = function (searchVal, resolve) {
    if (isEmpty(searchVal)) {
      searchValue.value = searchVal;
      resolve([]);
    } else {
      // 同步查询
      const items = [];
      searchVal = searchVal.toLocaleLowerCase();
      $grid.value.getStore().cascade((item) => {
        const code = item.code?.toString().toLocaleLowerCase() ?? '';
        const text = item.text?.toString().toLocaleLowerCase() ?? '';
        const pinyinText = pinyin(text);
        if (
          code.includes(searchVal) ||
          text.includes(searchVal) ||
          pinyinText.includes(searchVal)
        ) {
          items.push(parseTreeSearchItem(item));
        }
      });
      resolve(items);
    }
  };
  const onSelect = function (val, option) {
    searchValue.value = val;
    if (!option) return false;
    const row = $grid.value.getRowById(option.id);
    if (!row) return false;
    if (row) {
      $grid.value.setTreeExpand4Path({ row, expand: true }).then(() => {
        $grid.value.setSelectRow(row);
        $grid.value.scrollToRow(row);
      });
    }
  };

  const treequerySlot = function () {
    return (
      <Search
        class="je-func-grid-search-keyword"
        allowClear
        enter-button="搜索"
        v-model:value={searchValue.value}
        onSearch={onSearch}
        onSelect={onSelect}
        label-field="text"
        sub-label-field="code"
        v-slots={{
          prefix() {
            return <i class="fal fa-search"></i>;
          },
        }}
      />
    );
  };
  return { treequerySlot };
}
