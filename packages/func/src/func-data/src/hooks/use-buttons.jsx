import { Button, Dropdown, Menu, Modal } from '@jecloud/ui';
import { split, pick, isNotEmpty, unescape } from '@jecloud/utils';
import { useFuncConfig } from '../../../func-config/hooks/use-func-config';
import { FuncButtonTypeEnum, useInjectFunc } from '../../../func-manager';
import { h } from 'vue';

/**
 * 列表按钮
 * @param {*} param0
 * @returns
 */
export function useButtons({ $grid, type }) {
  const funcContext = useInjectFunc();
  const { bindButtonEvents } = funcContext.action;
  const { configMenuSlot } = useFuncConfig({
    $func: funcContext,
    parent: funcContext,
    disabled: type !== 'func',
  }); // 功能配置菜单
  // 组装数据
  const tbarBtns = { barButtons: [], moreButtons: [], multiButtons: [], groupButtons: [] };
  const buttons = funcContext.getGridButtons();
  buttons.forEach((button) => {
    const pos = split(button.gridBarPosition, ',');
    // 依附按钮
    if (isNotEmpty(button.children)) {
      tbarBtns.groupButtons.push(...button.children);
    }
    // 工具条
    if (pos.includes('BAR') || pos.length === 0) {
      tbarBtns.barButtons.push(button);
    }
    // 更多按钮
    if (pos.includes('MORE')) {
      tbarBtns.moreButtons.push(button);
    }
    // 多选按钮
    if (pos.includes('MULTI')) {
      tbarBtns.multiButtons.push(button);
    }
  });
  // 更多按钮
  if (tbarBtns.moreButtons.length) {
    tbarBtns.barButtons.push({
      icon: 'fal fa-ellipsis-h-alt',
      code: '__more__',
      children: tbarBtns.moreButtons,
    });
  }
  // 菜单按钮事件
  const menuButtons = [...tbarBtns.moreButtons, ...tbarBtns.groupButtons];
  const menuBtnEvents = {};
  const menuClick = ({ key }) => {
    let events = menuBtnEvents[key];
    if (!events) {
      const button = menuButtons.find((button) => button.code === key);
      events = menuBtnEvents[key] = bindButtonEvents({ type: 'grid', button });
    }
    events.onClick?.();
  };

  // 按钮插槽
  const funcButtonSlot = (buttons) => {
    return buttons
      .filter((button) => !button.hidden) // 过滤隐藏的按钮
      .map((button) => {
        // 更多按钮
        if (button.code === '__more__') {
          return (
            <Dropdown
              trigger={['click']}
              v-slots={{
                overlay: () => (
                  <Menu onClick={menuClick}>
                    {button.children.map((item) => {
                      return (
                        <Menu.Item icon={item.icon} key={item.code} style="padding-right:17px;">
                          {item.text}
                        </Menu.Item>
                      );
                    })}
                  </Menu>
                ),
              }}
            >
              <Button icon={button.icon} data-code={button.code} class="bgcolor-grey" />
            </Dropdown>
          );
        } else {
          return (
            <Button.Group>
              <Button
                type="primary"
                data-code={button.code}
                loading={button.loading}
                style={button.children?.length > 0 ? 'padding-right:4px;' : ''}
                {...pick(button, ['icon', 'iconColor', 'bgColor', 'fontColor', 'borderColor'])}
                {...bindButtonEvents({ type: 'grid', button })}
              >
                {button.text}
              </Button>
              {/* 依附按钮 */}
              {button.children?.length > 0 ? (
                <Dropdown
                  trigger={['click']}
                  v-slots={{
                    overlay: () => (
                      <Menu onClick={menuClick}>
                        {button.children.map((item) => {
                          return (
                            <Menu.Item icon={item.icon} key={item.code} style="padding-right:17px;">
                              {item.text}
                            </Menu.Item>
                          );
                        })}
                      </Menu>
                    ),
                  }}
                >
                  <Button
                    type="primary"
                    icon="fal fa-chevron-down"
                    style="padding-left:4px;padding-right:10px;"
                    {...pick(button, ['iconColor', 'bgColor', 'fontColor', 'borderColor'])}
                  />
                </Dropdown>
              ) : null}
            </Button.Group>
          );
        }
      });
  };
  // 工具条
  const tbarButtonSlot = () => {
    return funcButtonSlot(tbarBtns.barButtons);
  };
  // 多选工具条
  const multiButtonSlot = () => {
    // 如果列表是单选模式那就展示multipleNumSolt
    let buttons = funcButtonSlot(tbarBtns.multiButtons);
    const funcData = funcContext.getFuncData();
    // 获得列表多选配置
    const multiple = funcData?.info.gridOptions.multiple || false;
    if (multiple) {
      buttons = multipleNumSolt().concat(buttons);
    }
    return buttons;
  };

  // 添加按钮
  const insertButtonSlot = () => {
    const button = tbarBtns.barButtons.find(
      (item) => item.code === FuncButtonTypeEnum.GRID_INSERT_BUTTON,
    );
    return button && !funcContext.store.readonly ? (
      <Button
        // class="grid-insert-button"
        class="bgcolor-grey"
        style="padding:4px 10px;"
        icon="fal fa-plus"
        data-code={button.code}
        {...bindButtonEvents({ type: 'grid', button })}
      ></Button>
    ) : null;
  };

  // 取消列表选中
  const clearSelected = () => {
    $grid.value.clearSelectedRecords();
  };

  // 列表选中个数提示
  const multipleNumSolt = () => {
    // 列表的选中个数
    const checkedNum = $grid.value.selection.length;
    return [
      <div class="multiple-num">
        <span class="context">
          已选择<span class="num">{checkedNum}</span>条
        </span>
        <i onClick={clearSelected} class="icon jeicon jeicon-times"></i>
      </div>,
    ];
  };

  // 使用说明按钮
  const helpButtonSlot = useHelpButton(funcContext);
  return {
    tbarButtonSlot,
    multiButtonSlot,
    configMenuSlot,
    insertButtonSlot,
    helpButtonSlot,
    tbarBtns,
  };
}
/**
 * 帮助按钮
 */
export function useHelpButton($func) {
  const funcData = $func.getFuncData();
  const { funcHelpContent } = funcData?.info;
  const handlerClick = () => {
    const html = unescape(funcHelpContent || '');
    Modal.window({
      title: '使用说明',
      headerStyle: 'height:50px;',
      bodyStyle: 'padding:0 20px 20px;',
      content: () => h('div', { innerHTML: html, style: 'height:100%;' }),
    });
  };

  // 使用说明按钮
  return () =>
    isNotEmpty(funcHelpContent) ? (
      <Button onClick={handlerClick} class="bgcolor-grey" icon="fal fa-question-circle"></Button>
    ) : null;
}
