import { Panel } from '@jecloud/ui';
import FuncGrid from '../../../func-grid';
import FuncTree from '../../../func-tree';
import FuncGroupQuery from '../querys/group-query';
import { useToolbar } from './use-toolbar';
import { FuncTypeEnum, FuncDataTypeEnum, FuncTreeTypeEnum } from '../../../func-manager/enum';
import { isEmpty } from '@jecloud/utils';
import { ref, watch } from 'vue';
export function useRenderer({
  funcCode,
  $tree,
  $grid,
  $groupQuery,
  $func,
  gridProps,
  treeProps,
  type = FuncDataTypeEnum.FUNC,
}) {
  // 工具条插槽
  const { tbarSlot } = useToolbar({ $grid, type, $func });
  /**
   * 快速查询树
   * @returns
   */
  const funcData = $func.getFuncData();
  const { treeQueryOptions } = funcData.info;
  let { title, width, lazy, helpMessage } = treeQueryOptions;
  const treeWidth = width || 230;
  const treeTitle = title || '快速查询';
  const dictionarys = funcData.getQuerys('tree', type);
  const treeFunc = funcData.info.funcType === FuncTypeEnum.TREE;
  // 树形类型，树形功能作为查询选择时，使用功能树
  const treeType = treeFunc ? FuncTreeTypeEnum.FUNC : FuncTreeTypeEnum.DD;
  // 启用树形查询
  let enableTree = true;
  // 树形功能，禁用快速查询，使用功能树
  if (treeFunc && FuncDataTypeEnum.FUNC === type) enableTree = false;
  // 非树形功能，并且没有查询项，禁用
  if (!treeFunc && isEmpty(dictionarys)) enableTree = false;

  // 懒加载
  const treeCollapsed = ref(lazy);
  const lazyRendered = ref(!lazy);
  const watchLazy = watch(
    () => treeCollapsed.value,
    (collapsed) => {
      if (!collapsed && !lazyRendered.value) {
        lazyRendered.value = true;
        // 取消监听
        watchLazy();
      }
    },
  );
  const treeSlot = () => {
    return enableTree ? (
      <Panel.Item
        region="left"
        split
        collapsible
        size={treeWidth}
        v-model:collapsed={treeCollapsed.value}
      >
        {lazyRendered.value ? (
          <FuncTree
            ref={$tree}
            title={treeTitle}
            class="je-tree-func-search"
            type={treeType}
            funcCode={funcCode}
            dictionarys={dictionarys}
            onlyItem={false}
            helpMessage={helpMessage}
            enableSelectMode
            {...treeProps}
          />
        ) : null}
      </Panel.Item>
    ) : null;
  };
  // 高级查询
  const disabled = funcData.info.disableQuerys.includes('group') || isEmpty(funcData.groupQuerys);
  const { collapsed } = funcData.info.groupQueryOptions;
  const groupQuerySlot = () => {
    return disabled ? null : (
      <Panel.Item autoSize region="top" collapsible collapsed={collapsed}>
        <FuncGroupQuery ref={$groupQuery} />
      </Panel.Item>
    );
  };
  /**
   * 列表
   * @returns
   */
  const gridSlot = () => {
    return (
      <Panel.Item>
        <FuncGrid
          ref={$grid}
          type={type}
          funcCode={funcCode}
          readonly={$func.store.readonly}
          class={{ 'use-query-tree': treeFunc || enableTree }}
          {...gridProps}
          v-slots={{ tbar: tbarSlot }}
        />
      </Panel.Item>
    );
  };

  return {
    $tree,
    $grid,
    gridSlot,
    treeSlot,
    groupQuerySlot,
  };
}
