import { ref, getCurrentInstance, computed } from 'vue';
import { Hooks, Tree, Panel } from '@jecloud/ui';
import { findDDAsyncNodes, isEmpty, pinyin } from '@jecloud/utils';
import { parseTreeSearchItem } from '../../../utils';
import {
  FuncTreeView,
  useInjectFunc,
  loadTreeLsitSearch,
  FuncTreeTypeEnum,
} from '../../../func-manager';
import { useRenderer } from './use-renderer';
export function useBase({ props, context }) {
  const { expose, emit } = context;
  const $func = useInjectFunc();
  const $plugin = ref();
  const multiple = Hooks.useModelValue({ props, context, key: 'multiple' });
  const singleMode = ref(false); // 树形功能检索模式开始
  const async = computed(() => {
    // 异步树
    return props.async || !!props.dictionarys?.find?.((item) => item.async);
  });

  const $tree = new FuncTreeView({
    $func,
    props,
    context,
    type: props.type,
    pluginRef: $plugin,
    panelItem: Panel.useInjectPanelItem(),
  });

  $tree.mixin({
    instance: getCurrentInstance(),
    props,
    multiple,
    async,
    singleMode,
    /**
     * 树形检索
     * @param {*} keyword
     * @returns
     */
    search(keyword) {
      if (isEmpty(keyword)) {
        return Promise.resolve([]);
      } else if (async.value) {
        // 异步查询
        if (props.type == FuncTreeTypeEnum.FUNC) {
          const funcInfo = $tree.$func?.getFuncData()?.info;
          const params = {
            tableCode: funcInfo?.tableCode,
            funcCode: funcInfo.funcCode,
            searchName: keyword,
          };
          return loadTreeLsitSearch({
            params,
            pd: funcInfo.productCode,
            action: funcInfo.funcAction,
          }).then((data = []) => {
            const items = data.map((item) => {
              item.async = true;
              return parseTreeSearchItem(item);
            });
            return items;
          });
        } else {
          const params = $tree.store.getLoadOptionsCache()?.params;
          return findDDAsyncNodes({
            value: keyword,
            strData: params?.strData,
          }).then((data = []) => {
            const items = data.map((item) => {
              item.async = true;
              return parseTreeSearchItem(item);
            });
            return items;
          });
        }
      } else {
        // 同步查询
        const items = [];
        keyword = keyword.toLocaleLowerCase();
        $tree.getStore().cascade((item) => {
          const code = item.code?.toString().toLocaleLowerCase() ?? '';
          const text = item.text?.toString().toLocaleLowerCase() ?? '';
          const pinyinText = pinyin(text);
          if (code.includes(keyword) || text.includes(keyword) || pinyinText.includes(keyword)) {
            items.push(parseTreeSearchItem(item));
          }
        });
        return Promise.resolve(items);
      }
    },
  });
  // 继承plugin方法
  $tree.mixin(Hooks.useExtendMethods({ plugin: $plugin, keys: Tree.MethodKeys }));
  expose($tree);

  // 树形插槽
  const { treeSlot } = useRenderer({ props, context, $tree });
  return { $tree, treeSlot };
}
