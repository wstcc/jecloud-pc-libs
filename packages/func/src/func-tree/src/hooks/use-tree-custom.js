import { useBase } from './use-base';
import { useTreeStore } from './use-tree-store';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
/**
 * 自定义接口树
 */
export function useTreeCustom({ props, context }) {
  const { url, product, params } = props;
  const { $tree, treeSlot } = useBase({ props, context });
  const store = useTreeStore({ type: FuncTreeTypeEnum.CUSTOM, url, params, product, $tree });
  $tree.setStore(store);
  return { $tree, treeSlot };
}
