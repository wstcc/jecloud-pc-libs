import { Tree, Toolbar, Switch, Panel } from '@jecloud/ui';
import { nextTick, watch } from 'vue';

export function useRenderer({ props, context, $tree }) {
  const { attrs, slots } = context;
  const { multiple, pluginRef, search, async, singleMode } = $tree;
  const panelContext = Panel.useInjectPanel();

  // 检索模式，禁用选择模式，并且单选
  watch(
    () => singleMode.value,
    () => {
      if (singleMode.value) {
        nextTick(() => {
          multiple.value = false;
        });
      }
    },
  );
  // 底部工具栏
  const bbarSlot = function () {
    const bbar = [];
    // 切换单选多选模式
    if (props.enableSelectMode) {
      bbar.push(
        <Switch
          v-model:checked={multiple.value}
          disabled={singleMode.value}
          checked-children="多选"
          un-checked-children="多选"
        />,
      );
    }
    // 树形功能检索模式
    if (props.enableSearchMode) {
      bbar.push(
        <Switch
          v-model:checked={singleMode.value}
          checked-children="检索"
          un-checked-children="检索"
        />,
      );
    }
    const bottom = [];
    // 帮助说明
    if (props.helpMessage) {
      bottom.push(
        <div class="je-func-help-message">
          <div class="content">{props.helpMessage}</div>
        </div>,
      );
    }
    if (bbar.length) {
      bottom.push(
        <Toolbar class="je-tree-bbar" justify="center">
          {bbar}
        </Toolbar>,
      );
    }
    return bottom;
  };
  // 顶部工具栏
  const tbarSlot = function () {
    return props.title ? (
      <Toolbar class="je-tree-tbar" space={4}>
        <span>{props.title}</span>
        <Toolbar.Fill />
        <Toolbar.Tool
          icon="fal fa-chevron-left"
          title="收起"
          onClick={() => {
            panelContext.getPanelItem('left')?.setExpand(false);
          }}
        />
        <Toolbar.Tool
          icon="fal fa-chevron-up"
          title="收起"
          onClick={() => {
            $tree.setAllTreeExpand(false);
          }}
        />
        <Toolbar.Tool
          icon="fal fa-sync"
          title="刷新"
          onClick={() => {
            $tree.reload();
          }}
        />
      </Toolbar>
    ) : null;
  };

  const treeSlot = function () {
    const store = $tree.getStore();
    return (
      <Tree
        class="je-tree-func"
        {...attrs}
        ref={pluginRef}
        search={(keyword, resolve) => {
          search(keyword).then(resolve);
        }}
        treeConfig={{
          ...props.treeConfig,
          lazy: async.value,
          loadMethod({ row }) {
            return $tree.loadAsyncNode(row);
          },
        }}
        multiple={multiple.value}
        allowDeselect={true}
        store={store}
        v-slots={{ ...slots, bbar: bbarSlot, tbar: tbarSlot }}
      />
    );
  };

  return { treeSlot };
}
