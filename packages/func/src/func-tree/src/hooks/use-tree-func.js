import { useBase } from './use-base';
import { useTreeStore } from './use-tree-store';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
import { encode } from '@jecloud/utils';
import { useInjectFunc } from '../../../func-manager';
/**
 * 功能树
 */
export function useTreeFunc({ props, context }) {
  const { funcCode, onlyItem, querys } = props;
  const $func = useInjectFunc();

  const { $tree, treeSlot } = useBase({ props, context });
  const store = useTreeStore({
    type: FuncTreeTypeEnum.FUNC,
    querys,
    onlyItem,
    $func,
    $tree,
    params: { async: props.async },
  });
  $tree.setStore(store);
  $tree.mixin({
    reset() {
      const params = store.queryParser.initQuerys({ $func });
      store.load({ params: { j_query: encode(params) } });
    },
  });
  return { $tree, treeSlot };
}
