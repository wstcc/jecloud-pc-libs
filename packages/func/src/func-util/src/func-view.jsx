import { ref } from 'vue';
import { Modal, Drawer } from '@jecloud/ui';
import {
  getBodyHeight,
  getBodyWidth,
  isPlainObject,
  split,
  isNumber,
  toNumber,
} from '@jecloud/utils';
import SelectWindow from '../../func-select-window';
import Func from '../../func/src/func';
import FuncQuerys from '../../func-querys';
import { loadFuncInfoApi } from '../../func-manager';
/**
 * 打开功能
 * @param {String} funcCode 功能编码
 * @param {Object} [options] 配置信息
 * @param {number} options.width 宽
 * @param {number} options.height 高
 * @param {boolean} options.isFuncForm 表单功能
 * @param {boolean} options.isDDFunc 字典功能，启用后，funcCode为字典编码
 * @param {string} options.beanId 主键，用户初始化表单数据使用
 * @param {Object} options.bean 默认值，用户添加使用
 * @param {boolean} options.readonly 只读
 * @param {Array} options.querys 查询条件
 * @param {Array} options.orders 排序条件
 * @param {Object} options.params 参数，功能事件里可以通过$func.getParams()获得
 * @param {Object} options.modalOptions 窗口配置，可以参考JE.windown()的参数
 * @returns {$modal} 窗口对象
 */
export function showFunc(funcCode, options = {}) {
  if (isPlainObject(funcCode)) {
    options = funcCode;
    funcCode = options.funcCode;
  }
  const {
    width = getBodyWidth() - 100,
    height = getBodyHeight() - 100,
    isFuncForm,
    isDDFunc,
    beanId,
    bean,
    modalOptions,
    readonly,
    querys,
    orders,
    params,
  } = options;

  const model = Modal.window({
    title: '功能加载中...',
    width,
    height,
    minWidth: isNumber(width) ? Math.min(width, 500) : '',
    minHeight: isNumber(height) ? Math.min(height, 500) : '',
    bodyStyle: { padding: 0 },
    headerStyle: { height: '50px', borderBottom: '1px solid #f0f2f5' },
    footerStyle: { borderTop: '1px solid #f0f0f0' },
    cancelButton: '关闭',
    content: () => (
      <Func
        funcCode={funcCode}
        querys={querys}
        orders={orders}
        params={params}
        isFuncForm={isFuncForm}
        isDDFunc={isDDFunc}
        beanId={beanId}
        bean={bean}
        readonly={readonly}
        isWindow
      ></Func>
    ),
    ...modalOptions,
  });
  return model;
}
/**
 * 打开功能右侧滑出
 * @param {String} funcCode 功能编码
 * @param {Object} [options] 配置信息
 * @param {number} options.width 宽
 * @param {boolean} options.isFuncForm 表单功能
 * @param {boolean} options.isDDFunc 字典功能，启用后，funcCode为字典编码
 * @param {string} options.beanId 主键，用户初始化表单数据使用
 * @param {Object} options.bean 默认值，用户添加使用
 * @param {boolean} options.readonly 只读
 * @param {Array} options.querys 查询条件
 * @param {Array} options.orders 排序条件
 * @param {Object} options.params 参数，功能事件里可以通过$func.getParams()获得
 * @returns {$modal} 窗口对象
 */
export function showDrawerForm(funcCode, options = {}) {
  if (isPlainObject(funcCode)) {
    options = funcCode;
    funcCode = options.funcCode;
  }
  const {
    width = 500,
    isFuncForm,
    isDDFunc,
    beanId,
    bean,
    modalOptions,
    readonly,
    querys,
    orders,
    params,
    title = '编辑',
  } = options;
  // 改变面板宽度
  const btnType = ref(true);
  const doUpdateWidth = (type) => {
    // body的宽度
    const bodyWidth = getBodyWidth();
    // 面板的真实宽度
    let modelWidth = model.$drawer.width;
    // 如果是百分比
    if (modelWidth?.split('%')?.length >= 2) {
      //百分比转小数
      const decimal = parseFloat(modelWidth.slice(0, -1)) / 100;
      modelWidth = bodyWidth * decimal;
    } else {
      modelWidth = toNumber(modelWidth);
    }
    // 如果是放大并且弹窗的宽度小于body*0.5的宽度那么宽度就是50%
    if (type == 'magnify' && bodyWidth * 0.5 > modelWidth) {
      model.changeWidth('50%');
      btnType.value = true;
      // 如果是全屏 || 是放大并且窗体的宽度大于等于body*0.5的宽度那么宽度就是100%
    } else if (type == 'ALL' || (type == 'magnify' && bodyWidth * 0.5 <= modelWidth)) {
      model.changeWidth('100%');
      btnType.value = false;
    } else {
      model.changeWidth(options.width);
      btnType.value = true;
    }
  };
  const model = Drawer.show({
    title,
    width,
    class: 'je-func-drawer',
    slots: {
      extra: () => {
        return (
          <div class="je-func-drawer-header">
            {btnType.value ? (
              <i
                class="je-func-drawer-header-btn fal fa-angles-left"
                onClick={() => {
                  doUpdateWidth('magnify');
                }}
                title="放大"
              ></i>
            ) : null}
            {btnType.value ? (
              <i
                class="je-func-drawer-header-btn fal fa-arrows-maximize"
                onClick={() => {
                  doUpdateWidth('ALL');
                }}
                title="全屏"
              ></i>
            ) : null}
            {!btnType.value ? (
              <i
                class="je-func-drawer-header-btn fal fa-arrows-minimize"
                onClick={() => {
                  doUpdateWidth('recover');
                }}
                title="缩小"
              ></i>
            ) : null}
          </div>
        );
      },
    },
    bodyStyle: { padding: 0 },
    content: () => (
      <Func
        funcCode={funcCode}
        querys={querys}
        orders={orders}
        params={params}
        isFuncForm={isFuncForm}
        isDDFunc={isDDFunc}
        beanId={beanId}
        bean={bean}
        readonly={readonly}
        isWindow
      ></Func>
    ),
    ...modalOptions,
  });
  return model;
}
/**
 * 打开功能表单
 * @param {String} funcCode 功能编码
 * @param {Object} [options] 配置信息
 * @param {number} options.width 宽
 * @param {number} options.height 高
 * @param {string} options.beanId 主键，用户初始化表单数据使用
 * @param {Object} options.bean 默认值，用户添加使用
 * @param {boolean} options.readonly 只读
 * @param {Object} options.params 参数，功能事件里可以通过$func.getParams()获得
 * @param {Object} options.modalOptions 窗口配置，可以参考JE.windown()的参数
 * @returns {$modal} 窗口对象
 */
export function showFuncForm(funcCode, options = {}) {
  if (isPlainObject(funcCode)) {
    options = funcCode;
    funcCode = options.funcCode;
  }
  options.isFuncForm = true;
  // 表单侧滑
  if (options?.showType == 'FORMDRAWER') {
    return showDrawerForm(funcCode, options);
  } else {
    // 表单弹窗
    return showFunc(funcCode, options);
  }
}
/**
 * 打开功能表单，异步，根据配置计算宽高
 * @param {*} funcCode
 * @param {*} options
 * @returns
 */
export function showFuncForm4Promise(funcCode, options = {}) {
  return loadFuncInfoApi({ funcCode }).then((data) => {
    const wh = split(data.FUNCINFO_WINFORMWH, ',');
    const width = options.width ? options.width : wh[0];
    const height = options.height ? options.height : wh[1];
    options.width = width;
    options.height = height;
    options.title = data.FUNCINFO_FUNCNAME;
    return showFuncForm(funcCode, options);
  });
}

/**
 * 打开关联选择器
 * @param {Object} options 配置信息
 * @returns {$modal} 窗口对象
 */
export function showFuncSelect(options) {
  return SelectWindow.showGrid(options);
}
/**
 * 打开树形选择器
 * @param {Object} options 配置信息
 * @returns {$modal} 窗口对象
 */
export function showTreeSelect(options) {
  return SelectWindow.showTree(options);
}
/**
 * 打开人员选择器
 * @param {Object} options 配置信息
 * @returns {$modal} 窗口对象
 */
export function showUserSelect(options) {
  return SelectWindow.showUser(options);
}

/**
 * 打开查询选择，自行配置类型
 * @param {Object} options 配置信息
 * @returns {$modal} 窗口对象
 */
export function showSelectWindow(options) {
  return SelectWindow.show(options);
}
/**
 * 打开SQL拼装器
 * @param {*} options
 */
export function showFuncQuerys(options = {}) {
  const { callback } = options;
  const dataRef = ref(options);
  const sqlFilter = ref();
  let modal = null; // 窗口对象

  // 确认操作
  const okButton = function (data) {
    const textareaValue = sqlFilter.value.state.textareaValue;
    if (sqlFilter.value.okHandler()) {
      callback?.(textareaValue);
      modal.close();
    }
  };

  // 初始选中值
  const onShow = function ({ $modal }) {
    modal = $modal;
  };
  const SqlFilterSlot = () => {
    return <FuncQuerys ref={sqlFilter} queryParams={dataRef.value} model={model} />;
  };
  const model = Modal.window({
    title: '查询条件拼装器',
    width: '1000px',
    maximizable: true,
    bodyStyle: { padding: '0' },
    headerStyle: { height: '60px' },
    okButton:
      dataRef.value.btnType != 'close'
        ? {
            closable: false,
            handler: okButton,
          }
        : false,
    cancelButton: dataRef.value.btnType != 'close' ? true : { text: '关闭' },
    onShow,
    slots: {
      default: SqlFilterSlot,
    },
  });
  return model;
}
