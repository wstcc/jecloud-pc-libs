import { defineComponent, reactive, onActivated, nextTick, ref } from 'vue';
import { Panel, Loading } from '@jecloud/ui';
import { useFunc } from './hooks/use-func';
import { useFuncRenderer } from './hooks/use-func-renderer';
import MenuDisabledImage from '../../assets/images/menu-disabled.png';
import Menu404Image from '../../assets/images/menu-404.png';
export default defineComponent({
  name: 'JeFunc',
  inheritAttrs: false,
  props: {
    title: [Boolean, String],
    funcCode: String,
    isFuncForm: Boolean,
    isDDFunc: Boolean, // 字典功能
    loadFirstData: Boolean,
    beanId: String,
    bean: [Object, Function],
    orders: Array,
    querys: Array,
    params: Object,
    readonly: Boolean,
    isWindow: Boolean,
    isChildFunc: Boolean,
  },
  slots: ['title'],
  setup(props, context) {
    const { $func } = useFunc({ props, context, funcCode: props.funcCode });
    const { loading } = $func;
    const { funcLayoutSlot } = useFuncRenderer({ props, context, $func });
    const errors = reactive({});
    const events = {}; // 功能事件
    // 增加激活事件
    const activateEvent = () => {
      nextTick(() => {
        events.onFuncActivate?.();
      });
    };
    // keep-alive切换触发
    onActivated(() => {
      !loading.value && activateEvent();
    });
    // 子功能展示方式是主功能并排展示的数据
    const siblingFuncConfig = ref([]);
    // 重新loadFunc方法 用于刷新siblingFuncConfig数据
    const oldLoadFunc = $func.loadFunc;
    $func.loadFunc = function (...args) {
      return oldLoadFunc.apply(this, args).then(() => {
        // 从子功能配置初始化兄弟面板数据
        const childFuncConfig = $func.reactiveObject.childFuncConfig;
        if (childFuncConfig?.length > 0) {
          siblingFuncConfig.value = childFuncConfig.filter(
            (item) => item.layout == 'parentFuncRow' && !item.hidden,
          );
        }
      });
    };
    // 加载功能
    $func
      .loadFunc()
      .then(() => {
        // 初始化绑定事件
        Object.assign(events, $func.getFuncEvents());
        // 初始化触发
        activateEvent();
      })
      .catch((error) => {
        Object.assign(errors, error);
        loading.value = false;
        console.error(error);
      });

    return () => {
      if (loading.value) {
        if (props.isFuncForm || props.isChildFunc) {
          // 表单视图
          return <Loading loading={loading.value} />;
        } else {
          return <div></div>;
        }
      } else if (errors.code) {
        // 错误信息
        switch (errors.code) {
          case '404':
            // 没有功能
            return (
              <div class="func-perm-disabled">
                <img class="func-perm-disabled-image" src={Menu404Image}></img>
                <div class="func-perm-disabled-text">功能不存在，请联系管理员！</div>
              </div>
            );
          case '403':
            // 没有权限
            return (
              <div class="func-perm-disabled">
                <img class="func-perm-disabled-image" src={MenuDisabledImage}></img>
                <div class="func-perm-disabled-text">您没有操作此功能的权限，请联系管理员！</div>
              </div>
            );
          default:
            return (
              <div class="func-perm-disabled">
                <div class="func-perm-disabled-text">出错了</div>
                <div>{errors.message || ''}</div>
              </div>
            );
        }
      } else {
        return funcLayoutSlot(siblingFuncConfig.value);
      }
    };
  },
});
