import JeFunc from './src/func';
import JeFuncSelect from './src/func-select';
import { withInstall } from '../utils';

JeFunc.installComps = {
  // 注册依赖组件
  FuncSelect: { name: JeFuncSelect.name, comp: JeFuncSelect },
};
export const Func = withInstall(JeFunc);
export default Func;
