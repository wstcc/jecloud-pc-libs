import { ref, onMounted, nextTick, onUnmounted, watch } from 'vue';
import { Hooks } from '@jecloud/ui';
import { createElement } from '@jecloud/utils';
import { FuncChildrenLayoutEnum } from '../../func-manager/enum';
/**
 * 横向子功能
 * @param {*} param0
 * @returns
 */
export function useFuncChildrenHorizontal({ props, context, $func, $child, children }) {
  // 当前激活标签
  const activeItem = Hooks.useModelValue({ props, context, key: 'activeItem' });
  const defaultActiveItem = activeItem.value ?? children.value[0]?.code;
  activeItem.value = defaultActiveItem;
  Object.assign($child, {
    activeItem,
    setActiveItem(code) {
      activeItem.value = code;
    },
  });

  // 表单外部横向展示，增加表单组件
  if (FuncChildrenLayoutEnum.FORM_OUTER_HORIZONTAL === props.layout) {
    const form = $child.setupForm();
    children.value.unshift(form);
  }

  // 表单横向返回按钮
  const slots = {};
  const backBoxRef = ref();
  const tabsRef = ref();
  const doBack = () => {
    $func.action.doFormBack();
    activeItem.value = defaultActiveItem;
  };
  if (
    !$func.isFuncForm &&
    FuncChildrenLayoutEnum.FORM_OUTER_HORIZONTAL === props.layout &&
    children.value.length > 1
  ) {
    slots.rightExtra = () => {
      return (
        <div class="je-func-children-backbutton" ref={backBoxRef}>
          <div class="je-func-children-backbutton-text" onClick={doBack}>
            <i class="fas fa-reply" /> 返回
          </div>
        </div>
      );
    };
    let backBoxEl;
    let tabsNav;
    const absoluteCls = 'is-absolute';
    // 计算位置
    const computedPosition = () => {
      const backBoxWidth = backBoxEl.getWidth();
      const tabsNavWrap = tabsNav.down('.ant-tabs-nav-wrap');
      const tabsNavWrapWidth = tabsNavWrap.getWidth();
      const tabsNavList = tabsNavWrap.down('.ant-tabs-nav-list');
      const tabsNavListWidth = tabsNavList.getWidth();
      const tabsNavWidth = tabsNav.getWidth();
      const paddingLeft = 18; // tabs左边的padding

      // 是否悬浮定位
      const isAbsolute =
        tabsNavWrapWidth < tabsNavListWidth + (backBoxEl.hasClass(absoluteCls) ? backBoxWidth : 0);
      // 通过计算进行定位
      if (isAbsolute) {
        backBoxEl.removeClass(absoluteCls);
      } else {
        backBoxEl.addClass(absoluteCls);
        backBoxEl.setStyle(
          'right',
          `${tabsNavWidth - tabsNavListWidth - backBoxWidth - paddingLeft}px`,
        );
      }
    };
    onMounted(() => {
      nextTick(() => {
        // 返回按钮
        backBoxEl = createElement(backBoxRef.value);
        // tabsEl
        tabsNav = createElement(tabsRef.value.$el).down('.ant-tabs-nav');
        // 根据尺寸变化进行按钮定位展示
        tabsNav.addResizeListener(computedPosition);
      });
    });
    onUnmounted(() => {
      tabsNav.removeResizeListener(computedPosition);
    });
    // 监听子功能配置，重新计算
    watch(
      () => children.value,
      () => {
        if (backBoxEl && tabsNav) {
          nextTick(() => {
            computedPosition();
          });
        }
      },
      { deep: true },
    );
  }

  return { tabsRef, slots, activeItem };
}
