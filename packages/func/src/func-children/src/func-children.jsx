import { defineComponent } from 'vue';
import FuncChildrenHorizontal from './func-children-horizontal';
import FuncChildrenVertical from './func-children-vertical';
import { Row, Col } from '@jecloud/ui';
import { FuncChildrenLayoutEnum } from '../../func-manager/enum';
import { pick } from '@jecloud/utils';

export default defineComponent({
  name: 'JeFuncChildren',
  inheritAttrs: false,
  props: {
    funcCode: String,
    field: Object,
  },
  setup(props, { attrs }) {
    const prefixCls = 'je-func-children';
    const config = pick(props.field.otherConfig, [
      'bgColor',
      'titleColor',
      'titleBgColor',
      'borderColor',
    ]);
    Object.assign(config, {
      bgColor: '#FFFFFF',
      titleColor: '#3F3F3F',
      titleBgColor: '#F5F5F5',
      borderColor: '#D9D9D9',
    });
    return () => (
      <Row class={prefixCls} {...attrs}>
        <Col span={24} data-anchor-parent>
          <FuncChildrenHorizontal
            funcCode={props.funcCode}
            config={config}
            layout={FuncChildrenLayoutEnum.FORM_INNER_HORIZONTAL}
            style={{ margin: '10px' }}
          />
        </Col>
        <Col span={24}>
          <FuncChildrenVertical
            funcCode={props.funcCode}
            config={config}
            layout={FuncChildrenLayoutEnum.FORM_INNER_VERTICAL}
            childAttrs={{ style: { padding: '10px' } }}
          />
        </Col>
      </Row>
    );
  },
});
