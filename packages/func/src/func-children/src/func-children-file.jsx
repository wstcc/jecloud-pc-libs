import { defineComponent, watch, ref } from 'vue';
import { PreviewFile } from '@jecloud/ui';
import { loadFuncFileInfoApi, useInjectFunc } from '../../func-manager';
/**
 * 附件子功能
 */
export default defineComponent({
  name: 'JeFuncChildrenFile',
  props: {
    model: Object,
  },
  setup() {
    const $func = useInjectFunc();
    const funcData = $func.getFuncData();
    const { funcCode } = funcData;
    const child = funcData.getChildren().find((item) => item.type === 'file');
    // 附件数据
    const fileList = ref([]);
    //子功能附件类型刷新
    const refreshFileList = () => {
      const fileParams = {
        funcRelationId: child.id, // 子功能关联ID
        dataId: $func.store.getBeanId(), // 当前一条数据ID
        funcCode, //功能code
      };
      loadFuncFileInfoApi({ params: fileParams }).then((res) => {
        fileList.value = res?.children;
      });
    };
    // 获取文件类型子功能数据
    watch(() => [$func.store.activeBean, $func.store.activeBeanEmitter], refreshFileList, {
      immediate: true,
    });
    return () => (
      <PreviewFile
        onRefreshFileList={refreshFileList}
        fileList={fileList.value}
        treeDataType
        showRefresh
        showOpenNewView
      />
    );
  },
});
