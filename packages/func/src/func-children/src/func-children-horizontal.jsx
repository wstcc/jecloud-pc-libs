import { defineComponent, watch, computed } from 'vue';
import { Tabs } from '@jecloud/ui';
import { isEmpty, isNotEmpty } from '@jecloud/utils';
import { useFuncChildren } from '../hooks/use-func-children';
import { useFuncChildrenHorizontal } from '../hooks/use-func-children-horizontal';
import { FuncChildrenLayoutEnum } from '../../func-manager/enum';
/**
 * 子功能横向展示
 */
export default defineComponent({
  name: 'JeFuncChildrenHorizontal',
  inheritAttrs: false,
  props: {
    funcCode: String,
    layout: String, // 展示位置
    activeItem: String,
    height: Number,
    hiddenTabs: Boolean,
    form: Object, // 表单功能
    config: { type: Object, default: () => {} },
  },
  emits: ['update:activeItem'],
  setup(props, context) {
    const { expose, attrs } = context;
    const prefixCls = 'je-func-children-horizontal';
    // 子功能数据
    const { children, $child, $func, computeHeight, childItemSlot } = useFuncChildren({
      props,
      context,
    });
    const { tabsRef, slots, activeItem } = useFuncChildrenHorizontal({
      props,
      context,
      $func,
      $child,
      children,
    });

    expose($child);

    // 隐藏tab标签
    const hiddenTabs = computed(() => {
      return (
        props.hiddenTabs ||
        (FuncChildrenLayoutEnum.FORM_OUTER_HORIZONTAL === props.layout &&
          children.value.length <= 1)
      );
    });

    // 隐藏panel
    const hiddenPanel = computed(() => {
      return !children.value.find((child) => !child.hidden);
    });

    // 设置渲染标记
    if (children.value.length) {
      watch(
        () => activeItem.value,
        () => {
          children.value.find((item) => item.code === activeItem.value).rendered = true;
        },
        { immediate: true },
      );
      // 子功能显隐表达式，初始激活功能
      watch(
        () => children.value,
        (childs) => {
          let tempActiveItem = childs.find(
            (child) => child.code === activeItem.value && !child.hidden,
          );
          if (!tempActiveItem) {
            tempActiveItem = childs.find((child) => !child.hidden);
            tempActiveItem && (activeItem.value = tempActiveItem.code);
          }
        },
        { deep: true },
      );
    }
    return () =>
      isEmpty(children.value) ? null : (
        <Tabs
          v-model:activeKey={activeItem.value}
          ref={tabsRef}
          class={prefixCls}
          style={{ height: computeHeight(), display: hiddenPanel.value ? 'none' : undefined }}
          tabBarStyle={{
            display: hiddenTabs.value ? 'none' : undefined,
          }}
          {...attrs}
          v-slots={slots}
        >
          {children.value.map((child) => {
            return child.hidden ? null : (
              <Tabs.TabPane
                key={child.code}
                v-slots={{
                  tab() {
                    return (
                      <div>
                        {isNotEmpty(child.icon) ? (
                          <i class={child.icon} style="margin-right:5px" />
                        ) : null}
                        {child.title}
                      </div>
                    );
                  },
                  default() {
                    return activeItem.value === child.code || child.rendered
                      ? childItemSlot(child, {
                          class: { [`${prefixCls}-single`]: children.value.length === 1 },
                          style: { display: activeItem.value === child.code ? undefined : 'none' },
                        })
                      : null;
                  },
                }}
              />
            );
          })}
        </Tabs>
      );
  },
});
