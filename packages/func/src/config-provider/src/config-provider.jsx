import { defineComponent } from 'vue';
export default defineComponent({
  name: 'JeFuncConfigProvider',
  inheritAttrs: false,
  setup(props, { slots }) {
    return () => slots.default?.();
  },
});
