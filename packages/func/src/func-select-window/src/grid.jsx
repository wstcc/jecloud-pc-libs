import { getBodyHeight, getBodyWidth } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';
import { useFuncSelect } from './hooks/use-renderer';
/**
 *
 * @param {Object} options
 * @param {String} configInfo 配置项
 * @param {String|Number} width 宽
 * @param {String|Number} height 高
 */

export function gridSelect(options) {
  const {
    width = getBodyWidth() - 100,
    height = getBodyHeight() - 100,
    title = '查询选择',
  } = options;

  const { pluginSlot, modalOptions } = useFuncSelect({ selectOptions: options });
  const model = Modal.window({
    title,
    width,
    height,
    minWidth: Math.min(width, 500),
    minHeight: Math.min(height, 500),
    content: () => pluginSlot(),
    ...modalOptions,
    className: 'je-func-select-window',
  });
  return model;
}
