import { defineComponent, ref, onMounted, reactive } from 'vue';
import { Panel } from '@jecloud/ui';
import { toQuerysTemplate, cloneDeep, isEmpty } from '@jecloud/utils';
import JeFuncSelectUserResult from './default-panel';
import JeFuncSelectUserTabs from './left-panel';
import { isNotEmpty, encode } from '@jecloud/utils';
import { parseConfigInfo } from '../../../../func-util';
import { addUserCommonUserApi, getUserPersonInfoEchoApi } from '../../../../func-manager';

export default defineComponent({
  name: 'JeFuncSelectUser',
  inheritAttrs: false,
  props: { selectOptions: Object, modal: Object, model: Object },
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const configInfo = props.selectOptions.configInfo;
    let multipleType = props.selectOptions.multiple ? 'M' : 'S';
    const selectData = ref([]);
    const unCheckDatas = ref([]);
    const config = parseConfigInfo({ type: 'user', configInfo, options: props.selectOptions });
    const async = ref(config?.async || false);
    const showDeveloper = ref(props.selectOptions.showDeveloper ? '1' : '0');
    const hideLeftPanel = ref(false);
    const showLeftTabs = ref(props.selectOptions.queryType);
    const querysConfig = ref(props.selectOptions.querysConfig);

    const initSelectData = () => {
      let { value, valueField = 'id', model = {} } = props.selectOptions;
      if (isEmpty(value)) {
        hideLeftPanel.value = true;
        return false;
      }
      // 参数
      const params = { accountDeptIds: '', userIds: '', deptIds: '' };
      // 设置参数
      const setParams = (code, value) => {
        switch (code) {
          case 'id':
            params.accountDeptIds = value;
            break;
          case 'USER_ID':
            params.userIds = value;
            break;
          case 'DEPARTMENT_ID':
            params.deptIds = value;
            break;
        }
      };
      if (config && model) {
        const { sourceFields, targetFields } = config;
        // 提取数据id
        sourceFields.forEach((code, index) => {
          const val = model[targetFields[index]];
          setParams(code, val);
        });
      } else {
        setParams(valueField, value);
      }
      // 存在账号id || 用户id，进行数据查询
      if (params.accountDeptIds || params.userIds) {
        getUserPersonInfoEchoApi({ params }).then((data) => {
          if (isNotEmpty(data)) {
            selectData.value = data;
          }
          hideLeftPanel.value = true;
        });
      } else {
        hideLeftPanel.value = true;
      }
    };
    onMounted(() => {
      initSelectData();
    });

    if (isNotEmpty(configInfo)) {
      const type = configInfo.split(',')[3];
      multipleType = type || multipleType;
    }
    if (props.selectOptions.async) {
      async.value = props.selectOptions.async;
    }
    //选中的数据
    const getSelectData = ({ checked, data, closeModal }) => {
      let checkedDatas = [];
      //如果是单选
      if (['S', 'S_'].includes(multipleType)) {
        if (checked) {
          selectData.value = data;
        } else {
          selectData.value = [];
        }
        if (closeModal) {
          select();
        }
        return false;
      }
      //如果是拖拽的数据直接复制
      if (checked == 'drop') {
        checkedDatas = data;
      } else {
        //多选处理
        if (selectData.value && selectData.value.length > 0) {
          checkedDatas = cloneDeep(selectData.value);
        }
        //选中
        if (checked) {
          const newDatas = [];
          data.forEach((item) => {
            const flag = checkedDatas.some((_item) => _item.id == item.id);
            if (!flag) {
              newDatas.push(item);
            }
          });
          checkedDatas = checkedDatas.concat(newDatas);
          //取消选中
        } else {
          const newDatas = [];
          checkedDatas.forEach((item) => {
            const flag = data.some((_item) => _item.id == item.id);
            if (!flag) {
              newDatas.push(item);
            }
          });
          checkedDatas = newDatas;
          unCheckDatas.value = data;
        }
      }
      selectData.value = checkedDatas;
    };

    //删除的用户数据
    const uncheckUser = (unCheckData, checkData) => {
      unCheckDatas.value = unCheckData;
      selectData.value = checkData;
    };

    //保存常用人员
    const addCommonUserData = () => {
      const ids = [];
      if (selectData.value.length > 0) {
        selectData.value.forEach((item) => {
          ids.push(item.id);
        });
        addUserCommonUserApi({ params: { accountDeptIds: ids.join(',') } });
      }
    };

    //点击确定按钮触发方法
    const select = () => {
      if (
        props.selectOptions.callback({
          rows: selectData.value,
          $model: props.modal.value,
          config,
        }) !== false
      ) {
        //保存常用人员
        addCommonUserData();
        //关闭弹窗
        props.modal.value.close();
      }
    };

    expose({ select });

    return () => (
      <Panel class="je-func-select-user">
        <Panel.Item region="left" size="401">
          <JeFuncSelectUserTabs
            multipleType={multipleType}
            onGetSelectData={getSelectData}
            unCheckDatas={unCheckDatas.value}
            hideLeftPanel={hideLeftPanel.value}
            checkedUser={selectData.value}
            showDeveloper={showDeveloper.value}
            showLeftTabs={showLeftTabs.value}
            querysObj={querysConfig}
            async={async.value}
          />
        </Panel.Item>
        <Panel.Item>
          <div style="width:100%;height:100%">
            <JeFuncSelectUserResult
              selectData={selectData}
              onUncheckUser={uncheckUser}
              onGetSelectData={getSelectData}
            />
          </div>
        </Panel.Item>
      </Panel>
    );
  },
});
