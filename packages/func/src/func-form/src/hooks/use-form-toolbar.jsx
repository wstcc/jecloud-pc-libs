import { useFuncToolbar } from './use-form-toolbar-func';
export function useFormToolbar({ $form }) {
  const { type } = $form;
  let tbarSlot = () => {};
  if (type === 'func') {
    tbarSlot = useFuncToolbar({ $form });
  }
  return {
    tbarSlot,
  };
}
