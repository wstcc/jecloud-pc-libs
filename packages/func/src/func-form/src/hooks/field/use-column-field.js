import { resolveComponent, h } from 'vue';
import { omit, isNotEmpty } from '@jecloud/utils';
import { Grid } from '@jecloud/ui';
import {
  parseFieldEvents,
  useModelValue,
  parseColumnFieldProps,
  FuncFieldTypeEnum,
} from '../../util';
/**
 * 列表字段
 * @param {*} param0
 */
export function useColumnField({ field, $func, $grid }) {
  let editRender, editSlot;
  const { component, props } = parseColumnFieldProps({ field, $func });

  if (component) {
    editRender = Grid.Renderer[component] ?? {};
    editSlot = (options) => {
      const { row } = options;
      const vnode = resolveComponent(component);
      if (component !== vnode) {
        let parentModel = {};
        if ($func && isNotEmpty($func.parentFunc)) {
          parentModel = $func.parentFunc.store.activeBean || {};
        }
        const model = row;
        // 绑定列表字段事件
        Object.assign(
          props,
          parseFieldEvents({ field, type: 'column', $grid, $func, model, ...options }),
        );
        Object.assign(props, useModelValue({ model, key: field.name }));
        Object.assign(props, { model, parentModel });
        // 解决评星组件编辑时样式问题
        if (component === FuncFieldTypeEnum.STAR.component) {
          Object.assign(props, { style: 'margin:0 10px' });
        }
        return h(vnode, omit(props, ['height']));
      } else {
        return null;
      }
    };
  }
  return { editRender, editSlot };
}
