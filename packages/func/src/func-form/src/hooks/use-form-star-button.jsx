import { reactive, ref } from 'vue';
import { Button, Dropdown, Menu, Form, Input, InputSelect, Modal } from '@jecloud/ui';
import { isNotEmpty, isEmpty } from '@jecloud/utils';
import {
  collectionFormDataApi,
  shareFormDataApi,
  loadWhetherShareFormApi,
} from '../../../func-manager';
/**
 * 收藏按钮
 * @param {*} param0
 * @returns
 */
export function useStarButton({ $func }) {
  const starButtonSlot = () => {
    const { pkCode } = $func.getFuncData().info;
    const pkValue = $func.store.activeBean?.[pkCode];
    // 收藏标识
    const shareFlag = ref(false);
    const dropdownClick = () => {
      if (isNotEmpty(pkValue)) {
        // 获得表单是否收藏
        loadWhetherShareFormApi({ pkValue }).then((data) => {
          shareFlag.value = data;
        });
      }
    };
    return (
      <Dropdown
        trigger={['click']}
        v-slots={{
          overlay() {
            return (
              <Menu
                onClick={({ key }) => {
                  if (key === 'collection') {
                    showCollectionForm({ $func, shareFlag });
                  } else {
                    showShareForm({ $func });
                  }
                }}
                style="padding:0;"
              >
                {shareFlag.value ? (
                  <Menu.Item style="padding-right:17px" key="collection">
                    <i
                      class="fas fa-star-sharp"
                      style="color:#FCD53F;min-width: 12px;margin-right: 8px;font-size: 12px;"
                    ></i>
                    收藏
                  </Menu.Item>
                ) : (
                  <Menu.Item style="padding-right:17px;" icon="fal fa-star" key="collection">
                    收藏
                  </Menu.Item>
                )}
                <Menu.Item style="padding-right:17px;" icon="fal fa-share" key="share">
                  共享
                </Menu.Item>
              </Menu>
            );
          },
        }}
      >
        <Button icon="fal fa-star" class="bgcolor-grey" onClick={dropdownClick} />
      </Dropdown>
    );
  };

  return { starButtonSlot };
}
/**
 * 打开收藏表单
 * @param {*} param0
 */
function showCollectionForm({ $func, shareFlag }) {
  if (shareFlag.value) {
    return Modal.alert('不可重复收藏！', 'warning');
  }
  const { tableCode, funcCode, funcName, pkCode } = $func.getFuncData().info;
  const pkValue = $func.store.activeBean?.[pkCode];
  if (isEmpty(pkValue)) {
    return Modal.alert('请先保存表单数据！', 'warning');
  }
  const model = reactive({ title: '' });
  const rulesRef = reactive({
    title: [
      {
        required: true,
        message: '该输入项为必填项',
      },
      {
        max: 40,
        message: '不能大于40个字符',
      },
      {
        pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
        message: '不能输入非法字符',
      },
    ],
  });
  const { validate, validateInfos } = Form.useForm(model, rulesRef);
  const form = (
    <Form model={model} labelCol={{ style: 'width:100px' }}>
      <Form.Item colon={false} label="收藏标题" name="title" {...validateInfos.title}>
        <Input v-model:value={model.title}></Input>
      </Form.Item>
    </Form>
  );
  Modal.dialog({
    title: '收藏',
    width: 600,
    content: () => form,
    buttonAlign: 'center',
    okButton: {
      closable: false,
      handler({ $modal }) {
        validate()
          .then(() => {
            const params = { tableCode, funcCode, funcName, pkValue, ...model };
            collectionFormDataApi(params).then(() => {
              Modal.notice('收藏成功！', 'success');
              $modal.close();
              shareFlag.value = true;
            });
          })
          .catch(() => {});
      },
    },
  });
}

/**
 * 打开共享表单
 * @param {*} param0
 */
function showShareForm({ $func }) {
  const { tableCode, funcCode, funcName, pkCode } = $func.getFuncData().info;
  const pkValue = $func.store.activeBean?.[pkCode];
  if (isEmpty(pkValue)) {
    return Modal.alert('请先保存表单数据！', 'warning');
  }
  const model = reactive({ title: '', userNames: '', accountIds: '' });
  const rulesRef = reactive({
    title: [
      {
        required: true,
        message: '该输入项为必填项',
      },
      {
        max: 40,
        message: '不能大于40个字符',
      },
      {
        pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
        message: '不能输入非法字符',
      },
    ],
    userNames: [{ required: true, message: `该输入项为必填项` }],
  });
  const { validate, validateInfos } = Form.useForm(model, rulesRef);
  const form = (
    <Form model={model} labelCol={{ style: 'width:100px' }}>
      <Form.Item colon={false} label="共享标题" name="title" {...validateInfos.title}>
        <Input v-model:value={model.title}></Input>
      </Form.Item>
      <Form.Item colon={false} label="共享人" name="userNames" {...validateInfos.userNames}>
        <InputSelect.User
          height="200"
          v-model:value={model.userNames}
          textarea
          configInfo="JE_RBAC_VACCOUNTDEPT,accountIds~userNames,id~text,M"
        ></InputSelect.User>
      </Form.Item>
    </Form>
  );
  Modal.dialog({
    title: '共享',
    width: 600,
    buttonAlign: 'center',
    content: () => form,
    okButton: {
      closable: false,
      handler({ $modal }) {
        validate()
          .then(() => {
            const params = { tableCode, funcCode, funcName, pkValue, ...model };
            shareFormDataApi(params).then(() => {
              Modal.notice('共享成功！', 'success');
              $modal.close();
            });
          })
          .catch(() => {});
      },
    },
  });
}
