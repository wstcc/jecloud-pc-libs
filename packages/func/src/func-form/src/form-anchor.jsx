import { useModelValue } from '@jecloud/ui';
import { defineComponent, nextTick, onMounted, ref } from 'vue';
import { createElement, forEach, getPaddingSize } from '@jecloud/utils';
/**
 * 表单定位
 */
export default defineComponent({
  name: 'JeFuncFormAnchor',
  inheritAttrs: false,
  props: {
    options: { type: Array, default: () => [] },
    activeKey: String,
    target: Function,
  },
  emits: ['update:activeKey'],
  setup(props, context) {
    const activeKey = useModelValue({ props, context, key: 'activeKey' });
    activeKey.value = props.options[0]?.code;
    const $el = ref();
    let clickScrolling = false;
    let target;
    let targetWrapper;
    onMounted(() => {
      nextTick(() => {
        target = createElement(props.target());
        targetWrapper = target.up('.je-form-func-wrapper');
        targetWrapper.on('mousewheel', () => {
          // 增加滚动标识
          clickScrolling = false;
        });
        targetWrapper.on('scroll', () => {
          // 定位activeKey
          var scrollTop = targetWrapper.dom.scrollTop;
          !clickScrolling &&
            forEach(props.options, (option) => {
              const item = target.down(`[data-code=${option.code}][data-anchor]`);
              if (item) {
                const offsetBox = item.getOffsetBox(targetWrapper);
                const top = offsetBox.y;
                const bottom = top + item.getHeight();
                if (scrollTop >= top && scrollTop <= bottom) {
                  activeKey.value = option.code;
                  return false;
                }
              }
            });
        });

        const offsetSizeX = 20; // 偏移量
        const paddingLRSize = getPaddingSize(targetWrapper.el).left * 2; // 父容器左右边距
        // 初始化
        const el = createElement($el.value);
        // 根据尺寸变化，隐藏导航
        targetWrapper.addResizeListener(() => {
          el.setStyle({
            left:
              (targetWrapper.getWidth() - target.getWidth()) / 2 -
              el.getWidth() -
              offsetSizeX +
              'px',
          });
          if (
            target.getWidth() + el.getWidth() * 2 + paddingLRSize + offsetSizeX <=
            targetWrapper.getWidth()
          ) {
            el.setStyle('visibility', 'visible');
          } else {
            el.setStyle('visibility', 'hidden');
          }
        });
      });
    });

    // 定位
    const scrollTo = (key) => {
      activeKey.value = key;
      clickScrolling = true;
      const item = target.down(`[data-code=${activeKey.value}][data-anchor]`);
      if (item) {
        const itemEl = createElement(item);
        if (itemEl.hasClass('je-func')) {
          const anchorParent = itemEl.up('[data-anchor-parent]');
          if (anchorParent) {
            targetWrapper.scrollBy(anchorParent);
            anchorParent.highlight();
            return;
          }
        }
        targetWrapper.scrollBy(item);
        item.highlight();
      }
    };

    return () => (
      <div class="je-form-func-anchor" ref={$el} style="text-align: right">
        {props.options
          .filter((item) => !item.hidden)
          .map((item) => {
            return (
              <div
                class={{
                  'je-form-func-anchor-item': true,
                  'is--active': activeKey.value === item.code,
                }}
                onClick={() => {
                  scrollTo(item.code);
                }}
              >
                {item.text}
              </div>
            );
          })}
      </div>
    );
  },
});
