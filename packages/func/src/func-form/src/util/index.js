import { FuncFieldTypeEnum } from '../../../func-manager/enum';
export { FuncFieldTypeEnum };
/**
 * 解析列表编辑字段
 * @param {*} param0
 */
export function parseColumnFieldProps({ field, $func }) {
  let component = FuncFieldTypeEnum.getComponent(field.xtype);
  let props = $func.action.parseFieldProps({ field, $func });
  // 禁用列表编辑的组件
  if (!FuncFieldTypeEnum.includesColumnXtype(field.xtype) || props.readonly) {
    component = null;
  } else if (
    // 单选框，复选框转成下拉框
    [FuncFieldTypeEnum.RADIO_GROUP.component, FuncFieldTypeEnum.CHECKBOX_GROUP.component].includes(
      component,
    )
  ) {
    component = FuncFieldTypeEnum.SELECT.component;
  } else if (
    // 所有的选择字段，禁用textarea，统一使用input
    [
      FuncFieldTypeEnum.INPUT_SELECT_GRID.component,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.component,
      FuncFieldTypeEnum.INPUT_SELECT_USER.component,
    ].includes(component)
  ) {
    props.textarea = false;
  } else if ([FuncFieldTypeEnum.UPLOAD_INPUT.component].includes(component)) {
    // 附件
    props.afterFileRemove = ({ $upload }) => {
      const { pkCode } = $func.getFuncData().info;
      const pkValue = $upload.props.model?.[pkCode];
      if (pkValue) {
        $func.action.doFormUpdateById(pkValue, { [field.name]: '' }).then(({ dynaBean }) => {
          $func.action.doGridUpdate4View({ records: [dynaBean] });
        });
      }
    };
  }

  // 菜单渲染父节点
  props.getPopupContainer = () => document.body;
  return { props, component };
}

/**
 * v-model
 * @param {*} param0
 * @returns
 */
export function useModelValue({ model, key }) {
  return {
    value: model[key],
    'onUpdate:value': function (val) {
      model[key] = val;
    },
  };
}

/**
 * 解析字段事件
 * @param {*} param0
 * @returns
 */
export function parseFieldEvents({ field, type, $func, model, ...options }) {
  const { bindFormFieldEvents } = $func.action;
  return bindFormFieldEvents({ field, type, model, ...options });
}
