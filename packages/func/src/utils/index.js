import { forEach, split } from '@jecloud/utils';
import { ConfigProvider } from '@jecloud/ui';
import { getCurrentInstance } from 'vue';
/**
 * 安装注册事件
 * @param comp 组件
 * @returns
 */
export function withInstall(comp, afterFn) {
  // 绑定依赖组件
  forEach(comp.installComps, (item, key) => {
    comp[key] = item.comp;
  });
  // 安装插件方法
  comp.install = function (app) {
    // 注册组件，默认使用自己的组件，如果封装ant组件，则注册ant组件
    app.component(comp.displayName || comp.name, comp);
    // 注册依赖组件
    forEach(comp.installComps, (item) => {
      app.component(item.name, item.comp);
    });

    afterFn?.(app);
  };
  return comp;
}

/**
 * 安装依赖的第三方插件，在setup中调用
 *
 * @export
 * @param {*} plugins
 */
const installeds = [];
export function installPlugins(...plugins) {
  const appContext =
    ConfigProvider.getGlobalConfig('appContext') ?? getCurrentInstance()?.appContext?.app;
  if (appContext) {
    appContext &&
      forEach(plugins, (plugin) => {
        if (
          !appContext.component(plugin.displayName || plugin.name) &&
          !installeds.includes(plugin)
        ) {
          appContext.use(plugin);
          installeds.push(plugin);
        }
      });
  }
}

/**
 * 解析树形顶部查询项的值
 * @param {*} item
 * @returns
 */
export function parseTreeSearchItem(item) {
  if (!item.id || !item.nodeInfo) return item;
  // 处理节点路径，拼接nodeInfo
  const ids = split(item.nodePath, '/').filter((id) => id);
  const path = ids.map((id) => `${id}_${item.nodeInfo}`).join('/');

  return {
    id: item.id,
    text: item.text,
    code: item.code,
    value: item.text,
    async: item.async,
    nodePath: path,
  };
}
