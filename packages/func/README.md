# JECloud 功能解析引擎

## 目录结构

```
├── src                             // 业务代码目录
|    ├─ config-provider             // 
|    ├─ data                        // 数据
|    ├─ field                       // 功能字段解析
|    ├─ form                        // 功能表单解析
|    ├─ func                        // 功能解析
|    ├─ func-children               // 子功能解析
|    ├─ func-config                 // 功能配置
|    ├─ func-data                   // 功能展示页面(左侧树、列表、...)
|    ├─ func-edit                   // 功能编辑页面(表单、子功能、...)
|    ├─ func-manager                // 功能消息
|    ├─ func-util                   // 工具类
|    ├─ fun-workflow                // 工作流
|    ├─ grid                        // 列表
|    ├─ log                         // 日志
|    ├─ select-window               // 查询选择、树形选择、人员选择器弹出方法
|    ├─ tree                        // 树
|    ├─ utils                       // 工具类
|    ├─ async-components.js         // 
|    ├─ components.js               // 
|    ├─ index.js                    // 主入口
|    └── style.less                 // 样式入口
├── gulpfile.js                     // 
├── README.MD                       // 
├── package.json                    // 
└── vite.config.js                  //  
```
