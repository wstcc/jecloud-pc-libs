import { ContentTypeEnum, RequestEnum } from '../enum';
import { forEach } from 'lodash-es';
import qs from 'qs';
/**
 * web request
 * @param {Object} config 配置信息
 * @returns {Promise}
 */
export function request4sync(config) {
  const { headers = {}, responseType = 'json' } = config;
  // 创建请求对象
  var xhr = new XMLHttpRequest();
  let url = config.url;
  let params = qs.stringify(config.params || {}, { arrayFormat: 'brackets' });
  let method = config.method?.toLocaleUpperCase() || RequestEnum.POST;

  // 处理git参数
  if (method === RequestEnum.GET) {
    if (url.includes('?')) {
      url += params;
    } else {
      url = url + '?' + params;
    }
  }

  xhr.open(method, url, false);

  // 请求参数处理，对外只需配置params，内部自行处理
  if (method === RequestEnum.POST) {
    xhr.setRequestHeader(
      'Content-Type',
      headers['content-type'] || ContentTypeEnum.FORM_URLENCODED,
    );
  }
  // 设置请求头
  forEach(headers, (item, key) => {
    xhr.setRequestHeader(key, headers[key]);
  });

  // 设置超时时间
  var timeout = config.timeout;
  var timedOut = false;

  // 设置一个定时器，在超时时间到达时中断请求
  var timer = setTimeout(function () {
    timedOut = true;
    xhr.abort();
    console.log('请求超时');
  }, timeout);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      // 在请求完成后，清除定时器
      clearTimeout(timer);
      if (timedOut) {
        return; // 如果已经超时，不再处理响应
      }
    }
  };

  // 发送请求
  xhr.send(params);
  // 响应数据
  if (responseType === 'json' || responseType === 'text') {
    try {
      xhr.data = JSON.parse(xhr.responseText);
    } catch (error) {
      xhr.data = xhr.responseText;
    }
  } else {
    xhr.data = xhr.response;
  }
  return xhr;
}
