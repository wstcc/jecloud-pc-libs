import { isEmpty } from '../../lodash';
/**
 * app request
 * @param {Object} config 配置信息
 * @returns {Promise}
 */
export function request4app(config) {
  // 请求参数处理，对外只需配置params，内部自行处理
  config.data = config.params;
  // 请求头处理，对外只需配置headers，内部自行处理
  config.header = config.headers;

  delete config.params;
  delete config.headers;

  // 过滤undefined值
  Object.keys(config.data || {}).forEach((key) => {
    if (isEmpty(config.data[key], true)) {
      delete config.data[key];
    }
  });

  return new Promise((resolve) => {
    uni.request({
      ...config,
      complete(response) {
        response.status = response.statusCode; // 兼容axios
        resolve(response);
      },
    });
  });
}
