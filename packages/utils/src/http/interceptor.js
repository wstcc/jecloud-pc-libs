import { RequestEnum, AjaxEventEnum } from './enum';
import { isPromise } from '../lodash';
import { checkStatus } from './util';

/**
 * 请求拦截器处理
 * @param {Object} config 请求配置
 */
export function requestInterceptors(config, ajaxInstance, sync) {
  // 解析请求配置
  config = transformRequestConfig(config, ajaxInstance);
  // 自定义拦截器
  const result = ajaxInstance.fire(AjaxEventEnum.BEFORE_REQUEST, { config, sync });
  // 同步拦截器
  if (sync) {
    return result;
  } else {
    if (isPromise(result)) {
      return result;
    } else if (result === false) {
      return Promise.reject(config);
    } else {
      return Promise.resolve(config);
    }
  }
}

/**
 * 响应拦截器处理
 * @param {*} response 响应对象
 * @param {*} config 请求配置
 * @returns 处理后的响应数据
 */
export function responseInterceptors(response, config, ajaxInstance) {
  // 禁用拦截器
  if (config.responseInterceptors === false) return response;
  const tempData = ajaxInstance.fire(AjaxEventEnum.RESPONSE, { response, config });
  return tempData || response;
}

/**
 * 响应错误处理
 * @param {*} error 响应错误
 * @param {*} config 请求配置
 * @returns 错误信息
 */
export function responseInterceptorsCatch(error, config, ajaxInstance) {
  // 禁用拦截器
  if (config.responseInterceptorsCatch === false) return error;
  error.message =
    error.message ||
    checkStatus(error.status, error.data)?.message ||
    error.errMsg ||
    error.statusText;
  const tempData = ajaxInstance.fire(AjaxEventEnum.RESPONSE_CATCH, { error, config });
  return tempData || error;
}
/**
 * 解析请求参数
 * @param {*} config
 */
export function transformRequestConfig(config, ajaxInstance) {
  // 如果为字符串，默认为url
  if (typeof config === 'string') {
    config = { url: config };
  }
  // 默认配置
  const defaultConfig = ajaxInstance.getDefaultConfig();
  // baseURL 处理
  config.url =
    (config.baseURL !== false ? config.baseURL || ajaxInstance.baseURL : '') + config.url;
  // headers 处理
  config.headers = Object.assign({ ...defaultConfig.headers }, config.headers || {});
  // 请求参数解析
  config.params = config.params || config.data || {};
  // 请求时长处理
  config.timeout = config.timeout > 0 ? config.timeout : defaultConfig.timeout;
  // 请求函数
  config.method = config.method || defaultConfig.method;
  // 非POST请求，删除Content-Type
  if (RequestEnum.POST !== config.method.toLocaleUpperCase()) {
    delete config.headers['Content-Type'];
  }
  return config;
}
