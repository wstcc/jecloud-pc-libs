import { ajax, transformAjaxData, getAxiosBaseURL } from '../http';
import { isEmpty } from '../lodash';
import { uploadFile } from './file';
const API_EXCEL_IMPORT = '/je/office/jeExcel/impExcel';
const API_EXCEL_IMPORT_ASYNC = '/je/office/jeExcel/impExcelAsync';
const API_EXCEL_EXPORT = '/je/office/jeExcel/expExcel';
const API_EXCEL_EXPORT_ASYNC = '/je/office/jeExcel/expExcelAsync';
const API_EXCEL_CACHE_PARAM = '/je/office/jeExcel/doCacheParam';
import { createDeferred, isNotEmpty, decode } from '../lodash';
import { Modal } from '../modal';
import { vueCall, useJE } from '../je';

/**
 * Excel导入
 * @param {string} templateCode 模版编号
 * @param {Object} [params] 参数
 * @param {Boolean} [params.async] 异步导出
 */
export function excelImport(templateCode, params) {
  if (isEmpty(templateCode)) {
    return Promise.reject({ message: '模板编码不能为空！' });
  }

  // 异步导入
  if (params?.async) {
    return excelImportAsync(templateCode, params);
  }

  // 上传文件
  return uploadFile({ includeSuffix: ['xls', 'xlsx'] }).then(([file]) => {
    const { fileKey } = file;
    // 执行导入模板
    return ajax({
      url: API_EXCEL_IMPORT,
      params: {
        templateCode,
        fileKey,
        ...params,
      },
    });
  });
}
/**
 * Excel导入异步
 * @param {*} templateCode
 * @param {*} params
 * @returns
 */
function excelImportAsync(templateCode, params) {
  const fileUUid = vueCall('ref');
  const deferred = new createDeferred();
  uploadFile({ includeSuffix: ['xls', 'xlsx'] })
    .then(([file]) => {
      const loadingModal = excelExportLoadingModal(fileUUid, deferred, 'import');
      const { fileKey } = file;
      // 执行导入模板
      ajax({
        url: API_EXCEL_IMPORT_ASYNC,
        params: {
          templateCode,
          fileKey,
          ...params,
        },
      })
        .then(transformAjaxData)
        .then((data) => {
          fileUUid.value = data;
        })
        .catch((e) => {
          Modal.alert(e.message, 'error');
          deferred.reject();
          loadingModal?.close();
        });
    })
    .catch((e) => {
      Modal.alert(e.message, 'error');
      deferred.reject();
    });
  return deferred.promise;
}

/**
 * Excel导出
 * @param {string} templateCode 模版编号
 * @param {Object} params 参数
 * @param {Object} params.funcCode 功能编码
 * @param {Object} params.pd 产品编码
 * @param {Object} [params.j_query] 查询条件
 * @param {Object} [params.title] 导出Excel文件标题
 * @param {Object} [params.fileName] 导出Excel文件名称
 * @param {Boolean} [params.async] 异步导出
 */
export function excelExport(templateCode, params) {
  if (isEmpty(templateCode)) {
    return Promise.reject({ message: '模板编码不能为空！' });
  }
  if (isEmpty(params?.funcCode)) {
    return Promise.reject({ message: '功能编码不能为空！' });
  }
  if (isEmpty(params?.pd)) {
    return Promise.reject({ message: '产品编码不能为空！' });
  }

  // 异步导出
  if (params?.async) {
    return excelExportPromise(templateCode, params);
  }

  // 执行导入模板
  return ajax({
    url: API_EXCEL_CACHE_PARAM,
    params: {
      templateCode,
      ...params,
    },
  })
    .then(transformAjaxData)
    .then((key) => {
      // 打开导出的文件
      window.open(
        `${getAxiosBaseURL()}${API_EXCEL_EXPORT}?type=cache&data=${key}&pd=${params?.pd}`,
      );
    });
}

/**
 * Excel导出 异步
 * @param {string} templateCode 模版编号
 * @param {Object} params 参数
 * @param {Object} params.funcCode 功能编码
 * @param {Object} params.pd 产品编码
 * @param {Object} [params.j_query] 查询条件
 * @param {Object} [params.title] 导出Excel文件标题
 * @param {Object} [params.fileName] 导出Excel文件名称
 */
function excelExportPromise(templateCode, params) {
  const fileUUid = vueCall('ref');

  const deferred = new createDeferred();
  const loadingModal = excelExportLoadingModal(fileUUid, deferred);
  // 后台缓存数据接口
  ajax({
    url: API_EXCEL_CACHE_PARAM,
    params: {
      templateCode,
      ...params,
    },
  })
    .then(transformAjaxData)
    .then((key) => {
      // 返回文件的uuid
      ajax({
        url: API_EXCEL_EXPORT_ASYNC,
        params: {
          type: 'cache',
          data: key,
          pd: params.pd,
        },
        method: 'GET',
      })
        .then(transformAjaxData)
        .then((data) => {
          // 获得文件的uuid
          fileUUid.value = data;
        })
        .catch((e) => {
          loadingModal.close();
          deferred.reject();
          Modal.alert(e.meaasge, 'error');
        });
    })
    .catch((e) => {
      loadingModal.close();
      deferred.reject();
      Modal.alert(e.meaasge, 'error');
    });
  return deferred.promise;
}
/**
 * 附件加载loading窗口
 * @param {*} fileUUid
 * @returns
 */
function excelExportLoadingModal(fileUUid, deferred, type) {
  const webNum = vueCall('ref');
  const admin = useJE()?.useAdmin();
  let watchFn = null;
  let loadingModal = null;
  // 接收webSoclet消息
  if (admin) {
    watchFn = admin?.watchWebSocket(({ busType, content }) => {
      console.log(fileUUid.value, content);
      if (busType == 'excelExp' && isNotEmpty(content)) {
        const fileData = decode(content);
        // 失败
        if (!fileData.success) {
          // 关闭弹窗
          deferred.reject();
          loadingModal?.close();
          return Modal.alert('导出异常，请联系管理员！');
        } else {
          webNum.value = 99;
          // 是同一个文件 并且不是导入
          if (fileUUid.value == fileData?.uuid && type != 'import') {
            // 下载文件
            window.open(`${getAxiosBaseURL()}/je/document/down/${fileData.fileKey}`);
          }
          deferred.resolve();
          loadingModal?.close();
        }
      }
    });
  }

  // 加载附件定时器
  webNum.value = 0;
  let timer = null;
  const doUpdateNum = () => {
    if (webNum.value < 99) {
      webNum.value++;
    } else {
      // 读数大于99 关闭定时器
      clearInterval(timer);
    }
  };
  timer = setInterval(doUpdateNum, 1000);

  loadingModal = Modal.window({
    escClosable: false,
    showHeader: false,
    resize: false,
    visible: false,
    title: '窗口',
    width: 150,
    height: 200,
    content() {
      return vueCall(
        'h',
        'div',
        {
          class: 'je-export-form-loading',
          style: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          },
        },
        vueCall('h', 'div', {}, [
          vueCall(
            'h',
            'div',
            {
              class: 'je-select-loading',
              style: {
                position: 'relative',
              },
            },
            [
              vueCall('h', 'div', {
                textContent: webNum.value + '%',
                style: {
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  transform: 'translate(-50%, -50%)',
                  fontSize: '14px',
                },
              }),
              vueCall(
                'h',
                'div',
                {
                  class: 'sk-circle',
                  style: {
                    width: '80px',
                    height: '80px',
                  },
                },
                [
                  vueCall('h', 'div', {
                    class: 'sk-circle1 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle2 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle3 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle4 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle5 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle6 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle7 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle8 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle9 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle10 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle11 sk-child',
                  }),
                  vueCall('h', 'div', {
                    class: 'sk-circle12 sk-child',
                  }),
                ],
              ),
            ],
          ),
          vueCall('h', 'div', {
            textContent: '附件加载中...',
            style: {
              marginTop: '10px',
              fontSize: '14px',
              color: '#a9aba9',
            },
          }),
        ]),
      );
    },
    onClose() {
      // 停止监听，防止重复执行
      watchFn && watchFn();
      // 关闭定时器
      clearInterval(timer);
    },
  });
  return loadingModal;
}
