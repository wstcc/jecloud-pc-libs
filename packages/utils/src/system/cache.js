import { isMap, cloneDeep } from 'lodash-es';
/**
 * 系统配置，不需要token
 */
let systemConfig = {};
/**
 * 账号信息
 */
let currentAccount = {};
/**
 * 字典缓存
 */
export let ddCache = new Map();
/**
 * 字典项缓存
 */
export let ddItemCache = new Map();
/**
 * sql模板数据
 */
export let sqlTemplateMap = new Map();
/**
 * js脚本库
 */
export let jsCodeMap = new Map();

/**
 * 获取当前账号
 * @returns {Object}
 */
export function getCurrentAccount() {
  return currentAccount;
}

/**
 * 设置当前账号
 * @param {*} account
 */
export function setCurrentAccount(account) {
  currentAccount = account;
}

/**
 * 获取当前用户
 * @returns {Object}
 */
export function getCurrentUser() {
  return currentAccount?.realUser;
}

/**
 * 获取系统变量
 * @param {string} code 变量编码
 * @param {Object} [defaultValue] 默认值
 * @returns {Object}
 */
export function getSystemConfig(code, defaultValue) {
  return code
    ? systemConfig?.[code] || systemConfig?.variables?.[code] || defaultValue
    : systemConfig;
}

/**
 * 设置系统变量
 * @param {*} config
 */
export function setSystemConfig(config) {
  systemConfig = config;
}
/**
 * 设置系统信息
 */
export function setSystemInfo(options) {
  const systemInfo = getSystemInfo();
  options &&
    Object.keys(options).forEach((key) => {
      const data = options[key];
      if (isMap(data) && isMap(systemInfo[key])) {
        systemInfo[key].clear();
        data.forEach((item, mapKey) => {
          systemInfo[key].set(mapKey, item);
        });
      } else {
        Object.assign(systemInfo[key], data);
      }
    });
}

/**
 * 获得系统缓存数据
 * @returns
 */
export function getSystemInfo() {
  return {
    currentAccount,
    ddCache,
    ddItemCache,
    sqlTemplateMap,
    jsCodeMap,
  };
}

/**
 * 获得字典项缓存数据
 * @param {string} ddCode 字典编码
 * @returns {Array}
 */
export function getDDItemCache(ddCode) {
  return cloneDeep(ddItemCache.get(ddCode));
}

/**
 * 获得字典项数据集合
 * @param {string} ddCode 字典编码
 * @returns {Array} ddItemList=[{id:'',code:'',text:'',textColor:'',backgroundColor:''}]
 */
export function getDDItemList(ddCode) {
  return getDDItemCache(ddCode);
}
/**
 * 获取字典项单个数据对象
 * @param {string} ddCode 字典编码
 * @param {string} ddItemCode 字典项编码
 * @returns {Object} ddItemInfo={id:'',code:'',text:'',textColor:'',backgroundColor:''}
 */
export function getDDItemInfo(ddCode, ddItemCode) {
  const items = ddItemCache.get(ddCode);
  return items?.find((item) => item.code === ddItemCode);
}

/**
 * 获得字典缓存数据
 * @param {string} ddCode 字典编码
 * @returns {Object}
 */
export function getDDCache(ddCode) {
  return cloneDeep(ddCache.get(ddCode));
}
/**
 * 获得字典信息
 * @param {string} ddCode 字典编码
 * @returns {Object} ddInfo={id:'',code:'',name:'',type:''}
 */
export function getDDInfo(ddCode) {
  return getDDCache(ddCode);
}

/**
 * 字典项编码转名称
 * @param {string} ddCode 字典编码
 * @param {string} ddItemCode 字典项编码
 * @returns {string}
 */
export function toDDItemText(ddCode, ddItemCode) {
  const items = ddItemCache.get(ddCode);
  if (items && ddItemCode) {
    const texts = ddItemCode
      .split(',')
      .map((code) => items.find((item) => item.code === code)?.text);
    return texts.join(',');
  }
  return ddItemCode;
}
/**
 * 清空字典缓存
 * @export
 * @param {Array} ddcodes 字段编码
 */
export function clearDDItemCache(ddcodes) {
  ddcodes.forEach((ddcode) => {
    ddItemCache.delete(ddcode);
  });
}
