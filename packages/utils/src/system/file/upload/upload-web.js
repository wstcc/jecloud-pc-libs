import { uniqueId } from 'lodash-es';
import Flow from './flow/flow';

/**
 * 上传附件
 * @param {*} options
 * @param {Function} options.onFlowInit  Flowjs初始完毕
 * @param {Object} options.inputFileAttrs input=file组件自定义属性，例如，您可以将accept属性设置为image/*. 这意味着用户将只能选择图像
 * @param {Object} options.flowOptions Flowjs配置项
 */
export function uploadFile4Web({ options, task }) {
  const { flowOptions = {}, isDirectory, inputFileAttrs, onFlowInit } = options;

  // 创建选择按钮
  const id = uniqueId('upload-file-');
  const button = window.document.createElement('button');
  button.setAttribute('id', id);
  button.setAttribute('style', {
    visposition: 'fixed',
    left: '-1000px',
    top: '-1000px',
    visibility: 'hidden',
  });

  // 设置头信息
  flowOptions.headers = Object.assign(flowOptions.headers || {}, task.headers);

  // 创建上传对象
  const flow = new Flow({
    target: task.url,
    testChunks: false,
    fileParameterName: task.fileParameterName,
    chunkSize: 500 * 1024 * 1024,
    query: task.params,
    ...flowOptions,
  });
  flow.assignBrowse(button, isDirectory, !task.multiple, inputFileAttrs);

  // 注册事件
  onFlowInit?.(flow);

  // 全部文件添加完毕，开始上传
  flow.on('filesSubmitted', (files) => {
    // 文件校验
    task
      .validate(files, ({ file }) => {
        flow.removeFile(file);
      })
      .then(() => {
        // 有效文件，开始上传
        flow.upload();
      })
      .catch((options) => task.failure({ code: 'validate', ...options }));
  });
  // 文件添加
  flow.on('fileAdded', task.fileAdded);
  // 文件上传成功
  flow.on('fileSuccess', task.fileSuccess);
  // 文件上传失败
  flow.on('fileError', task.fileError);
  // 文件上传进度条
  flow.on('fileProgress', (file) => {
    task.fileProgress(file, Math.round(file.progress() * 100));
  });
  // 所有文件上传完成
  flow.on('complete', task.complete);
  // 文件上传失败
  flow.on('error', task.fileError);

  // 打开文件选择器
  button.click();
}
