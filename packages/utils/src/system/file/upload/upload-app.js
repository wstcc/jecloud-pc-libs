// import { compressorFile } from '../index';
import { createDeferred } from '../../../lodash';
/**
 * 上传附件
 */
export function uploadFile4App({ task, options }) {
  const count = task.multiple ? task.maxCount || 9 : 1;
  return chooseFile({ ...options, count })
    .then((files) => task.validate(files)) // 校验文件
    .then((files) => {
      // 有效文件，开始上传
      const ps = [];
      files.forEach((ufile) => {
        // 文件添加完成
        task.fileAdded(ufile);
        // 开始压缩，上传
        ps.push(uploadFile({ file: ufile, task }));
      });
      // 上传完成
      return Promise.all(ps).then(() => {
        task.complete();
      });
    })
    .catch((options) => task.failure({ code: 'validate', ...options }));
}
/**
 * 选择文件
 * @param {*} param0
 * @returns
 */
function chooseFile({ type, count, afterChooseFile }) {
  let chooseFileFn;
  switch (type) {
    case 'image':
      chooseFileFn = uni.chooseImage;
      break;
    case 'video':
      chooseFileFn = uni.chooseVideo;
      break;

    default:
      chooseFileFn = uni.chooseFile;
      // eslint-disable-next-line no-undef
      if (typeof wx !== 'undefined' && typeof wx.chooseMessageFile === 'function') {
        // eslint-disable-next-line no-undef
        chooseFileFn = wx.chooseMessageFile;
      }
      break;
  }

  if (typeof chooseFileFn !== 'function') {
    return Promise.reject({
      messge: '请指定 type 类型，该平台仅支持选择 image 或 video。',
    });
  }
  return new Promise((resolve, reject) => {
    chooseFileFn({
      count,
      success(res) {
        if (afterChooseFile) {
          afterChooseFile(res).then(resolve);
        } else {
          resolve(res.tempFiles);
        }
      },
      fail(res) {
        reject(res);
      },
    });
  });
}
/**
 * 格式化文档
 * @param {*} file
 * @returns
 */
// function transformFile(file) {
//   return compressorFile(file).then((cFile) => {
//     // 由于压缩文件类库修改了文件类型和后缀名，并且结合后台上传后会根据文件类型来命名文件名，所以统一修改文件里为.jpeg类型
//     if (cFile.name !== file.name) {
//       cFile.name = cFile.name.replace('.jpg', '.jpeg');
//     }
//     return cFile;
//   });
// }

function uploadFile({ file, task }) {
  // 参数加密
  const options = {
    url: task.url,
    name: task.fileParameterName,
    formData: task.params,
    header: {
      authorization: uni.getStorageSync('authorization') || '',
      ...task.headers,
    },
    file,
  };
  const deferred = createDeferred();
  const uploadTask = uni.uploadFile({
    ...options,
    success(res) {
      task.fileSuccess(file, res.data).then(deferred.resolve);
    },
    fail(res) {
      task.fileError(file, res);
    },
  });
  uploadTask.onProgressUpdate((res) => {
    // console.log('上传进度' + res.progress);
    // console.log('已经上传的数据长度' + res.totalBytesSent);
    // console.log('预期需要上传的数据总长度' + res.totalBytesExpectedToSend);
    task.fileProgress(file, res.progress);
  });
  return deferred.promise;
}
