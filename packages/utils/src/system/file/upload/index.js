import UploadTask from './upload-task';
import { uploadFile4Web } from './upload-web';
import { uploadFile4App } from './upload-app';
import { isApp } from '../../../je';

/**
 * 上传附件
 * @param {Object} options
 * @param {string} [options.url] 上传服务器地址，默认使用系统上传地址
 * @param {Object} [options.params]  上传参数
 * @param {boolean} [options.multiple]  多选
 * @param {Array} [options.includeSuffix]  允许上传的文件后缀
 * @param {Array} [options.includeMessage]  允许上传的文件后缀校验失败的提示信息
 * @param {Array} [options.excludeSuffix]  禁用上传的文件后缀
 * @param {Array} [options.excludeMessage]  禁用上传的文件后缀校验失败的提示信息
 * @param {number} [options.maxCount]  允许上传的文件最大数量
 * @param {string} [options.maxCountMessage]  允许上传的文件最大数量校验失败的提示信息
 * @param {number} [options.maxSize]  允许上传的文件最大值，M
 * @param {number} [options.maxSizeMessage]  允许上传的文件最大值校验失败的提示信息
 * @param {Function} [options.onFileProgress]  文件上传进度
 * @param {Function} [options.onFileSuccess] 文件上传成功
 * @param {Function} [options.onUploadStart] 文件开始上传
 * @param {Function} [options.onValidate] 文件上传前校验
 * @returns {Promise}
 */
export function uploadFile(options = {}) {
  // 创建上传任务
  const task = new UploadTask(options);

  // 上传
  if (isApp) {
    uploadFile4App({
      options,
      task,
    });
  } else {
    uploadFile4Web({
      options,
      task,
    });
  }

  return task.start();
}
