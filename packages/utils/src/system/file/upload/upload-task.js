import { decode, toNumber, isPromise, createDeferred } from '../../../lodash';
import { getAxiosBaseURL, transformAjaxData } from '../../../http';
import { getFileSystemIncludeSuffix, getFileSystemMaxSize } from '../util';
import { API_DOCUMENT_FILE } from '../../api/urls';
import { toParamsEncrypt } from '../../../des';
/**
 * 上传任务
 */
export default class UploadTask {
  /**
   * 延时任务
   *
   * @memberof UploadTask
   */
  deferred = createDeferred();

  /**
   * 上传后的文件数据
   *
   * @memberof UploadTask
   */
  data = [];

  /**
   * 文件参数名
   *
   * @memberof UploadTask
   */
  fileParameterName = 'files';

  constructor(options) {
    /**
     * 上传链接
     */
    this.url = options.url || getAxiosBaseURL() + API_DOCUMENT_FILE;
    /**
     * 参数
     */
    this.params = options.params || {};
    /**
     * 请求头
     */
    this.headers = options.headers || {};
    /**
     * 多选
     */
    this.multiple = options.multiple || false;

    // 参数加密
    const encryptInfo = toParamsEncrypt(this.params);
    if (encryptInfo) {
      Object.assign(this.headers, encryptInfo);
    }
    /**
     * 最大数量
     */
    this.maxCount = toNumber(options.maxCount);
    this.tipMaxCount = options.tipMaxCount; // 用于多附件，分批上传提示使用
    this.maxCountMessage = options.maxCountMessage;

    /**
     * 最大限制，单位: M
     * 取最小值
     */
    this.maxSize = getFileSystemMaxSize(options.maxSize);
    this.maxSize4Byte = this.maxSize > 0 ? this.maxSize * 1024 * 1024 : 0;
    this.maxSizeMessage = options.maxSizeMessage;
    /**
     * 允许上传文件的后缀名
     */
    this.includeSuffix = getFileSystemIncludeSuffix(options.includeSuffix);
    this.includeMessage = options.includeMessage;
    /**
     * 禁用上传文件的后缀名
     */
    this.excludeSuffix = options.excludeSuffix || [];
    this.excludeMessage = options.excludeMessage;
    /**
     * 上传前文件校验
     */
    this.onValidate = options.onValidate;
    /**
     * 单文件上传成功
     */
    this.onFileSuccess = options.onFileSuccess;
    /**
     * 文件上传进度条
     */
    this.onFileProgress = options.onFileProgress;
    /**
     * 开始上传
     */
    this.onUploadStart = options.onUploadStart;

    /**
     * 文件添加
     */
    this.onFileAdded = options.onFileAdded;
  }

  /**
   * 文件校验
   * @param {Array} files 文件数组
   * @param {Function} failureFn 单文件验证失败
   * @returns errors
   */
  validate(files = [], failureFn) {
    const errors = [];
    const uploadFiles = [];
    // 超出最大上传数量
    if (this.maxCount > 0 && files.length > this.maxCount) {
      errors.push({
        type: 'maxCount',
        message: this.maxCountMessage || `上传数量超过最大限制数【${this.tipMaxCount}】`,
      });
    }
    files.forEach((file) => {
      file.relName = file.name; // 兼容，跟后台数据保持一致
      const itemErrors = [];
      const suffix = file.name.split('.').pop();
      if (this.maxSize4Byte > 0 && file.size > this.maxSize4Byte) {
        itemErrors.push({
          file,
          type: 'maxSize',
          message: this.maxSizeMessage || `上传大小禁止超过【${this.maxSize}M】`,
        });
      }

      if (this.includeSuffix.length && !this.includeSuffix.includes(suffix)) {
        itemErrors.push({
          file,
          type: 'include',
          message: this.includeMessage || `上传允许的格式为【${this.includeSuffix.join('，')}】`,
        });
      }

      if (this.excludeSuffix.includes(suffix)) {
        itemErrors.push({
          file,
          type: 'exclude',
          message: this.excludeMessage || `上传禁止的格式为【${this.excludeSuffix.join('，')}】`,
        });
      }

      if (itemErrors.length) {
        failureFn?.({ file, errors: itemErrors });
        errors.push(...itemErrors);
      } else {
        uploadFiles.push(file);
      }
    });

    // 验证通过，开始上传
    return this.customValidate({ files, errors }).then(() => {
      this.onUploadStart?.({ files: uploadFiles });
      return uploadFiles;
    });
  }

  /**
   * 自定义校验
   * @param {*} param0
   */
  customValidate = (options) => {
    if (options.errors.length > 0) {
      return Promise.reject(options);
    } else if (this.onValidate) {
      const result = this.onValidate(options);
      if (isPromise(result)) {
        return result;
      } else {
        return result !== false ? Promise.resolve(options) : Promise.reject(options);
      }
    } else {
      return Promise.resolve(options);
    }
  };

  /**
   * 文件添加
   * @param {*} file
   */
  fileAdded = (file) => {
    this.onFileAdded?.({ file });
  };
  /**
   * 单文件上传成功
   * @param {*} file
   * @param {*} message
   * @returns
   */
  fileSuccess = (file, message) => {
    return transformAjaxData(decode(message))
      .then((data) => {
        this.data.push(...data);
        this.onFileSuccess?.({ file, fileData: data });
        return data;
      })
      .catch((data) => {
        this.failure({ file, data });
      });
  };

  /**
   * 上传失败
   * @param {*} file
   * @param {*} message
   */
  fileError = (file, message) => {
    this.failure({ file, message });
  };

  /**
   * 文件进度条
   * @param {*} file
   */
  fileProgress = (file, progress) => {
    this.onFileProgress?.({ file, progress });
  };
  /**
   * 上传完成
   * @param  {...any} args
   */
  complete = (...args) => {
    this.deferred.resolve(this.data, ...args);
  };
  /**
   * 上传失败
   * @param  {...any} args
   */
  failure = (...args) => {
    this.deferred.reject(...args);
  };

  /**
   * 开始任务
   * @returns Promise
   */
  start() {
    return this.deferred.promise;
  }
}
