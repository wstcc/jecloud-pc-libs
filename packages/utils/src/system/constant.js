/**
 * 浏览数据传输加密
 */
export const JE_SYS_ENCRYPT = 'JE_SYS_ENCRYPT';
/**
 * 传输加密参数
 */
export const JE_SYS_ENCRYPT_FIELD = 'JE_SYS_ENCRYPT_FIELD';
/**
 * 加密秘钥
 */
export const JE_SYS_ENCRYPT_KEY = 'JE_SYS_ENCRYPT_KEY1';
/**
 * ajax全局请求时长
 */
export const JE_CORE_AJAXTIMEOUT = 'JE_CORE_AJAXTIMEOUT';
/**
 * 允许上传附件大小
 */
export const JE_SYS_UPLOADFILESIZE = 'JE_SYS_UPLOADFILESIZE';

/**
 * 传输加密参数
 */
export const JE_SYS_ENCRYPT_FIELD_DEFAULT = 'tableCode,funcCode,account,password,j_query,strData';
/**
 * 加密秘钥
 */
export const JE_SYS_ENCRYPT_KEY_DEFAULT = 'JECLOUDDESCRYPT';
