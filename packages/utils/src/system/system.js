import {
  initCurrentAccountApi,
  initSystemConfigApi,
  logoutApi,
  loginApi,
  validateTokenApi,
  loadPluginPermsApi,
} from './api/system';
import { setCurrentAccount, setSystemConfig, getSystemInfo } from './cache';
import { initDDData } from './dictionary';
import { initCodes } from './code';
import { initSqlTemplateData } from './sql';
/**
 * 退出登录，校验登录状态
 */
export { loginApi, logoutApi, validateTokenApi, loadPluginPermsApi };
/**
 * 初始化当前账号
 * @returns {Promise}
 */
export function initCurrentAccount() {
  return initCurrentAccountApi().then((account) => {
    setCurrentAccount(account);
    return account;
  });
}
/**
 * 初始化前端系统配置
 * @returns
 */
export function initSystemConfig() {
  return initSystemConfigApi().then((config) => {
    setSystemConfig(config);
    return config;
  });
}

/**
 * 初始化系统数据
 * @returns
 */
export function initSystemApi() {
  return Promise.all([initCurrentAccount(), initDDData(), initSqlTemplateData(), initCodes()]).then(
    getSystemInfo,
  );
}
