import { logBuryApi, LogActionEnum } from './api/log';
/**
 * 菜单访问日志埋点
 * @param {*} menu
 */
function menuLogBury(menu) {
  if (!menu?.text || !menu?.id) return;
  const action = menu.menuType === 'TOP' ? LogActionEnum.TOP_MENU_OPEN : LogActionEnum.MENU_OPEN;
  logBuryApi({
    objName: menu.text,
    objCode: menu.id,
    typeCode: action.code,
    typeName: action.text,
  });
}

export const Log = { menuLogBury };
