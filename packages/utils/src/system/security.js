import { useJE } from '../je';
/**
 * 密级管理
 * @returns
 */
export function loadSecurityMicro(options) {
  const admin = useJE().useAdmin();
  if (admin?.hasMicroConfig('JE_PLUGIN_SECURITY')) {
    return admin.emitMicroEvent('JE_PLUGIN_SECURITY', 'security-init', {
      JE: useJE(),
      ...options,
    });
  } else {
    return Promise.resolve();
  }
}
