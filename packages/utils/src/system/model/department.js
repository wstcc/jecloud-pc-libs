import Organization from './organization';
/**
 * 公司部门
 */
export default class Department extends Organization {
  /**
   * 所属公司ID
   */
  companyId;
  /**
   * 所属公司编码
   */
  companyName;
  /**
   * 所属集团公司ID
   */
  groupCompanyId;
  /**
   * 所属集团公司Name
   */
  groupCompanyName;
  /**
   * 主管ID
   */
  majorId;
  /**
   * 主管编码
   */
  majorCode;
  /**
   * 主管名称
   */
  majorName;
  /**
   * 职能描述
   */
  funcDesc;
  /**
   * 父级部门
   */
  parent;
  /**
   * 部门路径
   */
  path;
  /**
   * 是否启用
   */
  disabled;
  /**
   * 是否默认主部门
   */
  main;
  /**
   * 部门监管部门
   */
  monitorDepts = [];

  constructor() {
    super();
  }
}
