/**
 * 真实用户
 */
export default class User {
  /**
   * 主键ID
   */
  id;
  /**
   * 机构用户名称
   */
  name;
  /**
   * 机构用户编码
   */
  code;
  /**
   * 头像
   */
  avatar;
  /**
   * 手机号
   */
  phone;
  /**
   * 生日
   */
  birthDate;
  /**
   * 邮件
   */
  email;
  /**
   * 用户唯一Id
   */
  deptmentUserId;
  /**
   * 所属机构
   */
  organization;
  /**
   * 真实的所属机构
   */
  relOrg;

  /**
   * 照片信息
   */
  photo;
  /**
   * 直接领导ID
   */
  directLeaderId;
  /**
   * 直接领导编码
   */
  directLeaderCode;
  /**
   * 直接领导名称
   */
  directLeaderName;
  /**
   * 主管ID
   */
  majorId;
  /**
   * 主管编码
   */
  majorCode;
  /**
   * 主管名称
   */
  majorName;
  /**
   * 性别编码
   */
  sexCode;
  /**
   * 性别名称
   */
  sexName;
  /**
   * 是否是主管
   */
  major;
  /**
   * 是否已婚
   */
  married;
  /**
   * 证件编码
   */
  cardNum;
  /**
   * 证件类型编码
   */
  idTypeCode;
  /**
   * 证件类型名称
   */
  idTypeName;
  /**
   * 年龄
   */
  age;
  /**
   * 入职日期
   */
  entryDate;
  /**
   * 离职日期
   */
  leaveDate;
  /**
   * 行政职务编码
   */
  postCode;
  /**
   * 行政职务名称
   */
  postName;
  /**
   * 文化程度编码
   */
  educationCode;
  /**
   * 文化程度名称
   */
  educationName;
  /**
   * 籍贯
   */
  nativePlace;
  /**
   * 是否初始化
   */
  init;
  /**
   * 监管单位
   */
  monitorDepts;
  /**
   * 监管公司
   */
  monitorCompanys;
  /**
   * 所属公司ID
   */
  companyId;
  /**
   * 所属公司名称
   */
  companyName;
  /**
   * 所属集团公司ID
   */
  groupCompanyId;
  /**
   * 所属集团公司名称
   */
  groupCompanyName;
}
