import {
  JE_SYS_ENCRYPT,
  JE_SYS_ENCRYPT_FIELD,
  JE_SYS_ENCRYPT_KEY,
  JE_SYS_ENCRYPT_FIELD_DEFAULT,
  JE_SYS_ENCRYPT_KEY_DEFAULT,
} from './system/constant';
import {
  split,
  random,
  isFormData,
  isNotEmpty,
  toBoolean,
  isNumber,
  getSystemConfig,
} from './lodash';
import CryptoJS from 'crypto-js';

/**
 * DES加密
 * @param {string} message 加密信息
 * @param {string} key 秘钥
 * @returns String
 */
export function encryptByDES(message, key) {
  const keyHex = CryptoJS.enc.Utf8.parse(key); // 秘钥
  const encrypted = CryptoJS.DES.encrypt(message, keyHex, {
    mode: CryptoJS.mode.ECB, // 加密模式
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.ciphertext.toString(); //  加密出来为 hex格式密文
}
/**
 * DES解密
 * @param {string} ciphertext 密文
 * @param {string} key 秘钥
 * @returns String
 */
export function decryptByDES(ciphertext, key) {
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const decrypted = CryptoJS.DES.decrypt(
    {
      ciphertext: CryptoJS.enc.Hex.parse(ciphertext),
    },
    keyHex,
    {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    },
  );
  return decrypted.toString(CryptoJS.enc.Utf8);
}

/**
 * 系统请求参数加密
 * @param {*} params
 */
export function toParamsEncrypt(params) {
  // 启用请求参数加密
  const encrypt = toBoolean(getSystemConfig(JE_SYS_ENCRYPT));
  // 加密参数编码
  const fields = split(getSystemConfig(JE_SYS_ENCRYPT_FIELD, JE_SYS_ENCRYPT_FIELD_DEFAULT), ',');
  // 随机参数编码
  const fieldKeys = [];
  // 加密秘钥
  const key = getSystemConfig(JE_SYS_ENCRYPT_KEY, JE_SYS_ENCRYPT_KEY_DEFAULT);
  // 启用加密 && 有加密参数
  if (encrypt && fields.length && isNotEmpty(params)) {
    const reg = 'abcdefghigklmnopqrstuvwxyz'.split('');
    fields.forEach((field) => {
      // 随机参数
      let fieldKey;
      while (!fieldKey || fieldKeys.includes(fieldKey)) {
        fieldKey = reg[random(23)];
      }
      fieldKeys.push(fieldKey);
      // 加密内容
      let value = isFormData(params) ? params.get(field) : params[field];
      if (isNotEmpty(value)) {
        if (isNumber(value)) {
          value = value.toString();
        }
        value = encryptByDES(value, key);
        if (isFormData(params)) {
          params.delete(field); // 删除原始参数
          params.set(fieldKey, value); // 增加随机参数，加密信息
        } else {
          delete params[field]; // 删除原始参数
          params[fieldKey] = value; // 增加随机参数，加密信息
        }
      }
    });
    return { 'params-keys': fieldKeys.join(',') };
  }
}
