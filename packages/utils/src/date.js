import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import isLeapYear from 'dayjs/plugin/isLeapYear';
import isBetween from 'dayjs/plugin/isBetween';
import weekday from 'dayjs/plugin/weekday';
import 'dayjs/locale/zh-cn';
import { isString } from 'lodash-es';

dayjs.locale('zh-cn'); // 国际化
dayjs.extend(relativeTime); // 相对时间
dayjs.extend(isLeapYear); // 闰年
dayjs.extend(isBetween); // 日期区间
dayjs.extend(weekday); // 星期

/**
 * 自定义dayjs插件
 * @param option
 * @param dayjsClass
 * @param dayjsFactory
 */
// eslint-disable-next-line
const plugin = (option, dayjsClass, dayjsFactory) => {
  // 获取当前时间
  dayjsClass.prototype.now = () => new Date();
};
dayjs.extend(plugin);

/**
 * 创建Dayjs实例
 * @param  {...Object} args 参数
 * @returns {Dayjs}
 */
export function dateUtil(...args) {
  return dayjs(...args);
}

/**
 * 日期格式化为字符串
 * @param {Date} date 日期对象
 * @param {string} format 格式化字符串，默认YYYY-MM-DD HH:mm:ss
 * @returns {string}
 * @example
    format格式说明：
    输入  示例      描述
    YY    18       两位数的年份
    YYYY  2018     四位数的年份
    M     1-12     月份，从 1 开始
    MM    01-12    月份，两位数
    D     1-31     月份里的一天
    DD    01-31    月份里的一天，两位数
    H     0-23     小时
    HH    00-23    小时，两位数
    h     1-12     小时, 12 小时制
    hh    01-12    小时, 12 小时制, 两位数
    m     0-59     分钟
    mm    00-59    分钟，两位数
    s     0-59     秒
    ss    00-59    秒，两位数
    S     0-9      毫秒，一位数
    SS    00-99    毫秒，两位数
    A     AM / PM  上午 下午 大写
    a     am / pm  上午 下午 小写
 */
export function dateFormat(date, format = 'YYYY-MM-DD HH:mm:ss') {
  return dateUtil(date).format(format);
}
/**
 * 日期字符串解析为日期对象
 * @param {string} dateStr 日期字符串
 * @param {string} format 格式化字符串，默认YYYY-MM-DD HH:mm:ss
 * @returns {Date}
 */
export function dateParse(dateStr, format = 'YYYY-MM-DD HH:mm:ss') {
  return dateUtil(dateStr, format).toDate();
}
/**
 * 日期清空时分秒
 * @param {Date} date 日期对象
 * @returns {Date}
 */
export function dateClearTime(date) {
  return dateUtil(date).hour(0).minute(0).second(0).toDate();
}

/**
 * 解析日期格式化配置
 * @param {Object} options
 * @param {string} options.format 显示值格式化内容
 * @param {string} options.valueFormat 值格式化内容
 * @param {string} [options.type] 类型，默认date
 * @returns {Obejct} {format,valueFormat}
 */
export function parseDateFormat({ format, valueFormat, type }) {
  let tempFormat = '';
  let tempValueFormat = '';
  switch (type?.toLocaleLowerCase()) {
    case 'datetime':
      tempFormat = format || 'YYYY-MM-DD HH:mm:ss';
      tempValueFormat = valueFormat || 'YYYY-MM-DD HH:mm:ss';
      break;
    case 'year':
      tempFormat = format || 'YYYY';
      tempValueFormat = valueFormat || 'YYYY';
      break;
    case 'quarter':
      tempFormat = format || 'YYYY-Q';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
    case 'month':
      tempFormat = format || 'YYYY-MM';
      tempValueFormat = valueFormat || 'YYYY-MM';
      break;
    case 'week':
      tempFormat = format || 'YYYY-wo';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
    case 'time':
      tempFormat = format || 'HH:mm:ss';
      tempValueFormat = valueFormat || 'HH:mm:ss';
      break;
    default:
      tempFormat = format || 'YYYY-MM-DD';
      tempValueFormat = valueFormat || 'YYYY-MM-DD';
      break;
  }

  return { type, format: tempFormat, valueFormat: tempValueFormat };
}
/**
 * 格式化日期数据
 * @param {string} value 日期字符串
 * @param {Object} options
 * @param {string} options.format 显示值格式化内容
 * @param {string} options.valueFormat 值格式化内容
 * @param {string} [options.type] 类型，默认date
 * @returns
 */
export function rendererDateValue(value, options) {
  let { type } = options;
  const config = parseDateFormat(options);
  if (value && config.format !== config.valueFormat) {
    // 处理时间
    if (type === 'time' && isString(value)) {
      value = `${new Date().getFullYear()} ${value}`;
    }
    return dateUtil(value, config.valueFormat).format(config.format);
  } else {
    return value;
  }
}
