import { camelCase, split, join, forEach } from '../lodash';
import { useJE } from '../je';

/**
 * model:value
 * @param {*} props
 * @returns
 */
export function useModelValue({
  props,
  context,
  key = 'value',
  multiple = false,
  changeEvent = false,
  changeValid,
  separator = ',',
}) {
  if (!hasVue()) return;
  const { watch, computed, ref } = useVue();
  const valueRef = ref(multipleValue(props[key], multiple, separator));
  const { emit } = context;
  // 监听值改变
  watch(
    () => props[key],
    (val) => {
      const newValue = multipleValue(val, multiple, separator);
      valueRef.value !== newValue && (valueRef.value = newValue);
    },
    {
      deep: true,
    },
  );
  const value = computed({
    set(val) {
      valueRef.value = multipleValue(val, multiple, separator);
      emit('update:' + key, multipleValue(val, multiple, separator, false));
      setTimeout(() => {
        // 字段校验方法
        changeValid?.();
        // formItemContext?.$plugin?.value?.onFieldChange();
      });
    },
    get() {
      return valueRef.value;
    },
  });
  // 监听v-model:value的chenge事件
  if (changeEvent && key === 'value') {
    const { hasListener } = useListeners({ props });
    hasListener('value-change') &&
      watch(
        () => value.value,
        (newValue, oldValue) => {
          emit('value-change', newValue, oldValue);
        },
      );
  }
  return value;
}
/**
 *  事件处理器
 * TODO:小程序无效
 * @param {*} param
 * @returns
 */
export function useListeners({ props }) {
  let vnode;
  if (hasVue()) {
    vnode = useVue().getCurrentInstance().vnode;
  }
  const hasListener = function (name) {
    const listener = camelCase(`on-${name}`);
    return props[listener] || vnode?.props?.[listener];
  };

  // 触发事件
  const fireListener = function (name, ...args) {
    const listener = hasListener(name);
    if (listener) {
      return listener?.(...args);
    }
  };

  return {
    hasListener,
    fireListener,
  };
}

/**
 * 安装注册事件
 * @param comp 组件
 * @returns
 */
export function withInstall(comp, afterFn) {
  // 绑定依赖组件
  forEach(comp.installComps, (item, key) => {
    comp[key] = item.comp;
  });
  // 安装插件方法
  comp.install = function (app) {
    // 注册组件，默认使用自己的组件
    app.component(comp.displayName || comp.name, comp);
    // 注册依赖组件
    forEach(comp.installComps, (item) => {
      app.component(item.name, item.comp);
    });
    afterFn?.(app);
  };
  return comp;
}

/**
 * 初始多选数据
 * 多选时，会将modelValue当做字符串，进行分割
 * @param {*} value props.value
 * @param {*} array 默认是字符串转数组,false 是将数组转字符串
 * @returns
 */
function multipleValue(value, multiple, separator, array = true) {
  return multiple ? (array ? split(value, separator) : join(value, separator)) : value;
}

function useVue() {
  return useJE().useVue();
}
function hasVue() {
  return !!useVue();
}

export const Hooks = {
  useModelValue,
  useListeners,
  withInstall,
};
