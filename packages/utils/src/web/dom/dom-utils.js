import {
  upperFirst,
  isString,
  toNumber,
  isNumber,
  isNumeric,
  isPlainObject,
  isEmpty,
} from '../../lodash';
/**
 * 高亮html节点对象背景
 * @param {HTMLElement} el html节点对象
 */
export function highlightElement(el) {
  const highlightCls = 'is--highlight';
  if (!hasClass(el, highlightCls)) {
    addClass(el, highlightCls);
    setTimeout(() => {
      removeClass(el, highlightCls);
    }, 1000);
  }
}

/**
 * 获得相对目标dom的位置信息
 * @param {HTMLElement} source 原dom
 * @param {HTMLElement} target 目标dom
 */
export function getOffsetBox(source, target) {
  var box = { x: source.offsetLeft, y: source.offsetTop };
  if (target && source.offsetParent != target) {
    var _box = getOffsetBox(source.offsetParent, target);
    box.x += _box.x;
    box.y += _box.y;
  }
  return box;
}

export function getBoundingClientRect(element) {
  if (!element || !element.getBoundingClientRect) {
    return 0;
  }
  return element.getBoundingClientRect();
}

function trim(string) {
  return (string || '').replace(/^[\s\uFEFF]+|[\s\uFEFF]+$/g, '');
}

/**
 * 判断html节点对象是否包含指定样式
 * @param {HTMLElement} el html节点对象
 * @param {string} cls 样式名
 * @returns {boolean}
 */
export function hasClass(el, cls) {
  if (!el || !cls) return false;
  if (cls.indexOf(' ') !== -1) throw new Error('className should not contain space.');
  if (el.classList) {
    return el.classList.contains(cls);
  } else {
    return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
  }
}

/**
 * 添加html节点对象样式
 * @param {HTMLElement} el html节点对象
 * @param {string} cls 样式名
 */
export function addClass(el, cls) {
  if (!el) return;
  let curClass = el.className;
  const classes = (cls || '').split(' ');

  for (let i = 0, j = classes.length; i < j; i++) {
    const clsName = classes[i];
    if (!clsName) continue;

    if (el.classList) {
      el.classList.add(clsName);
    } else if (!hasClass(el, clsName)) {
      curClass += ' ' + clsName;
    }
  }
  if (!el.classList) {
    el.className = curClass;
  }
}
/**
 * 删除html节点对象样式
 * @param {HTMLElement} el html节点对象
 * @param {string} cls 样式名
 */
export function removeClass(el, cls) {
  if (!el || !cls) return;
  const classes = cls.split(' ');
  let curClass = ' ' + el.className + ' ';

  for (let i = 0, j = classes.length; i < j; i++) {
    const clsName = classes[i];
    if (!clsName) continue;

    if (el.classList) {
      el.classList.remove(clsName);
    } else if (hasClass(el, clsName)) {
      curClass = curClass.replace(' ' + clsName + ' ', ' ');
    }
  }
  if (!el.classList) {
    el.className = trim(curClass);
  }
}

/**
 * 切换html节点对象样式
 * @param {HTMLElement} el html节点对象
 * @param {string} cls 样式名
 * @param {boolean} toggle 切换状态，true添加，false删除
 */
export function toggleClass(el, cls, toggle) {
  if (isString(el)) {
    toggle = cls;
    cls = el;
    el = document.body;
  }
  const flag = isEmpty(toggle, true) ? !hasClass(el, cls) : toggle;
  if (flag) {
    !hasClass(el, cls) && addClass(el, cls);
  } else {
    hasClass(el, cls) && removeClass(el, cls);
  }
}
/**
 * Get the left and top offset of the current element
 * left: the distance between the leftmost element and the left side of the document
 * top: the distance from the top of the element to the top of the document
 * right: the distance from the far right of the element to the right of the document
 * bottom: the distance from the bottom of the element to the bottom of the document
 * rightIncludeBody: the distance between the leftmost element and the right side of the document
 * bottomIncludeBody: the distance from the bottom of the element to the bottom of the document
 *
 * @description:
 */
export function getViewportOffset(element) {
  const doc = document.documentElement;

  const docScrollLeft = doc.scrollLeft;
  const docScrollTop = doc.scrollTop;
  const docClientLeft = doc.clientLeft;
  const docClientTop = doc.clientTop;

  const pageXOffset = window.pageXOffset;
  const pageYOffset = window.pageYOffset;

  const box = getBoundingClientRect(element);

  const { left: retLeft, top: rectTop, width: rectWidth, height: rectHeight } = box;

  const scrollLeft = (pageXOffset || docScrollLeft) - (docClientLeft || 0);
  const scrollTop = (pageYOffset || docScrollTop) - (docClientTop || 0);
  const offsetLeft = retLeft + pageXOffset;
  const offsetTop = rectTop + pageYOffset;

  const left = offsetLeft - scrollLeft;
  const top = offsetTop - scrollTop;

  const clientWidth = window.document.documentElement.clientWidth;
  const clientHeight = window.document.documentElement.clientHeight;
  return {
    left: left,
    top: top,
    right: clientWidth - rectWidth - left,
    bottom: clientHeight - rectHeight - top,
    rightIncludeBody: clientWidth - left,
    bottomIncludeBody: clientHeight - top,
  };
}

export function hackCss(attr, value) {
  const prefix = ['webkit', 'Moz', 'ms', 'OT'];

  const styleObj = {};
  prefix.forEach((item) => {
    styleObj[`${item}${upperFirst(attr)}`] = value;
  });
  return {
    ...styleObj,
    [attr]: value,
  };
}

/* istanbul ignore next */
export function on(element, event, handler) {
  if (element && event && handler) {
    element.addEventListener(event, handler, false);
  }
}

/* istanbul ignore next */
export function off(element, event, handler) {
  if (element && event && handler) {
    element.removeEventListener(event, handler, false);
  }
}

/**
 * 复制文字到剪切板，然后通过control+v进行复制
 * @param {string} text
 */
export function copy(text) {
  // 增加兼容性处理
  const clipboardData = window.clipboardData;
  if (clipboardData) {
    clipboardData.setData('Text', text);
  } else {
    const aux = document.createElement('textarea');
    aux.value = text;
    document.body.appendChild(aux);
    aux.select();
    document.execCommand('copy');
    document.body.removeChild(aux);
  }
}
/**
 * 获得页面高
 * @return {number}
 */
export function getBodyHeight() {
  return getDomNode().height;
}
/**
 * 获得页面宽
 * @return {number}
 */
export function getBodyWidth() {
  return getDomNode().width;
}
/**
 * 获得dom节点信息
 *
 * @export
 * @return {*}
 */
export function getDomNode() {
  const documentElement = document.documentElement;
  const bodyElem = document.body;
  return {
    scrollTop: documentElement.scrollTop || bodyElem.scrollTop,
    scrollLeft: documentElement.scrollLeft || bodyElem.scrollLeft,
    height: documentElement.clientHeight || bodyElem.clientHeight,
    width: documentElement.clientWidth || bodyElem.clientWidth,
  };
}

/**
 * 检查触发源是否属于目标节点
 */
export function getEventTargetNode(evnt, container, queryCls, queryMethod) {
  let targetElem;
  let target = evnt.target;
  while (target && target.nodeType && target !== document) {
    if (queryCls && hasClass(target, queryCls) && (!queryMethod || queryMethod(target))) {
      targetElem = target;
      return { flag: true, container, targetElem: targetElem };
    } else if (target === container) {
      return { flag: queryCls ? !!targetElem : true, container, targetElem: targetElem };
    }
    target = target.parentNode;
  }
  return { flag: false };
}

export function getPaddingSize(el) {
  const padding = { top: 0, bottom: 0, left: 0, right: 0 };
  if (el) {
    const computedStyle = getComputedStyle(el);
    return Object.assign(padding, {
      top: computedDomSize(computedStyle.paddingTop),
      bottom: computedDomSize(computedStyle.paddingBottom),
      left: computedDomSize(computedStyle.paddingLeft),
      right: computedDomSize(computedStyle.paddingRight),
    });
  }
  return padding;
}
export function getBorderSize(el) {
  const padding = { top: 0, bottom: 0, left: 0, right: 0 };
  if (el) {
    const computedStyle = getComputedStyle(el);
    return Object.assign(padding, {
      top: computedDomSize(computedStyle.borderTopWidth),
      bottom: computedDomSize(computedStyle.borderBottomWidth),
      left: computedDomSize(computedStyle.borderLeftWidth),
      right: computedDomSize(computedStyle.borderRightWidth),
    });
  }
  return padding;
}

export function getOffsetWidth(el) {
  return el ? el.offsetWidth : 0;
}
export function getOffsetHeight(el) {
  return el ? el.offsetHeight : 0;
}

export function computedDomSize(config) {
  if (!isPlainObject(config)) {
    config = { size: config };
  }
  let { size, unit, parentSize } = config;
  let computedSize = 0;
  if (isString(size) && size !== 'auto') {
    size = size.toLocaleLowerCase();
    if (size.endsWith('px')) {
      computedSize = toNumber(size.replace('px', ''));
    } else if (size.endsWith('%')) {
      computedSize = isNumber(parentSize)
        ? (toNumber(size.replace('%', '')) * parentSize) / 100
        : size;
    } else if (isNumeric(size)) {
      computedSize = toNumber(size);
    }
  } else {
    computedSize = size;
  }

  if (!isNaN(computedSize)) {
    return unit ? `${computedSize}${unit}` : computedSize;
  } else {
    return size;
  }
}
