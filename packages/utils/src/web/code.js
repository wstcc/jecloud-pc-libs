import { useJE } from '../je';
import { execScript } from './sandbox';
import { createStyleSheet } from './style';
import { initJsCodeApi, initCssCodeApi } from '../system/api/code';
const jsMap = new Map();
/**
 * 初始化全局脚本
 * @returns
 */
export function initWebCodes() {
  const less = window.less || { render: (str) => Promise.resolve({ css: str }) };
  const JE = useJE();
  // 注入JE全局函数
  Object.assign(JE, {
    /**
     * 执行全局脚本库
     * @param {string} code 方法名
     * @param {Object} options 参数
     * @returns
     */
    callCustomFn(code, options) {
      let fnCode = jsMap.get(code);
      if (fnCode) {
        try {
          return execScript(fnCode, { EventOptions: options || {} });
        } catch (error) {
          console.error(`js全局脚本库：【${code}】出错了！`);
          console.error(error);
        }
      }
    },
    /**
     * 更新全局脚本
     * @private
     * @param {*} code 方法名
     * @param {*} codes 代码
     */
    updateCustomFn(code, codes) {
      if (code && codes) {
        jsMap.set(code, codes);
      }
    },
    /**
     * 解析样式
     * @param {*} code 样式名
     * @param {*} codes 代码
     * @returns
     */
    parseCustomStyle(code, codes) {
      return less.render(`.${code}{${codes}}`).then(({ css }) => css);
    },
    /**
     * 注册样式
     * @param {*} code 样式名
     * @param {*} codes 代码
     * @returns
     */
    registCustomStyle(code, codes) {
      return JE.parseCustomStyle(code, codes).then((cssCode) => {
        createStyleSheet(cssCode, 'je_coustom_' + code);
      });
    },
  });
  // 加载脚本库
  return Promise.all([initJsCodeApi(), initCssCodeApi()]).then(([jsCodes, cssCodes]) => {
    // 缓存js脚本库
    jsCodes.rows.forEach((item) => {
      jsMap.set(item.QJJBK_FFM, item.QJJBK_JBDM);
    });
    // 解析css样式库
    const csss = [];
    cssCodes.rows.forEach((item) => {
      csss.push(`.${item.QJCSS_YSM}{${item.QJCSS_YSDM}}`);
    });
    // 写入样式
    csss.length > 0 &&
      less.render(csss.join('')).then(({ css }) => {
        createStyleSheet(css);
      });
  });
}
