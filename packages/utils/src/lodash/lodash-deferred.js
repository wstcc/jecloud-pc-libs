/**
 * Promise异步队列函数
 */
class Deferred {
  promise;

  resolve;

  reject;

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}

/**
 * Promise异步队列函数构造器
 * @returns {Deferred}
 */
export function createDeferred() {
  return new Deferred();
}
