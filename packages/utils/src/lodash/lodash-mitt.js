import { isString, forEach, isPlainObject } from 'lodash-es';
/**
 * 事件触发器
 * @returns
 */
export function mitt(listeners) {
  listeners = listeners || new Map();
  return {
    listeners,
    /**
     * 注册事件
     * @param name 事件名
     * @param handler 事件方法
     */
    on(name, handler) {
      if (name && isString(name) && handler) {
        const handlers = listeners.get(name) || [];
        handlers.push(handler);
        listeners.set(name, handlers);
      }
    },
    /**
     * 注销事件
     * @param name 事件名
     * @param handler 事件方法
     */
    off(name, handler) {
      let handlers = listeners.get(name);
      if (handler && handlers) {
        handlers = handlers.filters((item) => item !== handler);
      } else {
        listeners.delete(name);
      }
    },
    /**
     * 触发事件
     * @param name 事件名
     * @param args 事件参数
     * @returns 事件结果
     */
    emit(name, ...args) {
      const handlers = listeners.get(name);
      let result; // 事件结果
      forEach(handlers, (handler) => {
        if (isPlainObject(handler)) {
          if (handler.onece) {
            result = handler.fn?.(...args);
            handler = null;
          }
        } else {
          result = handler(...args);
        }
        return result !== false;
      });
      return result;
    },
    clear() {
      listeners.clear();
    },
  };
}
