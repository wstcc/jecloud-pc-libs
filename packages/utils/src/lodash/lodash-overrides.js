export * from 'lodash-es';
import {
  isFunction,
  isEmpty as _isEmpty,
  split as _split,
  isUndefined,
  isNull,
  isNumber,
  isBoolean,
} from 'lodash-es';
import { isNumeric } from './lodash-plus';

/**
 * 是否 空 对象
 * @param {Object} value 对象
 * @param {boolean} onlyNullOrUndefined 只判断是否为null或undefined
 * @return {boolean}
 */
export function isEmpty(value, onlyNullOrUndefined) {
  if (onlyNullOrUndefined) {
    return isUndefined(value) || isNull(value);
  }
  if (isNumber(value) || isBoolean(value) || isFunction(value)) return false;
  return _isEmpty(value);
}

/**
 * 拆分字符串
 * @param {string} str 字符串
 * @param {string|RegExp} separator 拆分的分隔符
 * @param {string} [limit] 限制结果的数量
 * @returns {Array}
 */
export function split(str, separator, limit) {
  if (isEmpty(str)) return [];
  return _split(str, separator, limit);
}

/**
 * 转换数字
 * @param {string} num
 * @return {number}
 */
export function toNumber(num) {
  if (isNumeric(num)) {
    num = Number(num, 0);
  } else {
    num = 0;
  }
  return num;
}
