import { getCurrentUser } from '../../lodash';
/**
 * 握手消息
 */
export default class HandshakeMessage {
  /**
   * 拖动设备id
   */
  cid;
  /**
   * 用户ID
   */
  userId;
  /**
   * 设备类型
   */
  device = 'web';
  /**
   * 系统类型-移动应用
   */
  osName = 'web';
  /**
   * 系统版本-移动应用
   */
  osVersion = '1.0';
  /**
   * 客户端版本-移动应用
   */
  clientVersion = '1.0';

  constructor(options = {}) {
    const user = getCurrentUser();
    this.userId = user?.id || '';
    this.cid = options.cid || '';
    this.device = options.device || this.device;
    this.deviceId = this.device;
    this.osName = options.osName || this.osName;
    this.osVersion = options.osVersion || this.osVersion;
    this.clientVersion = options.clientVersion || this.clientVersion;
  }
}
