import { isString, toNumber, decode } from '../../lodash';
/**
 * 接收消息体
 */
export default class ReceiveMessage {
  /**
   * 消息类型
   */
  type;
  /**
   * 业务标识
   */
  busType;
  /**
   * 消息内容
   */
  content;
  constructor(options) {
    const message = isString(options) ? decode(options) : options;
    this.type = toNumber(message.type);
    this.busType = message.busType;
    this.content = message.content;
  }
}
