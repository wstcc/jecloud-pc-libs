import { ajax, transformAjaxData } from '../../http';
import { omit } from 'lodash-es';

export function useAjaxPrexy(proxy = {}) {
  const baseParams = proxy.params;
  const read = (options = {}) => {
    const { params, url = proxy.url } = options;
    return ajax(
      {
        url,
        params: { ...baseParams, ...params },
        headers: proxy.headers,
        timeout: proxy.timeout,
      },
      { ...omit(options, ['url', 'params']) },
    )
      .then(transformAjaxData)
      .catch(({ response, errorInfo }) => {
        console.log(response.status, '：', response.data, errorInfo);
      });
  };

  return { read };
}
