import Store from './base-store';
/**
 * Store 数据集
 * @module Store
 */
export default class GridStore extends Store {
  /**
   * 构造参数
   * @param {Object} options 配置信息
   * @param {string} options.idProperty 主键名
   * @param {string} options.url 请求链接
   * @param {string} [options.params] 请求参数
   * @param {string} [options.pageSize] 每页条数，默认10条
   * @param {string} [options.rootProperty] 返回数据的json属性，默认rows
   * @param {string} [options.totalProperty] 返回总条数的json属性，默认totalCount
   */
  constructor(options = {}) {
    super(options);
    /**
     * 追加数据
     */
    this.appendData = options.appendData;
    /**
     * 每页条数
     */
    this.pageSize = options.pageSize || 30;
    /**
     * 请求链接
     */
    this.url = options.url;
    /**
     * 请求参数
     */
    this.params = options.params;
    /**
     * 返回数据的json属性，默认rows
     */
    this.rootProperty = options.rootProperty || 'rows';
    /**
     * 返回总条数的json属性，默认totalCount
     */
    this.totalProperty = options.totalProperty || 'totalCount';
    /**
     * 总条数
     */
    this.totalCount = 0;
  }

  /**
   * 当前页
   *
   * @memberof Store
   */
  _currentPage = 1;
  get currentPage() {
    return this._currentPage;
  }
  set currentPage(page) {
    this._currentPage = Math.min(Math.max(page, 1), this.totalPage);
  }

  /**
   * 起始条数
   *
   * @readonly
   * @memberof Store
   */
  get start() {
    return (this.currentPage - 1) * this.pageSize;
  }
  /**
   * 结束条数
   *
   * @readonly
   * @memberof Store
   */
  get end() {
    return this.start + this.pageSize - 1;
  }
  /**
   * 总页数
   *
   * @readonly
   * @memberof Store
   */
  get totalPage() {
    return Math.max(Math.ceil(this.totalCount / this.pageSize), 1);
  }

  /**
   * 更新缓存
   * @returns
   */
  getLoadOptionsCache() {
    return { ...this._loadOptionsCache, pageSize: this.pageSize };
  }

  /**
   * 分页条加载事件
   * @param {*} type
   * @param {*} param1
   */
  pagerLoad(type, { page = this.currentPage, pageSize = this.pageSize }) {
    switch (type) {
      case 'refresh':
        this.reload();
        break;
      case 'pageChange':
        this.loadPage(page);
        break;
      case 'pageSize':
        this.pageSize = pageSize;
        this.loadPage(1);
        break;
    }
  }

  /**
   * 按照页码加载数据
   * @param {number} page 页码
   * @param {Object} options 请求配置
   * @param {Object} [options.params] 请求参数
   * @param {Function} options.callback 回掉函数，参数：data,store
   */
  loadPage(page, options = {}) {
    return this.load({ ...this.getLoadOptionsCache(), ...options, page });
  }
  /**
   * 下一页
   * @param {Object} options 配置信息
   */
  nextPage() {
    return this.loadPage(this.currentPage + 1);
  }
  /**
   * 上一页
   * @param {Object} options 配置信息
   */
  prevPage() {
    return this.loadPage(this.currentPage - 1);
  }

  /**
   * 功能列表添加,主键id是后台生成的,需要替换数据
   * @param {Object} record
   */
  transformInsertRecord(record) {
    if (record?.__doInsertOldPkValue__) {
      // 老数据
      const oldRecord = this.getRecordById(record?.__doInsertOldPkValue__);
      const oldId = record?.__doInsertOldPkValue__;
      // 新ID
      const newId = this.getRecordId(record);
      // 更新新ID
      oldRecord[this.idProperty] = newId;
      // 重新设置缓存
      delete oldRecord.__doInsertOldPkValue__;
      // 删除老数据缓存
      this.dataMap.delete(oldId);
      this.sourceMap.delete(oldId);
      this._addCache(oldRecord);
    }
  }

  /* -------------------------override------------------------- */
  /**
   * 刷新数据
   */
  reload() {
    if (this.appendData) {
      this.loadPage(1);
    } else {
      this.loadPage(this.currentPage);
    }
  }
  /**
   * 加载数据
   * @param {*} data
   */
  loadData(data = []) {
    if (this.appendData && this.currentPage > 1) {
      // 追加数据
      this.data.push(...data);
      data.forEach((record) => {
        this._addCache(record);
      });
    } else {
      // 更新数据
      this._clearCache();
      this.data = data;
      this.data.forEach((record) => {
        this._addCache(record);
      });
    }
  }
  /**
   * 加载前
   * @override
   * @param {*} options
   */
  onBeforeLoad(options) {
    const { page = this.currentPage, pageSize = this.pageSize, params = this.params } = options;
    // 更新pageSize
    options.pageSize = this.pageSize = pageSize;
    // 更新页码
    this.currentPage = page;
    // 更新参数
    options.params = { ...params, page: this.currentPage, limit: this.pageSize, start: this.start };
  }

  /**
   * 加载后
   * @override
   * @param {*} data
   */
  onLoad(data) {
    // 数组数据
    if (Array.isArray(data)) {
      data = { [this.totalProperty]: 0, [this.rootProperty]: data };
    }
    // 更新总条数
    this.totalCount = data[this.totalProperty];
    this.loadData(data[this.rootProperty]);
  }
}
