import {
  isString,
  cloneDeep,
  isEmpty,
  isNotEmpty,
  uuid,
  get,
  isNumber,
  isEqual,
  isPlainObject,
  toValue,
  isBoolean,
  mitt,
  omit,
} from '../../lodash';
import { useJE } from '../../je';
import { useProxy } from '../proxy';
export default class Store {
  /**
   * store标识
   *
   * @readonly
   * @memberof Store
   */
  get isStore() {
    return true;
  }

  /**
   * 树形store
   *
   * @memberof Store
   */
  isTreeStore = false;

  /**
   * 主键属性
   *
   * @memberof Store
   */
  idProperty = 'id';

  /**
   * 数据model字段，用于检查是否改变
   *
   * @memberof Store
   */
  fields;

  /**
   * 数据
   *
   * @memberof Store
   */
  data = [];

  /**
   * 数据map，key为ID
   *
   * @memberof Store
   */
  dataMap = new Map();

  /**
   * 原始数据
   *
   * @memberof Store
   */
  sourceMap = new Map();

  /**
   * 事件
   *
   * @memberof Store
   */
  emitter = mitt();

  /**
   * 加载状态
   *
   * @memberof Store
   */
  loading = false;

  /**
   * 自动加载
   *
   * @memberof Store
   */
  autoLoad = true;

  /**
   * 加载项缓存
   *
   * @memberof Store
   */
  _loadOptionsCache = {};

  constructor(options = {}) {
    /**
     * 数据模型字段
     */
    this.fields = options.fields;

    /**
     * 是否自动加载
     */
    this.autoLoad = toValue(options.autoLoad, true, true);

    /**
     * 数据
     */
    this.data = options.data || [];

    /**
     * 主键名
     */
    this.idProperty = options.idProperty || 'id';

    /**
     *  数据请求代理
     */
    if (options.proxy) {
      this.proxy = useProxy(options.proxy, this);
    } else {
      this.autoLoad = false;
    }
  }

  /**
   * 注册事件
   * @param  {...any} args
   */
  on(...args) {
    this.emitter.on(...args);
  }
  /**
   * 触发事件
   * @param  {...any} args
   */
  emit(...args) {
    this.emitter.emit(...args);
  }

  /**
   * 数据代理
   * @returns
   */
  getProxy() {
    let proxy = this.proxy;
    if (!proxy) {
      proxy = {
        read(options) {
          return Promise.resolve(options?.data || this.data || []);
        },
      };
    }
    return proxy;
  }
  /**
   * 获取缓存数据
   * @returns
   */
  getLoadOptionsCache() {
    return this._loadOptionsCache;
  }

  /**
   * 加载数据
   * @param {Object} options 请求配置
   * @param {Object} [options.params] 请求参数
   * @param {Function} options.callback 回掉函数，参数：data,store
   */
  load(options = {}) {
    const store = this;
    store.loading = true;
    if (this.emitter.emit('before-load', { options, store }) !== false) {
      this._loadOptionsCache = options;
      this.onBeforeLoad(options);
      return this.getProxy()
        .read({ ...options, store })
        .then((data) => {
          store.loading = false;
          this.onLoad(data);
          this.emitter.emit('load', { store, success: true });
          // pageDataSize 兼容app分页 计算分页时需要当前页的数据条数
          return { store, pageDataSize: data?.rows.length };
        })
        .catch((err) => {
          this.emitter.emit('load', { store, error: err, success: false });
          store.loading = false;
        });
    } else {
      store.loading = false;
      return Promise.resolve({ store });
    }
  }

  /**
   * 刷新数据
   */
  reload() {
    this.load(this.getLoadOptionsCache());
  }

  /**
   * 加载数据，可以重写
   * @param {Array} data 数据
   */
  loadData(data) {
    data;
  }

  /**
   * 刷新数据
   */
  refreshData() {
    this.data = this.data.slice(0);
  }

  /**
   * 通过主键获得数据
   * @param {string} id 主键
   * @returns {Object}
   */
  getRecordById(id) {
    return this.dataMap.get(id);
  }

  /**
   * 获得主键
   * @param {*} record
   * @returns
   */
  getRecordId(record) {
    return isString(record) ? record : record?.[this.idProperty];
  }

  /**
   * 是否新增数据
   * @param {*} record
   * @returns
   */
  isInsertRecord(record) {
    return record['__action__'] === 'doInsert';
  }

  /**
   * 数据变化状态
   * @param {*} record
   * @returns
   */
  isChange(record, field) {
    const row = this._toRawRecord(record);
    if (field) {
      // 历史数据
      const id = this.getRecordId(row);
      const oldRow = this._toRawRecord(this.sourceMap.get(id));
      return !this.eqCellValue(row, oldRow, field);
    }
    // 不做判断的字段
    const excludeFilds = ['_record', '_X_ROW_KEY'];
    const changes = this.getChanges(row, false)[0];
    if (field) {
      if (excludeFilds.includes(field)) {
        return false;
      } else {
        return !!changes?.[field];
      }
    } else {
      const rawChanges = omit(changes, excludeFilds);
      return isNotEmpty(rawChanges);
    }
  }
  /**
   * 数据比较
   * @param {*} row1
   * @param {*} row2
   * @param {*} field
   * @returns
   */
  eqCellValue(row1, row2, field) {
    const val1 = get(row1, field);
    const val2 = get(row2, field);
    if (isEmpty(val1) && isEmpty(val2)) {
      return true;
    }
    if (isString(val1) || isNumber(val1)) {
      /* eslint-disable eqeqeq */
      return val1 == val2;
    }
    return isEqual(val1, val2);
  }
  /**
   * 获得修改数据
   * @param {*} record
   * @param {*} includeInsert 包含添加数据
   * @returns
   */
  getChanges(record, includeInsert) {
    const changes = [];
    let records = [];
    // 兼容数据
    if (isBoolean(record)) {
      includeInsert = record;
      record = null;
    }
    if (record) {
      records = [record];
    } else {
      records = Array.from(this.dataMap.values());
    }
    records.forEach((row) => {
      // 获取fields配置
      if (!this.fields) {
        this.fields = Object.keys(row).map((key) => ({ name: key }));
      }
      const recordChanges = {};
      const newRow = this._toRawRecord(row);
      const id = this.getRecordId(newRow);
      // 历史数据
      const oldRow = this._toRawRecord(this.sourceMap.get(id)) || {};
      // 新增数据
      if (includeInsert && this.isInsertRecord(newRow)) {
        Object.keys(newRow).forEach((name) => {
          recordChanges[name] = {
            field: name,
            oldValue: newRow[name],
            newValue: newRow[name],
          };
        });
      } else {
        this.fields.forEach((field) => {
          const name = field.name || field;
          const oldValue = oldRow[name];
          const newValue = newRow[name];
          if (!this.eqCellValue(oldRow, newRow, name)) {
            recordChanges[name] = {
              field: name,
              oldValue,
              newValue,
            };
          }
        });
      }
      if (isNotEmpty(recordChanges)) {
        recordChanges._record = newRow;
        changes.push(recordChanges);
      }
    });
    return changes;
  }

  /**
   * 获得修改的数据
   * @param {*} includeInsert
   * @returns
   */
  getUpdatedRecords(includeInsert) {
    const records = Array.from(this.dataMap.values());
    return records.filter((record) => {
      return (includeInsert && this.isInsertRecord(record)) || this.isChange(record);
    });
  }
  /**
   * 查找数据
   * @param {Function} filter 过滤函数
   * @returns {Array} records
   */
  findRecords(filter) {
    const records = [];
    filter &&
      this.dataMap.forEach((record) => {
        if (filter?.(record)) {
          records.push(record);
        }
      });
    return records;
  }

  /**
   * 提交修改状态
   * @param {*} record
   */
  commitRecord(record) {
    const id = this.getRecordId(record);
    const row = this.dataMap.get(id);
    let oldRow = this.sourceMap.get(id);
    // 新增数据
    if (this.isInsertRecord(record)) {
      oldRow = cloneDeep(record);
      this.sourceMap.set(id, oldRow);
    }
    if (oldRow && row && record) {
      Object.assign(row, record);
      Object.assign(oldRow, record);
      this._clearInsertMark(row);
      this._clearInsertMark(oldRow);
      // 数据项提交后事件
      this.emitter.emit('commit-record', { record: this.getRecordById(id) });
    }
  }

  /**
   * 格式化record，可以重写
   * @param {*} record
   * @returns
   */
  transformRecord(record) {
    return record;
  }
  /**
   * 获取兄弟节点，可以重写
   * @returns Array
   */
  getBrotherRecords() {
    return [];
  }
  /**
   * 加载前，可以重写
   * @param {Object} options 请求配置
   */
  onBeforeLoad(options) {
    options;
  }
  /**
   * 加载后，可以重写
   */
  onLoad(data) {
    // 加载数据
    this.loadData(data);
  }

  /**
   * 插入数据
   * @param {Array} records
   * @param {number|Object} index 插入的位置：null(第一行)，-1(最后一行)，数字(指定索引)，record(指定数据后面)
   */
  insert(records, index) {
    if (isPlainObject(records)) {
      records = [records];
    }
    // 响应式对象
    records = records.map((record) => this._addCache(record));
    const data = this.data.slice(0);
    if (index === -1) {
      data.push(...records);
    } else if (index) {
      let items = data;
      let rowIndex = isNumber(index) ? index : this.indexOfById(index, items);
      if (rowIndex === -1 && this.isTreeStore) {
        // 获取兄弟节点，特殊处理
        const record = index;
        items = this.getBrotherRecords(record);
        rowIndex = this.indexOfById(record, items);
      }
      items.splice(rowIndex, 0, ...records);
    } else {
      data.unshift(...records);
    }
    this.data = data;
    return records;
  }
  /**
   * 删除数据
   * @param {*} records
   */
  remove(records) {
    if (isPlainObject(records)) {
      records = [records];
    }
    const data = this.data.slice(0);
    records.forEach((record) => {
      let index = this.indexOfById(record, data);
      let items = data;
      if (index === -1 && this.isTreeStore) {
        // 获取兄弟节点，特殊处理
        items = this.getBrotherRecords(record, data);
        index = this.indexOfById(record, items);
      }
      items.splice(index, 1);
      this._removeCache(record);
    });
    this.data = data;
  }
  /**
   * 根据id获得索引
   * @param {*} record
   * @param {*} data
   * @returns
   */
  indexOfById(record, data = this.data) {
    const id = this.getRecordId(record);
    return data.map((item) => this.getRecordId(item)).indexOf(id);
  }

  /**
   * 数量大小
   * @returns
   */
  size() {
    return this.dataMap.size;
  }

  /**
   * 获取所有ID值
   * @returns
   */
  getAllIds() {
    return Array.from(this.dataMap.keys());
  }

  /**
   * 添加标记
   * @param {*} record
   */
  _setInsertMark(record) {
    record['__action__'] = 'doInsert';
  }
  /**
   * 清空标记
   * @param {*} record
   */
  _clearInsertMark(record) {
    delete record['__action__'];
  }
  /**
   * 添加缓存数据
   * @param {*} record
   */
  _addCache(record, type) {
    let id = this.getRecordId(record);
    if (isEmpty(id)) {
      id = uuid();
      record[this.idProperty] = id;
      this._setInsertMark(record);
    }
    const proxyRecord = this._toReactiveRecord(this.transformRecord(record, type));
    this.dataMap.set(id, proxyRecord);
    this.sourceMap.set(id, cloneDeep(this._toRawRecord(proxyRecord)));
    return proxyRecord;
  }
  /**
   * 删除缓存数据
   * @param {*} record
   */
  _removeCache(record) {
    const id = this.getRecordId(record);
    this.dataMap.delete(id);
    this.sourceMap.delete(id);
  }
  /**
   * 清空缓存数据
   */
  _clearCache() {
    this.dataMap.clear();
    this.sourceMap.clear();
  }
  /**
   * 原始数据
   */
  _toRawRecord(record) {
    return useJE().useVue()?.toRaw?.(record) || record;
  }
  /**
   * 响应数据
   */
  _toReactiveRecord(record) {
    return useJE().useVue()?.reactive?.(record) || record;
  }
}
