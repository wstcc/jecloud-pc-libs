import TreeNode from '../model/tree-node';
import Store from './base-store';
/**
 * TreeStore 数据集
 * @module TreeStore
 */
export default class TreeStore extends Store {
  /**
   * ROOT节点
   */
  _rootNode = new TreeNode({ text: 'ROOT', id: 'ROOT', code: 'ROOT', nodeType: 'ROOT' });
  get rootNode() {
    this._rootNode.children = this.data;
    return this._rootNode;
  }

  /**
   * 构造参数
   * @param {Object} options 配置信息
   * @param {string} options.idProperty 主键名
   * @param {string} options.url 请求链接
   * @param {string} [options.params] 请求参数
   */
  constructor(options = {}) {
    super(options);
    this.isTreeStore = true;
    /**
     * 文本字段属性
     */
    this.textProperty = options.textProperty || 'text';
    /**
     * 编码字段属性
     */
    this.codeProperty = options.codeProperty || 'code';
    /**
     * 父节点字段属性
     */
    this.parentProperty = options.parentProperty || 'parent';
    /**
     * 子节点属性
     */
    this.childrenProperty = options.childrenProperty || 'children';
  }

  /**
   * 节点路径
   * @param {Object} record
   * @param {string} pathField 路径字段
   * @param {boolean} includeSelf 包含当前节点信息
   * @returns String
   */
  getNodePath(record, pathField = this.idProperty, includeSelf = true) {
    return this.getNodePathArray(record, pathField, includeSelf).join('/');
  }

  /**
   * 节点路径数组
   * @param {Object} record
   * @param {string} pathField 路径字段
   * @param {boolean} includeSelf 包含当前节点信息
   * @returns Array
   */
  getNodePathArray(record, pathField = this.idProperty, includeSelf = true) {
    const paths = [];
    const item = this.getRecordById(this.getRecordId(record));
    this.cascadeParent(item, (parent) => {
      paths.push(parent[pathField]);
    });
    // 不包含当前节点，删除
    if (!includeSelf && paths.length > 0) {
      paths.shift();
    }
    return paths.reverse();
  }
  /**
   * 给节点添加子节点
   * @param {*} records
   * @param {*} row
   * @returns
   */
  appendChild(records = [], row) {
    if (!Array.isArray(records)) {
      records = [records];
    }
    // 响应式对象
    records = records.map((record) => this._addCache(record, true));
    // 处理数据
    const data = this.data.slice(0);
    if (row) {
      row.appendChild(records);
    } else {
      data.push(...records);
    }
    this.data = data;
    return records;
  }

  /**
   * 级联数据处理
   * @param {Function} fn 处理函数
   * @returns
   */
  cascade(fn) {
    this.rootNode.cascade(fn, false);
  }

  /**
   * 级联子节点数据处理
   * @param {Object} record 节点
   * @param {Function} fn 处理函数
   * @param {boolean} includeSelf 包含当前节点信息
   * @returns
   */
  cascadeChild(record, fn, includeSelf = true) {
    record.cascade(fn, includeSelf);
  }

  /**
   * 级联父节点
   * @param {*} record
   * @param {*} fn
   * @param {boolean} includeSelf 包含当前节点信息
   */
  cascadeParent(record, fn, includeSelf = true) {
    let parent = this.getRecordById(this.getRecordId(record));
    while (parent) {
      if ((!includeSelf && parent.id === record.id) || fn?.(parent) !== false) {
        parent = this.getRecordById(parent[this.parentProperty]);
      } else {
        break;
      }
    }
  }
  /* -------------------------override------------------------- */
  /**
   * 加载数据
   * @override
   * @param {*} data
   */
  loadData(data = []) {
    let nodes = data;
    if (!Array.isArray(data)) {
      nodes = data[this.childrenProperty] || [];
    }

    this._clearCache();
    this.data = nodes.map((node) => {
      return new TreeNode(node);
    });
    this.cascade((record) => {
      this._addCache(record);
    });
  }

  /**
   * 格式化record，可以重写
   * @override
   * @param {*} record
   * @returns
   */
  transformRecord(record) {
    return new TreeNode(record);
  }
  /**
   * 获取兄弟节点，可以重写
   * @override
   * @returns Array
   */
  getBrotherRecords(id, data = this.data) {
    let items = [];
    const record = this.getRecordById(id);
    record &&
      data.forEach((item) => {
        items.length === 0 &&
          item.cascade((node) => {
            if (this.getRecordId(node) === record[this.parentProperty]) {
              items = node[this.childrenProperty] || [];
              return false;
            }
          });
      });
    return items;
  }
}
