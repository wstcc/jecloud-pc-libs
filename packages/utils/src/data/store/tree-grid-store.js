import TreeStore from './tree-store';
import TreeNode from '../model/tree-node';
import { isNotEmpty, isBoolean } from '../../index';
/**
 * TreeStore 数据集
 * @module TreeStore
 */
export default class TreeGridStore extends TreeStore {
  /**
   * 构造参数
   * @param {Object} options 配置信息
   * @param {string} options.idProperty 主键名
   * @param {string} options.url 请求链接
   * @param {string} [options.params] 请求参数
   */
  constructor(options = {}) {
    super(options);
  }

  /**
   * 格式化record，可以重写
   * @override
   * @param {*} record
   * @returns
   */
  transformRecord(record, type) {
    if (type) {
      return new TreeNode(record);
    }
    return record;
  }

  /**
   * 获得修改数据
   * @param {*} record
   * @param {*} includeInsert 包含添加数据
   * @returns
   */
  getChanges(record, includeInsert) {
    const changes = [];
    let records = [];
    // 兼容数据
    if (isBoolean(record)) {
      includeInsert = record;
      record = null;
    }
    if (record) {
      records = [record];
    } else {
      records = Array.from(this.dataMap.values());
    }
    records.forEach((row) => {
      // 获取fields配置
      if (!this.fields) {
        // 排出children属性
        this.fields = Object.keys(row)
          .filter((item) => item != 'children')
          .map((key) => ({ name: key }));
      }
      const recordChanges = {};
      const newRow = this._toRawRecord(row);
      const id = this.getRecordId(newRow);
      // 历史数据
      const oldRow = this._toRawRecord(this.sourceMap.get(id)) || {};
      // 新增数据
      if (includeInsert && this.isInsertRecord(newRow)) {
        Object.keys(newRow).forEach((name) => {
          recordChanges[name] = {
            field: name,
            oldValue: newRow[name],
            newValue: newRow[name],
          };
        });
      } else {
        this.fields.forEach((field) => {
          const name = field.name || field;
          const oldValue = oldRow[name];
          const newValue = newRow[name];
          if (!this.eqCellValue(oldRow, newRow, name)) {
            recordChanges[name] = {
              field: name,
              oldValue,
              newValue,
            };
          }
        });
      }
      if (isNotEmpty(recordChanges)) {
        recordChanges._record = newRow;
        changes.push(recordChanges);
      }
    });
    return changes;
  }
}
