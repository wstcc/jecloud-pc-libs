import { ajax, transformAjaxData } from '../../../http';
import { pick } from 'lodash-es';
import { WorkflowOperatorEnum } from '../enum';
import * as urls from './urls';

export function useWorkflowApi() {
  return { getUrgeDataApi };
}

/**
 * 获取流程信息
 * @param {*} funcCode 功能编码
 * @param {*} tableCode 表编码
 * @param {*} beanId 主键
 * @param {*} prod 产品编码
 * @returns
 */
export function getWorkflowInfoApi({ funcCode, tableCode, beanId, prod }) {
  return baseAjax(urls.API_WORKFLOW_GET_INFO, { funcCode, tableCode, beanId, prod });
}

/**
 * 获取提交节点信息
 * @param {*} taskId 当前任务id
 * @param {*} taskId 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} prod 产品编码
 * @returns
 */
export function getSubmitNodeInfoApi({ taskId, pdid, beanId, tableCode, prod }) {
  return baseAjax(urls.API_WORKFLOW_GET_SUBMIT_NODE_INFO, {
    taskId,
    pdid,
    beanId,
    tableCode,
    prod,
  });
}
/**
 * 获取节点人员信息
 * @param {*} taskId 当前任务id
 * @param {*} pdid 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} prod 产品编码
 * @param {*} target 产品编码
 * @returns
 */
export function getSubmitNodeUsersApi(options) {
  return baseAjax(urls.API_WORKFLOW_GET_SUBMIT_NODE_USERS, options);
}

/**
 * 获取流程按钮操作参数详情
 * @param {*} operationId 按钮操作ID
 * @returns
 */
export function getButtonParamsApi({ operationId }) {
  return baseAjax(urls.API_WORKFLOW_BUTTON_GET_PARAMS, { operationId });
}

/**
 * 执行流程按钮操作
 * @param {*} operationId 按钮操作ID
 * @param {*} pdid 流程部署id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} funcCode 功能编码
 * @param {*} prod 流程部署id
 * @returns
 */
export function doButtonOperateApi(options) {
  return baseAjax(urls.API_WORKFLOW_BUTTON_OPERATE, options);
}
/**
 * 获取任务节点信息
 * @param {*} operationId 操作id
 * @param {*} pdid 流程部署id
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @returns
 */
export function getTaskNodeInfoApi(options) {
  let url = '';
  const { operationId } = options;
  switch (operationId) {
    case WorkflowOperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      url = urls.API_WORKFLOW_GET_GOBACK_NODE;
      break;
    case WorkflowOperatorEnum.TASK_RETREIEVE_OPERATOR:
      url = urls.API_WORKFLOW_GET_RETRIEVE_NODE; // 取回
      break;
    case WorkflowOperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
    case WorkflowOperatorEnum.TASK_SIGN_BACK_OPERATOR: //回签
    case WorkflowOperatorEnum.TASK_STAGING_OPERATOR: // 暂存
      url = urls.API_WORKFLOW_GET_DELEGATION_NODE;
      break;
    case WorkflowOperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      url = urls.API_WORKFLOW_GET_DIRECT_DELIVERY_NODE;
      break;
    case WorkflowOperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
      url = urls.API_WORKFLOW_GET_REJECT_NODE;
      break;
  }
  return url && baseAjax(url, pick(options, ['pdid', 'piid', 'taskId']));
}

/**
 * 获取流程传阅人
 * @param {*} pdid 流程部署id
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @param {*} beanId 主键
 * @param {*} tableCode 表编码
 * @param {*} funcCode 功能编码
 * @param {*} prod 流程部署id
 * @returns
 */
export function getPassroundUsersApi(options) {
  return baseAjax(urls.API_WORKFLOW_GET_PASSROUND_USERS, options);
}

/**
 * 获取会签操作人
 * @param {*} piid 流程实例id
 * @param {*} taskId 当前任务id
 * @returns
 */
export function getCounterSignerOperationalUsersApi(options) {
  return baseAjax(
    urls.API_WORKFLOW_GET_COUNTER_SIGNER_OPERATIONAL_USERS,
    pick(options, ['piid', 'taskId']),
  );
}

/**
 * 获得流程历史
 * @param {*} beanId 功能业务id
 * @returns
 */
export function loadWorkflowHistoryApi(options) {
  return baseAjax(urls.API_WORKFLOW_GET_CIRCULATION_HISTORY, pick(options, ['beanId']));
}

/**
 * 获得流程图
 * @param {*} beanId 功能业务id
 * @param {*} pdId 工作流id
 * @returns
 */
export function previewImageApi(params) {
  // 时间戳，防止缓存
  params._t = new Date().getTime();
  return ajax({
    url: urls.API_WORKFLOW_PREVIEW_IMAGE,
    params,
    method: 'GET',
  }).then(transformAjaxData);
}

/**
 * 获得会签节点的信息
 * @param {*} taskId 单前任务id
 * @returns
 */
export function getCountersignApprovalOpinionApi(options) {
  return baseAjax(urls.API_WORKFLOW_GET_COUNTERSIGN_APPROVAL_OPINION, options);
}

/**
 * 获得传阅流程历史
 * @param {*} params
 * @returns
 */
export function getPassRoundUsersListByTaskIdApi(params) {
  return baseAjax(urls.API_WORKFLOW_GET_PASSROUND_USERS_LIST_BY_TASKID, params);
}

/**
 * 获得提醒方式的数据
 * @param {*} params
 * @returns
 */
export function getMessageTypeApi(params) {
  return baseAjax(urls.API_WORKFLOW_GET_MESSAGE_TYPE, params);
}

/**
 * 获得催办历史
 * @param {*} params
 * @returns
 */
export function getUrgeDataApi(params) {
  return baseAjax(urls.API_WORKFLOW_GET_URGE_DATA, params);
}

/** 获得被催办人员
 * @param {*} params
 * @returns
 */
export function getAssigneeByTaskIdApi(params) {
  return baseAjax(urls.API_WORKFLOW_GET_ASSINGNEE_BY_TASKID, params);
}

/** 获取流程常用人员
 * @returns
 */
export function getCommonUsersApi() {
  return baseAjax(urls.API_WORKFLOW_GET_COMMON_USER);
}
/**
 * 基础ajax
 * @param {*} url
 * @param {*} params
 * @returns
 */
function baseAjax(url, params) {
  return ajax({
    url: url,
    params,
  }).then(transformAjaxData);
}
