import {
  doButtonOperateApi,
  getCountersignApprovalOpinionApi,
  getSubmitNodeUsersApi,
  getCounterSignerOperationalUsersApi,
  getSubmitNodeInfoApi,
  getMessageTypeApi,
  getAssigneeByTaskIdApi,
  getPassroundUsersApi,
} from '../api';
import { Modal } from '../../../modal';
import { Data } from '../../../data';
import { getDDItemInfo } from '../../../system/dictionary';
import { isNotEmpty, createDeferred, isEmpty } from '../../../lodash';
import { WorkflowOperatorEnum } from '../enum';
import TaskNode from '../model/task-node';
import { doEvents4Promise } from '../../util';
import WorkflowOperation from '../model/workflow-operation';
import TaskAssignee from '../model/task-assignee';

export function useOperatorAction() {
  return {
    doButtonOperation,
    doButtonSubmit,
  };
}
/**
 * 按钮执行操作
 * @param {*} $workflow
 * @param {*} options
 * @returns
 */
function doButtonOperation($workflow, options) {
  const { button } = options;
  $workflow.setCurrentOperationButton(button); // 设置操作按钮
  return beforeButtonOperation($workflow)
    .then(() => beforeButtonEvent($workflow))
    .then(() => buttonOperation($workflow, options));
}

/**
 * 按钮提交操作
 * @private
 * @param {WorkflowManager} $workflow 流程对象
 * @param {Object} [options] 自定义配置
 * @param {Object} options.button 操作按钮，用于操作成功后的提示
 * @param {Object} options.params 自定义参数，追加到默认参数
 * @returns
 */
function doButtonSubmit($workflow, options = {}) {
  const { params = {}, operation } = options;
  const submitParams = $workflow.getSubmitParams(params, operation);
  $workflow.state.submitting = true;
  return doButtonOperateApi(submitParams)
    .then((data) => {
      afterButtonSubmit($workflow, data);
      const button = options.button || $workflow.getCurrentOperationButton();
      Modal.notice(`${button.name || button.text}成功！`, 'success');
      return { data, success: true };
    })
    .catch((e) => {
      Modal.alert(e.message, 'error');
      return Promise.reject(e);
    })
    .finally((info) => {
      $workflow.state.submitting = false;
      operation?.finishSubmit?.(info);
    });
}

/**
 * 流程按钮操作前处理
 * 用于表单保存数据
 * @param {*} $workflow
 * @param {*} param1
 */
function beforeButtonOperation($workflow) {
  const { $func } = $workflow;
  const deferred = createDeferred();
  // 如果表单保存按钮没有隐藏并且表单有数据修改,先执行表单保存再提交流程
  if ($func) {
    // 表单对象
    const $form = $func.getFuncForm();
    // 表单保存按钮
    const saveBtn = $form?.getButtons('formSaveBtn');
    if ($form && saveBtn && !saveBtn.hidden) {
      // 表单是否修改
      const changeData = $form?.getChanges();
      if (isNotEmpty(changeData)) {
        $func.action.doFormSave().then(deferred.resolve).catch(deferred.reject);
      } else {
        $func.action.doFormValidate().then(deferred.resolve).catch(deferred.reject);
      }
    } else {
      // 提交流程
      deferred.resolve();
    }
  } else {
    // 提交流程
    deferred.resolve();
  }
  return deferred.promise;
}

/**
 * 按钮操作
 * @returns Promise({success,data})
 * 如果系统默认操作，success为true，如果系统没有操作，需要根据环境判断自行操作
 */
function buttonOperation($workflow, options) {
  const { button } = options;
  const params = $workflow.getOperationParams(); // 操作参数
  const customParams = { custom: true }; // 自定义操作参数
  const deferred = createDeferred();
  switch (button.operationId) {
    case WorkflowOperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      Modal.confirm('是否要撤销流程（撤销后将删除审批记录）！', () => {
        doButtonSubmit($workflow).then(deferred.resolve).catch(deferred.reject);
      });
      break;
    case WorkflowOperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
      doExigencyState({ $workflow, button }).then(() => {
        doButtonSubmit($workflow).then(deferred.resolve).catch(deferred.reject);
      });
      break;
    case WorkflowOperatorEnum.PROCESS_EMPTY_SPONSOR_OPERATOR: // 发起
      doWorkflowJudge({ $workflow, button }).then(() => {
        deferred.resolve(customParams);
      });
      break;
    case WorkflowOperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
    case WorkflowOperatorEnum.TASK_CLAIM_OPERATOR: // 领取
    case WorkflowOperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
      doButtonSubmit($workflow).then(deferred.resolve).catch(deferred.reject);
      break;
    case WorkflowOperatorEnum.TASK_URGE_OPERATOR: // 催办
      loadTaskUrgeData($workflow).then((data) => {
        customParams.urgeData = data;
        deferred.resolve(customParams);
      });
      break;
    case WorkflowOperatorEnum.TASK_PASSROUND_OPERATOR: //传阅
      loadTaskpassRoundUsers($workflow).then((data) => {
        customParams.passRoundUsers = data;
        deferred.resolve(customParams);
      });
      break;
    case WorkflowOperatorEnum.TASK_REBOOK_OPERATOR: //改签
      doRebookOperation({ params }).then((data) => {
        doButtonSubmit($workflow, data).then(deferred.resolve).catch(deferred.reject);
      });
      break;
    case WorkflowOperatorEnum.TASK_CHANGE_OPERATOR: // 切换任务
      const currentTaskId = $workflow.state.currentTaskNode.id;
      const tasks = $workflow.state.tasks;
      deferred.resolve({
        currentTaskId,
        tasks,
        ...customParams,
      });
      break;
    case WorkflowOperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR: // 人员调整
    case WorkflowOperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR: // 更换负责人
      doChangeUsersOperation({ params, button })
        .then((data) => ({ ...data, ...customParams }))
        .then(deferred.resolve)
        .catch(deferred.reject);
      break;
    default: // 打开审批页面
      doSimpleApproval({ $workflow, button }).then(() => {
        deferred.resolve(customParams);
      });

      break;
  }

  return deferred.promise;
}

/**
 * 弹窗审批面板前处理
 * @param {*} param0
 * @returns
 */
const doWorkflowJudge = ({ button, $workflow }) => {
  const defeered = createDeferred();
  doExigencyState({ button, $workflow }).then(() => {
    doSimpleApproval({ button, $workflow }).then(() => {
      defeered.resolve();
    });
  });
  return defeered.promise;
};
/**
 *  简易审批
 * @param {*} param0
 * @returns
 */
const doSimpleApproval = ({ button, $workflow }) => {
  const { workflowConfig } = button;
  const defeered = createDeferred();
  // TODO 没有接入展示参数
  const simpleType = workflowConfig?.simpleApproval || button.simpleApproval;
  // 简易审批需要弹窗意见框
  const enableSimpleComments = workflowConfig?.enableSimpleComments || button.enableSimpleComments;
  // 没有开启简易审批
  if (!simpleType) {
    defeered.resolve();
  } else {
    // 简易审批需要弹窗意见框,正常走审批
    if (enableSimpleComments) {
      defeered.resolve();
    } else {
      const params = $workflow.getOperationParams();
      // 如果启用了简易审批,通过一下判断才需要弹出审批窗口,否则直接通过。
      //有且只有一个节点,并且该节点下的人只有一个
      //1.获得流程节点信息
      getSubmitNodeInfoApi(params).then((data) => {
        if (data && data.length == 1) {
          //如果是结束节点
          if (data[0].type == 'end') {
            // 直接提交
            const operation = new WorkflowOperation({
              $workflow,
              button,
            });
            operation.state.taskNodes = new TaskNode(data[0]);
            operation.setActiveNode(operation.state.taskNodes);
            doButtonSubmit($workflow, {
              operation,
            });
          } else {
            //如果是1个节点需要继续判断节点
            const nodeParams = $workflow.getOperationParams({ target: data[0].target });
            getSubmitNodeUsersApi(nodeParams).then((nodeData) => {
              const { nodeAssigneeInfo } = nodeData;
              if (nodeAssigneeInfo && nodeAssigneeInfo.length == 1) {
                // 如果是1个节点,需要继续判断有几种人员
                const { users } = nodeAssigneeInfo[0];
                if (users && users.length == 1) {
                  // 只有一种人员,需要判断下面有多少人
                  const { user } = users[0];
                  let userNodes = [];
                  const getPersonNum = (user) => {
                    if (user.nodeInfoType == 'json') {
                      userNodes.push(user);
                    }
                    if (userNodes.length < 2) {
                      user.children.forEach((item) => {
                        getPersonNum(item);
                      });
                    }
                  };
                  getPersonNum(user);
                  if (userNodes.length == 1) {
                    // 只有一个人就直接调接口
                    const operation = new WorkflowOperation({
                      $workflow,
                      button,
                    });
                    operation.state.taskNodes = new TaskNode(data[0]);
                    operation.setActiveNode(operation.state.taskNodes);
                    // 送交信息
                    const taskAssignee = new TaskAssignee({
                      ...operation.state.taskNodes,
                      users: userNodes,
                    });
                    // 有多人弹出审批窗口
                    doButtonSubmit($workflow, {
                      operation,
                      params: {
                        assignee: TaskAssignee.parseAssigneeParam([taskAssignee]),
                      },
                    });
                  } else {
                    // 有多人弹出审批窗口
                    defeered.resolve();
                  }
                } else {
                  // 有多种人员就弹出审批窗口
                  defeered.resolve();
                }
              } else {
                // 如果该节点下面有多个节点那么弹出审批窗口(分支,聚合)
                defeered.resolve();
              }
            });
          }
        } else {
          // 如果是2个节点及以上那么弹出审批窗口
          defeered.resolve();
        }
      });
    }
  }

  return defeered.promise;
};
/**
 * 选取紧急状态
 */
const doExigencyState = ({ button, $workflow }) => {
  const defeered = createDeferred();
  // TODO 没有接入展示参数
  const exigencyType = button.exigency;
  if (!exigencyType) {
    defeered.resolve();
  } else {
    const { id } = button;
    // 启动按钮选取紧急状态
    if (['startBtn'].includes(id)) {
      Modal.dialog({
        title: '紧急状态',
        content: '请选取紧急状态？',
        buttons: [
          {
            text: '一般',
            type: 'primary',
            handler: () => {
              $workflow.state.exigency = 'ordinary';
              defeered.resolve();
            },
          },
          {
            text: '急',
            type: 'danger',
            handler: () => {
              $workflow.state.exigency = 'anxious';
              defeered.resolve();
            },
          },
          {
            text: '紧急',
            type: 'danger',
            handler: () => {
              $workflow.state.exigency = 'urgency';
              defeered.resolve();
            },
          },
          { text: '取消' },
        ],
      });
    } else {
      defeered.resolve();
    }
  }
  return defeered.promise;
};

/**
 * 流程完成后操作
 * @param {*} $workflow
 * @param {*} param1
 */
function afterButtonSubmit($workflow, { bean }) {
  afterButtonEvent($workflow);
  const { $func, state } = $workflow;
  if (bean) {
    // 更新表单数据
    $func.store.setActiveBean(bean);
    // 更新列表数据
    const funcGrid = $func.getFuncGrid();
    funcGrid && funcGrid.store.commitRecord(bean);
  }

  // 同步刷新列表
  if (state?.currentTaskNode?.listSynchronization) {
    $func?.getFuncGrid()?.store.reload();
  }
}
/**
 * 按钮操作前事件
 * @param {*} $workflow
 */
function beforeButtonEvent($workflow) {
  const $func = $workflow.$func;
  const $form = $func.getFuncForm();
  const button = $workflow.getCurrentOperationButton();
  const code = button?.getBeforeEvent();
  if (code) {
    return doEvents4Promise({ $func, code, eventOptions: { button, $form } });
  } else {
    return Promise.resolve();
  }
}
/**
 * 按钮操作后事件
 * @param {*} $workflow
 */
function afterButtonEvent($workflow) {
  const $func = $workflow.$func;
  const $form = $func.getFuncForm();
  const button = $workflow.getCurrentOperationButton();
  const code = button?.getAfterEvent();
  if (code) {
    return doEvents4Promise({ $func, code, eventOptions: { button, $form } });
  } else {
    return Promise.resolve();
  }
}

/**
 * 改签操作
 * @private
 * @param {*} param0
 */
function doRebookOperation({ params }) {
  //查询该会签节点的审批状态
  return getCountersignApprovalOpinionApi({ taskId: params.taskId }).then((data) => {
    const deferred = createDeferred();
    Modal.dialog({
      content: '是否要进行改签重新投票！',
      title: '改签',
      buttons: [
        {
          text: '通过',
          code: 'PASS',
          type: 'primary',
          disabled: data == 'PASS',
          handler: () => {
            deferred.resolve({ button: { text: '通过' }, params: { opinionType: 'PASS' } });
          },
        },
        {
          text: '否决',
          type: 'primary',
          code: 'VETO',
          disabled: data == 'VETO',
          handler: () => {
            deferred.resolve({ button: { text: '否决' }, params: { opinionType: 'VETO' } });
          },
        },
        {
          text: '弃权',
          type: 'primary',
          code: 'ABSTAIN',
          disabled: data == 'ABSTAIN',
          handler: () => {
            deferred.resolve({ button: { text: '弃权' }, params: { opinionType: 'ABSTAIN' } });
          },
        },
        {
          text: '取消',
        },
      ],
    });

    return deferred.promise;
  });
}

/**
 * 会签-更换负责人，调整人员
 * @private
 */
function doChangeUsersOperation({ button, params }) {
  const { currentNode } = button.workflowConfig;
  const target = currentNode.id;
  // 获取会签人员信息
  return Promise.all([
    getSubmitNodeUsersApi({ ...params, target }),
    getCounterSignerOperationalUsersApi(params),
  ]).then(([assigneeInfo, resultUser]) => {
    // 会签负责人
    const manager = { id: resultUser.oneBallotUserId, text: resultUser.oneBallotUserName };
    // 会签参与人员
    const operators = resultUser.usersInfo?.map((item) => {
      // 格式化状态信息
      if (item.opinion) {
        const stateDD = getDDItemInfo('JE_WF_OPERATION_TYPE', item.opinion);
        item.opinionColor = stateDD.textColor;
        item.opinionText = stateDD.text;
      }
      return item;
    });
    // 目标节点信息
    const { workflowConfig, users } = assigneeInfo.nodeAssigneeInfo[0];
    const node = new TaskNode({ assigneeNode: true, ...workflowConfig });
    // 更换负责人，设置用户数据
    const isChangeManager =
      button.operationId === WorkflowOperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR;
    if (isChangeManager) {
      node.multiple = false;
      node.users = [
        {
          text: node.name,
          id: node.id,
          children: operators.map((item) =>
            Data.createTreeNodeModel({
              id: item.userId,
              text: item.userName,
              nodeInfoType: 'json',
              bean: { ACCOUNT_AVATAR: item.userAvatar },
            }),
          ),
        },
      ];
    } else {
      // 调整人员，使用默认人员
      node.setUsers(users);
    }
    return { manager, operators, node, isChangeManager };
  });
}

/**
 * 获得催办类型和催办人员数据
 */
function loadTaskUrgeData($workflow) {
  // 获得参数
  const operationParams = $workflow.getOperationParams();
  // 获得提醒类型接口参数
  const messageParams = {
    beanId: operationParams.beanId,
    pdid: operationParams.pdid,
    prod: operationParams.prod,
    tableCode: operationParams.tableCode,
  };
  // 获得催办人员接口数据
  const assigneeParams = {
    taskId: operationParams.taskId,
    currentNodeId: $workflow.state.currentTaskNode?.id || '',
  };
  // 调用获得提醒类型和催办人员接口
  return Promise.all([
    getMessageTypeApi(messageParams),
    getAssigneeByTaskIdApi(assigneeParams),
  ]).then(([messages, users]) => {
    // 消息提醒类型数据
    const urgeMessages = messages?.message || [];
    // 默认的提醒内容
    const urgeContent = messages?.canUrged || '';
    // 催办人数据
    const urgeUsers = [];
    users.forEach((item) => {
      item.value = item.id;
      item.label = item.name;
      urgeUsers.push(item);
    });
    // 消息提醒类型默认选中的数据
    let selectMessages = '';
    // 根据数据给提醒方式赋值
    if (messages?.message && messages?.message?.length > 0) {
      const noticeData = [];
      messages?.message.forEach((item) => {
        if (item.checked == 'true') {
          noticeData.push(item.value);
        }
      });
      if (noticeData.length > 0) {
        selectMessages = noticeData.join(',');
      }
    }
    return { urgeMessages, urgeContent, urgeUsers, selectMessages };
  });
}
/**
 * 获得传阅的人员
 * @param {*} $workflow
 * @returns
 */
function loadTaskpassRoundUsers($workflow) {
  return getPassroundUsersApi($workflow.getOperationParams()).then((data) => {
    return data;
  });
}
