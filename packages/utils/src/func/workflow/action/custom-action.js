import TaskAssignee from '../model/task-assignee';
export function useCustomAction() {
  return {
    doChangeUsersSubmit,
    doTaskPassRoundSubmit,
  };
}

/**
 * 会签-更换负责人，调整人员 提交操作
 * @param {*} $workflow
 * @param {*} param1
 */
function doChangeUsersSubmit($workflow, { users, operators, manager, isChangeManager }) {
  let assignees;
  if (isChangeManager) {
    // 更换负责人
    assignees = users;
  } else {
    // 人员调整

    // 已操作人员
    assignees = operators
      .filter((item) => item.opinion)
      .map((item) => ({ id: item.userId, text: item.userName }));
    // 负责人
    if (manager.id) {
      assignees.push(manager);
    }
    // 调整人员
    const assigneeIds = assignees.map(({ id }) => id);
    // 排除已审批人员和负责人
    assignees.push(...users.filter((user) => !assigneeIds.includes(user.id)));
  }
  // 送交信息
  const taskAssignee = new TaskAssignee({ ...$workflow.state.currentTaskNode, users: assignees });

  // 送交
  return $workflow.action.doButtonSubmit({
    params: {
      assignee: TaskAssignee.parseAssigneeParam([taskAssignee]),
    },
  });
}

/**
 * 传阅提交操作
 * @param {*} $workflow
 * @param {*} param1
 */
function doTaskPassRoundSubmit($workflow, { users }) {
  // 送交信息
  const taskAssignee = new TaskAssignee({
    ...$workflow.state.currentTaskNode,
    users,
  });

  // 送交;
  return $workflow.action.doButtonSubmit({
    params: {
      assignee: TaskAssignee.parseAssigneeParam([taskAssignee]),
    },
  });
}
