/**
 * 流程表单配置
 */
export default class WorkflowFormConfig {
  get isModel() {
    return true;
  }
  constructor(options) {
    options = options || {};
    if (options.isModel) return options;
    /**
     * 按钮配置
     */
    this.buttonConfig = parseConfigToObject(options.formButtonList);
    /**
     * 字段配置
     */
    this.fieldConfig = parseConfigToObject(options.formFieldList);
    /**
     * 子功能配置
     */
    this.childFuncConfig = parseConfigToObject(options.formChildFuncs);
    /**
     * 表单只读
     */
    this.formEditable = options.formEditable;
  }
}

function parseConfigToObject(items = []) {
  const config = {};
  const attrs = ['enable', 'display', 'editable', 'hidden', 'readonly', 'required', 'readOnly'];
  items.forEach((item) => {
    // 提取有效对象
    const validItem = attrs.find((attr) => item[attr]);
    if (validItem) {
      config[item.code] = item;
    }
  });
  return config;
}
