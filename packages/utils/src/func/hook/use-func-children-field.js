import { toQuerysTemplate, isNotEmpty, decode, isEmpty, debounce } from '../../lodash';
import { useListeners } from '../../hooks';
import { parseConfigInfo } from '../util';
import { vueCall } from '../../je';
import { Modal } from '../../modal';
import {
  useManager,
  useInjectFunc,
  FuncRefEnum,
  FuncButtonTypeEnum,
  parseEventResult4Promise,
  loadFuncInfoApi,
  useProvideFunc,
  useWatchActiveBean,
  setFuncManager,
} from '../func';
/**
 * 子功能集合
 * @param {*} param0
 * @returns
 */
export function useFuncChildrenField({ props, context }) {
  const { expose, emit } = context;
  const { fireListener } = useListeners({ props, context });
  const gridRef = vueCall('ref');
  const { actionColumn = {}, fkCode, funcCode, parentFields = [], childFields = [] } = props;
  const {
    remove = '1',
    removes = '0',
    add = '1',
    adds = '0',
    drag,
    addsType = 'grid',
    addsConfigInfo,
    buttonAlign = 'right',
  } = actionColumn;
  const state = vueCall('reactive', {
    loading: true,
    errorMessage: '',
    events: {},
    enableRemove: remove == '1',
    enableRemoves: removes == '1',
    enableAdd: add == '1',
    enableAdds: adds == '1' && addsType && addsConfigInfo,
    enableDrag: drag == '1',
    buttonAlign,
    addsType,
    addsConfigInfo,
    fkCode,
    funcCode,
    parentFields, // 主子带值配置
    childFields, // 主子带值配置
  });
  const $parentFunc = useInjectFunc();
  const $func = useManager({ funcCode, props, context });
  const { watchParentBean } = useWatchActiveBean({ $func, exec: false });
  watchParentBean();
  // 追加子功能集合字段
  $parentFunc?.addChildFuncField(funcCode, $func);
  // 装载列表
  $func.setRefMaps(FuncRefEnum.FUNC_GRID, gridRef);
  // 对子暴露功能
  useProvideFunc($func);
  setFuncManager($func);
  // 子功能集合字段
  const $childField = {
    $func,
    $parentFunc,
    gridRef,
    props,
    context,
    state,
    action: {},
    /**
     * 添加数据
     * @param {*} param0
     */
    addRecords({ records = [], useDefaultValues = true }) {
      let data = [];
      // 增加默认值
      if (useDefaultValues) {
        const values = $childField.getDefaultValues();
        records.forEach((record) => {
          data.push({ ...values, ...record });
        });
      } else {
        data = records;
      }
      if (data.length) {
        // 添加数据
        return $func.action.doBaseGridInsert4Grid({ records: data }).then(({ insertRecords }) => {
          // 排序
          const $grid = $func.getFuncGrid();
          $grid.store.data.forEach((item, index) => {
            item.SY_ORDERINDEX = index + 1;
          });
          doChildHeight({ $childField });
          return insertRecords;
        });
      }
      return Promise.resolve([]);
    },
    /**
     * action事件
     * @param {*} eventName
     * @param {*} param1
     * @returns
     */
    doActionEvent(eventName, { actionType, actionOptions }) {
      const result = fireListener(eventName, {
        $fieldFunc: $func,
        $fieldGrid: $func.getFuncGrid(),
        actionType,
        actionOptions: actionOptions || {},
        model: props.model,
      });
      return parseEventResult4Promise(result);
    },
    /**
     * 加载功能数据
     * @returns
     */
    loadFunc() {
      // 加载功能数据
      let p = Promise.resolve();
      if (funcCode) {
        p = $func.loadFuncData();
      }
      return p;
    },
    /**
     * 获得父功能带值配置
     * @returns
     */
    getDefaultValues() {
      const values = {};
      const parentBean = props.model;
      // 带值配置
      parentBean &&
        state.parentFields?.forEach((parentField, index) => {
          const childField = state.childFields?.[index];
          if (childField) {
            // 防止主功能把已经修改的值覆盖掉
            values[childField] = parentBean[parentField];
          }
        });
      // 功能默认值
      const defValues = $func.getDefaultValues({
        buttonCode: FuncButtonTypeEnum.GRID_INSERT_BUTTON,
      });

      return { ...defValues, ...values };
    },
  };
  // 注册action函数
  const actions = { doActionAdd, doActionAdds, doActionRemove, doActionRemoves };
  Object.keys(actions).forEach((key) => {
    $childField.action[key] = actions[key].bind(this, $childField);
  });
  // 暴露对象
  expose($childField);
  // 追加功能函数
  Object.assign($func, {
    $field: $childField,
    entry: 'children-func',
    /**
     * 刷新功能
     */
    resetFunc: () => {
      // 修改中的数据，不做处理，业务自行处理
      if (!$func.doUpdateListIng) {
        gridRef.value?.reset();
      }
    },
    /**
     * 批量修改
     * @param {*} parentBean
     * @returns
     */
    doUpdateList: (parentBean) => doUpdateList({ parentBean, $childField }),
  });

  // change事件
  vueCall(
    'watch',
    () => gridRef.value?.store.data,
    // 防止频繁操作
    debounce(() => {
      vueCall('nextTick', () => {
        const options = {
          $field: $childField,
          $fieldFunc: $func,
          $fieldGrid: gridRef.value,
        };
        emit('change', options);
        // 用于功能业务事件处理，用户使用上面的change事件
        emit('private-change', options);
      });
    }, 200),
    {
      deep: true,
    },
  );

  return { state, gridRef, $childField, $func };
}

/**
 * 事件名称
 */
const ActionEvents = {
  BEFORE_ACTION: 'before-action',
  AFTER_ACTION: 'after-action',
};
/**
 * 添加
 * @param {*} $childField
 * @returns
 */
function doActionAdd($childField, bean) {
  const actionType = 'add';
  const { doActionEvent } = $childField;
  // 默认值
  const values = bean || $childField.getDefaultValues();
  return doActionEvent(ActionEvents.BEFORE_ACTION, { actionType, actionOptions: { values } }) // 操作前
    .then(() => $childField.addRecords({ records: [values], useDefaultValues: false })) //插入数据
    .then((insertRecords) =>
      // 操作后
      doActionEvent(ActionEvents.AFTER_ACTION, {
        actionType,
        actionOptions: { records: insertRecords },
      }),
    );
}
/**
 * 批量添加
 * @param {*} $childField
 */
function doActionAdds($childField) {
  const actionType = 'adds';
  const { doActionEvent, $func, state } = $childField;
  const { addsType, addsConfigInfo } = state;
  const childrenFuncCode = parseConfigInfo({ configInfo: addsConfigInfo })?.code;
  if (childrenFuncCode) {
    // 读取功能信息，加载标题
    loadFuncInfoApi({ funcCode: childrenFuncCode })
      .then((data) => {
        const selectConfig = {
          title: data.FUNCINFO_FUNCNAME,
          type: addsType,
          configInfo: addsConfigInfo,
          ...getAddsQuerys($childField),
        };
        // 操作前
        return doActionEvent(ActionEvents.BEFORE_ACTION, {
          actionType,
          actionOptions: selectConfig,
        })
          .then(() => $func.action.doBaseGridInsert4Multi(selectConfig)) // 批量选择
          .then(({ records }) => $childField.addRecords({ records, useDefaultValues: false })) // 批量插入
          .then((insertRecords) =>
            // 操作后
            doActionEvent(ActionEvents.AFTER_ACTION, {
              actionType,
              actionOptions: { records: insertRecords },
            }),
          )
          .catch((error) => {
            console.error(error);
          });
      })
      .catch(() => {
        Modal.alert(`功能【${childrenFuncCode}】不存在，请检查配置！`, 'error');
      });
  } else {
    Modal.alert(`批量添加配置为空，请检查配置！`, 'error');
  }
}
/**
 * 删除
 * @param {*} $childField
 * @param {*} options
 * @returns
 */
function doActionRemove($childField, options) {
  const actionType = 'remove';
  const { doActionEvent, $func } = $childField;
  const { row } = options;
  // 操作前
  return doActionEvent(ActionEvents.BEFORE_ACTION, { actionType, actionOptions: { row } })
    .then(() => $func.action.doBaseGridRemove({ row })) // 删除
    .then(() =>
      // 操作后
      doActionEvent(ActionEvents.AFTER_ACTION, { actionType, actionOptions: { row } }),
    );
}
/**
 * 批量删除
 * @param {*} $childField
 * @returns
 */
function doActionRemoves($childField) {
  const actionType = 'removes';
  const { doActionEvent, $func } = $childField;
  return doActionEvent(ActionEvents.BEFORE_ACTION, { actionType })
    .then(() => $func.action.doBaseGridRemove())
    .then(() => {
      doActionEvent(ActionEvents.AFTER_ACTION, { actionType });
    });
}

/**
 * 批量添加sql
 * @param {*} param0
 * @returns
 */
function getAddsQuerys($childField) {
  const { actionColumn = {}, model } = $childField.props;
  const { addsOtherConfig } = actionColumn;
  const { $parentFunc, $func } = $childField;
  const $grid = $func.getFuncGrid();
  const querys = { querys: [] };
  if (isNotEmpty(addsOtherConfig)) {
    // 如果配置了过滤条件
    if (isNotEmpty(addsOtherConfig.querys)) {
      const configQuerys = decode(addsOtherConfig.querys);
      // 父功能的父功能数据
      const parentModel = $parentFunc?.parentFunc?.store.activeBean || {};
      toQuerysTemplate({
        querys: configQuerys,
        model,
        parentModel,
      });
      querys.querys = configQuerys;
    }
    // 如果配置了唯一主键值
    if (isNotEmpty(addsOtherConfig.pkNames)) {
      const tableData = $grid.store.data || [];
      if (tableData.length > 0) {
        const values = [];
        tableData.forEach((item) => {
          values.push(item[addsOtherConfig.pkNames[1]]);
        });
        if (values.length > 0) {
          querys.querys.push({
            code: addsOtherConfig.pkNames[0],
            type: 'notIn',
            value: values,
          });
        }
      }
    }
  }
  return querys;
}

/**
 * 批量保存和修改
 * @param {*} param0
 */
function doUpdateList({ parentBean, $childField }) {
  const { $func, state } = $childField;
  const $grid = $func.getFuncGrid();
  if ($grid && parentBean) {
    // 增加修改标记
    $func.doUpdateListIng = true;
    const parentFuncData = $func.parentFunc.getFuncData();
    const parentPkCode = parentFuncData.info.pkCode;
    const parentPkValue = $func.parentFunc.store.activeBean?.[parentPkCode];
    // 外键
    const fkValue = parentBean[parentFuncData.info.pkCode];
    $grid.store.data.forEach((item) => {
      // 外键
      item[state.fkCode] = fkValue;
    });

    // 添加操作
    const doInsert = isEmpty(parentPkValue);
    // 操作事件
    const { doActionEvent } = $childField;
    const actionType = 'update';
    return doActionEvent(ActionEvents.BEFORE_ACTION, {
      actionType,
    })
      .then(() =>
        $func.action.doBaseGridUpdateList({
          showMessage: false,
          customParams: { subFuncForeignKey: state.fkCode },
        }),
      )
      .then((options) => {
        // 清除修改标记
        $func.doUpdateListIng = false;
        // 添加操作完成，刷新列表
        if (doInsert) {
          $func.resetFunc();
        } else {
          $grid.store.loadData($grid.store.data.slice());
        }
        // 操作后事件
        doActionEvent(ActionEvents.AFTER_ACTION, {
          actionType,
          actionOptions: options,
        });
      });
  }
  return Promise.resolve();
}

/**
 * 计算子功能高度
 */
function doChildHeight({ $childField }) {
  if (isNotEmpty($childField.props.pageSize)) {
    let num = 0;
    // 打开数据面板按钮高度
    if ($childField.props.maximize) {
      num = 22;
    }
    // 目前的高度
    const childHeight = parseInt($childField.style.height);
    // 根据pagesize计算的高度
    const customHeight = $childField.props.pageSize * 32 + 32 + num;
    const length = $childField.gridRef.value.getData().length;
    // 根据实际条数计算的高度
    const theoryHeight = length * 32 + 32 + num;
    if (theoryHeight > childHeight && childHeight < customHeight) {
      if (theoryHeight < customHeight) {
        $childField.style.height = theoryHeight;
      } else {
        $childField.style.height = customHeight;
      }
    }
  }
}
