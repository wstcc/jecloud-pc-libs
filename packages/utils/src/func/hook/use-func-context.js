import { vueCall } from '../../je';
export const FuncContextKey = Symbol('jeFuncContextKey');
export const FuncFormContextKey = Symbol('jeFuncFormContextKey');
export const FuncEditContextKey = Symbol('jeFuncEditContextKey');
export const MainFuncContextKey = Symbol('jeMainFuncContextKey');

export const useProvideFunc = (state) => {
  vueCall('provide', FuncContextKey, state);
};

export const useInjectFunc = () => {
  return vueCall('inject', FuncContextKey, null);
};
export const useProvideFuncForm = (state) => {
  vueCall('provide', FuncFormContextKey, state);
};

export const useInjectFuncForm = () => {
  return vueCall('inject', FuncFormContextKey, null);
};

export const useProvideFuncEdit = (state) => {
  vueCall('provide', FuncEditContextKey, state);
};

export const useInjectFuncEdit = () => {
  return vueCall('inject', FuncEditContextKey, null);
};

export const useProvideMainFunc = (state) => {
  vueCall('provide', MainFuncContextKey, state);
};

export const useInjectMainFunc = () => {
  return vueCall('inject', MainFuncContextKey, null);
};
