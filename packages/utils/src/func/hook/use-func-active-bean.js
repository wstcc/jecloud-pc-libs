import { vueCall } from '../../je';
export function useWatchActiveBean({ $func, exec = true }) {
  /**
   * 监听主功能的bean，刷新子功能数据
   */
  const watchParentBean = () => {
    $func.parentFunc &&
      vueCall(
        'watch',
        () => [$func.parentFunc.store.activeBean, $func.parentFunc.store.activeBeanEmitter],
        () => {
          vueCall('nextTick', () => {
            $func.resetFunc();
          });
        },
      );
  };

  /**
   * 监听bean
   */
  const watchBean = () => {
    vueCall(
      'watch',
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        // 执行表单审核
        $func.action.doFormAuditExecute();
        //判断表单是否有保存按钮,没有表单只读
        if (!$func.hasFormSavePerm()) {
          $func.getFuncForm()?.setReadOnly(true);
        }
      },
    );
  };

  /**
   * 监听表单bean，刷新流程信息
   */
  const watchBean4Workflow = () => {
    const { $workflow } = $func;
    vueCall(
      'watch',
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        const $form = $func.getFuncForm();
        if ($form && $workflow.isEnable()) {
          // 紧急状态为空
          $workflow.state.exigency = '';
          $workflow.refresh().then(() => {
            $workflow.refreshForm();
          });
        }
      },
    );
  };
  // 立即执行
  if (exec) {
    watchParentBean();
    watchBean();
    watchBean4Workflow();
  }
  return { watchBean, watchBean4Workflow, watchParentBean };
}
