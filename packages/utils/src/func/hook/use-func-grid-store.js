import { Data } from '../../data';
import { GridQueryParser } from '../model';
import { API_COMMON_LOAD, parseCommonUrl } from '../api';
/**
 * 功能列表store
 * @param {*} param0
 */
export function useFuncGridStore({ $func, querys, orders, pageSize, type }) {
  // 功能数据
  const funcData = $func.getFuncData();
  // 配置
  const {
    tableCode,
    funcCode,
    productCode = 'meta',
    pkCode,
    funcAction,
    gridOptions,
  } = funcData.info;
  const { timeout } = gridOptions;
  const fields = funcData.model;
  // 查询解析器
  const queryParser = new GridQueryParser({ querys, orders, $func, type });
  // 创建store
  const store = Data.useGridStore({
    fields,
    autoLoad: false,
    pageSize: pageSize || funcData.info.pageSize,
    idProperty: pkCode,
    proxy: {
      type: 'ajax',
      timeout: timeout > 0 ? timeout * 1000 : undefined,
      url: parseCommonUrl(API_COMMON_LOAD, funcAction),
      headers: { pd: productCode },
    },
  });
  // 绑定解析器
  store.queryParser = queryParser;
  // 查询前解析查询条件
  store.on('before-load', function ({ options }) {
    options.params = options.params || {};
    // 默认参数
    Object.assign(options.params, { tableCode, funcCode, ...queryParser.toJQuery() });
  });
  return store;
}
