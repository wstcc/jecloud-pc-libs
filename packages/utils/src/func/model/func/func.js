import FuncInfo from './func-info';
import Base from './base';
import DictionaryParam from '../query/dictionary-param';
import { parseConfigInfo } from '../../util';
import { get, pick, isNotEmpty } from '../../../lodash';
import { parseFunc, keyToData } from './func-parser';
/**
 *  功能数据
 *  所有解析数据，采用code存储，减少数据量
 */
export default class Func extends Base {
  /**
   * 基础数据
   *
   * @memberof Func
   */
  basic = {
    columns: new Map(),
    fields: new Map(),
    buttons: new Map(),
    ids: new Map(),
    children: new Map(),
    dictionarys: new Map(),
  };
  /**
   * 数据模型
   *
   * @memberof Func
   */
  model = [];
  /**
   * 查询策略
   *
   * @memberof Func
   */
  strategys = [];
  /**
   * 高级查询
   *
   * @memberof Func
   */
  groupQuerys = [];
  /**
   * 默认查询策略
   *
   * @memberof Func
   */
  defaultStrategy;
  /**
   * 列配置
   *
   * @memberof Func
   */
  columns = { func: [], select: [], data: [], statistics: [] };

  /**
   * 字段配置
   *
   * @memberof Func
   */
  fields = {
    form: [],
    anchor: [], //锚点
    groupQuery: [],
    childFunc: [],
    uploadFile: [],
    updateFiles: [],
    textcode: [],
    history: [],
    badge: [],
    codeGenFieldInfo: [], // 编号
  };

  /**
   * 按钮配置
   *
   * @memberof Func
   */
  buttons = { common: [], form: [], grid: [], group: [], locate: [], action: [] };

  /**
   * 表达式数据
   *
   * @memberof Func
   */
  exps = { fields: [], buttons: [], children: [] };
  /**
   * 绑定字段的表达式
   *
   * @memberof Func
   */
  bindExps = {};

  /**
   * 查询配置
   *
   * @memberof Func
   */
  querys = {
    func: {
      keyword: [], // 关键字查询
      mark: [], // 标记列
      tree: [], // 快速查询
      group: [], // 高级查询
      groupAll: [], // 高级查询所有展示列
    },
    select: {
      keyword: [], // 关键字查询
      mark: [], // 标记列
      tree: [], // 快速查询
      group: [], // 高级查询
      groupAll: [], // 高级查询所有展示列
    },
  };

  /**
   * 子功能配置
   *
   * @memberof Func
   */
  children = [];

  /**
   * 表单锚点信息
   *
   * @memberof Func
   */
  anchors = [];

  constructor(options) {
    super();
    this.info = new FuncInfo(options.info);
    this.model = options.model;
    Object.assign(this, pick(this.info, ['funcId', 'funcCode', 'funcName', 'pkCode', 'tableCode']));
    parseFunc({ func: this, options });
  }

  /**
   * 字段配置
   * @param {*} type
   * @returns
   */
  getFields(type) {
    return keyToData({ keys: this.fields[type], sourceMap: this.basic.fields });
  }
  /**
   * 列配置
   * @param {*} type
   * @returns
   */
  getColumns(type) {
    return keyToData({ keys: this.columns[type], sourceMap: this.basic.columns });
  }

  /**
   * 按钮配置
   * @param {*} type
   * @returns
   */
  getButtons(type) {
    return keyToData({ keys: this.buttons[type], sourceMap: this.basic.buttons });
  }
  /**
   * 子功能配置
   * @returns
   */
  getChildren() {
    return keyToData({ keys: this.children, sourceMap: this.basic.children });
  }

  /**
   * 表单锚点配置
   * @returns
   */
  getAnchors() {
    return this.anchors;
  }

  /**
   * 获得查询信息
   * @param {*} key
   * @param {*} type
   */
  getQuerys(key, type = 'func') {
    const querys = get(this, `querys.${type}.${key}`);
    let data = [];

    // 快速查询
    if (key == 'tree') {
      data = [];
      querys.forEach((key) => {
        const field = this.basic.fields.get(key);
        const column = this.basic.columns.get(key);
        // 字段存在配置信息
        if (field?.configInfo) {
          const config = parseConfigInfo({ configInfo: field.configInfo, type: 'tree' });
          const { queryField, nodeField, rootId } = field.otherConfig;
          config?.code &&
            data.push(
              new DictionaryParam({
                ddCode: config.code,
                ddName: column.text,
                queryField,
                nodeField,
                rootId,
                nodeInfo: column.code,
                enableValue: column.treeQueryValue,
                async: config.async,
                config,
                querys: field.querys || [],
                dicValueConfig: field.dicValueConfig || [],
              }),
            );
        }
      });
    } else {
      data = keyToData({ keys: querys, sourceMap: this.basic.columns });
    }
    return data;
  }

  /**
   * 获取校验规则
   * @param {*} options
   * @returns
   */
  getValidRules(options) {
    const { type } = options;
    const validRules = {};
    if (type === 'column') {
      this.basic.columns.forEach((column) => {
        const field = this.basic.fields.get(column.field);
        if (field && !column.hidden && column.editable) {
          const rules = field.getRules(options);
          if (isNotEmpty(rules)) {
            validRules[field.name] = rules;
          }
        }
      });
    } else {
      this.basic.fields.forEach((field) => {
        const rules = field.getRules(options);
        if (isNotEmpty(rules)) {
          validRules[field.name] = rules;
        }
      });
    }
    return validRules;
  }
  /**
   * 获得字段默认值
   */
  getDefaultValues() {
    const values = {};
    this.basic.fields.forEach((field) => {
      if (field.isBeanField?.()) {
        values[field.name] = field.getDefaultValue();
      }
    });
    return values;
  }

  /**
   * 获得字段配置
   * @param {*} code
   * @returns
   */
  getFieldConfig(code) {
    return this.basic.fields.get(code);
  }
  /**
   * 获得列表配置
   * @param {*} code
   * @returns
   */
  getColumnConfig(code) {
    return this.basic.columns.get(code);
  }
  /**
   * 获得按钮配置
   * @param {*} code
   * @returns
   */
  getButtonConfig(code) {
    return this.basic.buttons.get(code);
  }
  /**
   * 获得子功能配置
   * @param {*} code
   * @returns
   */
  getChildrenConfig(code) {
    return this.basic.children.get(code);
  }
}
