import { toNumber, decode, toBoolean, split, pick, omit } from '../../../lodash';
import Base from './base';
import { parseFuncTreeConfig } from './func-parser';
import { parseEvents } from '../../util';
/**
 * 功能配置数据
 */
export default class FuncInfo extends Base {
  constructor(options) {
    super(options);
    /**
     * 功能id
     */
    this.funcId = options.JE_CORE_FUNCINFO_ID;
    /**
     * 功能编码
     */
    this.funcCode = options.FUNCINFO_FUNCCODE;
    /**
     * 功能名
     */
    this.funcName = options.FUNCINFO_FUNCNAME;
    /**
     * 功能action
     */
    this.funcAction = options.FUNCINFO_FUNCACTION;
    /**
     * 功能类型，树形功能，普通功能
     */
    this.funcType = options.FUNCINFO_FUNCTYPE;
    /**
     * 类型，子系统，子功能集合，功能，模块
     */
    this.modelType = options.FUNCINFO_NODEINFOTYPE;
    /**
     * 主键名
     */
    this.pkCode = options.FUNCINFO_PKNAME;
    /**
     * 表名
     */
    this.tableCode = options.FUNCINFO_TABLENAME;
    /**
     * 表ID
     */
    this.tableId = options.JE_CORE_RESOURCETABLE_ID;
    /**
     * 操作表名
     */
    this.crudTableName = options.FUNCINFO_CRUDTABLENAME;
    /**
     * 标题图标
     */
    this.icon = options.FUNCINFO_ICON;
    /**
     * 每页条数
     */
    this.pageSize = toNumber(options.FUNCINFO_PAGESIZE);
    /**
     * 产品ID
     */
    this.productId = options.SY_PRODUCT_ID;
    /**
     * 产品CODE
     */
    this.productCode = options.SY_PRODUCT_CODE;
    /**
     * 产品名称
     */
    this.productName = options.SY_PRODUCT_NAME;

    /**
     * 扩展面板
     */
    this.extendPanels = options.FUNCINFO_EXPANDPANELS;

    /**
     * 子功能展示方式
     */
    this.childLayout = options.FUNCINFO_CHILDSHOWTYPE;

    /**
     * 启用工作流
     */
    this.enableWorkflow = toBoolean(options.FUNCINFO_USEWF);

    /**
     * 录入方式
     */
    this.insertType = options.FUNCINFO_INSERTTYPE;

    /**
     * 树形功能配置
     */
    this.funcTreeConfig = parseFuncTreeConfig(options.FUNCINFO_FUNCDICCONFIG);
    /**
     * 禁用查询元素
     */
    this.disableQuerys = split(options.FUNCINFO_DISABLEQUERYSQL, ',') || [];
    /**
     * 附件Bucket
     */
    this.fileBucket = options.FUNCINFO_ATTACHMENTPATH;
    /**
     * 启用审核功能
     */
    this.startAudit = toBoolean(options.FUNCINFO_ENABLEDSH);

    /**
     * 表单事件
     */
    this.formEvents = parseEvents(options.FUNCINFO_FORMJSLISTENER);
    /**
     * 列表事件
     */
    const gridEvents = parseEvents(options.FUNCINFO_GRIDJSLISTENER);
    this.gridEvents = omit(gridEvents, [...funcEventKeys, ...extendPanelEventKeys]);
    /**
     * 功能事件
     */
    this.funcEvents = pick(gridEvents, funcEventKeys);
    /**
     * 扩展面板事件
     */
    this.extendPanelEvents = pick(gridEvents, extendPanelEventKeys);
    /**
     * 子功能高度
     */
    this.childHeight = decode(options.FUNCINFO_CHILDHEIGHT || '[]');

    /**
     * 快速查询配置
     */
    this.treeQueryOptions = {
      title: options.FUNCINFO_TREETITLE, // 标题
      width: toNumber(options.FUNCINFO_TREEWIDTH), // 宽度
      lazy: toBoolean(options.FUNCINFO_TREEREFRESH), // 懒加载
      helpMessage: options.FUNCINFO_TREETIP, // 操作说明
      enableSearchMode: toBoolean(options.FUNCINFO_OPEN_SEARCH_BUTTON), // 检索模式
      closeDragSort: toBoolean(options.FUNCINFO_CLOSE_DRAG_SORT), // 关闭拖拽排序
    };

    /**
     * 高级查询配置
     */
    this.groupQueryOptions = {
      layout: options.FUNCINFO_LAYOUT_TYPE, // 布局
      strategy: toBoolean(options.FUNCINFO_ADD_QUERY_STRATEGY), // 启用存为查询策略
      cols: toNumber(options.FUNCINFO_LAYOUT_COLUMN_COUNT) || 3, // 列数
      changeQuery: !toBoolean(options.FUNCINFO_DISABLE_CHANGE), // 值改变，进行查询
      labelWidth: toNumber(options.FUNCINFO_GROUPQUERY_LABEL_WIDTH), // 标签宽度
      enableQuery: toBoolean(options.FUNCINFO_GROUPQUERY_ENABEL_QUERY), // 启用查询按钮
      enableExport: toBoolean(options.FUNCINFO_GROUPQUERY_START_EXPORT), // 启用导出按钮
      collapsed: toBoolean(options.FUNCINFO_GROUPFORMOPEN), //高级查询分步加载
    };

    /**
     * 列表配置
     */
    this.gridOptions = {
      size: options.FUNCINFO_COLUMN_WIDTH, //默认行高
      stripe: toBoolean(options.FUNCINFO_INTERLACED_DISCOLOUR), // 隔行变色
      helpMessage: options.FUNCINFO_GRIDTIP, // 操作说明
      pagePostion: this.pageSize == -1 ? 'hidden' : options.FUNCINFO_PAGEINFOALIGN, // 分页条位置
      hideRightBorder: options.FUNCINFO_GRIDHIDELINES?.includes('right'), // 隐藏竖线
      hideBottomBorder: options.FUNCINFO_GRIDHIDELINES?.includes('bottom'), // 隐藏横线
      statisticsType: split(options.FUNCINFO_STATISTICALSTRATEGY, ','), // 统计类型
      ddTdBackgroundColor: false, // 字典背景色，使用td全背景色
      multiple: toBoolean(options.FUNCINFO_MULTISELECT), // 允许多选
      sort: toBoolean(options.FUNCINFO_DDORDER), // 拖动排序
      hiddenTbar: toBoolean(options.FUNCINFO_HIDEGRIDTBAR), // 隐藏工具条
      timeout: toNumber(options.FUNCINFO_GRIDTIMEOUT), // 请求超时
      rowEdit: toBoolean(options.FUNCINFO_LINE_EDIT), // 行编辑
      autoLoad: toBoolean(options.FUNCINFO_INIT_LOAD_DATA), // 自动加载
      simpleBar: toBoolean(options.FUNCINFO_SIMPLEBAR), //启动简洁按钮条
    };

    /**
     * 表单配置
     */
    this.formOptions = {
      labelWidth: toNumber(options.FUNCINFO_FORMLABELWIDTH) || 140, // 标签宽度
      cols: toNumber(options.FUNCINFO_FORMCOLS || 3), //表单列数
      width: options.FUNCINFO_FORMWIDTH, //表单宽度
      minWidth: toNumber(options.FUNCINFO_FORMMINWIDTH), // 最小宽度
      size: options.FUNCINFO_FORMFIELD_SPACING === '0' ? 'small' : 'middle', // 表单字段间距
      anchor: toBoolean(options.FUNCINFO_GROUP_LOCATION), // 启用快速定位
      hiddenTbar: toBoolean(options.FUNCINFO_HIDEFORMTBAR), // 隐藏工具条
      title: options.FUNCINFO_FORMTITLE4FUNC, // 表单标题
      backgroundColor: options.FUNCINFO_FORMBGCOLOR, // 表单背景色
      labelColor: options.FUNCINFO_FORM_LABELCOLOR, //
      useWfLog: toBoolean(options.FUNCINFO_USEWFLOG), //展示审批记录
    };

    /**
     * 树形配置
     */
    this.treeOptions = {
      action: toBoolean(options.FUNCINFO_OPEN_MORE_BUTTON), // 启用更多按钮
      showType: options.FUNCINFO_SHOW_TYPE, // 展示方式
      lazy: toBoolean(options.FUNCINFO_SFLJZ), // 是否懒加载
    };

    /**
     * 移动端配置
     */
    this.appOptions = {
      formLabelWidth: options.FUNCINFO_APPFORM_LABEL_WIDTH, //字段标题宽度
      formDisableCrumb: toBoolean(options.FUNCINFO_APPFORM_DISABLE_CRUMB), //隐藏表单子功能导航
    };

    // 树形功能按钮
    this.setTreeActionButtons(options.FUNCINFO_MORE_BUTTON_CONFIG);

    // 功能使用说明
    this.funcHelpContent = options.FUNCINFO_HELP;
  }

  /**
   * 树形功能操作按钮
   * @param {*} config
   */
  setTreeActionButtons(config) {
    const buttons = [];
    (decode(config || '[]') || []).forEach((item) => {
      if (item.enable == '1') {
        item.events = { 'before-click': item.function };
        buttons.push(item);
      }
    });
    this.funcTreeActionButtons = buttons;
  }
}
/**
 * 功能事件key
 */
const funcEventKeys = ['func-activate'];
/**
 * 扩展面板事件
 */
const extendPanelEventKeys = [
  'top-renderer',
  'bottom-renderer',
  'left-renderer',
  'right-renderer',
  'tbar-renderer',
  'bbar-renderer',
  'lbar-renderer',
  'rbar-renderer',
];
