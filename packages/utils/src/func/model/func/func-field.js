import {
  toBoolean,
  isPlainObject,
  forEach,
  isEmpty,
  toNumber,
  decode,
  isNotEmpty,
  isNull,
  isString,
} from '../../../lodash';
import { execScript } from '../../../web/sandbox';
import { isApp } from '../../../je';
import Base from './base';
import { FuncFieldTypeEnum } from '../../enum';
import { parseEvents, doEvents4Sync } from '../../util';
export default class FuncField extends Base {
  constructor(options) {
    super(options);
    /**
     * 字段ID
     */
    this.id = options.JE_CORE_RESOURCEFIELD_ID;
    /**
     * 字段编码
     */
    this.name = options.RESOURCEFIELD_CODE;
    /**
     * 字段标签
     */
    this.label = options.RESOURCEFIELD_NAME;
    /**
     * 组件类型
     */
    this.xtype = options.RESOURCEFIELD_XTYPE;
    /**
     * 默认值
     */
    this.value = options.RESOURCEFIELD_VALUE;
    this.value = isNull(this.value) ? undefined : this.value;
    /**
     * 默认值方法
     */
    this.valueFN = options.RESOURCEFIELD_VALUEFN;
    /**
     * 必填
     */
    this.required = toBoolean(options.RESOURCEFIELD_REQUIRE);
    /**
     * 只读
     */
    this.disabled = toBoolean(options.RESOURCEFIELD_READONLY);
    /**
     * 隐藏
     */
    this.hidden = toBoolean(options.RESOURCEFIELD_HIDDEN);
    /**
     * 配置信息
     */
    this.configInfo = options.RESOURCEFIELD_CONFIGINFO;

    /**
     * 标签位置
     */
    this.labelAlign = options.RESOURCEFIELD_LABELALIGN || 'right';

    /**
     * 移动端标签位置
     */
    this.appLabelAlign = options.RESOURCEFIELD_APP_LABELALIGN || 'left';

    /**
     * 适用范围
     */
    this.scope = options.RESOURCEFIELD_USE_SCOPE;

    /**
     * 查询条件
     */
    this.querys = decode(options.RESOURCEFIELD_WHERESQL) || [];

    /**
     * 字典带值配置
     */
    this.dicValueConfig = decode(options.RESOURCEFIELD_DD_CUSTOMER_VARIABLES) || [];

    /**
     * 所属分组框
     */
    this.groupName = options.RESOURCEFIELD_GROUPNAME || '';
    /**
     * 排序
     */
    this.orderIndex = toNumber(options.SY_ORDERINDEX);

    /**
     * 所占列数
     */
    this.colSpan = toNumber(options.RESOURCEFIELD_COLSPAN || 1);
    /**
     * 内容列数
     */
    this.cols = toNumber(options.RESOURCEFIELD_COLS || 2);

    /**
     * 输入提示
     */
    this.placeholder = options.RESOURCEFIELD_EMPTYTEXT;
    /**
     * 标签隐藏
     */
    this.labelHidden = toBoolean(options.RESOURCEFIELD_HIDELABEL);
    /**
     * 标签颜色
     */
    this.labelColor = options.RESOURCEFIELD_LABELCOLOR;
    /**
     * 数值颜色
     */
    this.fieldColor = options.RESOURCEFIELD_FIELDCOLOR;

    /**
     * 高度
     */
    this.height = toNumber(options.RESOURCEFIELD_HEIGHT);
    /**
     * 宽度
     */
    this.width = toNumber(options.RESOURCEFIELD_WIDTH);

    /**
     * 后缀
     */
    this.addonAfter = decode(options.RESOURCEFIELD_UNITLISTTPL) || [];

    /**
     * 字段样式
     */
    this.cls = options.RESOURCEFIELD_FIELDCLS;
    /**
     * 帮助信息
     */
    this.help = {
      enable: isNotEmpty(options.RESOURCEFIELD_HELP),
      html: true,
      message: options.RESOURCEFIELD_HELP,
      width: toNumber(options.RESOURCEFIELD_HELP_TIP_W),
      height: toNumber(options.RESOURCEFIELD_HELP_TIP_H),
    };

    /**
     * 表达式
     */
    this.exps = {
      data: {
        // 绑定表达式
        exp: options.RESOURCEFIELD_BINDING,
        fn: options.RESOURCEFIELD_BINDINGFN,
      },
      visible: {
        // 显隐表达式
        exp: options.RESOURCEFIELD_INTERPRETER,
        fn: options.RESOURCEFIELD_INTERPRETERFN,
      },
      readonly: {
        // 只读表达式
        exp: options.RESOURCEFIELD_READONLYEXP,
        fn: options.RESOURCEFIELD_READONLYEXPFN,
      },
      required: {
        // 必填表达式
        exp: options.RESOURCEFIELD_REQUIRE_EXP,
        fn: options.RESOURCEFIELD_REQUIRE_EXPFN,
      },
      select: {
        // 可选表达式
        exp: options.RESOURCEFIELD_ENABLEEXP,
        fn: options.RESOURCEFIELD_ENABLEEXPFN,
        message: options.RESOURCEFIELD_ENABLETIP,
      },
    };
    /**
     * 可选表达式
     */
    this.selectExp;
    /**
     * 启用表达式
     */
    this.expsEnable = false;
    /**
     * 启用绑定字段表达式
     */
    this.bindExpsEnable = false;
    /**
     * 绑定特定字段触发的表达式
     */
    this.bindExps = {};
    // 提取有效表达式
    Object.keys(this.exps).forEach((key) => {
      const item = this.exps[key];
      if (item.exp || item.fn) {
        if (key === 'select') {
          this.selectExp = item;
          delete this.exps[key];
        } else if (item.exp && item.fn) {
          this.bindExps[key] = item;
          this.bindExpsEnable = true;
          delete this.exps[key];
        } else {
          this.expsEnable = true;
        }
      } else {
        delete this.exps[key];
      }
    });

    /**
     * 校验信息
     */
    this.validation = {
      regex: options.RESOURCEFIELD_REGEX, // 正则
      regexMessage: options.RESOURCEFIELD_REGEXTEXT, // 正则提示
      max: toNumber(options.RESOURCEFIELD_MAXLENGTH), // 最大值
      vtype: options.RESOURCEFIELD_VTYPE, // 校验类型
      vtypeFn: options.RESOURCEFIELD_VTYPEFN, // 自定义校验方法
    };

    /**
     * 功能ID
     */
    this.funcId = options.RESOURCEFIELD_FUNCINFO_ID;
    /**
     * 事件
     */
    this.events = parseEvents(options.RESOURCEFIELD_JSLISTENER);
    /**
     * 高级查询事件
     */
    this.queryEvents = parseQueryEvents(this);
    /**
     * 绑定的事件，如onClick
     */
    this.listeners;

    /**
     * 超链接事件，增加配置
     */
    this.link = !!this.events['link-click'];

    /**
     * 辅助配置项
     */
    this.otherConfig = decode(options.RESOURCEFIELD_OTHERCONFIG) || {};

    /**
     * 快速定位
     */
    this.anchor = false;

    /**
     * 多选
     */
    this.multiple = false;

    /**
     *  子元素
     */
    this.children = [];

    /**
     * 个性化处理
     */
    switch (this.xtype) {
      // 编号字段
      case FuncFieldTypeEnum.INPUT_CODE.xtype:
        this.value = '<系统自动生成>';
        this.readonly = true;
        this.disabled = true;
        break;
      // 子功能
      case FuncFieldTypeEnum.FUNC_CHILD.xtype:
        this.groupName = '';
        break;
      // 分组框
      case FuncFieldTypeEnum.FIELDSET.xtype:
        this.anchor = toBoolean(this.otherConfig.quickPositioning); // 定位
        break;
      // 多选框
      case FuncFieldTypeEnum.CHECKBOX_GROUP.xtype:
        this.multiple = true;
        break;
      // 下拉框，查询选择，树形选择，人员选择
      case FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype:
      case FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype:
      case FuncFieldTypeEnum.INPUT_SELECT_USER.xtype:
      case FuncFieldTypeEnum.SELECT.xtype:
        const configInfos = this.configInfo?.split(',') || [];
        this.multiple = ['M', 'M_'].includes(configInfos[3]);
        break;
    }
  }

  /**
   * 字段编码
   *
   * @readonly
   * @memberof FuncField
   */
  get code() {
    return this.name;
  }

  /**
   * 字段名称
   *
   * @readonly
   * @memberof FuncField
   */
  get text() {
    return this.label;
  }

  /**
   * 只读
   *
   * @memberof FuncField
   */
  get readonly() {
    return this.disabled;
  }
  set readonly(readonly) {
    this.disabled = readonly;
  }

  addChild(data, cascade) {
    if (isPlainObject(data)) {
      data = [data];
    }
    forEach(data, (item) => {
      if (!item.isModel) {
        item = new FuncField(item);
      }
      if (this.name === item.groupName) {
        if (cascade) {
          item.addChild(data, cascade);
        }
        this.children.push(item);
      }
    });
  }
  /**
   * FormItem字段，用于展示
   * @returns
   */
  isFormItem() {
    return ![FuncFieldTypeEnum.FIELDSET.xtype, FuncFieldTypeEnum.FUNC_CHILD.xtype].includes(
      this.xtype,
    );
  }
  /**
   * bean字段，用于提取数据
   * @returns
   */
  isBeanField() {
    return ![
      FuncFieldTypeEnum.FIELDSET.xtype,
      FuncFieldTypeEnum.FUNC_CHILD.xtype,
      FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype,
      FuncFieldTypeEnum.DISPLAY.xtype,
    ].includes(this.xtype);
  }
  /**
   * 获取默认值
   * @returns
   */
  getDefaultValue() {
    if (isEmpty(this.value)) {
      if ([FuncFieldTypeEnum.INPUT_NUMBER.xtype].includes(this.xtype)) {
        // 防止数字必填时，给默认值校验失效
        this.value = this.required ? this.value : 0;
      }
    }
    // 执行默认值js代码
    if (isNotEmpty(this.valueFN)) {
      return execScript(this.valueFN, {
        EventOptions: { field: this },
      });
    } else {
      return this.value;
    }
  }

  /**
   * 校验规则
   */
  getRules(options) {
    let rules = [];
    const defaultMessage = '该输入项不符合验证规则';

    // 子功能集合校验
    if (this.xtype === FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype) {
      rules.push({
        required: this.required,
        message: `该输入项为必填项`,
        validator: async (...args) => {
          return options.$field?.value?.validator(...args);
        },
      });
      return rules;
    }

    // 附件校验是否上传完成
    if (
      [
        FuncFieldTypeEnum.UPLOAD_INPUT.xtype,
        FuncFieldTypeEnum.UPLOAD.xtype,
        FuncFieldTypeEnum.UPLOAD_IMAGE.xtype,
      ].indexOf(this.xtype) != -1
    ) {
      rules.push({
        message: `附件没有上传完成`,
        validator: async (...args) => {
          return options.$field?.value?.validator(...args);
        },
      });
    }

    // 必填校验
    if (this.required) {
      rules.push({ required: true, message: `该输入项为必填项` });
    }
    // 正则校验
    if (this.validation.regex) {
      rules.push({
        pattern: new RegExp(this.validation.regex),
        message: this.validation.regexMessage,
      });
    }
    // 最大长度
    if (this.validation.max > 0) {
      rules.push({
        max: toNumber(this.validation.max),
        message: `该输入内容最大长度为${this.validation.max}字`,
      });
    }
    // 自定义校验
    switch (this.validation.vtype) {
      case 'method': // 自定义规则，写法参考ant表单规则，返回rules
        if (this.validation.vtypeFn) {
          rules.push({
            custom: true,
            validator: (rule, value) => {
              const eventOptions = { ...options, value };
              // 适配列表参数
              if (options.type === 'column') {
                Object.assign(eventOptions, {
                  model: rule.row,
                  value: rule.cellValue,
                  ...rule,
                });
              }

              // 异步函数校验
              return doEvents4Sync({
                code: this.validation.vtypeFn,
                eventOptions,
                async: true,
              }).then((result) => {
                // 错误信息 || false
                if (isString(result) || result === false) {
                  return Promise.reject(result || defaultMessage);
                } else {
                  return Promise.resolve();
                }
              });
            },
          });
        }
        break;
      case 'unique':
        // 平台唯一值校验，后续集成...
        rules.push({
          message: `该输入项已存在，请重新输入`,
          validator: async (rule, value) => {
            if (isEmpty(value)) return true;
            return options.$func?.action.doFormFieldUniqueCheck(this.name, value);
          },
        });
        break;
    }
    // 增加默认提示信息，防止报错
    rules.forEach((rule) => {
      rule.message = rule.message || defaultMessage;
      if (rule.custom) {
        delete rule.message;
      }
    });
    // 兼容uni表单校验规则
    if (isApp) {
      const attrMaps = [
        { uni: 'maxLength', ant: 'max' },
        { uni: 'errorMessage', ant: 'message' },
        { uni: 'pattern', ant: 'pattern' },
        { uni: 'required', ant: 'required' },
        { uni: 'validateFunction', ant: 'validator' },
      ];
      rules = rules.map((rule) => {
        const item = {};
        Object.keys(rule).forEach((key) => {
          const uniKey = attrMaps.find((attr) => attr.ant == key)?.uni || key;
          item[uniKey] = rule[key];
        });
        return item;
      });
    }

    return rules;
  }

  /**
   * 获得值
   * @returns
   */
  getValue() {
    return this.$form.getValue(this.name);
  }
  /**
   * 设置值
   * @param {Object} value
   */
  setValue(value) {
    this.$form.setValue(this.name, value);
  }
  /**
   * 设置只读
   * @param {boolean} readonly
   */
  setReadOnly(readonly) {
    this.readonly = readonly;
  }
  /**
   * 设置必填
   * @param {boolean} required
   */
  setRequired(required) {
    this.required = required;
  }
  /**
   * 设置正则
   * @param {string} regex
   * @param {string} regexMessage
   */
  setRegex(regex, regexMessage) {
    this.validation.regex = regex;
    this.validation.regexMessage = regexMessage;
  }
  /**
   * 设置配置信息
   * @param {string} configInfo
   */
  setConfigInfo(configInfo) {
    this.configInfo = configInfo;
  }
  /**
   * 显示
   */
  show() {
    this.setVisible(true);
  }
  /**
   * 隐藏
   */
  hide() {
    this.setVisible(false);
  }
  /**
   * 显隐
   * @param {boolean} visible
   */
  setVisible(visible) {
    this.hidden = !visible;
  }
  /**
   * 重置
   * @param {boolean} useDefaultValue 使用默认值
   */
  reset(useDefaultValue) {
    let value = '';
    if (useDefaultValue) {
      value = this.getDefaultValue();
    }
    this.setValue(value);
  }
}

export function parseDisplayFields(data) {
  const fields = [];
  data.forEach((item) => {
    if (!item.hidden && isEmpty(item.groupName)) {
      // 级联添加子信息
      item.addChild(data, true);
      fields.push(item);
    }
  });
  return fields;
}
/**
 * 解析高级查询字段的事件
 * query-开头的事件为高级查询字段专用
 * @param {*} field
 * @returns
 */
function parseQueryEvents(field) {
  const events = {};
  const prefix = 'query-';
  Object.keys(field.events).forEach((name) => {
    if (name.startsWith(prefix)) {
      events[name.substring(prefix.length)] = field.events[name];
      delete field.events[name];
    }
  });
  return events;
}
