import { toBoolean, toNumber } from '../../../lodash';
import Base from './base';
import FuncChildrenAssociation from './func-children-association';

export default class FuncChildren extends Base {
  constructor(options = {}) {
    super();
    /**
     * 主键
     */
    this.id = options.JE_CORE_FUNCRELATION_ID;
    /**
     * 编码
     */
    this.code = options.FUNCRELATION_CODE;
    /**
     * 标题
     */
    this.title = options.FUNCRELATION_NAME;
    /**
     * 功能ID
     */
    this.funcId = options.FUNCRELATION_FUNCID;
    /**
     * 类型
     */
    this.type = options.FUNCRELATION_RELYONTYPE;
    /**
     * 配置信息
     */
    this.configInfo = options.FUNCRELATION_TABLENAME;
    /**
     * 展示位置
     */
    this.layout = options.FUNCRELATION_SHOWTYPE;
    /**
     * 适用范围
     */
    this.scope = options.FUNCRELATION_USE_SCOPE;
    /**
     * 表达式
     */
    this.exps = {
      visible: {
        // 显隐表达式
        exp: options.FUNCRELATION_INTERPRETER,
      },
    };
    /**
     * 启用表达式
     */
    this.expsEnable = !!this.exps.visible.exp;
    /**
     * 排序
     */
    this.orderIndex = toNumber(options.SY_ORDERINDEX);
    /**
     * 启用
     */
    this.enable = toBoolean(options.FUNCRELATION_ENABLED);
    /**
     * 隐藏
     */
    this.hidden = false;
    /**
     * 只读
     */
    this.readonly = false;
    /**
     * 显示标题
     */
    this.showTitle = toBoolean(options.FUNCRELATION_SHOWTITLE);
    /**
     * 定位
     */
    this.anchor = toBoolean(options.FUNCRELATION_LOCATE);
    /**
     * 分组
     */
    this.groupName = options.FUNCRELATION_GROUP;
    /**
     * 高度
     */
    this.height = toNumber(options.FUNCRELATION_HEIGHT);
    /**
     * 表单展示
     */
    this.isFuncForm = options.FUNCRELATION_ZCGX === '1:1';
    /**
     * 标题图标
     */
    this.icon = options.FUNCRELATION_ICON;
    /**
     * 功能ID
     */
    this.funcId = options.FUNCRELATION_FUNCINFO_ID;

    /**
     * 级联关系
     */
    this.associations = (options.associations || []).map((item) => {
      return new FuncChildrenAssociation(item);
    });
  }
  /**
   * 显示
   */
  show() {
    this.setVisible(true);
  }
  /**
   * 隐藏
   */
  hide() {
    this.setVisible(false);
  }
  /**
   * 显隐
   * @param {*} visible
   */
  setVisible(visible) {
    this.hidden = !visible;
  }

  /**
   * 设置标题
   */
  setTitle(title) {
    this.title = title || this.title;
  }
}
