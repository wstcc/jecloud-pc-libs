import Base from './base';
import { toBoolean, toNumber, decode } from '../../../lodash';
/**
 * 查询策略项
 */
export default class FuncStrategy extends Base {
  constructor(options) {
    super();
    this.id = options.JE_CORE_QUERYSTRATEGY_ID;
    this.text = options.QUERYSTRATEGY_NAME;
    this.fn = options.QUERYSTRATEGY_FN;
    this.sql = options.QUERYSTRATEGY_SQL;
    this.default = toBoolean(options.QUERYSTRATEGY_DEF);
    this.orderIndex = toNumber(options.SY_ORDERINDEX);
  }
  get querys() {
    return this.sql && decode(this.sql);
  }
}
