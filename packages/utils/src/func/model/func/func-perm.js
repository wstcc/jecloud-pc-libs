import { getDDCache } from '../../../lodash';
import { loadPluginPermsApi } from '../../../system/api/system';
import { loadFuncPermsApi } from '../../api';
import { FuncButtonTypeEnum } from '../../enum';
/**
 * 功能权限
 */
export default class FuncPerm {
  $func;
  funcCode = '';
  isFuncField = false; // 子功能集合
  isDDFunc = false; // 字典功能
  funcDDCode = null; // 字典编码
  perms = []; // 权限信息
  ddFuncPerms = []; // 字典功能权限

  constructor(func) {
    this.$func = func;
    this.funcCode = func.funcCode;
    this.isFuncField = func.isFuncField;
    this.isDDFunc = func.isDDFunc;
    this.funcDDCode = func.funcDDCode;
  }

  /**
   * 功能数据
   * @returns
   */
  getFuncData() {
    return this.$func.getFuncData();
  }

  /**
   * 获取功能权限数据
   * @param {*} param0
   * @returns
   */
  loadFuncPerm() {
    return Promise.all([loadFuncPermsApi(this.funcCode), this.loadDDFuncPerm()]).then(
      ([perms, ddFuncPerms]) => {
        this.perms = perms;
        this.ddFuncPerms = ddFuncPerms;
        if (!this.hasShowPerm()) return Promise.reject({ code: '403' });
      },
    );
  }
  /**
   * 字典功能权限
   * @returns
   */
  loadDDFuncPerm() {
    if (this.isDDFunc) {
      const dd = getDDCache(this.funcDDCode);
      if (dd) {
        return loadPluginPermsApi(this.funcDDCode);
      } else {
        return Promise.reject({ code: '404' });
      }
    } else {
      return Promise.resolve([]);
    }
  }

  /**
   * 获得权限按钮
   * @param {*} type
   * @returns
   */
  getPermButtons(type) {
    let buttons = this.getFuncData().getButtons(type);
    buttons = buttons.filter((item) => this.hasPerm({ type: 'button', code: item.code }));
    // 层级处理，依附按钮
    return parseButtons(buttons);
  }
  /**
   * 获取权限子功能
   * @returns
   */
  getPermChildren() {
    const children = this.getFuncData().getChildren();
    return children.filter((item) => this.hasPerm({ type: 'subfunc', code: item.code }));
  }

  /**
   * 获取权限锚点
   */
  getPermAnchors() {
    let anchors = this.getFuncData().getAnchors();
    anchors = anchors.filter(
      (item) => !item.child || this.hasPerm({ type: 'subFunc', code: item.code }),
    );
    return anchors;
  }

  /**
   *  获取功能权限
   */
  hasPerm({ type, code }) {
    // 子功能集合，拥有全部权限
    if (this.isFuncField) return this.hasFuncFieldPerm();
    // 字典功能权限，拥有全部权限
    if (this.isDDFunc) return this.hasDDFuncPerm();

    const funcCode = this.funcCode;
    let template = '';
    switch (type) {
      case 'button':
        template = `pc-func-button-${funcCode}-${code}-show`;
        break;
      case 'config':
        template = `pc-func-${funcCode}-config`;
        break;
      case 'show':
        template = `pc-func-${funcCode}-show`;
        break;
      case 'subfunc':
        template = `pc-subfunc-${funcCode}-${code}-show`;
        break;
    }
    return this.perms?.includes(template);
  }

  /**
   * 子功能集合，拥有全部权限
   * @returns
   */
  hasFuncFieldPerm() {
    return true;
  }

  /**
   * 拥有字典功能权限
   * @returns
   */
  hasDDFuncPerm() {
    return this.isDDFunc && this.ddFuncPerms.length > 0;
  }
  /**
   * 获取表单保存权限
   */
  hasFormSavePerm() {
    // 获得保存按钮的配置
    let formSaveBtnConfig = this.$func?.getFormButtons(FuncButtonTypeEnum.FORM_SAVE_BUTTON);
    return (
      this.hasButtonPerm(FuncButtonTypeEnum.FORM_SAVE_BUTTON, 'form') &&
      !formSaveBtnConfig?.metaData?.hidden
    );
  }
  /**
   * 获取表格编辑权限
   * @returns
   */
  hasGridEditPerm() {
    return this.hasButtonPerm(FuncButtonTypeEnum.GRID_EDIT_BUTTON, 'grid');
  }
  /**
   * 获取表格保存权限
   * @returns
   */
  hasGridSavePerm() {
    return this.hasButtonPerm(FuncButtonTypeEnum.GRID_UPDATE_BUTTON, 'grid');
  }

  /**
   * 按钮权限
   * @param {*} buttonCode
   * @param {*} type
   * @returns
   */
  hasButtonPerm(buttonCode, type) {
    // 1. 判断是否包含按钮
    const button = this.isFuncField
      ? this.hasFuncFieldPerm()
      : this.getFuncData().buttons[type].find((code) => buttonCode === code);
    // 2. 判断是否有按钮权限
    const buttonPerm = this.hasPerm({
      type: 'button',
      code: buttonCode,
    });
    return !!(button && buttonPerm);
  }

  /**
   * 配置权限
   * @returns
   */
  hasConfigPerm() {
    // 字典功能，没有配置权限
    return this.isDDFunc ? false : this.hasPerm({ type: 'config' });
  }
  /**
   * 功能使用权限
   * @returns
   */
  hasShowPerm() {
    return this.hasPerm({ type: 'show' });
  }

  /**
   * 表单返回按钮
   */
  isFormBackButton(code) {
    return code === FuncButtonTypeEnum.FORM_BACK_BUTTON;
  }
}
/**
 * 解析按钮，处理依附按钮配置
 * @param {*} data
 * @param {*} groupName
 * @returns
 */
function parseButtons(data, groupName = '') {
  const items = [];
  data.forEach((item) => {
    const code = item.code;
    // 启用隐藏项
    if (item.groupName === groupName) {
      const children = parseButtons(data, code);
      if (children.length) {
        item.children = children;
      }
      items.push(item);
    }
  });
  return items;
}
