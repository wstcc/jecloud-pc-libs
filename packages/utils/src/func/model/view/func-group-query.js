import BaseView from './base';
import { vueCall } from '../../../je';

export default class FuncGroupQueryView extends BaseView {
  /**
   * 查询model
   *
   * @memberof FuncGroupQueryView
   */
  model = vueCall('reactive', {});
  /**
   * 表单数据model，用于辅助查询使用
   *
   * @memberof FuncGroupQueryView
   */
  formModel = vueCall('reactive', {});

  constructor(options) {
    super(options);
    this.store = options.store;
    this.type = options.type;
  }

  /**
   * 获得查询项的值
   * @param {string} fieldCode
   * @returns
   */
  getQueryItem(fieldCode) {
    return this.model[fieldCode];
  }
  /**
   * 设置查询项的值
   * @param {string} fieldCode
   * @param {Object} queryItem
   */
  setQueryItem(fieldCode, queryItem = {}) {
    Object.assign(this.model[fieldCode], queryItem);
  }

  /**
   * 设置值
   * @param {Object} values
   */
  setValues(values = {}) {
    Object.keys(values).forEach((key) => {
      this.model[key].value = values[key];
    });
  }
  /**
   * 设置单字段值
   * @param {string} fieldCode
   * @param {Object} value
   */
  setValue(fieldCode, value) {
    this.setValues({ [fieldCode]: value });
  }
  /**
   * 获得表单值
   * @param {string} fieldCode
   * @returns Object
   */
  getValues(fieldCode) {
    const values = [];
    Object.keys(this.model).forEach((key) => {
      values[key] = this.model[key].value;
    });
    return fieldCode ? values?.[fieldCode] : values;
  }
  /**
   * 获得字段值
   * @param {string} fieldCode
   * @returns Object
   */
  getValue(fieldCode) {
    return this.getValues(fieldCode);
  }

  /**
   * 创建查询项的值
   * @param {*} field
   * @returns
   */
  createQueryItem(field) {
    this.model[field.name] = {
      sort: '',
      value: '',
      betweenValue: '',
      type: field.queryType,
      defaultType: field.queryType,
      values: {}, //用于存储选择字段带值，便于解析sql
    };
  }

  /**
   * 查询
   */
  doQuery() {}

  /**
   * 重置查询条件
   */
  doResetQuery() {
    return this.doReset();
  }

  /**
   * 排序
   * @param {Object} options
   * @param {string} options.code 字段编码
   * @param {string} options.type 排序类型，asc|desc
   */
  doSortQuery(options) {
    this.doSort(options);
  }
}
