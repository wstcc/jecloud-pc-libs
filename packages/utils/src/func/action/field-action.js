import { isNotEmpty, split, toNumber, toBoolean, decode } from '../../lodash';
import { parseConfigInfo, doEvents4Sync } from '../util';
import { Modal } from '../../modal';
import { isApp, useJE, vueCall } from '../../je';

import { FuncFieldTypeEnum } from '../enum';
export function useFieldAction() {
  return {
    parseFieldProps,
    parseFieldProps4Common,
    parseFieldProps4Upload,
    parseFieldAddons,
  };
}

/**
 * 解析字段公共有效的配置
 * @param {*} field
 * @returns
 */
function parseFieldProps($func, { field }) {
  const props = {};
  [
    'disabled',
    'configInfo',
    'placeholder',
    'selectExp',
    'height',
    'width',
    'querys',
    'link',
  ].forEach((key) => {
    if (isNotEmpty(field[key], true)) {
      props[key] = field[key];
    }
  });
  // 功能属性
  Object.assign(props, parseFieldProps4Common($func, { field }));
  const {
    inputType,
    pkName,
    winHeight,
    winWidth,
    cascadeType,
    optionalAndInput,
    optionalAndQuery,
    penetrate,
    rootId,
  } = field.otherConfig;
  switch (field.xtype) {
    case FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype: // 树形选择
      // 文本域
      props.textarea = inputType === 'textarea';
      // 可选可输入
      props.editable = toBoolean(optionalAndInput);
      // 其他配置项
      props.selectOptions = {
        width: winWidth > 0 ? winWidth : undefined,
        height: winHeight > 0 ? winHeight : undefined,
        cascadeType,
        rootId,
      };
      // 字典参数
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }
      break;
    case FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype: // 关联选择
      // 文本域
      props.textarea = inputType === 'textarea';
      // 可选可输入
      props.editable = toBoolean(optionalAndInput);
      // 其他配置项
      props.selectOptions = {
        idProperty: pkName,
        width: winWidth > 0 ? winWidth : undefined,
        height: winHeight > 0 ? winHeight : undefined,
      };
      // 可穿透
      if (toBoolean(penetrate)) {
        props.editable = false;
        props.onLinkClick = () => {
          const selectOptions = parseConfigInfo({ configInfo: props.configInfo });
          const pkName = props.selectOptions?.idProperty;
          // 表单对应存储主键值的字段名
          const formPkName = selectOptions.fieldMaps.sourceToTarget[pkName]?.[0];
          const values = split(props.model?.[formPkName], ',');
          const beanId = values[0];
          if (!pkName || !formPkName) {
            Modal.alert('字段配置项【唯一】为空，请检查！');
          } else if (!beanId) {
            Modal.alert('主键为空，请检查数据！');
          } else {
            const func = useJE().useFunc?.();
            func?.showFuncForm(selectOptions.code, { beanId });
          }
        };
      }
      break;
    case FuncFieldTypeEnum.INPUT_SELECT_USER.xtype: // 人员选择
      // 文本域
      props.textarea = inputType === 'textarea';
      // 其他配置项
      props.selectOptions = {};
      // 人员查询类型
      const { queryType } = field.otherConfig;
      const allTab = ['dept', 'role', 'organ', 'common'];
      const hideTab = split(queryType, ',');
      const displayTab = allTab.filter((tab) => !hideTab.includes(tab));
      const querysConfig = {};
      ['dept', 'role', 'organ'].forEach((item) => {
        const key = item + 'Query';
        const querys = field.otherConfig[key];
        if (isNotEmpty(querys)) {
          querysConfig[item] = decode(querys);
        }
      });

      props.selectOptions['queryType'] = displayTab.join(',');
      props.selectOptions['querysConfig'] = querysConfig;
      break;
    case FuncFieldTypeEnum.UPLOAD.xtype: // 多附件
    case FuncFieldTypeEnum.UPLOAD_INPUT.xtype: // 附件
    case FuncFieldTypeEnum.UPLOAD_IMAGE.xtype: //图片选择器
      Object.assign(props, parseFieldProps4Upload($func, { field }));
      break;
    case FuncFieldTypeEnum.SELECT.xtype: // 下拉框
      // 可选可查询
      props.showSearch = toBoolean(optionalAndQuery);
      // 可选可输入
      props.editable = toBoolean(optionalAndInput);
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }
      break;
    case FuncFieldTypeEnum.CHECKBOX_GROUP.xtype: // 复选框
    case FuncFieldTypeEnum.RADIO_GROUP.xtype: // 单选框
      if (isNotEmpty(field.dicValueConfig)) {
        props.dicValueConfig = field.dicValueConfig;
      }
      //处理单选多选的配置信息
      if (field.configInfo) {
        const configs = field.configInfo.split(',');
        configs[3] = field.xtype === FuncFieldTypeEnum.CHECKBOX_GROUP.xtype ? 'M' : 'S';
        props.configInfo = configs.join(',');
      }
      break;
    case FuncFieldTypeEnum.EDITOR_CODE.xtype: // 代码编辑器
      props.readonly = field.readonly;
      props.language = field.otherConfig.language;
      props.hideApi = toBoolean(field.otherConfig.hideApi);
      break;
    case FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype: // 子功能集合
      const [funcCode, fkCode, pageSize, parentFields, childFields] = split(props.configInfo, ',');
      Object.assign(props, {
        funcCode,
        fkCode,
        pageSize,
        parentFields: split(parentFields, '~'),
        childFields: split(childFields, '~'),
        actionColumn: field.otherConfig,
        field,
        maximize: toBoolean(field.otherConfig.enableDataPanel), // 最大化
        // 绑定私有change事件，用于表达式处理
        ...$func.action.bindFormFieldPrivateChange({ field }),
      });
      break;
    case FuncFieldTypeEnum.TEXTAREA.xtype: // 文本域
    case FuncFieldTypeEnum.INPUT.xtype: // 单选框
      if (isApp) {
        // TODO现在PC和APP的校验展示不一致，待和领导沟通，是否需要统一, -1为不限制
        props.maxlength = field.validation.max || -1;
        break;
      }
  }

  return props;
}
/**
 * 附件属性
 * @param {*} param0
 * @returns
 */
function parseFieldProps4Upload($func, { field }) {
  const props = {};
  const { fileBucket, tableCode, pkCode, productCode } = $func.getFuncData().info;
  const { fileSize, fileNum, allow, notallow, showButtons } = field.otherConfig;
  props.maxSize = toNumber(fileSize);
  props.includeSuffix = split(allow, ',');
  props.excludeSuffix = split(notallow, ',');
  props.params = { bucket: fileBucket, tableCode, pkCode, productCode };
  // 删除后，更新表单数据
  props.afterFileRemove = ({ value, $upload }) => {
    const pkValue = $upload.props.model?.[pkCode];
    if (pkValue) {
      $func.action.doFormUpdateById(pkValue, { [field.name]: value });
    }
  };
  // 多附件
  if (field.xtype == FuncFieldTypeEnum.UPLOAD.xtype) {
    props.menuButtons = isNotEmpty(showButtons) ? showButtons.split(',') : [];
  }
  //图片选择器
  if (field.xtype == FuncFieldTypeEnum.UPLOAD_IMAGE.xtype && fileNum) {
    props.maxCount = toNumber(fileNum);
  }
  return props;
}
/**
 * 公共属性
 * 不受表单，列表，高级查询影响
 * @param {*} param0
 */
function parseFieldProps4Common($func, { field }) {
  const props = {};
  switch (field.xtype) {
    case FuncFieldTypeEnum.INPUT_SELECT_COLOR.xtype: // 颜色选择器
      const colors = split(field.configInfo, ',');
      props.colors = colors.length ? colors : undefined;
      break;
    case FuncFieldTypeEnum.DATE_PICKER.xtype: // 日期
    case FuncFieldTypeEnum.TIME_PICKER.xtype: // 时间
      const picker = field.otherConfig.dateType;
      props.format = field.configInfo || undefined;
      if (picker) {
        props.picker = picker.toLocaleLowerCase();
      }
      break;
    case FuncFieldTypeEnum.INPUT_NUMBER.xtype: // 数值框
      if (isNotEmpty(field.configInfo)) {
        props.precision = toNumber(field.configInfo);
      }
      break;
  }
  return props;
}
/**
 * 解析字段前后缀插槽
 * @param {*} $func
 * @param {*} param1
 */
function parseFieldAddons($func, { field }) {
  const addonConfig = { slots: {}, props: {} };
  // 纵向布局,文本域组件使用
  const verticalLayout =
    field.xtype === FuncFieldTypeEnum.TEXTAREA.xtype || field.otherConfig.inputType === 'textarea';
  // 解析插槽
  ['addonAfter', 'addonBefore'].forEach((key) => {
    const addon = field[key];
    if (addon) {
      // 背景色
      let addonBgColor;
      // 节点
      const vnodes = [];
      addon.forEach((item) => {
        const { bgColor } = item;
        // 设置整体背景色，默认取第一个配置
        if (!addonBgColor && bgColor) {
          addonBgColor = bgColor;
        }
        const addonNode = addonItemSlot({ $func, item });
        if (addonNode) {
          vnodes.push(addonNode);
        }
      });

      // 插槽
      Object.assign(addonConfig.slots, {
        [key]: vnodes.length ? () => vnodes : undefined,
      });

      // 属性
      Object.assign(addonConfig.props, {
        [`${key}Class`]: 'je-input-group-addon ' + (verticalLayout ? 'vertical-layout' : ''),
        [`${key}Style`]: { backgroundColor: addonBgColor },
      });
    }
  });
  return addonConfig;
}

/**
 * 后缀项插槽
 * @param {*} param0
 * @returns
 */
function addonItemSlot({ $func, item }) {
  const { text, showWays, textColor, textfold, functionText, icon } = item;
  let items = [];
  switch (showWays) {
    case 'text':
      if (text) {
        items.push(text);
      } else if (icon) {
        items.push(vueCall('h', 'i', { class: icon }));
      }
      break;
    case 'icon':
      if (icon) {
        items.push(vueCall('h', 'i', { class: icon }));
      } else if (text) {
        items.push(text);
      }
      break;
    case 'icon-text':
      if (icon) {
        items.push(vueCall('h', 'i', { class: icon }));
      }
      if (text) {
        items.push(text);
      }
      if (items.length == 2) {
        items.splice(1, 0, ' ');
      }
      break;
  }
  if (items.length) {
    // 样式处理
    const style = {
      color: textColor,
      fontWeight: toBoolean(textfold) ? 'bold' : undefined,
    };
    // 事件处理
    const onClick = () => {
      doEvents4Sync({ $func, code: functionText });
    };
    return vueCall(
      'h',
      'div',
      {
        class: { 'addon-item': true, 'addon-item-link': isNotEmpty(functionText) },
        style,
        onClick,
      },
      items,
    );
  }
}
