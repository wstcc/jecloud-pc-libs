import { isPromise, toTemplate } from '../../lodash';
import { execScript4Return } from '../../web/sandbox';
import { doEvents4Sync } from '../util';
/**
 * 表达式action
 */
export function useExpsAction() {
  return {
    doFormFieldExps,
    doFormFieldExp,
    doFormFieldExp4Sync,
    bindFormFieldPrivateChange,
    doFormFieldExps4BindFields,
  };
}
/**
 * 执行绑定字段的表达式
 * @param {*} $func
 * @param {*} param1
 */
function doFormFieldExps4BindFields($func, { fieldExps }) {
  // 字段表达式
  Object.keys(fieldExps).forEach((fieldName) => {
    const exps = fieldExps[fieldName];
    doFormFieldAllExps($func, { exps, fieldName });
  });
}

/**
 * 执行表单字段表达式
 * @param {*} param0
 */
function doFormFieldExps($func) {
  const promises = [];
  const $form = $func.getFuncForm();
  if (!$form) return Promise.resolve();
  const funcData = $func.getFuncData();
  const model = $func.store.activeBean;
  const rejectFn = (error) => {
    console.log(error);
  };
  // 字段表达式
  const fields = funcData.exps.fields.map((fieldName) => funcData.basic.fields.get(fieldName)); //表达式赋值字段
  fields.forEach((field) => {
    const fieldAllExp = doFormFieldAllExps($func, { fieldName: field.name, exps: field.exps });
    promises.push(fieldAllExp);
  });
  // 按钮显隐表达式
  const buttons = funcData.exps.buttons.map((code) => $form?.getButtons(code));
  // 子功能显隐表达式
  const childFuncs = funcData.exps.children.map((code) => $form?.getChildFuncConfig(code));
  // 处理表达式
  [buttons, childFuncs].forEach((items) => {
    items
      .filter((item) => item)
      .forEach((item) => {
        const btnChildfExp = doFormFieldExp($func, {
          exp: item.exps.visible,
          model,
        })
          .then((visible) => {
            item.hidden = !visible;
          })
          .catch(rejectFn);
        promises.push(btnChildfExp);
      });
  });
  return Promise.all(promises);
}
/**
 * 触发字段的所有表达式
 * @param {*} $func
 * @param {*} param1
 */
function doFormFieldAllExps($func, { fieldName, exps }) {
  const promises = [];
  const $form = $func.getFuncForm();
  const saveButton = $form?.getButtons('formSaveBtn');
  const { store } = $func;
  // 获取表达式
  Object.keys(exps).forEach((key) => {
    const exp = exps[key];
    // 执行表达式
    const fieldieldExp = doFormFieldExp($func, { exp, model: store.activeBean })
      .then((result) => {
        switch (key) {
          case 'data':
            store.activeBean[fieldName] = result;
            break;
          case 'visible':
            $form?.setFieldVisible(fieldName, result);
            break;
          case 'readonly':
            // 没有保存按钮不执行
            if (saveButton && !saveButton.hidden) {
              $form?.setFieldReadOnly(fieldName, result);
            }
            break;
          case 'required':
            $form?.setFieldRequired(fieldName, result);
            break;
        }
      })
      .catch((error) => {
        console.log(error);
      });
    promises.push(fieldieldExp);
  });
  return Promise.all(promises);
}

/**
 * 执行表达式
 * @param {*} param0
 */
function doFormFieldExp($func, { model, exp }) {
  // 执行表达式
  const result = doFormFieldExp4Sync($func, { model, exp });
  if (isPromise(result)) {
    // Promise结果
    return result;
  } else {
    // 普通
    return Promise.resolve(result);
  }
}
/**
 * 执行表达式-同步
 * @param {*} param0
 * @returns
 */
export function doFormFieldExp4Sync($func, { model, exp }) {
  // 函数表达式
  if (exp.fn) {
    return doEvents4Sync({ $func, code: exp.fn, eventOptions: { model } });
  } else {
    // 字符串表达式
    return execScript4Return(toTemplate(exp.exp, model));
  }
}
/**
 * 通过事件绑定触发表达式
 * @param {*} param0
 * @returns
 */
function bindFormFieldPrivateChange($func, { field }) {
  const fieldName = field.name;
  const bindExps = $func.getFuncData().bindExps[fieldName];
  // 如果绑定字段，注册change事件，然后触发绑定的表达式
  const onPrivateChange = () => {
    doFormFieldExps4BindFields($func, { fieldExps: bindExps });
  };

  if (bindExps) {
    return { onPrivateChange };
  }
}
