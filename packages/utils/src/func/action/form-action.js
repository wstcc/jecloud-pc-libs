import { Modal } from '../../modal';
import { vueCall } from '../../je';
import {
  encode,
  isEmpty,
  simpleDateFormat,
  cloneDeep,
  isArray,
  createDeferred,
  getCurrentUser,
} from '../../lodash';
import { FuncRefEnum, FuncTypeEnum, FuncChangeViewActionEnum } from '../enum';
import { doSaveApi, doTreeSaveApi, checkFieldUniqueApi } from '../api';
export function useFormAction() {
  return {
    doFormSave,
    doFormValidate,
    doFormBack,
    doFormAudit,
    doFormAuditExecute,
    doFormFieldUniqueCheck,
    doFormUpdateById,
  };
}
/**
 * 表单校验
 * @param {*} $func
 * @returns
 */
function doFormValidate($func) {
  const $form = $func.getFuncForm();
  return $form
    .validate()
    .then(() => {
      // 表单验证通过，重置错误信息
      $func.store.formError = {
        popoverVisible: false,
        errorFields: [],
      };
      return Promise.resolve();
    })
    .catch((options) => {
      console.log('表单验证未通过：', options);
      options.formError = true;
      $func.store.formError = {
        popoverVisible: true,
        errorFields: options.errorFields,
      };
      return Promise.reject(options);
    });
}

/**
 * 表单保存
 * @param {*} $func
 * @returns
 */
function doFormSave($func) {
  const { store } = $func;
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcType, funcAction, treeOptions } =
    funcData.info;
  const { codeGenFieldInfo } = funcData.fields;
  const funcTree = funcType === FuncTypeEnum.TREE;
  const bean = vueCall('toRaw', store.activeBean);
  const beanId = bean[pkCode];
  return $func.action.doFormValidate().then(() => {
    const saveApi = funcTree ? doTreeSaveApi : doSaveApi;
    return saveApi({
      params: {
        funcCode,
        tableCode,
        pkCode,
        codeGenFieldInfo: encode(codeGenFieldInfo), // 编号
        ...bean,
      },
      pkValue: beanId,
      pd: productCode,
      action: funcAction,
    })
      .then((options) => {
        // 公共逻辑处理
        const { dynaBean } = options;
        // 子功能集合修改
        return $func.action.doFuncFieldUpdate({ bean: dynaBean }).then(() => options);
      })
      .then(({ dynaBean, node }) => {
        if (beanId) {
          // 更新bean数据
          store.commitActiveBean(dynaBean);
          store.activeBeanEmitter++;
        } else {
          // 重新绑定bean
          store.setActiveBean(dynaBean);
          // 列表分页总数+1;
          const $grid = $func.getFuncGrid();
          $grid && $grid.store.totalCount++;
        }
        const eventResult = { insert: !beanId, bean: dynaBean, node };
        // 树形功能并且是树形展示需要把node的bean解构出来
        if (funcTree && treeOptions.showType == 'tree') {
          Object.assign(eventResult.node, { ...node.bean });
        }
        $func.action.doFormSaveAfter?.(eventResult);
        Modal.notice(beanId ? '保存成功！' : '添加成功！', 'success');
        return eventResult;
      })
      .catch((err) => {
        Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
        console.error(err);
      });
  });
}
/**
 * 页面返回
 * @param {*} $func
 */
function doFormBack($func) {
  const changes = $func.store.getBeanChanges();
  const $form = $func.getFuncForm();
  const saveButton = $form?.getButtons('formSaveBtn');
  // 如果没有保存按钮 || 没有改变值，直接返回
  if (saveButton?.hidden || isEmpty(changes)) {
    $func.setActiveView(FuncRefEnum.FUNC_DATA, FuncChangeViewActionEnum.FORM_BACK);
    return vueCall('nextTick');
  } else {
    const deferred = createDeferred();
    // 返回方法
    const back = () => {
      $func.store.formError = {
        popoverVisible: false,
        errorFields: [],
      };
      $form?.clearValidate();
      $func.store.resetActiveBean();
      $func.setActiveView(FuncRefEnum.FUNC_DATA, FuncChangeViewActionEnum.FORM_BACK);
      deferred.resolve();
    };
    Modal.dialog({
      content: '数据有改动，未保存，确定返回吗？',
      status: Modal.status.question,
      buttons: [
        {
          text: '保存返回',
          type: 'primary',
          handler: () => {
            $form
              .validate()
              .then(() => {
                doFormSave($func).then(back);
              })
              .catch((options) => {
                $func.store.formError = {
                  popoverVisible: true,
                  errorFields: options.errorFields,
                };
              });
          },
        },
        {
          text: '确定返回',
          handler: back,
        },
        { text: '取消' },
      ],
    });
    return deferred;
  }
}

/**
 * 表单审核操作
 * @param {*} param0
 */
function doFormAudit($func, options = {}) {
  const deferred = createDeferred();
  const { ackFlag, button } = options;
  const { text } = button;
  const { store } = $func;
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcAction } = funcData.info;
  Modal.confirm(`您是否确认${text}？`, () => {
    const bean = cloneDeep(store.activeBean);
    // 数据主键
    const beanId = bean[pkCode];
    // 用户信息
    const userData = getCurrentUser();
    // 参数封装
    bean.SY_ACKFLAG = ackFlag; //审核标识
    bean.SY_ACKUSERNAME = userData.name; //审核人
    bean.SY_ACKUSERID = userData.id; //审核人Id
    bean.SY_ACKTIME = simpleDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'); //审核时间
    // 调用接口保存数据
    doSaveApi({
      params: {
        funcCode,
        tableCode,
        pkCode,
        ...bean,
      },
      pkValue: beanId,
      pd: productCode,
      action: funcAction,
    })
      .then(({ dynaBean }) => {
        // 更新bean数据
        store.commitActiveBean(dynaBean);
        // 更新表单
        doFormAuditExecute($func);
        // 更新列表数据
        $func.action.doFormSaveAfter?.({ bean: dynaBean });
        Modal.notice(`${text}成功！`, 'success');
        deferred.resolve();
      })
      .catch((err) => {
        Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
        console.error(err);
        deferred.reject();
      });
  });

  return deferred.promise;
}

/**
 * 执行表单审核
 * @param {*} param0
 */
function doFormAuditExecute($func) {
  // 获得到表单对象
  const $form = $func.getFuncForm();
  if ($form) {
    // 获得功能信息startAudit(启用审核功能)enableWorkflow(启动工作流)
    const { startAudit, enableWorkflow } = $form.getFuncData().info;
    if (startAudit && !enableWorkflow) {
      // 获得审核标识
      const { SY_ACKFLAG } = $func.store.activeBean;
      // 数据审核
      if (SY_ACKFLAG === '1') {
        $form.setReadOnly(true);
      } else {
        // 数据取消审核,重置表单
        $form.resetMetaConfig();
      }
    }
  }
}

/**
 * 字段值唯一性验证
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doFormFieldUniqueCheck($func, fieldCode, value) {
  const { productCode, funcCode, funcAction, pkCode } = $func.getFuncData().info;
  let fields = [];
  if (isArray(fieldCode)) {
    fields = fieldCode;
  } else {
    fields.push({ fieldCode, value });
  }
  if (isEmpty(fields)) {
    return Promise.resolve(true);
  }
  const pkValue = $func.store.activeBean[pkCode];
  return checkFieldUniqueApi({
    params: { funcCode, strData: encode(fields), pkValue },
    action: funcAction,
    pd: productCode,
  }).then((data) => data.success);
}
/**
 * 根据ID进行表单数据跟新
 * @param {*} $func
 * @param {*} beanId
 * @param {*} model
 */
function doFormUpdateById($func, beanId, bean) {
  if (!beanId) return Promise.reject({ code: 'empty' });
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcAction } = funcData.info;
  bean[pkCode] = beanId;
  return doSaveApi({
    params: {
      funcCode,
      tableCode,
      pkCode,
      ...bean,
    },
    pkValue: beanId,
    pd: productCode,
    action: funcAction,
  });
}
