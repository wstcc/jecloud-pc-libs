import { useFuncAction } from './func-action';
import { useExpsAction } from './exps-action';
import { useFormAction } from './form-action';
import { useGridAction } from './grid-action';
import { useFieldAction } from './field-action';
import { useEventAction } from './event-action';
import { useButtonAction } from './button-action';
export function useAction($func) {
  const funcs = {
    ...useFormAction(),
    ...useFuncAction(),
    ...useExpsAction(),
    ...useGridAction(),
    ...useFieldAction(),
    ...useEventAction(),
    ...useButtonAction(),
  };
  const actions = {};
  Object.keys(funcs).forEach((key) => {
    actions[key] = funcs[key].bind(this, $func);
  });
  return actions;
}
