import {
  split,
  isPlainObject,
  isString,
  isEmpty,
  isArray,
  get,
  isNotEmpty,
  toQuerysTemplate,
  omit,
  toValue,
  pick,
  cloneDeep,
  getDDCache,
} from '../../lodash';
import { useListeners } from '../../hooks';
import { parseEventResult4Promise } from './func-event';
import { useJE } from '../../je';
import {
  loadDDItemByCode,
  loadDDItemByCode4Tree,
  DDTypeEnum,
  parseDDCustomVariables,
} from '../../system/dictionary';

/**
 * 配置信息分隔符
 */
const splitStr = '~';

/**
 * 解析字段配置
 * @param {*} param0
 * @returns
 */
export function parseConfigInfo({ type, configInfo, options = {} }) {
  if (isPlainObject(configInfo) || isEmpty(configInfo)) return configInfo;
  switch (type) {
    case 'pinyin':
      // field~pinyin,field2~PY
      let pinyinFields = [];
      if (configInfo.includes(splitStr)) {
        pinyinFields = configInfo.split(',').map((item) => {
          return {
            name: item.split(splitStr)[0],
            config: item.split(splitStr)[1],
          };
        });
      }
      return { type, targetFields: pinyinFields };
    default:
      // 功能编码|字典编码,表单字段1~表单字段2...,选择组件字段1~选择组件字段2...,S(单选)|M(多选)
      const configInfos = configInfo.split(',');
      const code = configInfos[0];
      const targetFields = split(configInfos[1], splitStr);
      const sourceFields = split(configInfos[2], splitStr);
      const multiple = toValue(options.multiple, ['M', 'M_'].includes(configInfos[3]), true);
      const async = toValue(options.async, ['S_', 'M_'].includes(configInfos[3]), true);
      const fieldMaps = { sourceToTarget: {}, targetToSource: {} };
      targetFields.forEach((field, index) => {
        const sourceField = sourceFields[index];
        fieldMaps.targetToSource[field] = sourceField;
        fieldMaps.sourceToTarget[sourceField] = fieldMaps.sourceToTarget[sourceField] || [];
        fieldMaps.sourceToTarget[sourceField].push(field);
      });
      return {
        type,
        code,
        async,
        targetFields,
        sourceFields,
        multiple,
        fieldMaps,
      };
  }
}

/**
 * 设置配置信息
 * @param {*} param0
 * @returns
 */
export function getConfigInfo({ type, configInfo, rows = [], paths = [] }) {
  const config = isString(configInfo) ? parseConfigInfo({ type, configInfo }) : configInfo;
  const value = {};
  switch (type) {
    default:
      rows = isArray(rows) ? rows : [rows];
      // 循环赋值
      config.sourceFields.forEach((field) => {
        const val = getConfigFieldValue({ rows, paths, field });
        const targetFields = config.fieldMaps.sourceToTarget[field] || [];
        targetFields.forEach((field) => {
          value[field] = val;
        });
      });
      break;
  }
  return value;
}
/**
 * 获得配置项字段的值
 * @param {*} param0
 * @returns
 */
export function getConfigFieldValue({ field, rows, paths }) {
  return rows
    .map((row, index) => {
      if (field.endsWith('_')) {
        return get(paths[index] || {}, field);
      } else {
        return get(row, field) || get(row.bean || {}, field);
      }
    })
    .join(',');
}

/**
 * 查找options数据项对应的text
 * @param {*} param0
 */
export function findOptionsText({ options, value, displayField = 'label', valueField = 'value' }) {
  let dataPromise;
  if (typeof options === 'function') {
    dataPromise = options();
  } else {
    dataPromise = Promise.resolve(options);
  }
  return dataPromise.then((data) => {
    const vals = value ? value.split(',') : [];
    const texts = [];
    vals.forEach((val) => {
      const option = data.find((item) => item[valueField] === val);
      if (option) {
        const text = option[displayField] || val;
        texts.push(text);
      }
    });

    return { data, text: texts.join(',') };
  });
}

/**
 * 加载字典数据选项
 * @param {Object} options
 * @param {string} options.ddCode // 字典编码
 * @param {Array} options.querys // 查询条件
 * @param {Object} options.model // 当前model
 * @param {Object} options.parentModel // 父功能model
 * @param {Object} options.dicValueConfig 字典带值配置项
 */
export function loadSelectOptions(options) {
  const { querys, ddCode } = options;
  const ddCache = getDDCache(ddCode);
  if (!ddCache) {
    return Promise.resolve([]);
  }
  let next;
  // 树形或sql查询
  if (ddCache?.type !== DDTypeEnum.LIST || isNotEmpty(querys)) {
    next = loadSelectOptionsByQuery(options);
  } else {
    // 普通字典
    next = loadDDItemByCode(ddCode).then((data) =>
      data.map?.((item) => {
        return Object.assign(item, { value: item.code, label: item.text });
      }),
    );
  }
  return next.then((data) =>
    data.map?.((item) => {
      return Object.assign(omit(item, ['disabled']), {
        value: item.code,
        label: item.text,
      });
    }),
  );
}

/**
 * 加载字典数据,sql
 * @param {*} options
 */
function loadSelectOptionsByQuery(options) {
  const { querys, model, parentModel, dicValueConfig, ddCode } = options;
  toQuerysTemplate({ querys, model, parentModel });
  // 字典过滤数据
  const customVariables = parseDDCustomVariables({ dicValueConfig, model, parentModel });
  // 解析RootID
  let rootId = 'ROOT';
  querys?.forEach?.((item) => {
    if (item.code === 'SY_PARENT') {
      rootId = item.value;
    }
  });
  return loadDDItemByCode4Tree({
    rootId,
    code: ddCode,
    querys,
    customVariables,
    excludes: ['children', 'nodeType'],
  });
}

/**
 * 功能选择
 */
export function useFuncSelect({ props, context }) {
  const { fireListener } = useListeners({ props, context });
  const fireSelectEvent = (eventName, options) => {
    return parseEventResult4Promise(fireListener(eventName, options));
  };

  // 解析配置信息
  let selectConfig;
  const configInfo = props.selectOptions?.configInfo || props.configInfo;
  if (configInfo) {
    selectConfig = parseConfigInfo({ configInfo });
  }
  // 功能对象
  const func = useJE().useFunc?.();
  const $func = func?.useInjectFunc();

  // 获取可修改的属性
  const getCloneConfig = () => {
    const config = Object.assign(
      pick(props, [
        'idProperty',
        'configInfo',
        'querys',
        'orders',
        'product',
        'selectExp',
        'dicValueConfig',
      ]),
      props.selectOptions || {},
    );
    return cloneDeep(config);
  };

  // 打开选择器
  const showSelectWindow = ({ type, model, value, callback, eventOptions, selectOptions }) => {
    // 功能类库判断
    if (!func) return Promise.reject({ message: '请注册功能引擎库' });
    // 类型判断
    const types = ['tree', 'grid', 'user'];
    if (!types.includes(type)) {
      const message = `功能选择字段类型不在支持的列表中：${[types.join('，')]}`;
      console.error(message);
      return Promise.reject({ message });
    }
    // 配置项
    const options = getCloneConfig();
    // 加载前事件，可以修改配置信息
    return fireSelectEvent('before-select', {
      ...eventOptions,
      $func,
      options,
    }).then(() => {
      const parentModel = $func?.parentFunc?.store.activeBean || {};
      // 解析查询参数
      if (options.querys) {
        toQuerysTemplate({
          querys: options.querys,
          model: model,
          parentModel,
        });
      }
      // 人员选择器查询处理
      if (options.querysConfig) {
        Object.values((querys) => {
          toQuerysTemplate({
            querys,
            model,
            parentModel,
          });
        });
      }
      func.showSelectWindow({
        value,
        model,
        parentModel,
        type,
        ...selectOptions,
        ...options,
        callback({ rows, paths }) {
          return fireSelectEvent('select', {
            ...eventOptions,
            rows,
            paths,
          }).then(() => {
            const values = getConfigInfo({ configInfo: options.configInfo, rows, paths });
            return callback({ rows, values, paths });
          });
        },
      });
    });
  };

  return { showSelectWindow, fireSelectEvent, getCloneConfig, selectConfig, func, $func };
}
