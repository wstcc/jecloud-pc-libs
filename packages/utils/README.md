# JECloud 工具库
工具库集成了`lodash`工具库，并扩展了一些工具函数，大家可以直接使用lodash的方法

### 集成第三方类库列表：
- [lodash](https://www.lodashjs.com/)
- [pinyin-pro](https://github.com/zh-lx/pinyin-pro)
- [dayjs](https://dayjs.fenxianglu.cn/)
- [axios](https://www.kancloud.cn/yunye/axios/234845)
- [loadjs](https://github.com/muicss/loadjs)
