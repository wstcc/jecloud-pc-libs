import { defineComponent, watch, ref } from 'vue';
import { Panel, Select } from '../../';
import { usePreviewFile } from './hooks/use-preview-file';
import { FileType } from './utils';
import { isNotEmpty } from '@jecloud/utils';
import { TreeSelect } from 'ant-design-vue';
import previewPdf from './preview-pdf';
import previewImage from './preview-image';
import previewVideo from './preview-video';
import previewAudio from './preview-audio';
import previewNosupport from './preview-nosupport';
import previewNofind from './preview-nofind';
import previewNofile from './preview-nofile';

export default defineComponent({
  name: 'JePreviewFile',
  inheritAttrs: false,
  props: {
    fileList: {
      //文件数据
      type: Array,
      default: () => [],
    },
    fileItem: {
      //当前预览的文件对象
      type: Object,
      default: () => null,
    },
    handleStatus: {
      //是否显示窗口右侧操作按钮
      type: Boolean,
      default: () => false,
    },
    treeDataType: {
      //是否为文件树结构数据
      type: Boolean,
      default: () => false,
    },
    style: {
      //当前窗口预览样式
      type: Object,
      default: () => null,
    },
    showRefresh: {
      //显示刷新按钮
      type: Boolean,
      default: () => false,
    },
    showOpenNewView: {
      //显示新浏览器页签打开按钮
      type: Boolean,
      default: () => false,
    },
    onClose: Function, //窗口关闭方法
    onMaxStaus: Function, //放大窗口
    onRevertModal: Function, //还原窗口大小
  },
  emits: ['refreshFileList'],
  setup(props, context) {
    const {
      previewFileList,
      previewFileKey,
      previewHandleStatus,
      previewFileType,
      previewFileName,
      previewFileObj,
      previewListDataType,
      previewTreeList,
      selectWidth,
      showOpenWindow,
      checkFileExist,
    } = usePreviewFile({ props });
    const filePreviewIndex = ref(0); //文件预览下标
    // const globalStore = useGlobalStore();

    //监听文件变化
    watch(
      () => previewFileKey.value,
      (newVal) => {
        if (previewListDataType.value) {
          // previewTreeList.value.forEach((file, index) => {
          //   if (file.value == newVal) {
          //     filePreviewIndex.value = index;
          //     previewFileObj.value = file;
          //     previewFileName.value = file.label;
          //     checkFileExist(file);
          //   }
          // });
          if (props.fileList?.length > 1) {
            //子应用功能多字段
            previewTreeList.value.forEach((file, index) => {
              if (file.value == newVal) {
                filePreviewIndex.value = index;
                previewFileObj.value = file;
                previewFileName.value = file.label;
                checkFileExist(file);
              }
            });
          } else {
            //子应用功能单字段
            previewFileList.value.forEach((file, index) => {
              if (file.value == newVal) {
                filePreviewIndex.value = index;
                previewFileObj.value = file;
                previewFileName.value = file.label;
                checkFileExist(file);
              }
            });
          }
        } else {
          if (isNotEmpty(previewFileList.value)) {
            previewFileList.value.forEach((file, index) => {
              if (file.value == newVal) {
                filePreviewIndex.value = index;
                previewFileObj.value = file;
                previewFileName.value = file.label;
                checkFileExist(file);
              }
            });
          } else {
            filePreviewIndex.value = 0;
            let curFileObj = { ...props.fileItem, value: props.fileItem?.fileKey };
            previewFileName.value = props.fileItem.relName;
            previewFileObj.value = curFileObj;
            checkFileExist(curFileObj);
          }
        }
      },
      { immediate: true },
    );
    //关闭弹窗
    const handleCloseModal = () => {
      props.onClose();
    };
    const maxStatus = ref(false);
    //最大化窗口
    const handleMaxModal = () => {
      maxStatus.value = !maxStatus.value;
      maxStatus.value ? props.onMaxStaus() : props.onRevertModal();
    };
    //上一个文件预览
    const handlePrevFile = () => {
      if (previewListDataType.value) {
        //树结构数据
        if (filePreviewIndex.value != 0) {
          filePreviewIndex.value -= 1;
          previewFileKey.value = previewTreeList.value[filePreviewIndex.value].value;
          previewFileName.value = previewTreeList.value[filePreviewIndex.value].label;
        }
      } else {
        //普通数组结构
        if (filePreviewIndex.value != 0) {
          filePreviewIndex.value -= 1;
          previewFileKey.value = previewFileList.value[filePreviewIndex.value].value;
          previewFileName.value = previewFileList.value[filePreviewIndex.value].label;
        }
      }
    };
    //下一个文件预览
    const handleNextFile = () => {
      if (previewListDataType.value) {
        //树结构数据
        if (filePreviewIndex.value != previewTreeList.value.length - 1) {
          filePreviewIndex.value += 1;
          previewFileKey.value = previewTreeList.value[filePreviewIndex.value].value;
          previewFileName.value = previewTreeList.value[filePreviewIndex.value].label;
        }
      } else {
        //普通数组结构
        if (filePreviewIndex.value != previewFileList.value.length - 1) {
          filePreviewIndex.value += 1;
          previewFileKey.value = previewFileList.value[filePreviewIndex.value].value;
          previewFileName.value = previewFileList.value[filePreviewIndex.value].label;
        }
      }
    };
    //打开新窗口页签
    const handleOpenBrowser = () => {
      const plan = location.hash.split('/')[1]; //激活方案
      const keyArr = []; //文件key数组
      if (previewListDataType.value) {
        //树结构文件
        if (isNotEmpty(previewTreeList.value)) {
          previewTreeList.value.forEach((file) => {
            keyArr.push(file.value);
          });
        }
      } else {
        //普通结构文件
        if (isNotEmpty(previewFileList.value)) {
          previewFileList.value.forEach((file) => {
            keyArr.push(file.value);
          });
        } else {
          keyArr.push(props.fileItem?.fileKey);
        }
      }
      const keys = keyArr.reverse().join(',');
      const urlHref = `/#/whitelist/preview/file/${keys}/${previewFileKey.value}`;
      window.open(urlHref, '_blank');
    };
    //文件刷新
    const handleRefreshFile = (e) => {
      context.emit('refreshFileList');
      previewFileKey.value = '';
      previewFileObj.value = null;
      e.preventDefault();
    };
    return () => (
      <Panel style={previewListDataType.value ? props.style : null}>
        <Panel.Item region="tbar">
          <div
            class="je-preview-file-header"
            style={{
              height: previewListDataType.value ? '40px' : '60px',
              padding: previewListDataType.value ? '0 18px 0 7px' : '0 20px',
            }}
          >
            <div class="preview-file-header-left">
              {isNotEmpty(props.fileItem) || isNotEmpty(props.fileList) ? (
                previewFileList.value.length > 1 || previewTreeList.value.length > 1 ? (
                  <div class="preview-file-multiple">
                    {previewListDataType.value && props.fileList?.length > 1 ? (
                      <TreeSelect
                        v-model:value={previewFileKey.value}
                        tree-default-expand-all={true}
                        dropdown-style={{
                          maxHeight: '300px',
                          overflow: 'auto',
                        }}
                        tree-data={previewFileList.value}
                        treeIcon={true}
                        style={{
                          width: selectWidth.value + 'px',
                          fontSize: '14px',
                        }}
                        v-slots={{
                          title(item) {
                            return item.nodeType == 'LEAF' ? (
                              <span
                                title={item.title}
                                style={{ height: '30px', lineHeight: '30px' }}
                              >
                                {item.title}
                              </span>
                            ) : (
                              <>
                                <i class="fas fa-folder-open" style="color: #FFB04A"></i>
                                <span
                                  style={{
                                    paddingLeft: '3px',
                                    height: '30px',
                                    lineHeight: '30px',
                                  }}
                                  title={item.title}
                                >
                                  {item.title}
                                </span>
                              </>
                            );
                          },
                        }}
                      ></TreeSelect>
                    ) : (
                      <Select
                        class="preview-file-select"
                        options={previewFileList.value}
                        v-model:value={previewFileKey.value}
                        allowClear={false}
                        style={{
                          width: selectWidth.value + 'px',
                          fontSize: props.showRefresh ? '14px' : '16px',
                        }}
                      ></Select>
                    )}
                    {/* <div
                    className={
                      filePreviewIndex.value == 0
                        ? 'preview-file-header-page-btn page-btn-disabled'
                        : 'preview-file-header-page-btn page-btn-normal'
                    }
                    onClick={handlePrevFile}
                  >
                    <i class="fal fa-circle-arrow-left"></i>
                    <span class="span-left">上一个</span>
                  </div>
                  <div
                    className={
                      filePreviewIndex.value ==
                      (previewListDataType.value
                        ? previewTreeList.value.length - 1
                        : previewFileList.value.length - 1)
                        ? 'preview-file-header-page-btn page-btn-disabled '
                        : 'preview-file-header-page-btn page-btn-normal'
                    }
                    onClick={handleNextFile}
                  >
                    <span class="span-right">下一个</span>
                    <i class="fal fa-circle-arrow-right"></i>
                  </div> */}
                  </div>
                ) : (
                  <div
                    class="preview-file-single"
                    style={{
                      fontSize: previewListDataType.value ? '14px' : '16px',
                      paddingLeft: props.showRefresh ? '11px' : 0,
                    }}
                  >
                    {previewFileName.value}
                  </div>
                )
              ) : null}
              {props.showRefresh ? (
                <div
                  class="preview-file-header-refresh"
                  onClick={handleRefreshFile}
                  style={{
                    marginLeft:
                      isNotEmpty(props.fileItem) || isNotEmpty(props.fileList) ? '20px' : '11px',
                  }}
                >
                  <span>刷新</span>
                </div>
              ) : null}
            </div>
            <div class="preview-file-header-right">
              {showOpenWindow.value &&
              (isNotEmpty(props.fileItem) || isNotEmpty(props.fileList)) ? (
                <span class="preview-file-header-btn" onClick={handleOpenBrowser}>
                  新浏览器页签打开
                </span>
              ) : null}

              {previewHandleStatus.value ? (
                <i
                  class="preview-file-header-btn fal fa-window-maximize"
                  title="最大化"
                  onclick={handleMaxModal}
                ></i>
              ) : null}
              {previewHandleStatus.value ? (
                <i
                  class="preview-file-header-btn fal fa-times"
                  title="关闭"
                  onclick={handleCloseModal}
                ></i>
              ) : null}
            </div>
          </div>
        </Panel.Item>
        {isNotEmpty(props.fileItem) || isNotEmpty(props.fileList) ? (
          <Panel.Item>
            {previewFileType.value == FileType.PDF ? (
              <previewPdf previewFile={previewFileObj.value} isFuncFile={props.treeDataType} />
            ) : null}
            {previewFileType.value == FileType.IMAGE ? (
              <previewImage previewFile={previewFileObj.value} isFuncFile={props.treeDataType} />
            ) : null}
            {previewFileType.value == FileType.VIDEO ? (
              <previewVideo previewFile={previewFileObj.value} />
            ) : null}
            {previewFileType.value == FileType.AUDIO ? (
              <previewAudio previewFile={previewFileObj.value} isFuncFile={props.treeDataType} />
            ) : null}
            {previewFileType.value == FileType.NOSUPPORT ? (
              <previewNosupport previewFile={previewFileObj.value} />
            ) : null}
            {previewFileType.value == FileType.NOFIND ? <previewNofind /> : null}
          </Panel.Item>
        ) : (
          <Panel.Item>
            <previewNofile />
          </Panel.Item>
        )}
      </Panel>
    );
  },
});
