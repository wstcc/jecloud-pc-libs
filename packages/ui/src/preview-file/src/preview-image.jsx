import { defineComponent, nextTick, watch, ref } from 'vue';
import { isEmpty } from '@jecloud/utils';
import { loadImageObject, getFileUrlByKey } from './utils';

export default defineComponent({
  name: 'JePreviewImage',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: Object,
      default: () => null,
    },
    isFuncFile: {
      //是否为子功能附件
      type: Boolean,
      default: () => false,
    },
  },
  setup(props) {
    const imageId = ref(props.isFuncFile ? 'funImagePreviewBox' : 'normalImagePreviewBox');
    const imageConfig = {
      //图片预览配置
      paginationable: true, // 分页mouseover切换图片
      showClose: false, // 显示关闭按钮
      showSwitchArrow: true, //显示上一张/下一张箭头
      showPagination: true, //显示分页导航栏
      showToolbar: true, //显示工具栏
      scalable: true, //缩放
      rotatable: true, //旋转
      movable: true, // 移动
      keyboard: {
        // 键盘配置
        // scale: ['equal', 'minus']
      },
    };
    let imgs = [];
    let zivObj = null;
    watch(
      () => props.previewFile,
      (newVal) => {
        if (!isEmpty(newVal)) {
          zivObj && zivObj.destroy();
          imgs = [];
          const imgItem = {
            url: getFileUrlByKey(newVal.value),
            angle: 0,
            fileName: newVal.label ?? newVal.relName,
          };
          imgs.push(imgItem);
          nextTick(() => {
            loadImageObject().then((ZxImageView) => {
              // 使用方法
              zivObj = new ZxImageView(imageConfig, imgs);
              setTimeout(function () {
                zivObj.view(0);
              }, 100);
            });
          });
        }
      },
      { immediate: true },
    );
    return () => (
      <div
        id={imageId.value}
        style={{ width: '100%', height: '100%', position: 'relative', overflow: 'hidden' }}
      ></div>
    );
  },
});
