import { defineComponent, h, createApp, resolveComponent, reactive } from 'vue';

let dynamicContainerElem;

export const dynamicStore = reactive({
  modals: [],
});

/**
 * 动态组件
 */
const VxeDynamics = defineComponent({
  setup() {
    return () => {
      const { modals } = dynamicStore;
      return h(
        'div',
        {
          class: 'vxe-dynamics--modal',
        },
        modals.map((item) => h(resolveComponent('je-modal'), item)),
      );
    };
  },
});

export const dynamicApp = createApp(VxeDynamics);

export function checkDynamic() {
  if (!dynamicContainerElem) {
    dynamicContainerElem = document.createElement('div');
    dynamicContainerElem.className = 'vxe-dynamics';
    document.body.appendChild(dynamicContainerElem);
    dynamicApp.mount(dynamicContainerElem);
  }
}
