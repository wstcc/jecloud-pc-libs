import { ref } from 'vue';
import Drawer from './drawer';
import { renderVNode } from '../../utils/vnode';
/**
 * 函数打开抽屉
 * @param {*} config
 * @returns
 */
export const show = (config) => {
  // v-model:visible
  const visible = ref(config.visible ?? true);
  Object.assign(config, {
    destroyOnClose: true,
    visible: visible.value,
    'onUpdate:visible': (val) => {
      visible.value = val;
    },
  });
  const refEl = ref();
  renderVNode(() =>
    visible.value ? <Drawer ref={refEl} {...config} v-slots={config?.slots} /> : null,
  );
  return refEl.value;
};
