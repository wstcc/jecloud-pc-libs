import { withInstall } from '../utils';
import JeHighlightText from './src/highlight-text';
export const HighlightText = withInstall(JeHighlightText);
export default HighlightText;
