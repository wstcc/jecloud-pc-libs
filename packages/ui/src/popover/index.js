import JePopover from './src/popover';
import { withInstall } from '../utils';

export const Popover = withInstall(JePopover);

export default Popover;
