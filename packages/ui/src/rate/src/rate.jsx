import { defineComponent } from 'vue';
import { Rate } from 'ant-design-vue';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeRate',
  inheritAttrs: false,
  props: { value: Number },
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs } = context;
    const value = useModelValue({ props, context, changeEvent: true });
    return () => (
      <Rate
        class={{
          'je-rate white-background': true,
          'disabled-background': attrs.disabled,
        }}
        {...attrs}
        v-model:value={value.value}
        v-slots={slots}
      ></Rate>
    );
  },
});
