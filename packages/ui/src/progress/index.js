import JeProgress from './src/progress';
import { withInstall } from '../utils';

export const Progress = withInstall(JeProgress);

export default Progress;
