import { defineComponent } from 'vue';

export default defineComponent({
  name: 'JeEditorMarkdown',
  props: {
    value: String,
    type: {
      type: String,
      default: 'editor',
    },
  },
  setup(props) {
    return () =>
      props.type == 'preview' ? <v-md-preview text={props.value}></v-md-preview> : null;
  },
});
