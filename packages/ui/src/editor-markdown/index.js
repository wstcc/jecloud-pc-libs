import { withInstall } from '../utils';
import JeEditorMarkdown from './src/editor-markdown';
import VMdPreview from '@kangc/v-md-editor/lib/preview';
import '@kangc/v-md-editor/lib/style/preview.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';
// highlightjs
import hljs from 'highlight.js';

// 注册依赖组件
export const EditorMarkdown = withInstall(JeEditorMarkdown, (app) => {
  VMdPreview.use(githubTheme, {
    Hljs: hljs,
  });
  app.use(VMdPreview);
});
export default EditorMarkdown;
