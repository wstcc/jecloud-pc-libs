/*
 * 拼音类型'py' | 'PY' | 'pinyin' | 'PINYIN' | 'PinYin'
 */
import { defineComponent, watch } from 'vue';
import Input from '../../input';
import { pinyin } from '@jecloud/utils';
import { useModelValue, useField } from '../../hooks';

export default defineComponent({
  name: 'JePinyin',
  components: { Input },
  inheritAttrs: false,
  props: {
    model: Object,
    configInfo: {
      type: String,
      default: 'pinyin',
      validator: (val) => {
        const config = ['py', 'PY', 'pinyin', 'PINYIN', 'PinYin'];
        return val.includes('~') || config.includes(val);
      },
    },
    value: { type: String, default: '' },
  },
  emits: ['update:value', 'update:model'],
  setup(props, context) {
    const { slots, attrs } = context;
    const value = useModelValue({ props, context });
    const { getModel } = useField({ props, context });

    // 解析配置：field~pinyin,field2~py,...
    let targetFields = [];
    const splitStr = '~';
    if (props.configInfo.includes(splitStr)) {
      targetFields = props.configInfo.split(',').map((item) => {
        return {
          name: item.split(splitStr)[0],
          config: item.split(splitStr)[1],
        };
      });
    }

    watch(
      () => value.value,
      (val) => {
        const model = getModel();
        targetFields.forEach((item) => {
          model[item.name] = pinyin(val, item.config);
        });
      },
    );
    return () => <Input v-model:value={value.value} {...attrs} v-slots={slots}></Input>;
  },
});
