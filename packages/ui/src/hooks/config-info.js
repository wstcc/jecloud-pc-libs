import { ref } from 'vue';
import { useField } from './field';
import { useFunc } from './use-func';
import {
  isArray,
  isEmpty,
  isNotEmpty,
  cloneDeep,
  pick,
  getDDCache,
  DDTypeEnum,
  parseEventResult4Promise,
} from '@jecloud/utils';
import { useModelValue, useListeners } from '.';

export function useConfigInfo({ props, context }) {
  const loading = ref(false);
  const searchType = ref(true);
  const { fireListener } = useListeners({ props, context });
  const options = useModelValue({ props, context, key: 'options' });
  const multiple = ref(props.multiple);
  const func = useFunc();
  let setValues = () => {};
  let onDropdownVisibleChange = () => {}; // 下拉框展开请求数据，非列表字典起效
  // 配置项
  const config = pick(props, ['configInfo', 'querys', 'dicValueConfig']);
  if (config.configInfo && func) {
    // 功能对象
    const $func = func.useInjectFunc();
    const { setModel, getModelOwner, getModel } = useField({ props, context });
    // 解析配置信息
    const configInfo = func.parseConfigInfo({ configInfo: config.configInfo });
    // 字典缓存信息
    const ddCache = getDDCache(configInfo.code);
    multiple.value = configInfo.multiple;
    // 设置值
    setValues = (values = []) => {
      if (isNotEmpty(values)) {
        values = isArray(values) ? values : [values];
        // 设置配置项的值
        const data = func.getConfigInfo({ configInfo, rows: values });
        setModel(data);
      }
    };
    // 请求字典项数据
    if (isEmpty(options.value)) {
      // 树形请求
      onDropdownVisibleChange = (open) => {
        if (!open) return;
        searchType.value = true;
        loading.value = true;
        // 配置信息
        const cloneConfig = cloneDeep(config);
        // 加载前事件，可以修改配置信息
        parseEventResult4Promise(
          fireListener('before-select', {
            ...getModelOwner(),
            options: cloneConfig,
          }),
        )
          .then((data) => {
            // 树形或sql查询
            if (ddCache?.type !== DDTypeEnum.LIST || isNotEmpty(cloneConfig?.querys)) {
              // 查询前，清空数据
              options.value = [];
            }
            // 自定义数据
            if (isArray(data)) {
              loading.value = false;
              options.value = data;
            } else {
              // 加载数据
              func
                .loadSelectOptions({
                  ...cloneConfig,
                  ddCode: ddCache?.code,
                  model: getModel(),
                  parentModel: $func?.parentFunc?.store.activeBean,
                })
                .then((data) => {
                  loading.value = false;
                  options.value = data;
                });
            }
          })
          .catch((error) => {
            error && console.log(error);
          });
      };
      // 初始化数据
      onDropdownVisibleChange(true);
    }
  }

  return { func, options, multiple, setValues, onDropdownVisibleChange, loading, searchType };
}
