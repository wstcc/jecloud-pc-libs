import { isValidSlot } from '../utils/props';
import { Input } from 'ant-design-vue';
/**
 * 前缀，后缀插槽
 * @param {*} param0
 * @returns
 */
export function useAddonSlot({ props, context, element }) {
  const { slots } = context;

  // 前后缀
  const {
    addonBefore = slots.addonBefore?.(),
    addonAfter = slots.addonAfter?.(),
    addonAfterClass,
    addonBeforeClass,
    addonAfterStyle,
    addonBeforeStyle,
  } = props;

  // 无前后缀，直接返回元素
  if (!isValidSlot(addonBefore) && !isValidSlot(addonAfter)) {
    return element;
  }

  return (
    <Input.Group class="je-addon-group-wrapper">
      {addonBefore ? (
        <span class={['ant-input-group-addon', addonBeforeClass]} style={addonBeforeStyle}>
          {addonBefore}
        </span>
      ) : null}
      {element}
      {addonAfter ? (
        <span class={['ant-input-group-addon', addonAfterClass]} style={addonAfterStyle}>
          {addonAfter}
        </span>
      ) : null}
    </Input.Group>
  );
}
/**
 * 属性
 */
export const addonProps = {
  addonBefore: String,
  addonAfter: String,
  addonBeforeClass: String,
  addonAfterClass: String,
  addonBeforeStyle: [String, Object],
  addonAfterStyle: [String, Object],
};
/**
 * 插槽
 */
export const addonSlots = ['addonBefore', 'addonAfter'];
