import { ref, reactive, h } from 'vue';
import {
  encode,
  downloadFile,
  uploadFile,
  isNotEmpty,
  createDeferred,
  isEmpty,
  removeFileApi,
  getFilePropertyApi,
} from '@jecloud/utils';
import { Modal, Form, Input } from '../../../index';
import { previewFile } from '../../../utils/file';
import { FileActions } from './enum';
export * from './enum';

/**
 * 文件操作
 * @param {*} param0
 */
export function doFileAction({ $upload, file, type }) {
  // 按钮操作
  switch (type) {
    case FileActions.upload.code: // 上传
      doFileUpload({ $upload });
      break;
    case FileActions.rename.code: // 重命名
      doFileRename({ $upload, file });
      break;
    case FileActions.downloadAll.code: // 全部下载
    case FileActions.download.code: // 下载
      doFileDownload({ $upload, file });
      break;
    case FileActions.removeAll.code: // 清空
    case FileActions.remove.code:
      doFileRemove({ $upload, file });
      break;
    case FileActions.preview.code: // 预览
      doFilePreview({ $upload, file });
      break;
    case FileActions.property.code: // 属性
      doFileProperty({ $upload, file });
      break;
  }
}

/**
 * 文件上传
 * @param {*} param0
 * @returns
 */
export function doFileUpload({ $upload, options = {} }) {
  // 上传中
  if ($upload.isUploading()) {
    return;
  }
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.upload.code, options)) return;

  const { props, state, multiple } = $upload;
  const { url, params, maxSize, excludeSuffix, includeSuffix } = props;
  const maxCount = props.maxCount > 0 ? props.maxCount - state.files.length : props.maxCount;
  const tipMaxCount = props.maxCount || 0;
  // 上传中
  return uploadFile({
    url,
    params,
    maxSize, //文件大小限制
    maxCount, //文件上传数量
    tipMaxCount,
    includeSuffix,
    excludeSuffix,
    multiple,
    onFlowInit(flow) {
      // 获得上传对象
      $upload.flowObj = flow;
    },
    onValidate({ files }) {
      // 多选文件，校验文件名是否重复
      if (multiple) {
        const reFiles = files.filter((file) => state.files.find((f) => f.relName === file.name));
        if (reFiles.length > 0) {
          const names = reFiles.map((file) => file.name);
          return Promise.reject({
            errors: [{ message: `${names.join('，')} 文件已存在！` }],
          });
        }
      }
    },
    // 上传前，追加文件信息
    onUploadStart({ files }) {
      $upload.addFiles(files);
      // 开始上传标识
      state.uploading = true;
    },
    // 单文件的上传进度
    onFileProgress({ file, progress }) {
      state.progress[file.name] = progress;
    },
    ...options,
  })
    .then((files) => {
      $upload.addFiles(files);
      $upload.refreshValue();
    })
    .catch((error) => {
      if (error.code === 'validate') {
        // 校验失败
        const { errors = [] } = error;
        isNotEmpty(errors) &&
          Modal.alert({
            title: '校验失败',
            status: Modal.status.warning,
            content() {
              return errors.map((err) => h('div', err.message));
            },
          });
      } else {
        // 文件上传失败
        error.message &&
          Modal.alert({
            title: '上传失败',
            message: error.message,
            status: Modal.status.error,
          });
      }
    })
    .finally(() => {
      state.uploading = false;
      state.progress = {};
    });
}
/**
 * 文件下载
 * @param {*} param0
 */
export function doFileDownload({ file, $upload }) {
  const { state } = $upload;
  let fileKeys = state.files.map((f) => f.fileKey);
  if (file) {
    fileKeys = [file.fileKey];
  }
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.download.code, { fileKeys })) return;
  isNotEmpty(fileKeys) && downloadFile(fileKeys);
}

/**
 * 文件删除
 * @param {*} param0
 */
export function doFileRemove({ file, $upload }) {
  const { state, modelValue } = $upload;
  let message = '是否要清空数据？';
  let fileKeys = state.files.map((f) => f.fileKey);
  if (file) {
    message = `是否要删除${file.relName}？`;
    fileKeys = [file.fileKey];
  }
  // 上传中
  if ($upload.isUploading() || isEmpty(fileKeys)) {
    return;
  }
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.remove.code, { fileKeys })) return;

  const deferred = createDeferred();

  Modal.confirm({
    content: message,
    okButton: () => {
      removeFileApi(fileKeys)
        .then(() => {
          if (file && state.files.length > 1) {
            const index = state.files.indexOf(file);
            state.files.splice(index, 1);
            $upload.refreshValue();
          } else {
            modelValue.value = '';
          }
          Modal.notice('删除成功！', Modal.status.success);
          // 删除后调用方法
          $upload.props.afterFileRemove?.({ value: modelValue.value, $upload });
          deferred.resolve();
        })
        .catch((error) => {
          Modal.alert(error.message, Modal.status.warning);
        })
        .finally(() => {
          if ($upload.isUploading()) {
            $upload.cancelUpload(); // 取消上传
          }
        });
    },
  });
  return deferred.promise;
}
/**
 * 文件重命名
 * @param {*} param0
 */
export function doFileRename({ file, $upload }) {
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.rename.code, { file })) return;

  const { state, modelValue } = $upload;
  const $form = ref();
  const { relName } = file;
  const formModel = reactive({ name: relName });
  const rules = {
    name: [
      {
        required: true,
        message: '该输入项为必填项',
      },
      {
        validator: (rule, value) => {
          if ($upload.getFileByName(value)) {
            return Promise.reject('文件已存在，请重新命名');
          } else {
            return Promise.resolve();
          }
        },
      },
    ],
  };
  Modal.window({
    title: '重命名',
    width: '500',
    height: 'auto',
    maximizable: false,
    content() {
      return (
        <Form ref={$form} model={formModel} layout="vertical" rules={rules}>
          <Form.Item label="请输入新的名称" name="name">
            <Input v-model:value={formModel.name} />
          </Form.Item>
        </Form>
      );
    },
    okButton: {
      handler: ({ $modal, button }) => {
        button.loading = true;
        $form.value
          .validate()
          .then(() => {
            file.relName = formModel.name;
            $upload.refreshValue();
            $modal.close();
            button.loading = false;
          })
          .catch(() => {
            button.loading = false;
          });
        return false;
      },
    },
    cancelButton: true,
  });
}
/**
 * 文件预览
 * @param {*} param0
 */
export function doFilePreview({ $upload, file, files }) {
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.preview.code, { file, files })) return;

  previewFile(file, $upload.state.files);
}
/**
 * 文件详细信息
 * @param {*} param0
 */
export function doFileProperty({ $upload, file }) {
  // 操作前事件
  if (!$upload.fireBeforeFileAction(FileActions.property.code, { file })) return;
  getFilePropertyApi(file.fileKey).then((resData) => {
    Modal.window({
      title: '详细信息',
      width: '500',
      height: 'auto',
      class: 'je-upload-files-property-modal',
      maximizable: false,
      content() {
        return (
          <>
            <p>名称：{resData.name}</p>
            <p>类型：{resData.type}</p>
            <p>大小：{resData.fileSize} KB</p>
            <p>上传人：{resData.createUser}</p>
            <p>上传人部门：{resData.department}</p>
            <p>上传时间：{resData.createTime}</p>
          </>
        );
      },
      cancelButton: { text: '关闭' },
    });
  });
}
