import { FileActions } from '../utils';
import { useUploadCommon } from './use-upload-common';
import { ref } from 'vue';
export function useUploadFiles({ props, context }) {
  const { state, computeReadonly, iconSlot, actionClick, actionsSlot } = useUploadCommon({
    props,
    context,
    multiple: true,
  });
  const tbarButtons = [FileActions.upload, FileActions.downloadAll, FileActions.removeAll];
  const contextMenu = [
    FileActions.preview,
    FileActions.download,
    FileActions.remove,
    FileActions.rename,
    FileActions.property,
  ];
  const actionButtons = ref([FileActions.preview, FileActions.download]);

  return {
    state,
    tbarButtons,
    actionButtons,
    computeReadonly,
    contextMenu,
    actionClick,
    iconSlot,
    actionsSlot,
  };
}
