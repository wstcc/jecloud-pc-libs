import { defineComponent } from 'vue';
import InputSelect from '../../input-select';
import { useUploadFile } from './hooks/use-upload-file';
export default defineComponent({
  name: 'JeUpload',
  props: {
    placeholder: String,
    url: String, // 接口路径
    params: Object, // 上传参数
    maxSize: Number, //允许上传的文件最大值，M
    includeSuffix: Array, //允许上传的文件后缀
    excludeSuffix: Array, //禁用上传的文件后缀
    value: { type: String, default: '' },
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    model: Object, // 当前model数据
    securityActionSlot: Function,
    afterFileRemove: Function,
  },
  emits: ['change', 'before-file-action'],
  setup(props, context) {
    const { attrs, slots } = context;
    const { onSelect, onBeforeReset, displaySlot, addonAfterSlot, modelValue, $file } =
      useUploadFile({
        props,
        context,
      });
    return () => (
      <InputSelect
        {...attrs}
        {...props}
        ref={$file}
        allowClear
        display={true}
        v-model:value={modelValue.value}
        onSelect={onSelect}
        onBeforeReset={onBeforeReset}
        type="upload"
        icon="fal fa-upload"
        v-slots={{
          ...slots,
          addonAfter: addonAfterSlot,
          display: displaySlot,
        }}
      />
    );
  },
});
