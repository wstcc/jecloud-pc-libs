import { withInstall } from '../utils';
import JeScroller from './src/scroller';

export const Scroller = withInstall(JeScroller);

export default Scroller;
