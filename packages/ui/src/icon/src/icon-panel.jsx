import { defineComponent, ref, reactive, watch, nextTick } from 'vue';
import { Row, Col } from 'ant-design-vue';
import { loadIcons, icons as fontIcons } from './util';
import { copy, debounce, Data } from '@jecloud/utils';
import Input from '../../input';
import Panel from '../../panel';
import Modal from '../../modal';
import Pager, { usePager } from '../../pager';
import Button from '../../button';
import Toolbar from '../../toolbar';
import Tabs from '../../tabs';

export default defineComponent({
  name: 'JeIconPanel',
  components: {
    Row,
    Col,
    Input,
    Pager,
    Button,
  },
  props: {
    copy: {
      type: Boolean,
      default: true,
    },
    help: {
      type: Boolean,
      default: true,
    },
    size: {
      type: String,
      default: '',
    },
  },
  emits: ['select'],
  setup(props, context) {
    const { emit, slots, expose } = context;
    // 当前激活标签
    const selectedKeys = ref(fontIcons[0].code);
    // 搜索关键字
    const keyword = ref('');
    // 图标占位
    const iconSpan = props.size === 'small' ? 4 : 3;
    const iconRowCount = 24 / iconSpan;
    const iconBody = ref();
    const defPageSize = iconRowCount * 25;
    // 数据store
    const store = Data.useGridStore({
      pageSize: defPageSize,
      autoLoad: true,
      rootProperty: 'data',
      proxy({ pageSize, page }) {
        iconBody.value.$el.parentNode.scrollTop = 0;
        return loadIcons({
          page,
          limit: pageSize,
          type: selectedKeys.value,
          keyword: keyword.value,
        });
      },
    });

    // 分页
    const pagerConfig = reactive({
      size: 'small',
      pageSizes: [10, 15, 20, 25, 30, 35, 40].map((num) => num * iconRowCount),
      align: 'center',
      layouts: ['Sizes', 'JumpNumber', 'Total'],
    });
    const { pagerSlot } = usePager({ props: { store }, config: pagerConfig, context });

    // 重置
    expose({
      reset() {
        nextTick(() => {
          keyword.value = '';
          selectedKeys.value = fontIcons[0].code;
          store.load();
        });
      },
    });

    // 获取选中按钮信息
    const getSelectedInfo = function () {
      return fontIcons.find((item) => {
        return selectedKeys.value === item.code;
      });
    };

    // 格式化图标样式
    const formatIcon = function (code) {
      const { cls, prefix } = getSelectedInfo();
      return `${cls} ${prefix}-${code}`;
    };
    // 复制图标
    const coopyCls = function (e) {
      const icon = e.target.getAttribute('class');
      emit('select', icon);
      if (props.copy) {
        copy(icon);
        Modal.message('复制成功！', 'success');
      }
    };

    // 查询方法
    const searchFn = debounce(() => {
      store.loadPage(1);
    }, 300);
    watch(
      () => [selectedKeys.value, keyword.value],
      () => {
        searchFn();
      },
      { immediate: true },
    );

    // 帮助说明
    const showHelp = () => {
      Modal.alert({
        title: '使用帮助',
        okButton: true,
        icon: 'fal fa-question-circle',
        width: '600',
        height: '300',
        content() {
          return (
            <>
              <p>
                {' '}
                1. 系统统一使用<span style="color:red;">
                  “细体图标”
                </span>，有特殊说明，可以使用其他{' '}
              </p>
              <p>
                2. 图标统一使用<span class="red" v-text="<i>"></span>标签，编写规范：
                <span style="color:red;" v-text='<i class="fal fa-abacus"></i>'></span>
              </p>
              <p>
                {' '}
                3. 点击图标，可以直接复制图标样式，复制内容：
                <span style="color:red;">fal fa-abacus</span>{' '}
              </p>
            </>
          );
        },
      });
    };
    // 查询条
    const tbar = () => {
      return (
        <Toolbar align="center" class="je-icon-panel-header">
          <Tabs v-model:activeKey={selectedKeys.value} class="je-icon-panel-menu">
            {fontIcons.map((item) => {
              return <Tabs.TabPane key={item.code} tab={item.text} />;
            })}
          </Tabs>
          <Input
            placeholder="搜索图标名称"
            style="width: 200px"
            v-model:value={keyword.value}
            allow-clear
            v-slots={{ prefix: () => <i class="fal fa-search"></i> }}
          />
          {props.help ? (
            <Button
              type="link"
              onClick={showHelp}
              v-slots={{ icon: () => <i class="fal fa-question-circle"></i> }}
            >
              使用帮助
            </Button>
          ) : null}
          <Toolbar.Fill />
          <span class="je-icon-panel-actions">{slots.action?.()}</span>
        </Toolbar>
      );
    };

    return () => {
      return (
        <Panel class={['je-icon-panel', props.size ? 'size--' + props.size : '']}>
          <Panel.Item region="tbar">{tbar()}</Panel.Item>
          <Panel.Item region="bbar">{pagerSlot()}</Panel.Item>
          <Panel.Item>
            <Row style="padding: 20px 0;height:auto;" wrap={true} ref={iconBody}>
              {store.data.map((icon) => {
                return (
                  <Col key={icon} span={iconSpan} class="je-icon-panel-icons">
                    <i
                      class={[formatIcon(icon)]}
                      title={props.copy ? '点击复制字体样式' : ''}
                      onClick={coopyCls}
                    ></i>
                    <div class="name" title={props.size === 'small' ? icon : ''}>
                      {icon}
                    </div>
                    <div class="cls">{formatIcon(icon)}</div>
                  </Col>
                );
              })}
            </Row>
          </Panel.Item>
        </Panel>
      );
    };
  },
});
