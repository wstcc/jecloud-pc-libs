const icons = [
  { code: 'light', text: '细体图标', cls: 'fal', prefix: 'fa', name: 'solid' },
  { code: 'solid', text: '实心图标', cls: 'fas', prefix: 'fa', name: 'solid' },
  { code: 'jeicon', text: 'JEIcons', cls: 'jeicon', prefix: 'jeicon', name: 'jeicon' },
];

const loadIcons = function ({ keyword, type, page = 1, limit }) {
  return importJson().then(({ i18n, iconData }) => {
    const icon = icons.find((item) => {
      return item.code === type;
    });
    let data = iconData[icon?.name];
    if (keyword) {
      data = data.filter((item) => {
        return (
          item.includes(keyword) ||
          i18n[item]?.includes(keyword) ||
          !!item.split('-').find((key) => i18n[key]?.includes(keyword))
        );
      });
    }
    const totalCount = data.length;
    const start = (page - 1) * limit;
    const end = start + limit;
    return { data: data.slice(start, end), totalCount };
  });
};

async function importJson() {
  const jeicon = (await import(/* @vite-ignore */ `../../assets/fonts/jeicon/iconfont.json`))
    .default;
  const faicon = (await import(/* @vite-ignore */ `../../assets/fonts/fa/fa.json`)).default;
  const i18n = (await import(/* @vite-ignore */ `../../assets/fonts/i18n.json`)).default;

  const jeIcons = [];
  jeicon.glyphs.forEach((item) => {
    jeIcons.push(item.font_class);
  });

  const iconData = {
    jeicon: jeIcons,
    solid: faicon,
  };

  return { iconData, i18n };
}
export { loadIcons, icons };
