import { mxgraph } from '..';
export function initMxGraph({ basePath }) {
  window.mxBasePath = `${basePath}/src`;
  window.mxLanguage = 'zh_CN';
  window.mxLanguages = ['zh_CN', 'en_US'];
  Object.assign(mxgraph, {
    RESOURCES_PATH: `${basePath}/resourse`,
    RESOURCE_BASE: '',
    STENCIL_PATH: `${basePath}/resourse/stencils`,
    IMAGE_PATH: `${basePath}/resourse/images`,
    STYLE_PATH: `${basePath}/resourse/styles`,
    CSS_PATH: `${basePath}/resourse/styles`,
  });
}
