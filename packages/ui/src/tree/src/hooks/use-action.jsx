import { ref } from 'vue';
import { Tooltip } from 'ant-design-vue';
import { installPlugins } from '../../../utils';
import { isNotEmpty } from '@jecloud/utils';
/**
 * 操作菜单
 * @param {*} $tree
 * @returns
 */
export function useAction($tree) {
  const { props, context } = $tree;
  const { slots, emit } = context;
  const { showAction } = props;
  installPlugins(Tooltip);

  // 操作按钮插槽，参数参考column default插槽的参数
  const actionSlot = (args) => {
    const tooltip = ref();
    return isNotEmpty(slots.action) && showAction(args) != false ? (
      <Tooltip
        ref={tooltip}
        placement="rightTop"
        trigger="click"
        overlay-class-name="je-tree-action-popover"
        destroy-tooltip-on-hide={true}
        v-slots={{
          title: () => {
            return slots.action({ tooltip: tooltip.value, ...args });
          },
        }}
      >
        <i
          class="actions fal fa-ellipsis-v"
          onClick={(evt) => {
            evt.stopPropagation();
            emit('action-click', args);
          }}
        />
      </Tooltip>
    ) : null;
  };
  return actionSlot;
}
