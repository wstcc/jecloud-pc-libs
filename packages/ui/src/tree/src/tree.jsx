import { defineComponent, watch } from 'vue';
import { tableEmits, tableProps, tableSlots, panelSlots, panelProps } from '../../grid/src/table';
import { Table } from 'vxe-table';
import Panel from '../../panel';
import { useTree } from './hooks/use-tree';
import { pick } from '@jecloud/utils';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeTree',
  components: { Table, Panel },
  inheritAttrs: false,
  props: {
    ...tableProps,
    ...panelProps,
    search: { type: [Boolean, Object, Function], default: true }, //{filter:fn,search:fn}
    size: {
      type: String,
      default: 'mini',
      validator: (value) => ['large', 'middle', 'small', 'mini'].includes(value),
    },
    bodyBorder: Boolean,
    store: Object,
    autoLoad: Boolean,
    multiple: Boolean,
    nodeColumn: Object,
    showAction: {
      type: Function,
      default: () => {
        return true;
      },
    },
  },
  emits: [...tableEmits, 'update:size', 'search-select', 'action-click'],
  slots: [...tableSlots, ...panelSlots],
  setup(props, context) {
    const { slots, attrs } = context;

    const { $table, $panel, $plugin, treeEvents, treeProps, treeSlots, searchSlot, loading } =
      useTree({
        props,
        context,
      });

    // 支持动态修改
    $table.size = useModelValue({ props, context, key: 'size' });
    watch(
      () => $table.size.value,
      () => {
        $panel.value.refreshLayout();
      },
    );

    return () => (
      <Panel
        class={{
          'je-tree': true,
          'is--loading': loading.value,
          'is--bodyborder': props.bodyBorder,
        }}
        ref={$panel}
        {...pick(attrs, ['style', 'class'])}
        {...pick(props, Object.keys(panelProps))}
        v-slots={{
          ...pick(slots, panelSlots),
          tbar: searchSlot,
        }}
      >
        <Table
          ref={$plugin}
          {...treeEvents}
          {...treeProps.value}
          size={$table.size.value}
          v-slots={treeSlots}
          height="100%"
          width="100%"
          style={props.bodyStyle}
          v-loading={loading.value}
        />
      </Panel>
    );
  },
});
