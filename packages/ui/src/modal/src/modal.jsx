import { defineComponent, ref, reactive, nextTick, watch, onMounted } from 'vue';
import { uniqueId } from '@jecloud/utils';
import { useMethods } from './hooks/use-methods';
import { useRender } from './hooks/use-render';
import { useProvideModal } from './context';

import GlobalConfig from './hooks/config';

export const allActivedModals = [];
export const msgQueue = [];
export default defineComponent({
  name: 'JeModal',
  inheritAttrs: false,
  props: {
    visible: Boolean, // 显隐
    id: String,
    type: {
      type: String,
      default: 'modal',
      validator: (value) => ['modal', 'dialog', 'prompt', 'message'].includes(value),
    }, // 窗口类型
    loading: { type: Boolean, default: null }, // 加载状态
    bodyStyle: [String, Object],
    headerStyle: [String, Object],
    footerStyle: [String, Object],
    title: String, // 标题
    status: String, // 提示状态
    icon: String, // 头部图标
    message: [Number, String, Function], // 内容
    content: [Number, String, Function], // 内容
    tools: { type: Array }, // 窗口右上角按钮,tool={text:'',handler:()=>{},icon:''}
    buttonAlign: {
      type: String,
      default: () => GlobalConfig.buttonAlign,
      validator: (value) => ['right', 'center', 'left'].includes(value),
    },
    buttonFocus: Boolean, // 底部操作按钮获取焦点
    buttons: { type: Array }, // 窗口底部按钮,button={text:'',handler:()=>{},icon:'',type:'',closeable:true}
    okButton: { type: [Boolean, String, Function, Object], default: () => false }, // 确认按钮
    cancelButton: { type: [Boolean, String, Function, Object], default: () => false }, // 取消按钮
    className: String, // 样式
    top: { type: [Number, String], default: () => GlobalConfig.top }, // 距离顶部位置
    position: [String, Object], // 位置
    duration: { type: [Number, String], default: () => GlobalConfig.duration }, // 持续时间
    lockView: { type: Boolean, default: () => GlobalConfig.lockView },
    lockScroll: Boolean,
    mask: { type: Boolean, default: () => GlobalConfig.mask }, // 遮罩
    maskClosable: { type: Boolean, default: () => GlobalConfig.maskClosable }, // 点击遮罩关闭
    escClosable: { type: Boolean, default: () => GlobalConfig.escClosable }, // esc关闭
    resize: { type: Boolean, default: () => GlobalConfig.resize }, // 调整大小
    showHeader: { type: Boolean, default: () => GlobalConfig.showHeader }, // 显示头部
    showFooter: { type: Boolean, default: () => GlobalConfig.showFooter }, // 显示底部
    maximizable: { type: Boolean, default: () => GlobalConfig.maximizable }, // 显示放大
    maximized: Boolean, // 全屏
    dblclickMaximized: { type: Boolean, default: () => GlobalConfig.dblclickMaximized }, // 双击头部缩放
    closable: { type: Boolean, default: () => GlobalConfig.closable }, // 显示关闭
    width: [Number, String], // 宽
    height: [Number, String], // 高
    minWidth: { type: [Number, String], default: () => GlobalConfig.minWidth }, // 最小宽
    minHeight: { type: [Number, String], default: () => GlobalConfig.minHeight }, // 最小高
    zIndex: Number,
    marginSize: { type: [Number, String], default: () => GlobalConfig.marginSize },
    draggable: { type: Boolean, default: () => GlobalConfig.draggable }, // 拖拽
    destroyOnClose: { type: Boolean, default: () => GlobalConfig.destroyOnClose }, // 关闭时销毁
    showTitleOverflow: { type: Boolean, default: () => GlobalConfig.showTitleOverflow }, // 标题省略号
    transfer: { type: Boolean, default: () => GlobalConfig.transfer },
    animat: { type: Boolean, default: () => GlobalConfig.animat }, // 动画
    size: { type: String, default: () => GlobalConfig.size || GlobalConfig.size }, //大小
    slots: Object,
    teleport: { type: Boolean, default: () => GlobalConfig.teleport },
    onBeforeshow: Function,
    onShow: Function,
    onBeforeclose: Function,
    onClose: Function,
  },
  emits: ['update:visible', 'beforeshow', 'show', 'beforeclose', 'close'],
  setup(props, context) {
    const { expose } = context;
    // 辅助数据
    const reactData = reactive({
      inited: false,
      active: false,
      contentVisible: false,
      modalTop: 0,
      modalZindex: 0,
      zoomLocat: null,
      firstOpen: false,
    });
    // 元素
    const refMaps = {
      el: ref(),
      box: ref(),
      body: ref(),
      header: ref(),
      footer: ref(),
      buttons: ref([]),
      tools: ref([]),
    };
    // 窗口对象
    const $modal = {
      modalId: uniqueId(),
      props,
      context,
      reactData,
      getRef: (key, dom) => {
        const refItem = refMaps[key];
        return refItem ? (dom ? refItem.value : refItem) : null;
      },
      title: ref(props.title),
    };

    // 注册方法
    const methods = useMethods($modal);
    const { recalculate, show, close } = methods;
    Object.assign($modal, methods);
    const { renderVN } = useRender($modal);
    Object.assign($modal, { renderVN });

    expose($modal);
    useProvideModal($modal);

    watch(() => props.width, recalculate);
    watch(() => props.height, recalculate);

    watch(
      () => props.visible,
      (value) => {
        if (value) {
          show();
        } else {
          close();
        }
      },
    );

    onMounted(() => {
      nextTick(() => {
        if (props.visible) {
          show();
        }
      });
    });

    return () => {
      return renderVN();
    };
  },
});
