import { toNumber, getDomNode, getEventTargetNode, toggleClass } from '@jecloud/utils';
import KeyCode from '../../../utils/keycode';

export function useEvents($modal) {
  const { props, reactData, zoom, close, fireEvent, getRef, updateZindex } = $modal;
  /**
   * 拖拽
   * @param {*} evnt
   */
  const dragEvent = (evnt) => {
    const { zoomLocat } = reactData;
    const marginSize = toNumber(props.marginSize);
    const boxElem = getRef('box', true);
    const el = getRef('el', true);
    const dragClass = 'is--draggable';
    if (!zoomLocat && evnt.button === 0 && !getEventTargetNode(evnt, boxElem, 'tool--btn').flag) {
      evnt.preventDefault();
      const domMousemove = document.onmousemove;
      const domMouseup = document.onmouseup;
      const disX = evnt.clientX - boxElem.offsetLeft;
      const disY = evnt.clientY - boxElem.offsetTop;
      const { height, width } = getDomNode();
      const timer = setTimeout(() => {
        toggleClass(el, dragClass, true);
      }, 150);
      document.onmousemove = (evnt) => {
        evnt.preventDefault();
        const offsetWidth = boxElem.offsetWidth;
        const minX = marginSize - offsetWidth;
        const maxX = width - marginSize;
        const minY = 0;
        const maxY = height - marginSize;
        let left = evnt.clientX - disX;
        let top = evnt.clientY - disY;
        if (left > maxX) {
          left = maxX;
        }
        if (left < minX) {
          left = minX;
        }
        if (top > maxY) {
          top = maxY;
        }
        if (top < minY) {
          top = minY;
        }
        boxElem.style.left = `${left}px`;
        boxElem.style.top = `${top}px`;
      };
      document.onmouseup = () => {
        clearTimeout(timer);
        toggleClass(el, dragClass, false);
        document.onmousemove = domMousemove;
        document.onmouseup = domMouseup;
      };
    }
  };

  /**
   *  调整大小
   */
  const resizeDirection = ['wl', 'wr', 'swst', 'sest', 'st', 'swlb', 'selb', 'sb'];
  const resizeEvent = (evnt) => {
    evnt.preventDefault();
    const { height: visibleHeight, width: visibleWidth } = getDomNode();
    const marginSize = 0;
    const targetElem = evnt.target;
    const type = targetElem.getAttribute('type');
    const minWidth = toNumber(props.minWidth);
    const minHeight = toNumber(props.minHeight);
    const maxWidth = visibleWidth;
    const maxHeight = visibleHeight;
    const boxElem = getRef('box', true);
    const domMousemove = document.onmousemove;
    const domMouseup = document.onmouseup;
    const clientWidth = boxElem.clientWidth;
    const clientHeight = boxElem.clientHeight;
    const disX = evnt.clientX;
    const disY = evnt.clientY;
    const offsetTop = boxElem.offsetTop;
    const offsetLeft = boxElem.offsetLeft;
    document.onmousemove = (evnt) => {
      evnt.preventDefault();
      let dragLeft;
      let dragTop;
      let width;
      let height;
      switch (type) {
        case 'wl':
          dragLeft = disX - evnt.clientX;
          width = dragLeft + clientWidth;
          if (offsetLeft - dragLeft > marginSize) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
              boxElem.style.left = `${offsetLeft - dragLeft}px`;
            }
          }
          break;
        case 'swst':
          dragLeft = disX - evnt.clientX;
          dragTop = disY - evnt.clientY;
          width = dragLeft + clientWidth;
          height = dragTop + clientHeight;
          if (offsetLeft - dragLeft > marginSize) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
              boxElem.style.left = `${offsetLeft - dragLeft}px`;
            }
          }
          if (offsetTop - dragTop > marginSize) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
              boxElem.style.top = `${offsetTop - dragTop}px`;
            }
          }
          break;
        case 'swlb':
          dragLeft = disX - evnt.clientX;
          dragTop = evnt.clientY - disY;
          width = dragLeft + clientWidth;
          height = dragTop + clientHeight;
          if (offsetLeft - dragLeft > marginSize) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
              boxElem.style.left = `${offsetLeft - dragLeft}px`;
            }
          }
          if (offsetTop + height + marginSize < visibleHeight) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
            }
          }
          break;
        case 'st':
          dragTop = disY - evnt.clientY;
          height = clientHeight + dragTop;
          if (offsetTop - dragTop > marginSize) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
              boxElem.style.top = `${offsetTop - dragTop}px`;
            }
          }
          break;
        case 'wr':
          dragLeft = evnt.clientX - disX;
          width = dragLeft + clientWidth;
          if (offsetLeft + width + marginSize < visibleWidth) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
            }
          }
          break;
        case 'sest':
          dragLeft = evnt.clientX - disX;
          dragTop = disY - evnt.clientY;
          width = dragLeft + clientWidth;
          height = dragTop + clientHeight;
          if (offsetLeft + width + marginSize < visibleWidth) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
            }
          }
          if (offsetTop - dragTop > marginSize) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
              boxElem.style.top = `${offsetTop - dragTop}px`;
            }
          }
          break;
        case 'selb':
          dragLeft = evnt.clientX - disX;
          dragTop = evnt.clientY - disY;
          width = dragLeft + clientWidth;
          height = dragTop + clientHeight;
          if (offsetLeft + width + marginSize < visibleWidth) {
            if (width > minWidth) {
              boxElem.style.width = `${width < maxWidth ? width : maxWidth}px`;
            }
          }
          if (offsetTop + height + marginSize < visibleHeight) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
            }
          }
          break;
        case 'sb':
          dragTop = evnt.clientY - disY;
          height = dragTop + clientHeight;
          if (offsetTop + height + marginSize < visibleHeight) {
            if (height > minHeight) {
              boxElem.style.height = `${height < maxHeight ? height : maxHeight}px`;
            }
          }
          break;
      }
      boxElem.className = boxElem.className.replace(/\s?is--drag/, '') + ' is--drag';

      fireEvent('resize', { type: 'resize' }, evnt);
    };
    document.onmouseup = () => {
      reactData.zoomLocat = null;
      document.onmousemove = domMousemove;
      document.onmouseup = domMouseup;
      setTimeout(() => {
        boxElem.className = boxElem.className.replace(/\s?is--drag/, '');
      }, 50);
    };
  };

  /**
   * 切换大小
   *
   * @param {*} evnt
   * @return {*}
   */
  const toggleZoomEvent = (evnt) => {
    const { zoomLocat } = reactData;
    const params = { type: zoomLocat ? 'revert' : 'max' };
    return zoom().then(() => {
      fireEvent('resize', { type: params.type }, evnt);
    });
  };

  /**
   * 点击遮罩层关闭
   *
   * @param {*} evnt
   */
  const clickMaskCloseEvent = (evnt) => {
    const el = getRef('el', true);
    if (props.mask && props.maskClosable && evnt.target === el) {
      closeEvent(evnt);
    }
  };
  /**
   * 激活窗口置顶层
   *
   */
  const boxMousedownEvent = () => {
    updateZindex();
  };

  const closeEvent = (evnt) => {
    close('close', evnt);
  };
  const keydownEvent = (evnt) => {
    if (props.escClosable && evnt.keyCode === KeyCode.ESC) {
      closeEvent(event);
      evnt.stopPropagation();
    }
  };

  return {
    keydownEvent,
    dragEvent,
    resizeEvent,
    resizeDirection,
    toggleZoomEvent,
    clickMaskCloseEvent,
    closeEvent,
    boxMousedownEvent,
  };
}
