// 全局 zIndex 起始值，如果项目的的 z-index 样式值过大时就需要跟随设置更大，避免被遮挡
// 由于使用了微应用，在微应用中，需要使用父应用的配置
window.zIndexConfig = window.rawWindow?.zIndexConfig ?? {
  notice: { zIndex: 999, index: 0, lastZindex: 1 },
  modal: { zIndex: 99, index: 0, lastZindex: 1 },
};

export function nextZIndex(type = 'modal') {
  const config = window.zIndexConfig[type];
  config.lastZindex = config.zIndex + config.index++;
  return config.lastZindex;
}

export function getLastZIndex(type = 'modal') {
  let { lastZindex } = window.zIndexConfig[type];
  return lastZindex;
}
