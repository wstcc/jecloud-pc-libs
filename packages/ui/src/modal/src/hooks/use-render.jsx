import GlobalConfig from './config';
import { useEvents } from './use-events';
import Button from '../../../button';
import { isArray, isFunction, isPlainObject, isString, omit } from '@jecloud/utils';

export function useRender($modal) {
  const { props, reactData, getRef, isMessage, title } = $modal;
  const { slots, attrs } = $modal.context;
  const {
    toggleZoomEvent,
    closeEvent,
    dragEvent,
    resizeEvent,
    resizeDirection,
    clickMaskCloseEvent,
    boxMousedownEvent,
    keydownEvent,
  } = useEvents($modal);
  const isUnRender = function () {
    return !reactData.inited || (props.destroyOnClose && !reactData.active);
  };
  // 标题
  const renderTitles = () => {
    const { slots: propSlots = {}, closable, maximizable, status, tools } = props;
    const { zoomLocation } = reactData;
    let titVNs = [];
    if (status || props.icon) {
      // 图标
      titVNs.push(
        <div class="je-modal--status-wrapper">
          <i
            class={[
              'je-modal--status-icon',
              props.icon || GlobalConfig.icon[`MODAL_${status}`.toLocaleUpperCase()],
            ]}
          />
        </div>,
      );
    }
    const titleSlot = slots.title || propSlots.title;
    titVNs = titVNs.concat(
      titleSlot
        ? titleSlot({ $modal })
        : [<span class="je-modal--title">{title.value ?? GlobalConfig.i18n('title')}</span>],
    );
    const toolBtns = [];
    if (tools && isArray(tools)) {
      // 自定义按钮
      tools.forEach((tool) => {
        toolBtns.push(
          <i
            class={['tool--btn', 'tool--' + tool.type, tool.icon]}
            title={tool.text}
            onClick={() => {
              tool.handler?.({ button: tool, $modal });
            }}
          ></i>,
        );
      });
    }
    // 最大化
    if (maximizable) {
      toolBtns.push(
        <i
          class={[
            'tool--btn',
            'tool--maximize',
            zoomLocation ? GlobalConfig.icon.MODAL_ZOOM_OUT : GlobalConfig.icon.MODAL_ZOOM_IN,
          ]}
          title={GlobalConfig.i18n(`zoom${zoomLocation ? 'Out' : 'In'}`)}
          onClick={toggleZoomEvent}
        ></i>,
      );
    }
    // 关闭
    if (closable) {
      toolBtns.push(
        <i
          class={['tool--btn', 'tool--close', GlobalConfig.icon.MODAL_CLOSE]}
          title={GlobalConfig.i18n('close')}
          onClick={closeEvent}
        ></i>,
      );
    }
    titVNs.push(<div class="je-modal--title-tools">{toolBtns}</div>);
    return titVNs;
  };

  const renderHeaders = () => {
    const { slots: propSlots = {}, maximizable, draggable, headerStyle } = props;
    const isMsg = isMessage();
    const headerSlot = slots.header || propSlots.header;
    const headVNs = [];
    if (props.showHeader) {
      const headerOns = { style: headerStyle };
      if (draggable) {
        headerOns.onMousedown = dragEvent;
      }
      if (maximizable && props.dblclickMaximized && props.type === 'modal') {
        headerOns.onDblclick = toggleZoomEvent;
      }
      headVNs.push(
        <div
          ref={getRef('header')}
          class={[
            'je-modal--header',
            { 'is--drag': draggable, 'is--ellipsis': !isMsg && props.showTitleOverflow },
          ]}
          {...headerOns}
        >
          {headerSlot ? (isUnRender() ? [] : headerSlot({ $modal })) : renderTitles()}
        </div>,
      );
    }
    return headVNs;
  };

  const renderBodys = () => {
    const { slots: propSlots = {}, message } = props;
    const content = props.content || message;
    const isMsg = isMessage();
    const defaultSlot = slots.default || propSlots.default;
    const contVNs = [];
    // 内容
    contVNs.push(
      <div class="je-modal--content">
        {defaultSlot
          ? isUnRender()
            ? []
            : defaultSlot({ $modal })
          : isFunction(content)
          ? content({ $modal })
          : content}
      </div>,
    );
    // loading
    if (!isMsg) {
      contVNs.push(
        <div class={['vxe-loading', { 'is--visible': props.loading }]}>
          <div class="vxe-loading--spinner"></div>
        </div>,
      );
    }
    return [
      <div class="je-modal--body" ref={getRef('body')} style={props.bodyStyle}>
        {contVNs}
      </div>,
    ];
  };

  const renderBtns = () => {
    const buttonVNs = [];
    let buttons = [];
    // 自定义按钮
    if (props.buttons && isArray(props.buttons)) {
      buttons = props.buttons;
    } else {
      // 默认按钮
      const defaultBtns = {
        okButton: {
          // ok
          type: 'primary',
          text: GlobalConfig.i18n('okText'),
        },
        cancelButton: {
          // cancel
          text: GlobalConfig.i18n('cancelText'),
        },
      };
      Object.keys(defaultBtns).forEach((key) => {
        const button = props[key];
        if (button) {
          const btn = defaultBtns[key];
          if (isPlainObject(button)) {
            Object.assign(btn, button);
          } else if (isString(button)) {
            btn.text = button;
          } else if (isFunction(button)) {
            btn.handler = button;
          }
          buttons.push(btn);
        }
      });
    }
    const btnRefs = [];
    buttons.forEach((btn, index) => {
      btn.closable = btn.closable !== false;
      buttonVNs.push(
        <Button
          class="je-modal--button"
          {...omit(btn, ['text', 'handler'])}
          ref={(item) => {
            btnRefs.push(item);
          }}
          onClick={() => {
            const button = btnRefs[index];
            // 禁用，加载中，不操作
            if (button.disabled || button.loading) {
              event.preventDefault();
              // 按钮自定义操作
            } else {
              if (btn.handler) {
                if (btn.handler?.({ button, $modal }) !== false) {
                  btn.closable && closeEvent();
                }
                // 无操作，直接关闭
              } else {
                btn.closable && closeEvent();
              }
            }
          }}
        >
          {btn.text}
        </Button>,
      );
    });

    const align = { right: 'flex-end', center: 'center', left: 'flex-start' };
    const buttonsSlot = slots.buttons;
    return buttonVNs.length || buttonsSlot ? (
      <div class="je-modal--footer-buttons" style={{ justifyContent: align[props.buttonAlign] }}>
        {buttonsSlot ? buttonsSlot($modal) : buttonVNs}
      </div>
    ) : (
      []
    );
  };

  // 底部工具条
  const renderFooters = () => {
    const { slots: propSlots = {}, footerStyle } = props;
    const footerSlot = slots.footer || propSlots.footer;
    const footVNs = [];
    // 底部工具条
    if (props.showFooter) {
      footVNs.push(
        <div class="je-modal--footer" ref={getRef('footer')} style={footerStyle}>
          {footerSlot ? (isUnRender() ? [] : footerSlot({ $modal })) : renderBtns()}
        </div>,
      );
    }
    return footVNs;
  };

  // 调整尺寸工具条
  const renderResizes = () => {
    const isMsg = isMessage();
    const resizeVNs = [];
    // 调整尺寸
    if (!isMsg && props.resize) {
      resizeVNs.push(
        <span class="je-modal--resize">
          {resizeDirection.map((type) => {
            return <span class={`${type}-resize`} type={type} onMousedown={resizeEvent}></span>;
          })}
        </span>,
      );
    }
    return resizeVNs;
  };

  const renderVN = () => {
    const { className, type, animat, loading, status, lockScroll, lockView, mask, resize } = props;
    const { zoomLocation, modalTop, contentVisible, active } = reactData;
    return (
      <div
        ref={getRef('el')}
        class={[
          'je-modal--wrapper',
          `type--${type}`,
          className || '',
          {
            [`status--${status}`]: status,
            'is--animat': animat,
            'lock--scroll': lockScroll,
            'lock--view': lockView,
            'is--resize': resize,
            'is--mask': mask,
            'is--maximize': zoomLocation,
            'is--visible': contentVisible,
            'is--active': active,
            'is--loading': loading,
          },
        ]}
        style={{
          zIndex: reactData.modalZindex,
          top: modalTop ? `${modalTop}px` : null,
        }}
        onMousedown={clickMaskCloseEvent}
        tabindex={-1}
        onKeydown={keydownEvent}
      >
        <div ref={getRef('box')} class="je-modal--box" onMousedown={boxMousedownEvent} {...attrs}>
          {renderHeaders().concat(renderBodys(), renderFooters(), renderResizes())}
        </div>
      </div>
    );
  };

  return { renderVN, renderHeaders, renderBodys };
}
