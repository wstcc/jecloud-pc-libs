import { notification } from 'ant-design-vue';
import { transformOpenFn } from './hooks/util';
transformOpenFn(notification);
export default notification;
