import { defineComponent, nextTick, onMounted, ref } from 'vue';
import { Collapse } from 'ant-design-vue';

export default defineComponent({
  name: 'JeCollapsePanel',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    const $el = ref();
    onMounted(() => {
      nextTick(() => {
        // 绑定key，方便查找使用
        $el.value.$el.setAttribute('data-key', attrs.panelKey);
      });
    });
    return () => <Collapse.Panel ref={$el} {...attrs} v-slots={slots} />;
  },
});
