import { defineComponent } from 'vue';
import { Tabs } from 'ant-design-vue';

export default defineComponent({
  name: 'JeTabs',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Tabs {...attrs} v-slots={slots}></Tabs>;
  },
});
