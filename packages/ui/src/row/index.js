import { withInstall } from '../utils';
import JeRow from './src/row';
export const Row = withInstall(JeRow);
export default Row;
