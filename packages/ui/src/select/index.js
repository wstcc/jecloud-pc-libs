import { withInstall } from '../utils';
import { Select as ASelect } from 'ant-design-vue';
import JeSelect from './src/select';

JeSelect.installComps = {
  Option: { name: 'JeSelectOption', comp: ASelect.Option },
  OptGroup: { name: 'JeSelectOptGroup', comp: ASelect.OptGroup },
};

export const Select = withInstall(JeSelect);

export default Select;
