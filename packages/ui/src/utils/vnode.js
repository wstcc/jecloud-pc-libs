import { ConfigProvider as AConfigProvider } from 'ant-design-vue';
import { globalConfigForApi } from 'ant-design-vue/es/config-provider';
import { createVNode, render as vueRender, h } from 'vue';
import { isFunction, uuid } from '@jecloud/utils';
import { useAppContext } from '../index';
/**
 * 动态渲染组件
 * @param {HTMLElement} container
 * @param {Function} slot 插槽
 * @returns VNode
 */
export function renderVNode(container, slot) {
  // 默认全局配置
  const configProviderProps = { ...globalConfigForApi, notUpdateGlobalConfig: false };
  // 默认容器
  if (isFunction(container)) {
    slot = container;
    container = renderVNodeContainer();
  }
  const appContext = useAppContext()?._context;
  // 组件插槽
  const wrapper = () => {
    return h(AConfigProvider, configProviderProps, slot?.());
  };
  // 动态渲染组件
  const vm = createVNode(wrapper);
  // 绑定vue
  vm.appContext = appContext || vm.appContext;
  vueRender(vm, container);
  return vm;
}

/**
 *  创建vnode容器
 */
export function renderVNodeContainer(className = 'je-vnode--container') {
  let vnodeContainer = document.querySelector(`.${className}`);
  if (!vnodeContainer) {
    vnodeContainer = document.createElement('div');
    vnodeContainer.className = className;
    document.body.appendChild(vnodeContainer);
  }
  const container = document.createElement('div');
  container.setAttribute('id', 'vnode-' + uuid());
  vnodeContainer.appendChild(container);
  return container;
}
