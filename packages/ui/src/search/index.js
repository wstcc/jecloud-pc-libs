import { withInstall } from '../utils';
import JeSearch from './src/search';

export const Search = withInstall(JeSearch);

export default Search;
