import { defineComponent } from 'vue';
import { Menu } from 'ant-design-vue';
import { useMenu } from './hooks';

export default defineComponent({
  name: 'JeSubMenu',
  components: { SubMenu: Menu.SubMenu },
  inheritAttrs: false,
  props: { icon: String },
  setup(props, { slots, attrs }) {
    const { iconSlot, key } = useMenu(props, { slots });
    return () => <SubMenu {...attrs} key={key} v-slots={{ ...slots, icon: iconSlot }}></SubMenu>;
  },
});
