import { withInstall } from '../utils';
import JeTimePicker from './src/time-picker';
import JeTimeRangePicker from './src/time-range-picker';

// 注册依赖组件
JeTimePicker.installComps = {
  TimeRangePicker: { name: JeTimeRangePicker.name, comp: JeTimeRangePicker },
};
export const TimePicker = withInstall(JeTimePicker);

export default TimePicker;
