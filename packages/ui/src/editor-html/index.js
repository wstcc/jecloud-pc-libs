import { withInstall } from '../utils';
import JeEditorHtml from './src/editor-html';
export const EditorHtml = withInstall(JeEditorHtml);

export default EditorHtml;
