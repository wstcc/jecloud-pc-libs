import { usePublicPath } from '../../../hooks/use-public-path';
import { previewFile } from '../../../utils/file';
export function useUrls({ plugins }) {
  const publicPath = usePublicPath();
  const baseDir = 'static/tinymce';
  const urls = {
    plugin_url: `${baseDir}/plugins/{plugin_url}/plugin.min.js`,
    language_url: `${baseDir}/langs/zh_CN.js`, //引入语言包文件
    skin_url: `${baseDir}/skins/ui/oxide`, //皮肤：浅色
    file_url: `${baseDir}/tinymce.min.js`,
  };
  Object.keys(urls).forEach((key) => {
    urls[key] = publicPath + urls[key];
  });

  // 插件url
  urls.external_plugins = plugins.map((plugin) => {
    return urls.plugin_url.replace('{plugin_url}', plugin);
  });

  // 插件根目录，不设置，微应用访问失败
  urls.baseURL = publicPath + baseDir;
  return urls;
}

/**
 *  双击预览图片
 * @param {*} editor
 */
export function onDblClickPreviewImage(editor) {
  editor?.iframeElement?.contentWindow.document.getElementsByTagName('body')[0].addEventListener(
    'dblclick',
    (e) => {
      const { target } = e;
      // 如果是图片
      if (target && target?.tagName == 'IMG') {
        const { src, alt } = target;
        const srcArray = src.split('/');
        const fileItem = {
          fileKey: srcArray[srcArray.length - 1],
          suffix: 'jpg',
          relName: alt || '图片预览',
        };
        previewFile(fileItem, [fileItem]);
      }
    },
    false,
  );
}
