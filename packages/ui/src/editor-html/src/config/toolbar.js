export default {
  toolbar: [
    'fullscreen', // 全屏
    'undo', // 撤销
    'redo', // 重做/重复
    '|',
    'styleselect', // 段落格式
    'bold', // 加粗
    'italic', // 斜体
    'underline', // 下划线
    'strikethrough', // 删除线
    'removeformat', // 清除格式
    '|',
    'forecolor', //	将前景/文本颜色应用于选择。
    'backcolor', //	将背景颜色应用于选择。
    '|',
    'alignleft', // 左对齐
    'aligncenter', // 居中对齐
    'alignright', // 左对齐
    'alignjustify', // 两端对齐
    '|',
    'lineheight', // 行高
    '|',
    'bullist', // 项目列表UL
    'numlist', // 编号列表OL
    '|',
    'outdent', // 减少缩进
    'indent', // 增加缩进
    '|',
    'table',
    'image', // 图片
    'link', // 超链接
    'media', // 多媒体
    '|',
    'codesample', // 代码示例
    'pagebreak', // 分页符
    '|',
    'preview', // 预览
    'print', // 打印
    '|',
    'newdocument', // 新文档
    'code', // 编辑源码
  ].join(' '),
  toolbar_mode: 'sliding',
  style_formats: [
    {
      title: 'Fonts',
      items: [
        { title: '微软雅黑', name: 'Microsoft YaHei,Helvetica Neue,PingFang SC,sans-serif' },
        { title: '苹果苹方', name: 'PingFang SC,Microsoft YaHei,sans-serif' },
        { title: '宋体', name: 'simsun,serif' },
        { title: '仿宋体', name: 'FangSong,serif' },
        { title: '黑体', name: 'SimHei,sans-serif;' },
        { title: 'Andale Mono', name: 'andale mono,times' },
        { title: 'Arial', name: 'arial,helvetica,sans-serif' },
        { title: 'Arial Black', name: 'arial black,avant garde' },
        { title: 'Book Antiqua', name: 'book antiqua,palatino' },
        { title: 'Comic Sans MS', name: 'comic sans ms,sans-serif' },
        { title: 'Courier New', name: 'courier new,courier' },
        { title: 'Georgia', name: 'georgia,palatino' },
        { title: 'Helvetica', name: 'helvetica' },
        { title: 'Impact', name: 'impact,chicago' },
        { title: 'Symbol', name: 'symbol' },
        { title: 'Tahoma', name: 'tahoma,arial,helvetica,sans-serif' },
        { title: 'Terminal', name: 'terminal,monaco' },
        { title: 'Times New Roman', name: 'times new roman,times' },
        { title: 'Trebuchet MS', name: 'trebuchet ms,geneva' },
        { title: 'Verdana', name: 'verdana,geneva' },
      ].map((item) => {
        return {
          title: item.title,
          inline: 'span',
          toggle: false,
          styles: { fontFamily: item.name },
          clear_child_styles: true,
        };
      }),
    },
    {
      title: 'Font Sizes',
      items: ['12px', '14px', '16px', '18px', '20px', '24px', '36px'].map((size) => {
        return {
          title: size,
          inline: 'span',
          toggle: false,
          styles: { fontSize: size },
          clear_child_styles: true,
        };
      }),
    },
    {
      title: 'Headings',
      items: [
        { title: 'Heading 1', format: 'h1' },
        { title: 'Heading 2', format: 'h2' },
        { title: 'Heading 3', format: 'h3' },
        { title: 'Heading 4', format: 'h4' },
        { title: 'Heading 5', format: 'h5' },
        { title: 'Heading 6', format: 'h6' },
      ],
    },
    {
      title: 'Blocks',
      items: [
        { title: 'Paragraph', format: 'p' },
        { title: 'Blockquote', format: 'blockquote' },
        { title: 'Div', format: 'div' },
        { title: 'Pre', format: 'pre' },
      ],
    },
  ],
};
