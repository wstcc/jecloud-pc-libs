import { defineComponent, reactive, ref, watch } from 'vue';
import { isEmpty, omit } from '@jecloud/utils';
import { Form } from 'ant-design-vue';
import Tooltip from '../../tooltip';
import { useInjectForm, useInjectAForm, useProvideFormItemContext } from './context';

export default defineComponent({
  name: 'JeFormItem',
  inheritAttrs: false,
  props: {
    size: { type: String, validator: (value) => ['middle', 'small'].includes(value) },
    validateType: { type: String, validator: (value) => ['error', 'tooltip'].includes(value) },
    name: String,
    label: String,
    labelText: String, // 辅助label插槽使用，可以获取label的文本信息
    htmlFor: { type: String, default: '' },
    rules: [Object, Array],
    hiddenErrors: Boolean, // 隐藏错误提示
    labelAlign: {
      type: String,
      default: 'right',
      validator: () => ['right', 'left', 'center', 'top'],
    },
    tooltipPlacement: {
      //tooltip模式下的错误提示展示位置
      type: String,
      default: 'bottomLeft',
    },
  },
  setup(props, { slots, attrs }) {
    const formContext = useInjectForm();
    const $plugin = ref();
    const size = ref(props.size ?? formContext?.size);
    const validateType = ref(
      size.value === 'small'
        ? 'tooltip'
        : props.validateType ?? formContext?.validateType ?? 'error',
    );
    // 校验内容
    const validateMessage = ref();

    // 重写AntForm的onValidate的校验方法
    if (formContext.enabled && validateType.value === 'tooltip' && props.name) {
      formContext.validateData = formContext.validateData ?? reactive({});
      formContext.validateData[props.name] = ref();
      watch(
        () => formContext.validateData[props.name],
        (value) => {
          validateMessage.value = value;
        },
      );
      // useForm校验方式
      watch(
        () => attrs.help,
        (help) => {
          formContext.validateData[props.name] = help?.[0] ?? '';
        },
      );
      // 校验规则改变后，清空校验信息
      watch(
        () => props.rules,
        (rules) => {
          if (isEmpty(rules)) {
            formContext.validateData[props.name] = null;
          }
        },
      );
      // 普通表单校验方式
      // 检查是否重写
      const aFormContext = useInjectAForm();
      if (!aFormContext.overridesValidate) {
        aFormContext.overridesValidate = true;
        const oldOnValidate = aFormContext.onValidate;
        aFormContext.onValidate = (name, status, errors) => {
          oldOnValidate(name, status, errors);
          formContext.validateData[name] = ref(errors);
        };
      }
    } else {
      if (validateType.value === 'tooltip' && !props.name) {
        console.error('FormItem的tooltip错误提示类型，必须配置name属性', props);
      }
      // 如果没有表单，默认不支持tooltip错误提示
      validateType.value = 'error';
    }
    // 插槽对象
    let slotItem = null;
    const $field = {
      enabled: true,
      name: props.name,
      label: props.label ?? props.labelText,
      $plugin,
      clearValidate() {
        $plugin.value?.clearValidate();
        slotItem?.clearValidate();
        validateMessage.value = '';
      },
      addSlotItem(item) {
        slotItem = item;
      },
    };
    formContext.addFormField?.($field.name, $field);
    useProvideFormItemContext($field);

    return () => {
      return (
        <Form.Item
          ref={$plugin}
          class={{
            // 自定义
            'je-form-item': true,
            'je-form-item-small': size.value === 'small',
            'je-form-item-error-tooltip': validateType.value === 'tooltip',
            'je-form-item-error-hidden': props.hiddenErrors,
            ['je-form-item-label-align-' + props.labelAlign]: true,
          }}
          rules={props.rules}
          {...attrs}
          htmlFor={props.htmlFor}
          label={props.label}
          name={props.name}
          v-slots={omit(slots, ['default'])}
        >
          {validateType.value === 'tooltip' ? (
            <Tooltip
              placement={props.tooltipPlacement}
              overlayClassName="je-form-item-tooltip"
              getPopupContainer={() => $plugin.value.$el}
              align={{ offset: [0, 0] }}
              v-slots={
                validateMessage.value
                  ? {
                      title() {
                        return (
                          <div role="alert" class="error">
                            {validateMessage.value}
                          </div>
                        );
                      },
                    }
                  : null
              }
            >
              {slots.default?.()}
            </Tooltip>
          ) : (
            slots.default?.()
          )}
        </Form.Item>
      );
    };
  },
});
