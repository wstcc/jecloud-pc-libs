import { inject, provide } from 'vue';
import { useInjectForm as useInjectAForm } from 'ant-design-vue/es/form/context';

export { useInjectAForm };

export const FormContextKey = Symbol('jeFormContextKey');
export const useProvideForm = (state) => {
  provide(FormContextKey, state);
};

export const useInjectForm = () => {
  return Object.assign(inject(FormContextKey, {}), useInjectAForm());
};

export const FormItemFieldKey = Symbol('jeFormItemFieldKey');

export const useProvideFormItemContext = (state) => {
  provide(FormItemFieldKey, state);
};

export const useInjectFormItemContext = () => {
  return inject(FormItemFieldKey, {});
};

export const MethodKeys = [
  'validate',
  'validateFields',
  'scrollToField',
  'resetFields',
  'clearValidate',
];
