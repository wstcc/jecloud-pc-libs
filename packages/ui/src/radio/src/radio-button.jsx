import { defineComponent } from 'vue';
import { Radio } from 'ant-design-vue';

export default defineComponent({
  name: 'JeRadioButton',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Radio.Button class="je-radio-button" {...attrs} v-slots={slots} />;
  },
});
