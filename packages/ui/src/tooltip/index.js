import JeTooltip from './src/tooltip';
import { withInstall } from '../utils';

export const Tooltip = withInstall(JeTooltip);

export default Tooltip;
