import JePager from './src/pager';
import { withInstall } from '../utils';
export * from './src/use-pager';

export const Pager = withInstall(JePager);

export default Pager;
