import { withInstall } from '../utils';
import { DatePicker as ADatePicker } from 'ant-design-vue';
import JeDatePicker from './src/date-picker';

// 注册依赖组件
JeDatePicker.installComps = {
  RangePicker: { name: 'JeRangePicker', comp: ADatePicker.RangePicker },
};
export const DatePicker = withInstall(JeDatePicker);

export default DatePicker;
