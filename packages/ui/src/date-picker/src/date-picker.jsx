import { defineComponent } from 'vue';
import { DatePicker } from 'ant-design-vue';
import { useDatePicker } from './hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';
export default defineComponent({
  name: 'JeDatePicker',
  inheritAttrs: false,
  props: {
    format: { type: [String, Function] },
    valueFormat: { type: String },
    value: { type: [String, Date] },
    picker: {
      type: String,
      default: 'date',
      validator: (value) =>
        ['date', 'dateTime', 'datetime', 'week', 'month', 'quarter', 'year'].includes(value),
    },
    ...addonProps,
  },
  slots: addonSlots,
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs } = context;
    const { format, valueFormat, dateValue } = useDatePicker({ props, context });
    return () => {
      const picker = props.picker.toLocaleLowerCase();
      const element = (
        <DatePicker
          showTime={picker === 'datetime'}
          getPopupContainer={(triggerNode) => triggerNode.parentNode} // 绑定
          {...attrs}
          picker={['date', 'datetime'].includes(picker) ? undefined : picker}
          v-model:value={dateValue.value}
          format={format}
          valueFormat={valueFormat}
          v-slots={slots}
        ></DatePicker>
      );
      return useAddonSlot({ props, context, element });
    };
  },
});
