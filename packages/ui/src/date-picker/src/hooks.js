import { useModelValue } from '../../hooks';
import { parseDateFormat } from '@jecloud/utils';
export function useDatePicker({ props, context, picker }) {
  const value = useModelValue({ props, context, changeEvent: true });
  const { format, valueFormat } = parseDateFormat({ ...props, type: picker || props.picker });

  return { format, valueFormat, dateValue: value };
}
