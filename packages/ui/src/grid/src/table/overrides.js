import { nextTick } from 'vue';
import {
  get,
  isEmpty,
  isString,
  isNumber,
  isEqual,
  findTree,
  mapTree,
  getEventTargetNode,
  pick,
} from '@jecloud/utils';
import { useColumn } from '../hooks/use-column';
import { ColumnCustomProps } from '../column';

export function useOverrides($grid) {
  const { $plugin, props } = $grid;
  // 复写方法
  const overrides = {
    // 重写tab键移动可编辑
    moveTabSelected(args, isLeft, evnt) {
      const $xetable = $plugin.value;
      const { editConfig } = $xetable.props;
      const { afterFullData, visibleColumn } = $xetable.internalData;
      const { computeEditOpts } = $xetable.getComputeMaps();
      const editOpts = computeEditOpts.value;
      let targetRow;
      let targetRowIndex;
      let targetColumnIndex;
      const params = Object.assign({}, args);
      const _rowIndex = $xetable.getVTRowIndex(params.row);
      const _columnIndex = $xetable.getVTColumnIndex(params.column);
      evnt.preventDefault();
      if (isLeft) {
        // 向左
        if (_columnIndex <= 0) {
          // 如果已经是第一列，则移动到上一行
          if (_rowIndex > 0) {
            targetRowIndex = _rowIndex - 1;
            targetRow = afterFullData[targetRowIndex];
            targetColumnIndex = visibleColumn.length - 1;
          }
        } else {
          targetColumnIndex = _columnIndex - 1;
        }
      } else {
        if (_columnIndex >= visibleColumn.length - 1) {
          // 如果已经是第一列，则移动到上一行
          if (_rowIndex < afterFullData.length - 1) {
            targetRowIndex = _rowIndex + 1;
            targetRow = afterFullData[targetRowIndex];
            targetColumnIndex = 0;
          }
        } else {
          targetColumnIndex = _columnIndex + 1;
        }
      }
      const targetColumn = visibleColumn[targetColumnIndex];
      if (targetColumn) {
        if (targetRow) {
          params.rowIndex = targetRowIndex;
          params.row = targetRow;
        } else {
          params.rowIndex = _rowIndex;
        }
        params.columnIndex = targetColumnIndex;
        params.column = targetColumn;
        params.cell = $xetable.getCell(params.row, params.column);
        if (editConfig) {
          if (editOpts.trigger === 'click' || editOpts.trigger === 'dblclick') {
            if (editOpts.mode === 'row') {
              $xetable.handleActived(params, evnt);
            } else if (
              editOpts.mode === 'cell' &&
              params.column.editRender &&
              params.column.editRender.enable !== false
            ) {
              // TODO: 支持cell编辑选中
              $xetable.handleActived(params, evnt);
            } else {
              $xetable
                .scrollToRow(params.row, params.column)
                .then(() => $xetable.handleSelected(params, evnt));
            }
          }
        } else {
          $xetable
            .scrollToRow(params.row, params.column)
            .then(() => $xetable.handleSelected(params, evnt));
        }
      }
    },
    /**
     * 加载列信息，支持自定义格式化
     * @param {*} columns
     */
    loadColumn(columns) {
      const $table = $plugin.value;
      // 列表column Map
      $grid.columnMaps = $grid.columnMaps || {};
      const _columns = mapTree(columns, (column) => {
        // 提取个性化配置
        let customProps = pick(column, ColumnCustomProps);
        // 缓存配置
        let baseCustomProps = $grid.columnMaps[column.field];
        // 更新配置
        if (baseCustomProps) {
          customProps = { ...baseCustomProps, ...customProps };
        }
        // 更新缓存
        $grid.columnMaps[column.field] = customProps;
        // 格式化列数据
        const { props, slots } = useColumn({
          props: { ...customProps, ...column },
          context: { slots: column.slots || {} },
        });
        props.slots = slots;
        return props;
      });
      return $table._loadColumn(_columns);
    },

    /**
     * 多选，选中所有事件
     */
    triggerCheckAllEvent(evnt, value) {
      const $table = this;
      $table.setAllCheckboxRow(value);
      if (evnt) {
        const options = {
          records: $table.getCheckboxRecords(),
          reserves: $table.getCheckboxReserveRecords(),
          indeterminates: $table.getCheckboxIndeterminateRecords(),
          checked: value,
        };
        $table.dispatchEvent('checkbox-all', options, evnt);
        // TODO: 新增selection-change事件
        $grid.triggerSelectionChange(options, evnt);
      }
    },
    /**
     * 列点击事件
     * 如果是单击模式，则激活为编辑状态
     * 如果是双击模式，则单击后选中状态
     */
    triggerCellClickEvent(evnt, params) {
      const $table = this;
      const { highlightCurrentRow, editConfig } = $table.props;
      const { editStore } = $table.reactData;
      const {
        computeExpandOpts,
        computeEditOpts,
        computeTreeOpts,
        computeRadioOpts,
        computeCheckboxOpts,
        computeRowOpts,
      } = $table.getComputeMaps();
      const expandOpts = computeExpandOpts.value;
      const editOpts = computeEditOpts.value;
      const treeOpts = computeTreeOpts.value;
      const radioOpts = computeRadioOpts.value;
      const checkboxOpts = computeCheckboxOpts.value;
      const rowOpts = computeRowOpts.value;
      const { actived } = editStore;
      const { row, column } = params;
      const { type, treeNode } = column;
      const isRadioType = type === 'radio';
      const isCheckboxType = type === 'checkbox';
      const isExpandType = type === 'expand';
      const cell = evnt.currentTarget;
      const triggerRadio = isRadioType && getEventTargetNode(evnt, cell, 'vxe-cell--radio').flag;
      const triggerCheckbox =
        isCheckboxType && getEventTargetNode(evnt, cell, 'vxe-cell--checkbox').flag;
      const triggerTreeNode =
        treeNode && getEventTargetNode(evnt, cell, 'vxe-tree--btn-wrapper').flag;
      const triggerExpandNode =
        isExpandType && getEventTargetNode(evnt, cell, 'vxe-table--expanded').flag;
      params = Object.assign(
        { cell, triggerRadio, triggerCheckbox, triggerTreeNode, triggerExpandNode },
        params,
      );
      if (!triggerCheckbox && !triggerRadio) {
        // 如果是展开行
        if (
          !triggerExpandNode &&
          (expandOpts.trigger === 'row' || (isExpandType && expandOpts.trigger === 'cell'))
        ) {
          this.triggerRowExpandEvent(evnt, params);
        }
        // 如果是树形表格
        if (
          treeOpts.trigger === 'row' ||
          (treeNode && treeOpts.trigger === 'cell') ||
          triggerTreeNode
        ) {
          $table.triggerTreeExpandEvent(evnt, params);
        }
      }
      // 如果点击了树节点
      if (!triggerTreeNode) {
        if (!triggerExpandNode) {
          // 如果是高亮行
          if (rowOpts.isCurrent || highlightCurrentRow) {
            if (!triggerCheckbox && !triggerRadio) {
              $table.triggerCurrentRowEvent(evnt, params);
            }
          }
          // 如果是单选框
          if (
            !triggerRadio &&
            (radioOpts.trigger === 'row' || (isRadioType && radioOpts.trigger === 'cell'))
          ) {
            $table.triggerRadioRowEvent(evnt, params);
          }
          // 如果是复选框
          if (
            !triggerCheckbox &&
            (checkboxOpts.trigger === 'row' || (isCheckboxType && checkboxOpts.trigger === 'cell'))
          ) {
            $table.handleToggleCheckRowEvent(evnt, params);
          }
        }
        // 如果设置了单元格选中功能，则不会使用点击事件去处理（只能支持双击模式）
        if (editConfig && editConfig.enabled !== false) {
          if (editOpts.trigger === 'manual') {
            if (actived.args && actived.row === row && column !== actived.column) {
              handleChangeCell($table, evnt, params);
            }
          } else if (!actived.args || row !== actived.row || column !== actived.column) {
            if (editOpts.trigger === 'click') {
              handleChangeCell($table, evnt, params);
            } else if (editOpts.trigger === 'dblclick') {
              if (editOpts.mode === 'row' && actived.row === row) {
                handleChangeCell($table, evnt, params);
              }
            }
          }
        }
      }
      $table.dispatchEvent('cell-click', params, evnt);
    },
    /**
     * 列双击点击事件
     * 如果是双击模式，则激活为编辑状态
     */
    triggerCellDblclickEvent(evnt, params) {
      const $table = this;
      const { editStore } = $table.reactData;
      const { computeEditOpts, computeTreeOpts } = $table.getComputeMaps();
      const treeOpts = computeTreeOpts.value;
      const { editConfig } = props;
      const editOpts = computeEditOpts.value;
      const { actived } = editStore;
      const cell = evnt.currentTarget;
      const { column } = params;
      const { treeNode } = column;
      const triggerTreeNode =
        treeNode && getEventTargetNode(evnt, cell, 'vxe-tree--btn-wrapper').flag;
      params = Object.assign({ cell }, params);
      if (isEnableConf(editConfig) && editOpts.trigger === 'dblclick') {
        if (!actived.args || evnt.currentTarget !== actived.args.cell) {
          if (editOpts.mode === 'row') {
            checkValidate('blur')
              .catch((e) => e)
              .then(() => {
                $table
                  .handleActived(params, evnt)
                  .then(() => checkValidate('change'))
                  .catch((e) => e);
              });
          } else if (editOpts.mode === 'cell') {
            $table
              .handleActived(params, evnt)
              .then(() => checkValidate('change'))
              .catch((e) => e);
          }
        }
      } else if (
        !triggerTreeNode && // TODO:点击展开按钮，不操作
        (treeOpts.trigger === 'row-dblclick' || (treeNode && treeOpts.trigger === 'cell-dblclick'))
      ) {
        $table.triggerTreeExpandEvent(evnt, params);
      }
      $table.dispatchEvent('cell-dblclick', params, evnt);
    },
    /**
     * 展开树节点事件
     */
    triggerTreeExpandEvent(evnt, params) {
      const $table = this;
      const { treeLazyLoadeds } = $table.reactData;
      const { computeTreeOpts } = $table.getComputeMaps();
      const treeOpts = computeTreeOpts.value;
      const { row, column } = params;
      const { lazy } = treeOpts;
      const tableMethods = $table;
      if (!lazy || $table.findRowIndexOf(treeLazyLoadeds, row) === -1) {
        const expanded = !tableMethods.isTreeExpandByRow(row);
        const columnIndex = tableMethods.getColumnIndex(column);
        const $columnIndex = tableMethods.getVMColumnIndex(column);
        tableMethods.setTreeExpand(row, expanded);
        tableMethods.dispatchEvent(
          'toggle-tree-expand',
          { expanded, column, columnIndex, $columnIndex, row },
          evnt,
        );
      }
    },
    /**
     * 复选框选择改变事件
     * @param {*} evnt
     * @param {*} params
     * @param {*} value
     */
    triggerCheckRowEvent(evnt, params, value) {
      const $table = this;
      const { computeCheckboxOpts } = $table.getComputeMaps();
      const checkboxOpts = computeCheckboxOpts.value;
      const { checkMethod } = checkboxOpts;
      if (!checkMethod || checkMethod({ row: params.row })) {
        $table.handleSelectRow(params, value);
        const evtParams = Object.assign(
          {
            records: $table.getCheckboxRecords(),
            reserves: $table.getCheckboxReserveRecords(),
            indeterminates: $table.getCheckboxIndeterminateRecords(),
            checked: value,
            _checkBox_: true,
          },
          params,
        );
        $table.dispatchEvent('checkbox-change', evtParams, evnt);
        // TODO: 新增selection-change事件
        $grid.triggerSelectionChange(evtParams, evnt);
      }
    },
    /**
     * TODO: 判断如果选中相同行，取消选择
     * @param {*} evnt
     * @param {*} params
     */
    triggerCurrentRowEvent(evnt, params) {
      const $table = this;
      const { currentRow: oldValue } = $table.reactData;
      let { row: newValue } = params;
      let isChange = oldValue !== newValue;
      // 默认允许取消选中
      if (props.allowDeselect) {
        if (isChange) {
          $table.setCurrentRow(newValue);
        } else {
          // TODO: 判断如果选中相同行，取消选择
          $table.clearCurrentRow();
          isChange = true;
          newValue = null;
        }
        $table.dispatchEvent('current-change', { oldValue, newValue, ...params }, evnt);
      } else {
        $table.setCurrentRow(newValue);
        if (isChange) {
          $table.dispatchEvent('current-change', { oldValue, newValue, ...params }, evnt);
        }
      }
      if (isChange) {
        // TODO: 新增selection-change事件
        $grid.triggerSelectionChange(
          {
            eventType: 'currentChange',
            oldValue,
            newValue,
            records: newValue ? [newValue] : [],
            ...params,
          },
          evnt,
        );
      }
    },

    /**
     * 校验行的修改标记
     * 修改数据校验规则,支持fieldConfig
     * @override
     * @param {*} row
     * @param {*} field
     * @return {*}
     */
    isUpdateByRow(row, field) {
      const $table = $plugin.value;
      const { store, fieldConfig } = props;
      const { keepSource, treeConfig } = $table.props;
      const { visibleColumn, tableSourceData, fullDataRowIdData } = $table.internalData;
      // TODO: store检查
      if (store) {
        return store.isChange(row, field);
      }

      if (keepSource) {
        let oRow, property;
        const rowid = $table.getRowid(row);
        // 新增的数据不需要检测
        if (!fullDataRowIdData[rowid]) {
          return false;
        }
        if (treeConfig) {
          const children = treeConfig.children;
          const matchObj = findTree(
            tableSourceData,
            (item) => rowid === $table.getRowid(item),
            treeConfig,
          );
          row = Object.assign({}, row, { [children]: null });
          if (matchObj) {
            oRow = Object.assign({}, matchObj.item, { [children]: null });
          }
        } else {
          const oRowIndex = fullDataRowIdData[rowid].index;
          oRow = tableSourceData[oRowIndex];
        }
        if (oRow) {
          if (arguments.length > 1) {
            return !eqCellValue(oRow, row, field);
          }
          // 增加字段配置，支持隐藏列修改标记
          const fields = fieldConfig
            ? fieldConfig.map((field) => {
                return isString(field) ? field : field.name;
              })
            : visibleColumn.map((column) => {
                return column.property;
              });
          for (let index = 0, len = fields.length; index < len; index++) {
            property = fields[index];
            if (property && !eqCellValue(oRow, row, property)) {
              return true;
            }
          }
        }
      }
      return false;
    },
  };
  return overrides;
}

const checkValidate = ($xetable, type) => {
  if ($xetable.triggerValidate) {
    return $xetable.triggerValidate(type);
  }
  return nextTick();
};

/**
 * 当单元格发生改变时
 * 如果存在规则，则校验
 */
const handleChangeCell = ($xetable, evnt, params) => {
  checkValidate('blur')
    .catch((e) => e)
    .then(() => {
      $xetable
        .handleActived(params, evnt)
        .then(() => checkValidate('change'))
        .catch((e) => e);
    });
};

const eqCellValue = (row1, row2, field) => {
  const val1 = get(row1, field);
  const val2 = get(row2, field);
  if (isEmpty(val1) && isEmpty(val2)) {
    return true;
  }
  if (isString(val1) || isNumber(val1)) {
    /* eslint-disable eqeqeq */
    return val1 == val2;
  }
  return isEqual(val1, val2);
};

function isEnableConf(conf) {
  return conf && conf.enabled !== false;
}
