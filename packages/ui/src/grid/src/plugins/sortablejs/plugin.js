import { useIndicatorPlugin } from './indicator-plugin';
export function usePlugin(Sortable) {
  // 拖动标记
  useIndicatorPlugin(Sortable);
  // 重写拖动前事件
  // 解决树形拖动前需要将展开节点收起，而且收起方法是Promise，导致nextEl失效报错
  const _prepareDragStart = Sortable.prototype._prepareDragStart;
  Sortable.prototype._prepareDragStart = function (evnt, touch, target) {
    // 增加开始前的Promise方法，可以异步开始
    if (this.options.onStartPromise) {
      this.options
        .onStartPromise({ item: target, evnt })
        .then(() => {
          _prepareDragStart.call(this, evnt, touch, target);
        })
        .catch(() => {
          this._nulling();
        });
    } else {
      _prepareDragStart.call(this, evnt, touch, target);
    }
  };
}
