import { toTemplate, get } from '@jecloud/utils';
import { VXETable } from 'vxe-table';
import { renderer } from 'vxe-table/es/v-x-e-table/src/renderer';
let inited = true;
if (inited) {
  VXETable.renderer = renderer;
  const lang = import('vxe-table/es/locale/lang/zh-CN');
  const render = import('../table/renderer');
  Promise.all([lang, render]).then((items) => {
    const langModel = items[0].default;
    const renderModel = items[1].default;
    // ant插件兼容
    VXETable.use(renderModel);
    // 按需加载的方式默认是不带国际化的，自定义国际化需要自行解析占位符 '{0}'，例如：
    VXETable.setup({
      i18n: (key, args) => toTemplate(get(langModel, key), args),
    });
  });
  inited = false;
}
