import { useTreeNode } from './use-column-treenode';
import { hasClass } from '@jecloud/utils';
/**
 * 单选，多选
 * @returns
 */
export function useCheckbox(options) {
  const { props, context } = options;
  const { slots } = context;
  let defaultSlot = slots.default;
  const align = 'center';
  const config = {
    slots: {},
    props: { align, headerAlign: align, resizable: false, width: 40, draggable: false },
  };
  if (props.treeNode) {
    // 树形节点
    const treeNode = useTreeNode(options);
    defaultSlot = treeNode.slots.default;
    config.props = {};
  }

  const createVNs = (params, labelVN, nodeprops) => {
    const { checked, disabled, indeterminate } = params;
    return (
      <span
        {...nodeprops}
        class={[
          'vxe-cell--checkbox',
          {
            'is--checked': checked,
            'is--disabled': disabled,
            'is--indeterminate': indeterminate,
          },
        ]}
      >
        <i
          class={[
            'vxe-cell--checkbox-icon',
            {
              'fas fa-check-square': checked,
              'fal fa-square': !checked && !indeterminate,
              'fas fa-minus-square': indeterminate,
            },
          ]}
        />
        {labelVN}
      </span>
    );
  };
  const headerSlot = (params) => {
    const { $table, checked, disabled, column } = params;
    const headerTitle = column.getTitle();
    const titleSlot = slots.title;
    let titleNV;
    if (titleSlot || headerTitle) {
      titleNV = (
        <span class="vxe-cell--checkbox-label">{titleSlot ? titleSlot(params) : headerTitle}</span>
      );
    }
    return createVNs(params, titleNV, {
      onClick(evnt) {
        if (!disabled) {
          $table.triggerCheckAllEvent(evnt, !checked);
        }
      },
    });
  };

  const checkboxSlot = (params) => {
    const { $table, disabled, visible, row } = params;
    const { computeCheckboxOpts } = $table.getComputeMaps();
    const checkboxOpts = computeCheckboxOpts.value;
    const { labelField } = checkboxOpts;
    let titleNV;
    if (defaultSlot || labelField) {
      titleNV = defaultSlot ? defaultSlot(params) : row[labelField];
    }
    return createVNs(params, titleNV, {
      onClick(evnt) {
        if (!disabled && visible && hasClass(evnt.target, 'vxe-cell--checkbox-icon')) {
          params.checked = !params.checked;
          $table.triggerCheckRowEvent(evnt, params, params.checked);
        }
      },
    });
  };

  config.slots = { header: headerSlot, checkbox: checkboxSlot };
  return config;
}
