import Tooltip from '../../../tooltip';
/**
 * 操作列
 * @returns
 */
export function useAction({ props }) {
  const config = {
    slots: {
      default: (config) => {
        const { buttons = [] } = props.actionConfig ?? {};
        return (
          <div class={['action-cell']}>
            {buttons
              .filter((button) => !button.onHidden?.(config))
              .map((button) => {
                return (
                  <>
                    <div
                      class={{
                        'action-cell-button': true,
                        'is--disable': button.onDisabled?.(config),
                      }}
                      onClick={(evt) => {
                        evt.preventDefault();
                        const disabled = button.onDisabled?.(config);
                        !disabled && button.onClick?.(config);
                      }}
                    >
                      {button.icon ? (
                        <Tooltip title={button.text}>
                          <i
                            class={['action-cell-button-icon', button.icon]}
                            style={{ color: button.iconColor }}
                          />
                        </Tooltip>
                      ) : (
                        <span class="action-cell-button-text" style={{ color: button.fontColor }}>
                          {button.text}
                        </span>
                      )}
                    </div>
                    <div class="action-cell-separator" />
                  </>
                );
              })}
          </div>
        );
      },
    },
    props: { align: 'center', cellRender: {} },
  };

  return config;
}
