import { withInstall } from '../utils';
import JePanel from './src/panel';
import JePanelItem from './src/item';
import { props, slots, emits } from './src/config';
import { useInjectPanel, useInjectPanelItem } from './src/context';
export { props, slots, emits };
// 注册依赖组件
JePanel.installComps = {
  Item: { name: JePanelItem.name, comp: JePanelItem },
};
Object.assign(JePanel, {
  Config: { props, slots, emits },
  useInjectPanel,
  useInjectPanelItem,
});

export const Panel = withInstall(JePanel);

export default Panel;
