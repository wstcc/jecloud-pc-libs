import { computedDomSize } from '@jecloud/utils';
export const bars = ['tbar', 'bbar', 'lbar', 'rbar'];
export const items = ['top', 'bottom', 'left', 'right'];
const propsFn = () => {
  const _props = {};
  bars.concat(items).forEach((item) => {
    _props[item] = { type: Object, default: null };
  });
  return _props;
};

export const props = {
  ...propsFn(),
  default: { type: Object, default: null },
  height: { type: [Number, String] },
  width: { type: [Number, String] },
  style: { type: [String, Object] },
  visible: { type: Boolean, default: true },
};
export const slots = items.concat(bars);

export const emits = Object.keys(props).map((key) => `update:${key}`);

const drawerSizeVar = '--drawer-size';
export function setDrawerSize(dom, size) {
  dom?.style.setProperty(drawerSizeVar, size + 'px');
}
export function getDrawerSize(dom) {
  return computedDomSize(dom?.style.getPropertyValue(drawerSizeVar)) ?? 0;
}
