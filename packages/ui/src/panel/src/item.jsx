import {
  computed,
  defineComponent,
  ref,
  watch,
  getCurrentInstance,
  nextTick,
  onMounted,
  onBeforeUnmount,
} from 'vue';
import {
  isBoolean,
  computedDomSize,
  debounce,
  addResizeListener,
  removeResizeListener,
} from '@jecloud/utils';
import {
  slots,
  items as regionItems,
  bars as regionBars,
  setDrawerSize,
  getDrawerSize,
} from './config';
import { useInjectPanel, useProvidePanelItem } from './context';
import { useModelValue } from '../../hooks';
export default defineComponent({
  name: 'JePanelItem',
  props: {
    region: {
      type: String,
      default: 'default',
      validator: (value) => [...slots, 'default'].includes(value),
    }, // 位置
    size: { type: [String, Number] }, // 尺寸
    hidden: Boolean, // 隐藏
    split: Boolean, // 调整
    collapsed: Boolean, // 收起
    collapsible: Boolean, // 允许收起，显示收起按钮
    maxSize: Number,
    minSize: Number,
    drawer: Boolean, // 抽屉模式
    autoSize: Boolean, // 自动计算尺寸
  },
  setup(props, context) {
    const { attrs, slots, expose } = context;
    const prefixCls = 'je-panel-item';
    const $el = ref();
    const $collapseEl = ref();
    const collapsed = useModelValue({ props, context, key: 'collapsed' });
    const transition = ref(collapsed.value);
    const size = useModelValue({ props, context, key: 'size' });
    const hidden = useModelValue({ props, context, key: 'hidden' });
    const splitter = ref();
    const panelContext = useInjectPanel();
    const isHover = ref(false);
    const $item = {
      instance: getCurrentInstance(),
      props,
      context,
      collapsed,
      isHover,
      $el,
      setSize(_size) {
        size.value = _size;
      },
      setVisible(visible) {
        hidden.value = !visible;
      },
      setExpand(expand) {
        collapsed.value = !expand;
      },
      getPanel() {
        return panelContext;
      },
    };
    panelContext.addPanelItem(props.region, $item);
    expose($item);
    useProvidePanelItem($item);
    // 刷新布局，收起和隐藏时有白屏，换成下面实现方式
    // const refreshLayout = debounce(() => {
    //   panelContext?.refreshLayout();
    // }, 20);
    const refreshLayout = () => {
      nextTick(() => {
        panelContext?.refreshLayout();
      });
    };
    watch(() => [collapsed.value, hidden.value, size.value], refreshLayout);
    /**
     * 是否工具条
     */
    const isBar = computed(() => {
      return props.region?.endsWith('bar');
    });
    // 分割条：split
    const computeSplit = computed(() => {
      const split = isBoolean(props.split)
        ? { enable: props.split }
        : props.split ?? {
            enable: false,
          };
      // 工具条，不支持split
      if (isBar.value) {
        split.enable = false;
      }
      return split;
    });
    $item.split = computeSplit.value.enable;
    // 统一使用 top，bottom，left，right进行位置区分
    const computeRegion = computed(() => {
      if (regionBars.includes(props.region)) {
        return regionItems[regionBars.indexOf(props.region)];
      }
      return props.region;
    });

    // 样式style
    const computeStyle = computed(() => {
      const style = {};
      // 设置尺寸
      if (size.value) {
        style[
          ['top', 'bottom'].includes(computeRegion.value) ? 'height' : 'width'
        ] = `${computedDomSize(size.value)}px`;
      }
      return style;
    });

    if (props.drawer) {
      watch(
        () => collapsed.value,
        () => {
          // 取消悬浮样式
          isHover.value = false;
          // 禁用动画
          transition.value = false;

          // 抽屉模式，监听收起状态，更新尺寸
          const drawerSize = getDrawerSize($el.value) || size.value;
          size.value = collapsed.value ? props.minSize : drawerSize;
          // 刷新布局
          panelContext?.refreshLayout();
          nextTick(() => {
            // 开启动画后会导致dom计算失效，所以延迟处理
            transition.value = collapsed.value;
            // 更新尺寸
            setDrawerSize($el.value, drawerSize);
          });
        },
        { immediate: true },
      );
    }

    // 组件尺寸发生变化，更新主组件布局
    // TODO:性能问题，通过配置进行刷新，不自动进行子组件监听，也可手动调用refreshLayout
    if (props.autoSize) {
      const refreshLayoutItem = debounce(function () {
        // 抽屉模式，并且是锁定状态，不刷新尺寸
        if (props.drawer && collapsed.value) {
          return;
        }
        panelContext?.refreshLayout();
      }, 100);
      // 初始刷新布局
      onMounted(() => {
        nextTick(() => {
          addResizeListener($el.value, refreshLayoutItem);
        });
      });

      // 销毁resize事件
      onBeforeUnmount(() => {
        removeResizeListener($el.value, refreshLayoutItem);
      });
    }

    return () => {
      const region = computeRegion.value;
      return (
        <>
          <div
            ref={$el}
            data-region={region}
            class={[
              prefixCls,
              {
                'je-panel-bar': isBar.value,
                [`${prefixCls}-region-${region}`]: region,
                'is--collapsible': props.collapsible,
                'is--collapsed': collapsed.value,
                'is--hidden': hidden.value,
                'is--drawer': props.drawer,
                'is--transition': transition.value,
              },
            ]}
            style={computeStyle.value}
            {...attrs}
          >
            {slots.default?.()}
          </div>

          {/* 调整尺寸条 */}
          {computeSplit.value.enable || props.collapsible ? (
            <div
              class={[
                `je-panel-splitter je-panel-splitter-${region}`,
                {
                  'is--split': computeSplit.value.enable,
                  'is--collapsible': props.collapsible,
                  'is--collapsed': collapsed.value,
                  'is--hidden': hidden.value,
                },
              ]}
              ref={splitter}
              style={computeSplit.value.style}
            >
              {/* 是否允许收起 */}
              {props.collapsible ? (
                <div
                  ref={$collapseEl}
                  class={[
                    'arrow',
                    `arrow-${region}`,
                    {
                      'arrow-is--collapsed': collapsed.value,
                      'arrow-is--expand': !collapsed.value,
                    },
                  ]}
                  onClick={() => {
                    // 防止折叠按钮抖动处理
                    $collapseEl.value.style.display = 'none';
                    setTimeout(() => {
                      $collapseEl.value.style.display = '';
                    }, 50);

                    collapsed.value = !collapsed.value;
                  }}
                >
                  <div class="arrow-bg" />
                  <i class={['arrow-icon', 'fas fa-caret-left']} />
                </div>
              ) : null}
            </div>
          ) : null}
        </>
      );
    };
  },
});
