import JeEditorHtmlEvets from '../editor-html/src/config/events';
/**
 * 组件扩展事件
 */
export const CustomEvents = {
  JeGrid: ['drop', 'before-drop', 'column-drop', 'selection-change', 'before-load', 'load'],
  JeColor: ['select'],
  JeEditorCode: ['init', 'change', 'save'],
  JeEditorHtml: JeEditorHtmlEvets,
  JeIcon: ['select'],
  JeInput: ['reset', 'before-reset'],
  JeInputSelect: ['select', 'reset'],
  JeInputSelectGrid: ['before-select', 'select', 'reset'],
  JeInputSelectTree: ['before-select', 'select', 'reset'],
  JeModal: ['beforeshow', 'show', 'beforeclose', 'close'],
  JePager: ['page-change', 'load'],
  JePinyin: ['pinyin'],
  JeSearch: ['search', 'select'],
  JeTree: [
    'drop',
    'before-drop',
    'column-drop',
    'selection-change',
    'before-load',
    'load',
    'search-select',
    'action-click',
  ],
};
