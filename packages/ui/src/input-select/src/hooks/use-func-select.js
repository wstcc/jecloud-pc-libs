import { useListeners, useField } from '../../../hooks';
import { useFuncSelect as _useFuncSelect } from '@jecloud/utils';
export function useFuncSelect({ props, context, value, type }) {
  const { showSelectWindow, selectConfig, func, $func } = _useFuncSelect({ props, context });
  if (!func) {
    return;
  }

  const { fireListener } = useListeners({ props, context });
  const { getModel, setModel, getName, getLabel, resetModel, getModelOwner } = useField({
    props,
    context,
  });

  // 重置函数
  const onReset = () => {
    // 触发reset事件
    fireListener('reset', { ...getModelOwner() });
    resetModel(selectConfig.targetFields);
  };

  // 选择函数
  const onSelect = () => {
    if (props.readonly) return false;
    const model = getModel();

    showSelectWindow({
      type,
      model,
      value: value.value,
      eventOptions: { ...getModelOwner(), $func },
      selectOptions: {
        title: getLabel(), // 选择框标题
        $func, // 增加功能对象
      },
      callback({ rows, values, paths }) {
        // 更新model数据
        setModel(values);

        // 设置字段值
        const name = getName();
        const sourceField = selectConfig.fieldMaps?.targetToSource[name];
        if (sourceField) {
          value.value = func.getConfigFieldValue({ field: sourceField, rows, paths });
        }
      },
    });
  };

  return { onReset, onSelect };
}
