import { defineComponent } from 'vue';
import InputSelect from './input-select';
import { pick } from '@jecloud/utils';
import { selectProps, selectEmits } from './input-select-grid';
import { useFuncSelect } from './hooks/use-func-select';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeInputSelectTree',
  inheritAttrs: false,
  props: selectProps,
  emits: selectEmits,
  setup(props, context) {
    const { attrs, slots } = context;
    const value = useModelValue({ props, context, changeValid: true });
    const type = 'tree';
    // 功能选择
    const { onReset, onSelect } = useFuncSelect({ props, context, value, type });
    return () => (
      <InputSelect
        {...attrs}
        {...pick(props, Object.keys(selectProps))}
        type={type}
        icon="fal fa-folder-tree"
        v-model:value={value.value}
        v-slots={slots}
        onSelect={onSelect}
        onReset={onReset}
      />
    );
  },
});
