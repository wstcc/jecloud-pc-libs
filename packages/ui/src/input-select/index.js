import { withInstall } from '../utils';
import JeInputSelect from './src/input-select';
import JeSelectTree from './src/input-select-tree';
import JeSelectGrid from './src/input-select-grid';
import JeSelectUser from './src/input-select-user';

JeInputSelect.installComps = {
  // 注册依赖组件
  Tree: { name: JeSelectTree.name, comp: JeSelectTree },
  Grid: { name: JeSelectGrid.name, comp: JeSelectGrid },
  User: { name: JeSelectUser.name, comp: JeSelectUser },
};

export const InputSelect = withInstall(JeInputSelect);
export default InputSelect;
