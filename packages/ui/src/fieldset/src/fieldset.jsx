import { defineComponent, ref } from 'vue';
import { Row, Col } from 'ant-design-vue';
import { toNumber, pick } from '@jecloud/utils';
import { filterEmpty } from '../../utils/props';
import { useModelValue, useStyle4Size } from '../../hooks';

/**
 * 字段集合
 * 跟Row的思路相反，先规定列数：cols，然后设定每个子项所占的列宽：colSpan
 */

export default defineComponent({
  name: 'JeFieldset',
  inheritAttrs: false,
  props: {
    title: String, // 标题
    cols: { type: Number, default: 1 }, // 列数
    simple: Boolean,
    collapsed: Boolean, // 收起
    collapsible: { type: Boolean, default: true }, // 允许收起
    align: String,
    justify: String,
    wrap: { type: Boolean, default: true },
    bgColor: String, //分组框背景
    titleColor: String, //标题色
    titleBgColor: String, //标题背景色
    borderColor: String, //边框色
    width: Number,
    height: Number,
  },
  emits: ['title-dblclick', 'title-click'],
  setup(props, context) {
    const { slots, attrs, emit } = context;
    const collapsed = useModelValue({ props, context, key: 'collapsed' });
    const $body = ref();
    const $title = ref();

    return () => {
      // 自动增加列数
      const vnodes = filterEmpty(slots.default?.());
      // 子项默认宽度
      const itemSpan = 24 / toNumber(props.cols);
      const items = vnodes.map((item) => {
        if (['ACol'].includes(item.type?.name)) return item;
        const config = {};
        if (item.props?.colSpan) {
          config.span = toNumber(item.props.colSpan) * itemSpan;
        }
        return item.props?.hidden ? null : <Col {...config}>{item}</Col>;
      });
      return (
        <div
          class={{
            'je-fieldset': true,
            'je-fieldset-simple': props.simple,
            'is--collapsible': props.collapsible,
            'is--collapsed': collapsed.value,
          }}
          style={{
            backgroundColor: props.bgColor,
            borderColor: props.borderColor,
            ...useStyle4Size({ props }),
          }}
          {...attrs}
        >
          {props.simple ? null : (
            <div
              class="je-fieldset-header"
              ref={$title}
              style={{ backgroundColor: props.titleBgColor, borderBottomColor: props.borderColor }}
            >
              <span
                class="je-fieldset-header-title"
                style={{ color: props.titleColor }}
                onClick={() => {
                  collapsed.value = !collapsed.value;
                  emit('title-click', { collapsed });
                }}
                onDblclick={() => {
                  emit('title-dblclick');
                }}
              >
                <i
                  class={{
                    'collapsible-icon': true,
                    'fas fa-caret-down': collapsed.value,
                    'fas fa-caret-up': !collapsed.value,
                  }}
                ></i>
                {slots.title?.() ?? props.title}
              </span>
            </div>
          )}

          {collapsed.value ? null : (
            <Row
              class="je-fieldset-body"
              ref={$body}
              {...pick(props, ['justify', 'align', 'wrap'])}
            >
              {items}
            </Row>
          )}
        </div>
      );
    };
  },
});
