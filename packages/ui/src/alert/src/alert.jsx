import { defineComponent } from 'vue';
import { Alert } from 'ant-design-vue';

export default defineComponent({
  name: 'JeAlert',
  components: { AAlert: Alert },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <a-alert {...attrs} v-slots={slots}></a-alert>;
  },
});
