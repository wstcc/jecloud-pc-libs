import { withInstall } from '../utils';
import { Input as AInput } from 'ant-design-vue';
import './src/overrides';
import JeInput from './src/input';
import JeTextarea from './src/textarea';
import JeDisplay from './src/display';

JeInput.installComps = {
  // 注册依赖组件
  Group: { name: 'JeInputGroup', comp: AInput.Group },
  Search: { name: 'JeInputSearch', comp: AInput.Search },
  Password: { name: 'JeInputPassword', comp: AInput.Password },
  TextArea: { name: JeTextarea.name, comp: JeTextarea },
  Display: { name: JeDisplay.name, comp: JeDisplay },
};

export const Input = withInstall(JeInput);

export default Input;
