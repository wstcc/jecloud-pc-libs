import {
  computed,
  defineComponent,
  getCurrentInstance,
  nextTick,
  onMounted,
  ref,
  watchEffect,
} from 'vue';
import ClearableLabeledInput from 'ant-design-vue/es/input/ClearableLabeledInput';
import ResizableTextArea from 'ant-design-vue/es/input/ResizableTextArea';
import { textAreaProps } from 'ant-design-vue/es/input/inputProps';
import { fixControlledValue, resolveOnChange, triggerFocus } from 'ant-design-vue/es/input/Input';
import classNames from 'ant-design-vue/es/_util/classNames';
import { useInjectFormItemContext } from 'ant-design-vue/es/form/FormItemContext';
import useConfigInject from 'ant-design-vue/es/_util/hooks/useConfigInject';
import omit from 'ant-design-vue/es/_util/omit';
import { useProvideInput } from './context';
import { useModelValue, useStyle4Size, useListeners } from '../../hooks';
import { addonProps } from '../../hooks/use-addon';

function fixEmojiLength(value, maxLength) {
  return [...(value || '')].slice(0, maxLength).join('');
}
export default defineComponent({
  name: 'JeInputTextarea',
  inheritAttrs: false,
  props: {
    ...textAreaProps(),
    allowClear: { type: Boolean, default: true },
    height: Number,
    width: Number,
    ...addonProps,
  },
  emits: ['reset', 'before-reset', 'update:value', 'change'],
  setup(props, context) {
    const { attrs, expose, emit, slots } = context;
    const { fireListener } = useListeners({ props, context });
    const formItemContext = useInjectFormItemContext();
    const stateValue = useModelValue({ props, context, changeEvent: true });
    const resizableTextArea = ref();
    const mergedValue = ref('');
    const { prefixCls, size, direction } = useConfigInject('input', props);
    const showCount = computed(() => {
      return props.showCount === '' || props.showCount || false;
    });
    // Max length value
    const hasMaxLength = computed(() => Number(props.maxlength) > 0);
    const compositing = ref(false);
    const instance = getCurrentInstance();

    // TODO: 注入对象，方便触发reset事件
    useProvideInput({
      type: 'textarea',
      instance,
      props,
      context,
      onBeforeReset() {
        return fireListener('before-reset');
      },
    });

    const focus = (option) => {
      triggerFocus(resizableTextArea.value?.textArea, option);
    };

    const blur = () => {
      resizableTextArea.value?.textArea?.blur();
    };

    const setValue = (value, callback) => {
      if (stateValue.value === value) {
        return;
      }
      if (props.value === undefined) {
        stateValue.value = value;
      } else {
        nextTick(() => {
          if (resizableTextArea.value.textArea.value !== mergedValue.value) {
            resizableTextArea.value?.instance.update?.();
          }
        });
      }
      nextTick(() => {
        callback && callback();
      });
    };

    const handleKeyDown = (e) => {
      if (e.keyCode === 13) {
        emit('pressEnter', e);
      }
      emit('keydown', e);
    };

    const onBlur = (e) => {
      const { onBlur } = props;
      onBlur?.(e);
      formItemContext.onFieldBlur();
    };
    const triggerChange = (e) => {
      stateValue.value = e.target.value;
      emit('input', e);
      emit('change', stateValue.value);
      formItemContext.onFieldChange();
    };

    const handleReset = (e) => {
      resolveOnChange(resizableTextArea.value.textArea, e, triggerChange);
      setValue('', () => {
        focus();
      });
    };

    const handleChange = (e) => {
      const { value, composing } = e.target;
      compositing.value = e.isComposing || composing;
      if ((compositing.value && props.lazy) || stateValue.value === value) return;
      let triggerValue = e.currentTarget.value;
      if (hasMaxLength.value) {
        triggerValue = fixEmojiLength(triggerValue, props.maxlength);
      }
      resolveOnChange(e.currentTarget, e, triggerChange, triggerValue);
      setValue(triggerValue);
    };
    const renderTextArea = () => {
      const { style, class: customClass } = attrs;
      const { bordered = true } = props;
      const resizeProps = {
        ...omit(props, ['allowClear']),
        ...attrs,
        style: showCount.value ? {} : style,
        class: {
          [`${prefixCls.value}-borderless`]: !bordered,
          [`${customClass}`]: customClass && !showCount.value,
          [`${prefixCls.value}-sm`]: size.value === 'small',
          [`${prefixCls.value}-lg`]: size.value === 'large',
        },
        showCount: null,
        prefixCls: prefixCls.value,
        onInput: handleChange,
        onChange: handleChange,
        onBlur,
        onKeydown: handleKeyDown,
      };
      if (props.valueModifiers?.lazy) {
        delete resizeProps.onInput;
      }
      return (
        <ResizableTextArea
          {...resizeProps}
          id={resizeProps.id ?? formItemContext.id.value}
          ref={resizableTextArea}
          maxlength={props.maxlength}
        />
      );
    };

    onMounted(() => {
      if (process.env.NODE_ENV === 'test') {
        if (props.autofocus) {
          focus();
        }
      }
    });
    expose({
      focus,
      blur,
      resizableTextArea,
    });

    watchEffect(() => {
      let val = fixControlledValue(stateValue.value);
      if (
        !compositing.value &&
        hasMaxLength.value &&
        (props.value === null || props.value === undefined)
      ) {
        // fix #27612 将value转为数组进行截取，解决 '😂'.length === 2 等emoji表情导致的截取乱码的问题
        val = fixEmojiLength(val, props.maxlength);
      }
      mergedValue.value = val;
    });
    return () => {
      const { maxlength, bordered = true } = props;
      const { style, class: customClass } = attrs;

      const inputProps = {
        ...props,
        ...attrs,
        prefixCls: prefixCls.value,
        inputType: 'text',
        handleReset,
        direction: direction.value,
        bordered,
        style: showCount.value ? undefined : style,
      };

      // TODO: 增加所有插槽，支持前后缀
      let textareaNode = (
        <ClearableLabeledInput
          {...inputProps}
          value={mergedValue.value}
          style={useStyle4Size({ props })}
          v-slots={{ ...slots, element: renderTextArea }}
        />
      );
      if (showCount.value) {
        const valueLength = [...mergedValue.value].length;
        let dataCount = '';
        if (typeof showCount.value === 'object') {
          dataCount = showCount.value.formatter({ count: valueLength, maxlength });
        } else {
          dataCount = `${valueLength}${hasMaxLength.value ? ` / ${maxlength}` : ''}`;
        }
        textareaNode = (
          <div
            class={classNames(
              `${prefixCls.value}-textarea`,
              {
                [`${prefixCls.value}-textarea-rtl`]: direction.value === 'rtl',
              },
              `${prefixCls.value}-textarea-show-count`,
              customClass,
            )}
            style={style}
            data-count={dataCount}
          >
            {textareaNode}
          </div>
        );
      }
      return textareaNode;
    };
  },
});
