import { defineComponent, nextTick, onMounted, ref, watch } from 'vue';
import { createQRCode } from '@jecloud/utils';
export default defineComponent({
  name: 'JeQrcode',
  inheritAttrs: false,
  props: {
    value: String,
    size: { type: Number, default: 200 },
    color: { type: String, default: '#000000' },
    bgColor: { type: String, default: '#ffffff' },
    level: { type: String, default: 'M' },
  },
  setup(props, context) {
    const { attrs } = context;
    const qrcodeRef = ref();
    let qrcode;
    const buildQRCode = () => {
      if (qrcodeRef.value) {
        qrcodeRef.value.innerHTML = '';
        qrcode = createQRCode(qrcodeRef.value, props);
      }
    };
    watch(
      () => props,
      () => buildQRCode,
      { deep: true },
    );

    onMounted(() => {
      nextTick(() => {
        buildQRCode();
      });
    });
    return () => (
      <div class="je-qrcode" {...attrs}>
        <div class="je-qrcode-content" ref={qrcodeRef} />
      </div>
    );
  },
});
