const { readFontsData, writeFontsData, i18nPath, i18nDataPath } = require('./utils');
// 读取功能数据，生成本地缓存文件
readFontsData('76ee6b2a-5af9-475c-afb6-dcefdb42c99e')
  .then((data) => {
    // 原始数据
    writeFontsData(i18nDataPath, data);
    // json数据
    const i18nJson = {};
    data.forEach((item) => {
      i18nJson[item.en] = item.zh;
    });
    writeFontsData(i18nPath, i18nJson);
  })
  .catch((error) => {
    console.error('出错了：', error);
  });
