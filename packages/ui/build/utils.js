const request = require('request');
const fs = require('fs');
const path = require('path');
const i18nPath = path.resolve(__dirname, '../', 'src/assets/fonts/i18n.json');
const i18nDataPath = path.resolve(__dirname, '../', 'src/assets/fonts/i18n-data.json');

/**
 * 读取字体图标数据
 */
const readFontsData = function (token) {
  if (!token) {
    return Promise.reject({ token: false });
  }
  return new Promise((resolve, reject) => {
    request.post(
      {
        url: 'http://develop.jecloud.net/je/common/load',
        form: {
          tableCode: 'JE_META_FONTICON',
          funcCode: 'JE_META_FONTICON',
          limit: -1,
        },
        headers: {
          pd: 'meta',
          authorization: token, //'',
        },
      },
      function (error, response, body) {
        if (error) {
          reject(error, response, body);
        } else {
          const data = JSON.parse(body);
          resolve(
            data.data.rows.map((item) => {
              return {
                icon: item.FONTICON_TB,
                en: item.FONTICON_TBYW,
                zh: item.FONTICON_TBZW,
              };
            }),
          );
        }
      },
    );
  });
};

/**
 * 写文件
 * @param {*} data
 */
const writeFontsData = function (path, data) {
  fs.writeFile(path, JSON.stringify(data), function (err) {
    if (err) {
      console.log('xxxxxx i18n.json构建失败：' + err.message);
    } else {
      console.log('<<<<<< i18n.json构建完成');
    }
  });
};

function isNumeric(value) {
  return !Number.isNaN(parseFloat(value)) && Number.isFinite(parseFloat(value));
}
/**
 * 翻译字体图标
 */
const translateFonts = function () {
  const faIcons = require('../src/assets/fonts/fa/fa.json');
  const jeIcons = require('../src/assets/fonts/jeicon/iconfont.json');
  const i18nJSON = require('../src/assets/fonts/i18n.json');
  let iconsCls = [];
  // fa
  iconsCls = iconsCls.concat(faIcons);
  // je
  iconsCls = iconsCls.concat(
    jeIcons.glyphs.map((item) => {
      return item.font_class;
    }),
  );
  // 解析是否翻译
  const keywords = [];
  iconsCls.forEach((icon) => {
    icon.split('-').forEach((str) => {
      if (str.length > 1 && !isNumeric(str) && !i18nJSON[str] && !keywords.includes(str)) {
        keywords.push(str);
      }
    });
  });
  if (keywords.length === 0) {
    console.log('没有新增图标需要翻译！');
  } else {
    // 翻译
    var params = {
      appid: '20181129000240924',
      appkey: 'tBUp4lzp7gVXKR7MEC94',
      salt: new Date().getTime(),
      q: keywords.join('\n'),
      from: 'en',
      to: 'zh',
      sign: '',
    };
    params.sign = crypto
      .createHash('md5')
      .update(params.appid + params.q + params.salt + params.appkey)
      .digest('hex'); //MD5版本
    let param = new URLSearchParams();
    for (var p in params) {
      var val = params[p];
      param.append(p, val);
    }
    request.post(
      { url: 'http://api.fanyi.baidu.com/api/trans/vip/translate', form: params },
      function (error, response, body) {
        const data = JSON.parse(body);
        data.trans_result.forEach((item) => {
          i18nJSON[item.src] = item.dst;
          console.log(item.src, ':', item.dst);
        });

        writeFontsData(i18nPath, i18nJSON);
      },
    );
  }
};

/**
 * 解析样式内容
 */
const transformStyles = function () {
  const contnet = fs.readFileSync('fontawesome.css').toString();
  const fonts = [];
  (contnet.match(/(fa-).*?(:before)/g) ?? []).forEach((item) => {
    const str = item.substring(3, item.length - 7);
    fonts.push(str);
  });
  fs.writeFileSync('fa.json', JSON.stringify(fonts.sort()));
};
module.exports = {
  readFontsData,
  writeFontsData,
  translateFonts,
  transformStyles,
  i18nPath,
  i18nDataPath,
};
