const gulp = require('gulp');
const del = require('del');
const utils = require('./utils');
const rename = require('gulp-rename'); // 重命名
const through2 = require('through2'); // 处理文件内容
const mergeStream = require('merge-stream'); //合并任务流
const ts = require('gulp-typescript');

// 清除打包目录
const buildClean = ({ outputDir }) => {
  return del([outputDir]);
};

// 打包lib
const buildModules = ({ srcDir, outputDir }) => {
  return gulp
    .src(srcDir + '/**/*.{ts,tsx,jsx,js}')
    .pipe(utils.babelParse())
    .pipe(
      ts({
        strict: false,
        moduleResolution: 'node',
        noImplicitAny: true,
        target: 'es5',
        module: 'esnext',
        allowJs: true,
        lib: ['dom', 'esnext'],
        isolatedModules: true,
      }),
    )
    .pipe(utils.terser())
    .pipe(gulp.dest(outputDir));
};

// 处理样式
const buildStyle = ({ srcDir, outputDir }) => {
  return gulp.src(srcDir + `/**/*.{less,css}`).pipe(gulp.dest(outputDir));
};

// 处理资源
const buildAssets = ({ srcDir, outputDir }) => {
  const font = gulp.src(srcDir + `/**/*.{eot,woff,woff2,ttf,svg}`).pipe(gulp.dest(outputDir));
  const image = gulp.src(srcDir + `/**/*.{png,jpg,jpeg,gif}`).pipe(gulp.dest(outputDir));
  const file = gulp.src(srcDir + `/**/*.{txt,json}`).pipe(gulp.dest(outputDir));
  return mergeStream([font, image, file]);
};

// 处理src样式
const buildSrcStyle = ({ srcDir }) => {
  return gulp
    .src(srcDir + `/*/style/index.less`)
    .pipe(
      // 生成style/index.js，支持按需加载
      through2.obj(function (chunk, enc, callback) {
        chunk.contents = Buffer.from("// 系统自动生成，无需修改\nimport('./index.less');\n");
        this.push(chunk);
        callback();
      }),
    )
    .pipe(
      rename({
        extname: '.js',
      }),
    )
    .pipe(gulp.dest(srcDir));
};
module.exports = {
  buildClean,
  buildModules,
  buildStyle,
  buildAssets,
  buildSrcStyle,
};
