const path = require('path');
const fsExtra = require('fs-extra');
const fs = require('fs');
const { resolve, readJsonFile } = require('../utils');
const outputDir = resolve('dist/libs');
const packages = resolve('packages');
// 删除历史数据
if (fs.existsSync(outputDir)) {
  fsExtra.removeSync(outputDir);
}
// 创建目录
fs.mkdirSync(outputDir, { recursive: true });
// 复制文件
fs.readdirSync(packages).forEach((dir) => {
  const entryLibDir = path.join(packages, dir);
  const json = readJsonFile(path.join(entryLibDir, 'package.json'));
  if (!json.private) {
    console.log(json.name, '开始构建...');
    const outputLibDir = path.join(outputDir, json.name);
    fsExtra.copySync(path.join(entryLibDir, 'src'), path.join(outputLibDir, 'src'));
    fsExtra.copyFileSync(
      path.join(entryLibDir, 'package.json'),
      path.join(outputLibDir, 'package.json'),
    );
    fsExtra.copyFileSync(path.join(entryLibDir, 'README.md'), path.join(outputLibDir, 'README.md'));
    console.log(json.name, '构建成功！\n');
  }
});
